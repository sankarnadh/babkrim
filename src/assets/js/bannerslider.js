window.onload = (function(){ prettyPrint(); });
$(document).ready(function()
{    
    var prettyprint = false;
    $("pre code").parent().each(function()
    {
        $(this).addClass('prettyprint linenums');
        prettyprint = true;
    });	
    if ( prettyprint )
    {
        $.getScript("https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js", function() { prettyPrint() });
    }
});