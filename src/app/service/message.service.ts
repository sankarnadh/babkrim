import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { mergeMapTo } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs'
import { Router } from '@angular/router';
// import { ServerService } from './server.service';

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDiU84LT6JdyTWJZTR3ab9CupLZ3K-mNBk",
    authDomain: "uppartime-7b4c7.firebaseapp.com",
    databaseURL: "https://uppartime-7b4c7.firebaseio.com",
    projectId: "uppartime-7b4c7",
    storageBucket: "uppartime-7b4c7.appspot.com",
    messagingSenderId: "1089640354923"
  }
};

@Injectable()
export class MessagingService {

  currentMessage: any = new BehaviorSubject(null);

  constructor(
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private router: Router,
    // private service : ServerService,
    private angularFireMessaging: AngularFireMessaging) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )
  }

  /**
   * update token in firebase database
   * 
   * @param userId userId as a key 
   * @param token token as a value
   */
  updateToken(userId, token) {
    // we can change this function to request our backend service
    this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token
        this.angularFireDB.object('fcmTokens/').update(data)
      })
  }

  /**
   * request permission for notification from firebase cloud messaging
   * 
   * @param userId userId
   */
  token: any;
  requestPermission(userId) {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log("my token " + token);
        //this.service.setStorage('firebasetoken',token);
        this.token = token
        this.updateToken(userId, token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.info("new message received. ", payload);
        this.currentMessage.next(payload['notification']);
       // this.router.navigate(['/my-jobs'])
        this.currentMessage.onAction().subscribe(() => {
          console.info("sethu done it")
          // window.open('http://upparttime.com/login');
// this.router.navigate(['/my-jobs'])
        })
      },
      (er) => {
        console.dir(er)
      })
  }
}