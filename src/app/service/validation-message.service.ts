import { Injectable } from "@angular/core";
import { ServerService } from "./server.service";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
  providedIn: "root"
})
export class ValidationMessageService {
  public mobile_otp_msg;
  constructor(public router: Router, public translate: TranslateService) {
    translate.get("level1EmailExists", {}).subscribe((res: string) => {
      console.info(res);
    });
  }
  translateValues() {
    this.translate.get("level1EmailExists", {}).subscribe((res: string) => {
        console.info(res);
        this.level1EmailExists = this.translate.instant("level1EmailExists");
        this.level1EmailInvalid = this.translate.instant("level1EmailInvalid");
        this.level1EmailLong = this.translate.instant("level1EmailLong");
        this.level1FullnameLong = this.translate.instant("level1FullnameLong");
        this.level1FullnameInvalid = this.translate.instant(
          "level1FullnameInvalid"
        );
        this.level1MobileInvalid = this.translate.instant("level1MobileInvalid");
        this.invalidMobile = this.translate.instant("invalidMobile");
        this.level1MobileLong = this.translate.instant("level1MobileLong");
        this.level1PasswordInvalid = this.translate.instant(
          "level1PasswordInvalid"
        );
        this.level1PasswordNotMatch = this.translate.instant(
          "level1PasswordNotMatch"
        );

    
    this.level1EmailInvalid = this.translate.instant("level1EmailInvalid");
    this.level1EmailLong = this.translate.instant("level1EmailLong");
    this.level1FullnameLong = this.translate.instant("level1FullnameLong");
    this.level1FullnameInvalid = this.translate.instant(
      "level1FullnameInvalid"
    );
    this.level1MobileInvalid = this.translate.instant("level1MobileInvalid");
    this.invalidMobile = this.translate.instant("invalidMobile");
    this.level1MobileLong = this.translate.instant("level1MobileLong");
    this.level1PasswordInvalid = this.translate.instant(
      "level1PasswordInvalid"
    );
    this.level1PasswordNotMatch = this.translate.instant(
      "level1PasswordNotMatch"
    );
    this.companyNameInvalid = this.translate.instant("companyNameInvalid");
    this.companyNameLong = this.translate.instant("companyNameLong");
    this.companyWebsieInvalid = this.translate.instant("companyWebsieInvalid");
    this.companyWebsiteLong = this.translate.instant("companyWebsiteLong");
    this.companyEmailInvalid = this.translate.instant("companyEmailInvalid");
    this.companyEmailLong = this.translate.instant("companyEmailLong");
    this.companyPhoneInvalid = this.translate.instant("companyPhoneInvalid");
    this.companyPhoneLong = this.translate.instant("companyPhoneLong");
    this.companyMobileInvalid = this.translate.instant("companyMobileInvalid");
    this.companyMobileLong = this.translate.instant("companyMobileLong");
    this.companyZipInvalid = this.translate.instant("companyZipInvalid");
    this.companyZipLong = this.translate.instant("companyZipLong");
    this.companyPoboxInvalid = this.translate.instant("companyPoboxInvalid");
    this.companyPoboxLong = this.translate.instant("companyPoboxLong");
    this.managerFnameInvalid = this.translate.instant("managerFnameInvalid");
    this.managerFnameLong = this.translate.instant("managerFnameLong");
    this.managerMnameInvalid = this.translate.instant("managerMnameInvalid");
    this.managerMnameLong = this.translate.instant("managerMnameLong");
    this.managerLnameInvalid = this.translate.instant("managerLnameInvalid");
    this.managerLnameLong = this.translate.instant("managerLnameLong");
    this.managerPositionInvalid = this.translate.instant(
      "managerPositionInvalid"
    );
    this.managerPositionLong = this.translate.instant("managerPositionLong");
    this.managerMobileInvalid = this.translate.instant("managerMobileInvalid");
    this.managerMobileLong = this.translate.instant("managerMobileLong");
    this.managerPhoneInvalid = this.translate.instant("managerPhoneInvalid");
    this.managerPhoneLong = this.translate.instant("managerPhoneLong");
    this.managerEmailInvalid = this.translate.instant("managerEmailInvalid");
    this.managerEmailLong = this.translate.instant("managerEmailLong");
    this.inavalidEmail = this.translate.instant("inavalidEmail");
    this.companyLocation = this.translate.instant("companyLocation");
    this.noProfileImage = this.translate.instant("noProfileImage");
    this.lastnameInvalid = this.translate.instant("lastnameInvalid");
    this.addressLine1Invalid = this.translate.instant("addressLine1Invalid");
    this.genderInvalid = this.translate.instant("genderInvalid");
    this.nationalityInvalid = this.translate.instant("nationalityInvalid");
    this.dobInvalid = this.translate.instant("dobInvalid");
    this.heightInvalid = this.translate.instant("heightInvalid");
    this.ageInavlid = this.translate.instant("ageInavlid");
    this.experienceInavlid = this.translate.instant("experienceInavlid");
    this.aboutme = this.translate.instant("aboutme");
    this.weight = this.translate.instant("weightValidation");
    this.address1 = this.translate.instant("address1Validation");
    this.address2 = this.translate.instant("address2Validation");
    this.mothertongueInvalid = this.translate.instant("mothertongueInvalid");
    this.id_typeInvalid = this.translate.instant("id_typeInvalid");
    this.id_noInvalid = this.translate.instant("id_noInvalid");
    this.ifscInvalid = this.translate.instant("ifscInvalid");
    this.id_issue_dateInvalid = this.translate.instant("id_issue_dateInvalid");
    this.educationInvalid = this.translate.instant("educationInvalid");
    this.collageInvalid = this.translate.instant("collageInvalid");
    this.graduation_dateInvalid = this.translate.instant(
      "graduation_dateInvalid"
    );
    this.r_wInvalid = this.translate.instant("r_wInvalid");
    this.conversation_levelInvalid = this.translate.instant(
      "conversation_levelInvalid"
    );
    this.other_langInvalid = this.translate.instant("other_langInvalid");
    this.previous_experience = this.translate.instant(
      "previous_experienceValidation"
    );
    this.previous_experienceLimit = this.translate.instant(
      "previous_experienceLimit"
    );
    this.wagesInvalid = this.translate.instant("wagesInvalid");
    this.invalidWageMode = this.translate.instant("invalidWageMode");
    this.invalidFixedRate = this.translate.instant("invalidFixedRate");
    this.invalidMinhour = this.translate.instant("invalidMinhour");
    this.generalspecialization = this.translate.instant(
      "generalspecialization"
    );
    this.specialization = this.translate.instant("specializationValidation");
    this.morespecialization = this.translate.instant("morespecialization");
    this.maxHrswrk = this.translate.instant("maxHrswrk");
    this.minHrswrk = this.translate.instant("minHrswrk");
    this.minHrswrkGreater = this.translate.instant("minHrswrkGreater");
    this.profileTitle = this.translate.instant("profileTitle");
    this.currencyInvalid = this.translate.instant("currencyInvalid");
    this.diabilityRequired = this.translate.instant("diabilityRequired");
    this.skillInvalid = this.translate.instant("skillInvalid");
    this.countryInvalid = this.translate.instant("countryInvalid");
    this.stateInvalid = this.translate.instant("stateInvalid");
    this.cityInvalid = this.translate.instant("cityInvalid");
    this.radioInvalid = this.translate.instant("radioInvalid");
    this.sessionExpired = this.translate.instant("sessionExpired");
    this.inavlidUserName = this.translate.instant("inavlidUserName");
    this.uploadInvalid = this.translate.instant("uploadInvalid");
    this.required = this.translate.instant("requiredValidation");
    this.invalidOtp = this.translate.instant("invalidOtp");
    this.otpVerified = this.translate.instant("otpVerified");
    this.emailVerify = this.translate.instant("emailVerify");

    this.emailVerifyInvalid = this.translate.instant("emailVerifyInvalid");
    this.mobileInvalid = this.translate.instant("mobileInvalid");
    this.inavlidPhone = this.translate.instant("inavlidPhone");
    this.allRequired = this.translate.instant("allRequired");
    this.onlyDigit = this.translate.instant("onlyDigit");
    this.invitationAcepted = this.translate.instant("invitationAcepted");
    this.proposalEdited = this.translate.instant("proposalEdited");
    this.coverLetter = this.translate.instant("coverLetterValidation");
    this.jobTitle = this.translate.instant("jobTitle");
    this.jobDescription = this.translate.instant("jobDescription");
    this.fromDate = this.translate.instant("fromDate");
    this.noOfDays = this.translate.instant("noOfDays");
    this.fromTime = this.translate.instant("fromTime");
    this.toTime = this.translate.instant("toTime");
    this.invalidInput = this.translate.instant("invalidInput");
    this.serverError = this.translate.instant("serverError");
    this.newjobwithinvitation = this.translate.instant("newjobwithinvitation");
    this.attachmentLimit = this.translate.instant("attachmentLimit");
    this.offeraccepted = this.translate.instant("offeraccepted");
    this.jobpost = this.translate.instant("jobpost");
    this.jobEdit = this.translate.instant("jobEdit");
    this.invitationsent = this.translate.instant("invitationsent");
    this.forgotPasswordEmailAlert = this.translate.instant(
      "forgotPasswordEmailAlert"
    );
    this.passwordUpdated = this.translate.instant("passwordUpdated");
    this.loginToContinue = this.translate.instant("loginToContinue");
    this.contractTitle = this.translate.instant("contractTitle");
    this.networkError = this.translate.instant("networkError");
    this.attachmentSizeLimit = this.translate.instant("attachmentSizeLimit");
    this.comentMissing = this.translate.instant("comentMissing");
    this.accountHoldermissing = this.translate.instant("accountHoldermissing");
    this.accountNumbermissing = this.translate.instant("accountNumbermissing");
    this.success=this.translate.instant("success");
    this.error=this.translate.instant("success");
    this.invalidFileType=this.translate.instant("invalidFileType");
    this.samesecondLanguage=this.translate.instant("samesecondLanguage");
    this.belowStandardWage=this.translate.instant("belowStandardWage");
    this.doornomissing=this.translate.instant("doornomissing");
    this.landmarkmissing=this.translate.instant("landmarkmissing");
    this.attachmentVideoSizeLimit=this.translate.instant("attachmentVideoSizeLimit");
this.samemotherToungueandotherLang=this.translate.instant('samemotherToungueandotherLang')
      });

  }
  public attachmentVideoSizeLimit
  public doornomissing
  public landmarkmissing
  public belowStandardWage
  public samesecondLanguage;
  public samemotherToungueandotherLang;
  public invalidFileType;
  public level1EmailExists;
public level1EmailInvalid;
public level1EmailLong;
public level1FullnameLong;
public level1FullnameInvalid;
public level1MobileInvalid;
public invalidMobile;
public level1MobileLong;
public level1PasswordInvalid;
public level1PasswordNotMatch;
public companyNameInvalid;
public companyNameLong;
public companyWebsieInvalid;
public companyWebsiteLong;
public companyEmailInvalid;
public companyEmailLong;
public companyPhoneInvalid;
public companyPhoneLong;
public companyMobileInvalid;
public companyMobileLong;
public companyZipInvalid;
public companyZipLong;
public companyPoboxInvalid;
public companyPoboxLong;
public managerFnameInvalid;
public managerFnameLong;
public managerMnameInvalid;
public managerMnameLong;
public managerLnameInvalid;
public managerLnameLong;
public managerPositionInvalid;
public managerPositionLong;
public managerMobileInvalid;
public managerMobileLong;
public managerPhoneInvalid;
public managerPhoneLong;
public managerEmailInvalid;
public managerEmailLong;
public inavalidEmail;
public companyLocation;
public noProfileImage;
public lastnameInvalid;
public addressLine1Invalid;
public genderInvalid;
public nationalityInvalid;
public dobInvalid;
public heightInvalid;
public ageInavlid;
public experienceInavlid;
public aboutme;
public weight;
public address1;
public address2;
public mothertongueInvalid;
public id_typeInvalid;
public id_noInvalid;
public id_issue_dateInvalid;
public educationInvalid;
public collageInvalid;
public graduation_dateInvalid;
public r_wInvalid;
public conversation_levelInvalid;
public other_langInvalid;
public ifscInvalid;
public previous_experience;
public previous_experienceLimit;
public wagesInvalid;
public invalidWageMode;
public invalidFixedRate;
public invalidMinhour;
public generalspecialization;
public specialization;
public morespecialization;
public maxHrswrk;
public minHrswrk;
public minHrswrkGreater;
public profileTitle;
public currencyInvalid;
public diabilityRequired;
public skillInvalid;
public countryInvalid;
public stateInvalid;
public cityInvalid;
public radioInvalid;
public sessionExpired;
public inavlidUserName;
public uploadInvalid;
public required;
public invalidOtp;
public otpVerified;
public emailVerify;
public emailVerifyInvalid;
public mobileInvalid;
public inavlidPhone;
public allRequired;
public onlyDigit;
public invitationAcepted;
public proposalEdited;
public coverLetter;
public jobTitle;
public jobDescription;
public fromDate;
public noOfDays;
public fromTime;
public toTime;
public invalidInput;
public serverError;
public newjobwithinvitation;
public attachmentLimit;
public offeraccepted;
public jobpost;
public jobEdit;
public invitationsent;
public forgotPasswordEmailAlert;
public passwordUpdated;
public loginToContinue;
public contractTitle;
public networkError;
public attachmentSizeLimit;
public comentMissing;
public accountHoldermissing;
public accountNumbermissing;
public success;
public error;
}
