import { Injectable } from '@angular/core';
import { ServerService } from '../server.service';

@Injectable({
  providedIn: 'root'
})
export class SettingserviceService {

  constructor() { }
  getLanguage(){
    return localStorage.getItem('language');
  }
}
