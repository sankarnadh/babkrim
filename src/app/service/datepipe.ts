import {Inject, LOCALE_ID, Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common'
import {formatDate} from './fromDate'
@Pipe({name: 'datepipes', pure: true})
export class DatePipes implements PipeTransform {
  constructor(@Inject(LOCALE_ID) private locale: string) {}

  /**
   * @param value The date expression: a `Date` object,  a number
   * (milliseconds since UTC epoch), or an ISO string (https://www.w3.org/TR/NOTE-datetime).
   * @param format The date/time components to include, using predefined options or a
   * custom format string.
   * @param timezone A timezone offset (such as `'+0430'`), or a standard
   * UTC/GMT or continental US timezone abbreviation. Default is
   * the local system timezone of the end-user's machine.
   * @param locale A locale code for the locale format rules to use.
   * When not supplied, uses the value of `LOCALE_ID`, which is `en-US` by default.
   * See [Setting your app locale](guide/i18n#setting-up-the-locale-of-your-app).
   * @returns A date string in the desired format.
   */
  timezone='+5.30'
  transform(value: any, format = 'dd MMM yyyy hh:mm a', timezone?: string, locale?: string): string|null {
    if (value == null || value === '' || value !== value) return null;

    try {
       console.info( formatDate(value, format, locale || this.locale, this.timezone))
      return formatDate(value, format, locale || this.locale, this.timezone);
    } catch (error) {
     console.log(error)
    }
  }

  
}