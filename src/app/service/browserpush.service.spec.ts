import { TestBed, inject } from '@angular/core/testing';

import { BrowserpushService } from './browserpush.service';

describe('BrowserpushService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BrowserpushService]
    });
  });

  it('should be created', inject([BrowserpushService], (service: BrowserpushService) => {
    expect(service).toBeTruthy();
  }));
});
