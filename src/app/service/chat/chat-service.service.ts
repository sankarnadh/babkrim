import { Injectable } from '@angular/core';
import { WebsocketService } from '../websocket.service';
import { Observable, Subject } from 'rxjs/Rx';
import { ServerService } from '../server.service'
import { Router } from '@angular/router'
import { ValidationMessageService } from '../validation-message.service'
import '../../../assets/js/js/chat.js'
import { url } from '../url';
import { NgProgress } from 'ngx-progressbar';
declare var customealert;
declare var $;
export interface Message {
  fromType: String,
  message: String,
  messageType: String,
  to: String,
  proposalId: String,
  status: number,
  receiveFrom: String,
  createdTime: any,
  fileName ?:any
}
const CHAT_URL = url.chatUrl+'web-'


@Injectable({
  providedIn: 'root'
})

export class ChatService {

  public messages: Subject<Message>;

  constructor(public wsService: WebsocketService,
    public ngProgress: NgProgress,
    public service: ServerService,
    public validationMsg: ValidationMessageService,
    public router: Router) {


    this.messages = <Subject<Message>>wsService
      .connect(CHAT_URL + this.service.getStorage('userIdentifier'))
      .map((response: MessageEvent): Message => {
        let data = JSON.parse(response.data);
        let url = this.router.url.split('/')[1];
        if ( url == 'employee-message' || url == 'message')
        this.chatRecentContact()
        return {
          fromType: data.fromType,
          message: data.message,
          messageType: data.messageType,
          to: data.to,
          proposalId: data.proposalId,
          status: data.status,
          receiveFrom: data.receiveFrom,
          createdTime: new Date(),
          fileName : data.fileName
        }
      });

    this.messages.subscribe(msg => {
     
      console.log(this.service.messageBadgeIcon++)
      // if (msg && url != 'employeemessage' && url != 'message') {
      // this.service.messageBadgeIcon++;
      // }
      // else
      //   this.service.messageBadgeIcon = 0;

    });
  }
  restartConnection() {
    this.wsService.connect(CHAT_URL + this.service.getStorage('userIdentifier'))
  }
  messageBox: any;
  receiverId = null

  proposalId = null
  sendMsg() {

    if (this.messageBox != ' ' && this.messageBox) {
      var date = new Date()
      let chatmessage: any = {
        fromType: this.service.getStorage('registerType'),
        message: this.messageBox,
        messageType: "text",
        to: this.receiverId,
        proposalId: this.proposalId,
        status: 20,
        receiveFrom: this.service.getStorage('userIdentifier'),
        createdTime: date
      }

      $("#message").val("");
      this.messageBox = null
      this.messages.next(chatmessage);
      this.message.push(
        {
          send: {
            message: chatmessage.message,
            messageType: chatmessage.messageType,
            receiveFrom: chatmessage.receiveFrom,
            proposalId: chatmessage.proposalId,
            createdTime: chatmessage.createdTime
          }
        }
      )
    }
    //this.chatmessage.message = null;
  }
  onKeydown(event) {
    if (event.key === "Enter") {

      this.sendMsg()
    }
  }


  message = []
  messageSendCount = 0
  receiveMsgCount = 0
  failed = false


  scrollTopPosition
  //Recent chat
  recentChat = [];
  recentContactLoader;
  flag = 0
  chatRecentContact() {
    this.recentContactLoader = true;
    this.failed = false
    try {
      let receiverId

      this.service.serverGetRequest('', 'chat/recent.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
        if (res['status'].responseCode == 20) {
          this.recentContactLoader = false
          this.service.loader = false
          this.recentChat = res['result'].recentChatList
          let contactCount = this.recentChat.length
          if (contactCount != 0) {
            if (this.recentChat[0].chatReadStatus == 0)
              this.service.messageRead(this.recentChat[0].chatSessionId)
            if (!this.receiverId && !this.proposalId) {
              this.getChatHistory(this.recentChat[0].targetUserId, this.recentChat[0].proposalId)

            }
            if (this.flag == 0) {
              this.receiverId = this.recentChat[0].targetUserId;//this.recentChat[0].createdUserId;
            //  this.proposalId = this.recentChat[0].proposalId
            }
            // this.contactName = this.recentChat[0].createdUserName;
            // this.contactProfileImage = this.recentChat[0].profileImage;
          }
        } else this.service.apiErrorHandler(res)
      }, error => { })
    }
    catch (er) {

    }
  }
  contactName;
  contactProfileImage;
  chatContact
  ContactLoader = false
  chatContactList() {
    this.ContactLoader = true;
    this.service.serverGetRequest('', 'chat/contacts.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      if (res['status'].responseCode == 20) {
        this.ContactLoader = false

        this.chatContact = res['result'].chatContactList


      } else this.service.apiErrorHandler(res)
    }, error => { })
  }
  historyLoad = true
  checkMessageType(val) {

    if (val)
      if (val['messageType'] == 'text')
        return true;
      else
        return false;
  }
  historyPageNo = 1
  historyTotalCount = 0
  getChatHistory(receiverId, proposalId) {
   
    this.getProposalDescription(proposalId)
    try {
      this.historyLoad = true
      this.failed = false
      if (this.historyPageNo == 1) {
        this.message = [];
        this.historyTotalCount = 0

      }
      this.service.loader = true;
      this.proposalId = proposalId
      this.receiverId = receiverId
      this.service.serverGetRequest('', 'chat/history.json?registerType=' + this.service.getStorage('registerType') + '&proposalId=' + proposalId + '&targetUserId=' + receiverId + "&page=" + this.historyPageNo).subscribe(res => {
        if (res['status'].responseCode == 20) {
          this.historyLoad = false
          this.historyTotalCount = res['result'].totalCount
          this.historyPageNo++
          this.chatFileLoader(receiverId, proposalId)
          this.service.loader = false
          let chatHistory: any[] = res['result'].chatHistory;
          chatHistory.forEach(element => {

            if (element)
              if (element.createdUserId == receiverId) {
                this.message.push(
                  {
                    receive: {
                      message: element.message,
                      messageType: element.messageType,
                      receiveFrom: element.receiveFrom,
                      proposalId: element.proposalId,
                      fileName: element.fileName,
                      createdTime: this.service.convertDateToGmt(element.createdTime)
                    }
                  }
                )

              }
              else {
                this.message.push(
                  {
                    send: {
                      message: element.message,
                      messageType: element.messageType,
                      receiveFrom: element.receiveFrom,
                      proposalId: element.proposalId,
                      fileName: element.fileName,
                      createdTime: this.service.convertDateToGmt(element.createdTime)
                    }
                  }
                );

              }

          });
          console.info(this.message)
          //  this.message.reverse()

        } else this.service.apiErrorHandler(res)
      }, error => { })
    }
    catch (er) {

    }
  }
  proposalDescription
  contractId
  getProposalDescription(proposalId) {
    this.service.serverGetRequest('', 'proposalDetails/' + proposalId + '.json?registerType=' + this.service.getStorage('registerType')).
      subscribe(res => {
        if (res['status'].responseCode == 20) {
          this.service.loader = false
          if (res['result'].proposal.jobDetails) {
            this.proposalDescription = res['result'].proposal.jobDetails.jobDescription;
            this.contractId = res['result'].proposal.contractId
          }
          if (this.service.isEmployeeLogin()) {
            this.contactName = res['result'].proposal.employerDetails.employerName
            
            this.contactProfileImage = res['result'].proposal.employerDetails.employerProfileImage
          }
          if (this.service.isEmployerLogin() && res['result'].proposal.employeeDetails) {
            this.contactName = res['result'].proposal.employeeDetails.employee
          
            this.contactProfileImage = res['result'].proposal.employeeDetails.profileImage
          }
        }
        else
          this.service.apiErrorHandler(res)
      })
  }
  chatFile;
  chatFileLoader(receiverId, proposalId) {
    try {
      this.service.loader = true;

      this.service.serverGetRequest('', 'chat/files.json?registerType=' + this.service.getStorage('registerType') + '&proposalId=' + proposalId + '&targetUserId=' + receiverId).subscribe(res => {
        if (res['status'].responseCode == 20) {
          this.service.loader = false
          this.chatFile = res['result'].chatfiles;

        } else this.service.apiErrorHandler(res)
      }, error => { })
    }
    catch (er) {

    }
  }
  dwnld_file = false;
  fileLoader: any;
  fileName
  fileUploadLoader = false;
  uploadFile(event) {

    let response;

    const fileSelected: File = event.target.files[0];


    if (!this.service.checkAllowedFiles(event.target.files[0].name.split('.').pop())) {

      this.service.showError(this.validationMsg.invalidFileType)
      return false
    }
    if (fileSelected.size > this.service.maxFileSize) {
      this.service.showError(this.validationMsg.attachmentSizeLimit);
      return false;
    }
    if (event.target.files[0])
     this.ngProgress.start();
    this.service.uploadFile(fileSelected, 'upload.json?fileType=chat')
      .subscribe((res: Response) => {
        response = res;
        this.ngProgress.done()

        if (response.status.responseCode == 20) {
          this.fileName = fileSelected.name
          this.fileUploadLoader = false
          this.chatFileLoader(this.receiverId, this.proposalId)
          this.fileChat(response.result.file1);
        }
        else {
          this.service.apiErrorHandler(res);


        }
      },
        (error) => {
          this.fileUploadLoader = false
          this.ngProgress.done()
          this.service.serviceErrorResponce(error)
        });
  }

  fileChat(fileId) {
    let chatmessage = {
      fromType: this.service.getStorage('registerType'),
      message: fileId,
      messageType: "file",
      to: this.receiverId,
      proposalId: this.proposalId,
      status: 20,
      receiveFrom: this.service.getStorage('userIdentifier'),
      createdTime: Date()
    }

    this.messages.next(chatmessage);
    this.message.push(
      {
        send: {
          fileName: this.fileName,
          message: fileId,
          messageType: chatmessage.messageType,
          receiveFrom: chatmessage.receiveFrom,
          proposalId: chatmessage.proposalId,
          createdTime: Date()
        }
      }
    )

  }
}
