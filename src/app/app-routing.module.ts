import { DemocodeComponent } from './layout/democode/democode.component';
import { JoblistnologinComponent } from './common/joblistnologin/joblistnologin.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './component/home/home.component';
import { ActivateEmailComponent } from './component/activate-email/activate-email.component'
import { SignupComponent } from './component/signup/signup.component';

import { LoginComponent } from './component/login/login.component';

import { EmpolyeehomeComponent } from './component/empolyeehome/empolyeehome.component'
import { EmpolyerhomeComponent } from './component/empolyerhome/empolyerhome.component'
import { NonUrgentRequestComponent } from './component/Employer/non-urgent-request/non-urgent-request.component';
import { UrgentRequestComponent } from './component/Employer/urgent-request/urgent-request.component';
import { NewapplicationJobsComponent } from './component/Employer/newapplication-jobs/newapplication-jobs.component';
import { EmployerEvaluateComponent } from './component/Employer/employer-evaluate/employer-evaluate.component';
import { JobAnnouncedComponent } from './component/Employer/job-announced/job-announced.component';
import { ProfileComponent } from './component/Employer/profile/profile.component';
import { ViewApplicantProfileComponent } from './component/Employer/view-applicant-profile/view-applicant-profile.component';
import { ViewApplicantsComponent } from './component/Employer/view-applicants/view-applicants.component';
import { EmployerEditJobComponent } from './component/Employer/employer-edit-job/employer-edit-job.component';
import { CompanyDetailsComponent } from './component/Employer/company-details/company-details.component';
import { ManagerDetailsComponent } from './component/Employer/manager-details/manager-details.component';
import { ViewEmployeeComponent } from './component/Employer/view-employee/view-employee.component';
import { HowItWorkComponent } from './component/how-it-work/how-it-work.component';
import { ProposalComponent } from './component/Employee/proposal/proposal.component';
import { VieweEmployeeDetailsComponent } from './component/Employer/viewe-employee-details/viewe-employee-details.component';
import { EmployerMyJobsComponent } from './component/Employer/employer-my-jobs/employer-my-jobs.component';
import { AllEmployeeListComponent } from './component/Employer/all-employee-list/all-employee-list.component';
import { SearchAllPersonComponent } from './component/search-all-person/search-all-person.component';
import { HireSearchEmployeeComponent } from './component/hire-search-employee/hire-search-employee.component';
import { UserDataSettingsComponent } from './component/user-data-settings/user-data-settings.component';
import { FaqComponent } from './component/faq/faq.component';
import { UserNotificationsComponent } from './component/user-notifications/user-notifications.component';
import { JobTimeLineComponent } from './component/job-time-line/job-time-line.component';
import { JobTimeLineChartComponent } from './component/job-time-line-chart/job-time-line-chart.component';
import { TimelogDetailsComponent } from './component/timelog-details/timelog-details.component';
import { NotsureComponent } from './component/notsure/notsure.component'
import { DisputesComponent } from './component/disputes/disputes.component'
import { PrivacyEnglishComponent } from './common/legal/privacy/privacy-english/privacy-english.component'
import { TermsEnglishComponent } from './common/legal/terms/terms-english/terms-english.component'
import { ForgotPasswordComponent } from './component/forgot-password/forgot-password.component';
import { CookiesComponent } from './common/legal/cookies/cookies.component';
import { UrgentRequestMapComponent } from './component/Employer/urgent-request-map/urgent-request-map.component';
import { DownloadappComponent } from './component/downloadapp/downloadapp.component';
import { AndroidDownloadEmployeeComponent } from './android-download-employee/android-download-employee.component';
import { AndroidDownloadEmployerComponent } from './component/android-download-employer/android-download-employer.component';


/*******
 * Employee 
 */
import { EmployeeJoblistComponent } from './component/Employee/employee-joblist/employee-joblist.component';
import { EmployeeViewjoblistComponent } from './component/Employee/employee-viewjoblist/employee-viewjoblist.component';
import { EmployeeEditComponent } from './component/Employee/employee-edit/employee-edit.component';
import { EmployeeEvaluateComponent } from './component/Employee/employee-evaluate/employee-evaluate.component';
import { EmployeeViewMapComponent } from './component/Employee/employee-view-map/employee-view-map.component';
import { AccountSetupComponent } from './component/Employee/account-setup/account-setup.component';
import { EducationSetupComponent } from './component/Employee/education-setup/education-setup.component';
import { PersonalSetupComponent } from './component/Employee/personal-setup/personal-setup.component';
import { MyoffersComponent } from './component/Employee/myoffers/myoffers.component';
import { EmployeemessageComponent } from './component/employeemessage/employeemessage.component';
import { InviteInterviewComponent } from './component/Employee/invite-interview/invite-interview.component';
import { ViewProposalComponent } from './component/view-proposal/view-proposal.component';
import { ContactsusComponent } from './component/contactsus/contactsus.component';
// import { CompanydetailsComponent } from './component/employee/companydetails/companydetails.component';
import { ViewCompanyDetailsComponent } from './component/Employee/view-company-details/view-company-details.component';

import { PaymentProcessingComponent } from './component/layout/payment-processing/payment-processing.component';
import { PaymentSuccessComponent } from './component/layout/payment-success/payment-success.component';
import { PaymentFailedComponent } from './component/layout/payment-failed/payment-failed.component';
import { AboutCompanyComponent } from './component/about-company/about-company.component';
import { PlansAndServicesComponent } from './common/plans-and-services/plans-and-services.component';
import { FeaturedworksComponent } from './layout/employee/featuredworks/featuredworks.component';
import { SidebarfiltermobileComponent } from './layout/sidebarfiltermobile/sidebarfiltermobile.component';
/**
 * Common 
 */
import { EditProposalComponent } from './component/Employee/edit-proposal/edit-proposal.component';

import { JobListComponent } from './layout/job-list/job-list.component'
import { ContractsComponent } from './common/contracts/contracts.component'
import { ChartComponent } from './layout/chart/chart.component';
import { MaptrackComponent } from './common/maptrack/maptrack.component';
import { MessageverificationComponent } from './common/messageverification/messageverification.component';
import { EvaluateComponent } from './common/evaluate/evaluate.component';
import { WaitingConfirmationPageComponent } from './common/waiting-confirmation-page/waiting-confirmation-page.component';
const routes: Routes = [
  { path: 'editproposal/:proposalId', component: EditProposalComponent },
  { path: 'SidebarfiltermobileComponent', component: SidebarfiltermobileComponent },
  { path: 'test', component: EvaluateComponent },
  { path: 'downloadapp', component: DownloadappComponent },
  { path: 'androidappemployee', component: AndroidDownloadEmployeeComponent },
  { path: 'androidappemployer', component: AndroidDownloadEmployerComponent },
  { path: 'demo', component: DemocodeComponent },
  { path: 'featuredworks', component: FeaturedworksComponent },
  { path: 'confirmation', component: WaitingConfirmationPageComponent },
  { path: 'plans_and_services', redirectTo: '/plans-and-services', pathMatch: 'full' },
  { path: 'plans-and-services', component: PlansAndServicesComponent },

  { path: 'about_us', redirectTo: '/about-us', pathMatch: 'full' },
  { path: 'about-us', component: AboutCompanyComponent },

  { path: 'payment_failed', redirectTo: '/payment-failed', pathMatch: 'full' },
  { path: 'payment-failed', component: PaymentFailedComponent },

  { path: 'payment_success', redirectTo: '/payment-success', pathMatch: 'full' },
  { path: 'payment-success', component: PaymentSuccessComponent },

  { path: 'payment_processing', redirectTo: '/payment-processing', pathMatch: 'full' },
  { path: 'payment-processing', component: PaymentProcessingComponent },

  { path: 'view_company_details', redirectTo: '/view-company-details', pathMatch: 'full' },
  { path: 'view-company-details', component: ViewCompanyDetailsComponent },

  { path: 'cookies', component: CookiesComponent },

  { path: 'forgot_password', redirectTo: '/forgot-password', pathMatch: 'full' },
  { path: 'forgot-password', component: ForgotPasswordComponent },

  { path: 'terms', component: TermsEnglishComponent },
  { path: 'privacy', component: PrivacyEnglishComponent },

  { path: 'contact_us', redirectTo: '/contactus', pathMatch: 'full' },
  { path: 'contactus', component: ContactsusComponent },

  { path: 'disputes', component: DisputesComponent },

  { path: 'how_it_work', redirectTo: '/how-it-work', pathMatch: 'full' },
  { path: 'how-it-work', component: HowItWorkComponent },

  { path: 'home', component: HomeComponent },
  { path: 'signup', component: SignupComponent },

  { path: 'login', component: LoginComponent },
  { path: 'faqs', component: FaqComponent },

  { path: 'user_notifications', redirectTo: '/user-notifications', pathMatch: 'full' },
  { path: 'user-notifications', component: UserNotificationsComponent },

  { path: 'jobtimeline/:contractId', redirectTo: '/job-time-line/:contractId', pathMatch: 'full' },
  { path: 'job-time-line/:contractId', component: JobTimeLineComponent },

  { path: 'jobtime_line_chart', redirectTo: '/job-timeline-chart', pathMatch: 'full' },
  { path: 'job-timeline-chart', component: JobTimeLineChartComponent },

  { path: 'time_log_details', redirectTo: '/time-logdetails', pathMatch: 'full' },
  { path: 'time-logdetails', component: TimelogDetailsComponent },

  { path: '', redirectTo: '/home', pathMatch: 'full' },

  { path: 'employee_home', redirectTo: '/employee-home', pathMatch: 'full' },
  { path: 'employee-home', component: EmpolyeehomeComponent },

  { path: 'activate_email', redirectTo: '/activate-email', pathMatch: 'full' },
  { path: 'activate-email', component: ActivateEmailComponent },

  { path: 'search_all_person', redirectTo: '/search-all-person', pathMatch: 'full' },
  { path: 'search-all-person', component: SearchAllPersonComponent },
  { path: 'user_settings', redirectTo: '/user-settings/1', pathMatch: 'full' },
  { path: 'user-settings/1', component: UserDataSettingsComponent },

  { path: 'user_settings/:tab', redirectTo: '/user-settings/:tab', pathMatch: 'full' },
  { path: 'user-settings/:tab', component: UserDataSettingsComponent },

  { path: 'viewProposal/:proposalId', redirectTo: '/view-proposal/:proposalId', pathMatch: 'full' },
  { path: 'view-proposal/:proposalId', component: ViewProposalComponent },

  { path: 'viewContract/:contractId', redirectTo: '/view-contract/:contractId', pathMatch: 'full' },
  { path: 'view-contract/:contractId', component: ViewProposalComponent },

  { path: 'employee_message', redirectTo: '/employee-message', pathMatch: 'full' },
  { path: 'employee-message', component: EmployeemessageComponent },

  { path: 'message/:receiver/:proposal', component: EmployeemessageComponent },

  { path: 'myoffers-employee', redirectTo: '/my-offers-employee', pathMatch: 'full' },
  { path: 'my-offers-employee', component: MyoffersComponent },

  { path: 'proposals', component: ProposalComponent },

  { path: 'job_announced', redirectTo: '/job-announced', pathMatch: 'full' },
  { path: 'job-announced', component: JobAnnouncedComponent },

  { path: 'employee_joblist', redirectTo: '/employee-job-list', pathMatch: 'full' },
  { path: 'employee-job-list', component: EmployeeJoblistComponent },

  { path: 'employee_viewjoblist/:data', redirectTo: '/employee-view-job-list/:data', pathMatch: 'full' },
  { path: 'employee-view-job-list/:data', component: EmployeeViewjoblistComponent },

  { path: 'view-job-details/:data', component: JoblistnologinComponent },

  { path: 'employee_edit', redirectTo: '/employee-edit', pathMatch: 'full' },
  { path: 'employee-edit', component: AccountSetupComponent },

  { path: 'employee_evaluate', redirectTo: '/employee-evaluate', pathMatch: 'full' },
  { path: 'employee-evaluate', component: EmployeeEvaluateComponent },

  { path: 'employee_view_map', redirectTo: '/employee-view-map', pathMatch: 'full' },
  { path: 'employee-view-map', component: EmployeeViewMapComponent },

  { path: 'employee_accountsetup', redirectTo: '/employeeaccountsetup', pathMatch: 'full' },
  { path: 'employeeaccountsetup', component: AccountSetupComponent },

  { path: 'employee_educationsetup', redirectTo: '/employee-education-setup', pathMatch: 'full' },
  { path: 'employee-education-setup', component: EducationSetupComponent },

  { path: 'employee_profetionalsetup', redirectTo: '/employee-profetional-setup', pathMatch: 'full' },
  { path: 'employee-profetional-setup', component: PersonalSetupComponent },

  { path: 'completed_contracts', redirectTo: '/contracts', pathMatch: 'full' },
  { path: 'contracts', component: ContractsComponent },

  { path: 'inviteinterview_employee', redirectTo: '/invite-interview-employee', pathMatch: 'full' },
  { path: 'invite-interview-employee', component: MyoffersComponent },


  //Employer Pages
  { path: 'employer_home', redirectTo: '/employer-home', pathMatch: 'full' },
  { path: 'employer-home', component: EmpolyerhomeComponent },

  { path: 'companyprofile', redirectTo: '/company-profile', pathMatch: 'full' },
  { path: 'company-profile', component: CompanyDetailsComponent },

  { path: 'not_sure', redirectTo: '/notsure', pathMatch: 'full' },
  { path: 'notsure', component: NotsureComponent },

  { path: 'urgentjobmap', redirectTo: '/urgent-job-map', pathMatch: 'full' },
  { path: 'urgent-job-map', component: UrgentRequestMapComponent },

  { path: 'job_post/:key', redirectTo: '/job-post/:key', pathMatch: 'full' },
  { path: 'job-post/:key', component: UrgentRequestComponent },

  { path: 'editjob/:key/:jobId', redirectTo: '/edit-job/:key/:jobId', pathMatch: 'full' },
  { path: 'edit-job/:key/:jobId', component: UrgentRequestComponent },

  { path: 'newapplication_jobs', redirectTo: '/new-application-jobs', pathMatch: 'full' },
  { path: 'new-application-jobs', component: NewapplicationJobsComponent },

  { path: 'employer_evaluate', redirectTo: '/employer-evaluate', pathMatch: 'full' },
  { path: 'employer-evaluate', component: EmployerEvaluateComponent },

  { path: 'view_applicants', redirectTo: '/view-applicants', pathMatch: 'full' },
  { path: 'view-applicants', component: ViewApplicantsComponent },

  { path: 'view_applicant_profile', redirectTo: '/view-applicant-profile', pathMatch: 'full' },
  { path: 'view-applicant-profile', component: ViewApplicantProfileComponent },

  { path: 'employer_edit_job', redirectTo: '/employer-edit-job', pathMatch: 'full' },
  { path: 'employer-edit-job', component: EmployerEditJobComponent },

  { path: 'employer_companydetails', redirectTo: '/employer-company-details', pathMatch: 'full' },
  { path: 'employer-company-details', component: CompanyDetailsComponent },

  { path: 'employer_managerdetails', redirectTo: '/employer-manager-details', pathMatch: 'full' },
  { path: 'employer-manager-details', component: ManagerDetailsComponent },

  { path: 'view_employee', redirectTo: '/view-employee', pathMatch: 'full' },
  { path: 'view-employee', component: ViewEmployeeComponent },

  { path: 'viewInvitationDetails', redirectTo: '/view-invitation-details', pathMatch: 'full' },

  { path: 'view-invitation-details', component: ViewEmployeeComponent },

  { path: 'view_employee_details', redirectTo: '/view-employee-details', pathMatch: 'full' },
  { path: 'view-employee-details', component: VieweEmployeeDetailsComponent },

  { path: 'my_jobs', redirectTo: '/my-jobs', pathMatch: 'full' },
  { path: 'my-jobs', component: EmployerMyJobsComponent },

  { path: 'all_employee_list/:jobId/:categoryId', redirectTo: '/all-employee-list/:jobId/:categoryId', pathMatch: 'full' },
  { path: 'all-employee-list/:jobId/:categoryId', component: AllEmployeeListComponent },

  { path: 'hire_person_direct', redirectTo: '/hire-person-direct', pathMatch: 'full' },
  { path: 'hire-person-direct', component: HireSearchEmployeeComponent },

  { path: 'employer_contracts', redirectTo: '/employer-contracts', pathMatch: 'full' },
  { path: 'employer-contracts', component: ContractsComponent },
  { path: 'track-employee', component: MaptrackComponent },
  { path: 'message-verification', component: MessageverificationComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
