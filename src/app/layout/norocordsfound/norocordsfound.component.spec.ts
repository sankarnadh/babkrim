import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NorocordsfoundComponent } from './norocordsfound.component';

describe('NorocordsfoundComponent', () => {
  let component: NorocordsfoundComponent;
  let fixture: ComponentFixture<NorocordsfoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NorocordsfoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NorocordsfoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
