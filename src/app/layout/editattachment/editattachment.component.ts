import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SafePipe, ServerService } from '../../service/server.service'
import '../../../assets/js/js/chat.js'
declare var customealert: any;
@Component({
  selector: 'app-editattachment',
  templateUrl: './editattachment.component.html',
  styleUrls: ['./editattachment.component.css']
})
export class EditattachmentComponent implements OnInit {

  message: string = "Hola Mundo!"

  @Output() messageEvent = new EventEmitter<string>();
  @Input() attachments: any;
  @Input() fileName: any;
  constructor(public service: ServerService) { }
  ngOnInit() {

  }
  sendMessage(val) {
    customealert.emptyFile()
   this.messageEvent.emit(val)
  }
}
