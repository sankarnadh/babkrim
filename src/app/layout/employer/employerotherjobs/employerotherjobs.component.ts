import { Component, OnInit, Input } from '@angular/core';
import { ServerService } from '../../../service/server.service'
import { Router} from '@angular/router'
@Component({
  selector: 'app-employerotherjobs',
  templateUrl: './employerotherjobs.component.html',
  styleUrls: ['./employerotherjobs.component.css']
})
export class EmployerotherjobsComponent implements OnInit {
  @Input() employerId;
  @Input() jobId;

  constructor(public service: ServerService, public router :Router) { }

  ngOnInit() {
    this.getEmployerJobs()
  }
  employerJob
  getEmployerJobs() {
    this.service.serverGetRequest('', 'employer/relativeJobs/'+this.employerId+'/'+this.jobId+'.json?pageNo=1').subscribe(res => {
      if (res['status'].responseCode == 20) { 
        this.employerJob=res['result'].jobDetails;
      }else 
      this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
  returnFloat(val){return parseFloat(val);}
  viewJobList(val) {

    this.service.setStorage('data', JSON.stringify(val))
    this.router.navigate(['/employee_viewjoblist' + '/' + val.jobId])
  }
}
