import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerotherjobsComponent } from './employerotherjobs.component';

describe('EmployerotherjobsComponent', () => {
  let component: EmployerotherjobsComponent;
  let fixture: ComponentFixture<EmployerotherjobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerotherjobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerotherjobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
