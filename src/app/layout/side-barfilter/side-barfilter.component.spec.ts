import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarfilterComponent } from './side-barfilter.component';

describe('SideBarfilterComponent', () => {
  let component: SideBarfilterComponent;
  let fixture: ComponentFixture<SideBarfilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideBarfilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarfilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
