import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeiconComponent } from './employeeicon.component';

describe('EmployeeiconComponent', () => {
  let component: EmployeeiconComponent;
  let fixture: ComponentFixture<EmployeeiconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeiconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeiconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
