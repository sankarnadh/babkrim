import { Component, OnInit, Input } from '@angular/core';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-employeeicon',
  templateUrl: './employeeicon.component.html',
  styleUrls: ['./employeeicon.component.css']
})
export class EmployeeiconComponent implements OnInit {
@Input() val;
@Input() showRating=0; // 1 hide
@Input() proposalRate=0; // change profilerate 
@Input() cssChange=0;
  constructor( public config: NgbRatingConfig) { 
    config.max = 5;
    config.readonly = true
  }

  ngOnInit() {
  }

}
