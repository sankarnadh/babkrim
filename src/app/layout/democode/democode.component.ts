import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-democode',
  templateUrl: './democode.component.html',
  styleUrls: ['./democode.component.css']
})

export class DemocodeComponent implements OnInit {

  private publishableKey: string = "pk_test_U4CiS23E5wkBliM1oEuuEzb9001BdF50ey";
  // extraData = {
  //   "name": 'sethu',
  //   "address_city": 'ekm',
  //   "address_line1": 'thuravoor',
  //   "address_line2": 'thuravoor theku',
  //   "address_state": 'kerala',
  //   "address_zip": '688531',
  // }

  onStripeInvalid(error: Error) {
    console.log('Validation Error', error)
  }

  setStripeToken(token) {
    console.log('Stripe token', token)
  }

  setStripeSource(source) {
    console.log('Stripe source', source)
  }

  onStripeError(error: Error) {
    console.error('Stripe error', error)
  }
  constructor() {
  
  }

  ngOnInit() { }
}

