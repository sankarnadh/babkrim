import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedworksComponent } from './featuredworks.component';

describe('FeaturedworksComponent', () => {
  let component: FeaturedworksComponent;
  let fixture: ComponentFixture<FeaturedworksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedworksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedworksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
