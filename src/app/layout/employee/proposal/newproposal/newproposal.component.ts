import { Component, OnInit } from '@angular/core';
import {ServerService} from '../../../../service/server.service'
import {Router} from '@angular/router'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-newproposal',
  templateUrl: './newproposal.component.html',
  styleUrls: ['./newproposal.component.css']
})
export class NewproposalComponent implements OnInit {

  constructor(public config: NgbRatingConfig,public service : ServerService,public router : Router) { 
    config.max = 5;
    config.readonly = true;
  }
  currentRate=0
  calculateRate(rating,count)
  {
return rating/count
  }
  ngOnInit() {
  }

}
