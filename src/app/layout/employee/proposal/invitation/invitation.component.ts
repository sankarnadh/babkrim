import { Component, OnInit } from '@angular/core';
import {ServerService} from '../../../../service/server.service'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import '../../../../../assets/js/js/chat.js'
import { ValidationMessageService } from 'src/app/service/validation-message.service';
declare var customealert;
@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})
export class InvitationComponent implements OnInit {

  constructor(   public config: NgbRatingConfig,public service : ServerService,public validationMsg: ValidationMessageService) {
    config.max=5;
    config.readonly=true
   }

  ngOnInit() {
  }
  offer;
  rejectProposals() {
    customealert.loaderShow('html');
    let url: any;
    this.offer.contractId ? url = 'contractDetails/offers/reject/' : url = 'invitation/reject/';
    let contractId = this.offer.contractId ? this.offer.contractId : this.offer.invitationId


    this.service.serverDeleteRequest(url + contractId + ".json?registerType=" + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.getInvitationToInterviewFromApi();
      } else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))
  }
  getInvitationToInterviewFromApi()
  {
    window.scrollTo(0,0)
    this.service.loader=true
    this.service.serverGetRequest('','invitationDetails.json').subscribe((res)=>{
      this.service.loader=false
      if(res['status'].responseCode==20)
      { 
          this.service.invitation=res['result'].invitations
          this.service.invitationLength=res['result'].invitations.length
          this.service.loader=false
         
      }
      else 
      this.service.apiErrorHandler(res)
    })
  }
}
