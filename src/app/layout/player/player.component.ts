import { Component, OnInit,Input,LOCALE_ID, OnDestroy } from '@angular/core';
import {ServerService} from '../../service/server.service'

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit,OnDestroy {

  constructor(public service : ServerService) {
    
  }
@Input() videoUrl : any ;
  ngOnInit() {
    setTimeout(()=>{
     

console.log("pause")
    },1)
  }
  vgApi
  onPlayerReady(event){
    console.log(event)
    this.vgApi=event
  }
ngOnDestroy(){
  this.vgApi.pause()
}
  pausePlayer(){
    this.vgApi.pause()
  }
}
