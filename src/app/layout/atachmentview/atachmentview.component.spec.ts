import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtachmentviewComponent } from './atachmentview.component';

describe('AtachmentviewComponent', () => {
  let component: AtachmentviewComponent;
  let fixture: ComponentFixture<AtachmentviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtachmentviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtachmentviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
