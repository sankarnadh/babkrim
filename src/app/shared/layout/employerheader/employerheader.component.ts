import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../service/server.service';

@Component({
  selector: 'app-employerheader',
  templateUrl: './employerheader.component.html',
  styleUrls: ['./employerheader.component.css']
})
export class EmployerheaderComponent implements OnInit {

  constructor(public service: ServerService) { }

  ngOnInit() {
  }
  logout() {
    this.service.logout();

    this.service.checkLogin(); //To change the header

    //this.router.navigate(['/home']);
  }
}
