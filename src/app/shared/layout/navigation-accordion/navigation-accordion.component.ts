import { Component, OnInit, Input } from '@angular/core';
import { ServerService } from '../../../service/server.service';

@Component({
  selector: 'app-navigation-accordion',
  templateUrl: './navigation-accordion.component.html',
  styleUrls: ['./navigation-accordion.component.css']
})
export class NavigationAccordionComponent implements OnInit {

  constructor(public service: ServerService) { }

  ngOnInit() {
    this.getAllCategory()
 console.info(this.service.categoryArray)
  }
  getAllCategory() {

    this.service.serverGetRequest('', 'custom/home/commonHeaders.json').subscribe(
      (res) => {
        if (res['status'].responseCode == 20) {
         
         
          this.service.categoryArray =res['result'].commonCategories;
          this.service.categoryLength=(this.service.categoryArray.length) / 4;
          

        }
      },
      err => {


      }
    );
  }
}
