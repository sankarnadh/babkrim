import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../service/server.service';

@Component({
  selector: 'app-employeeheader',
  templateUrl: './employeeheader.component.html',
  styleUrls: ['./employeeheader.component.css']
})
export class EmployeeheaderComponent implements OnInit {

  constructor(public service: ServerService) { }

  ngOnInit() {
  }
  logout() {
    this.service.logout();

    this.service.checkLogin(); //To change the header

    //this.router.navigate(['/home']);
  }
}
