import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ElementRef,
  ViewChild
} from "@angular/core";
import { ServerService } from "./service/server.service";
import { Router, NavigationEnd } from "@angular/router";
import { enableProdMode } from "@angular/core";
import { BrowserpushService } from "./service/browserpush.service";
import { NotificationComponent } from "./common/notification/notification.component";
import { MessagingService } from "./service/message.service";
import { TranslateService } from "@ngx-translate/core";
import { ValidationMessageService } from "./service/validation-message.service";
declare var google: any;
enableProdMode();
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //title = 'app';
  employee_login: any; //Employee Login
  employer_login: any; //Employer Login
  noLogin: any; //No logins
  category: any = [];
  loop: any = [0, 1, 2, 3];
  limitCount = 10;
  commonHeaders: any;
  baseurl: any;
  langSelected: boolean = this.service.langSelected();

  //Test
  all = "";
  other = "Other Countries";
  most = "Most Popular Countries";
  disabled = false;

  selectedItems = [];
  dropdownSettings = {};
  showMenu = false;
  constructor(
    public service: ServerService,
    private router: Router,
    public el: ElementRef,
    private messagingService: MessagingService,
    public notificationService: BrowserpushService,
    public translate:TranslateService,
    public validationMsg:ValidationMessageService
  ) {
    translate.get("income", {  }).subscribe((res: string) => {
      console.info(this.translate.instant('level1EmailExists'));
      this.validationMsg.translateValues()
    });

    router.events.subscribe(val => {
      // see also

      let urlNav = val instanceof NavigationEnd;
    });
    this.notificationService.requestPermission();
  }
  @ViewChild("appNotification") appNotification: NotificationComponent;
  /************************ */
  notify() {
    let data: Array<any> = [];
    data.push({
      title: "Approval",
      alertContent: "This is First Alert -- By Debasis Saha"
    });

    this.notificationService.generateNotification(data);
  }
  /************************** */
  ngOnInit() {
    // this.service.showError('test')
    //   setInterval(() => {

    //   }, 5000);
    const userId = "user001";
    this.messagingService.requestPermission(userId);
    this.messagingService.receiveMessage();

    if (this.service.isEmployeeLogin() || this.service.isEmployerLogin())
      this.service.getChatUnreadCount();

    setInterval(() => {
      if (this.service.getStorage("registerType"))
        this.selectSearch(this.service.getStorage("registerType"));
    }, 10);

    /*****
     *
     */
    this.getAllCategory();
    this.service.getCountry();
    this.service.checkLogin();
  }

  bodyscrolltop() {
    window.scrollTo(0, 0);
  }

  onSelectAll(items: any) {
    // console.log(items);
  }
  /***
   * Logout & destroy all storage
   */
  logout() {
    let res;

    res = this.service.logout();

    this.service.checkLogin(); //To change the header

    //this.router.navigate(['/home']);
  }
  /***
   * Get all category
   */
  ar: any;
  returnCategoryName(i, k) {
    try {
      return this.category[this.categoryIndex(i, k)]
        ? this.category[this.categoryIndex(i, k)].name
        : "";
    } catch (error) {
      //  console.info(error)
    }
  }
  returnSubCategories(i, k) {
    try {
      return this.category[this.categoryIndex(i, k)].subCategories;
    } catch (error) {}
  }
  categoryIndex(i, k) {
    //IMPORTANT
    let index;
    if (i == 0 && k == 0) return i;
    else {
      if (k == 0) return parseInt(i + 1);
      else return +(k * 10 + i + 1);
    }
  }

  isSub(val) {
    if (val) return true;
    else return false;
  }
  checkCat(val) {

    // switch (val) {
    //   case "House & Building Maintenance": {
    //     this.service.pushCategoryFilter("House & Building Maintenance", 12);
    //     this.service.searchCatId = 15;
    //     break;
    //   }
    //   case "Vehicle Repair Service": {
    //     this.service.pushCategoryFilter("Vehicle Repair Service", 14);
    //     this.service.searchCatId = 12;
    //     break;
    //   }
    //   case "Teaching & Home Tution": {
    //     this.service.pushCategoryFilter("Teaching & Home Tution", 15);
    //     this.service.searchCatId = 14;
    //     break;
    //   }

    //   case "Domestic Labour": {
    //     this.service.pushCategoryFilter("Domestic Labour", 36);
    //     this.service.searchCatId = 16;
    //     break;
    //   }
    //   case "Driver": {
    //     this.service.pushCategoryFilter("Driver", 17);
    //     this.service.searchCatId = 17;
    //     break;
    //   }

    //   case "الطب البشري": {
    //     this.service.pushCategoryFilter("الطب البشري", 15);
    //     this.service.searchCatId = 15;
    //     break;
    //   }
    //   case "قانوني": {
    //     this.service.pushCategoryFilter("قانوني", 12);
    //     this.service.searchCatId = 12;
    //     break;
    //   }
    //   case "تطوير الويب والجوال": {
    //     this.service.pushCategoryFilter("تطوير الويب والجوال", 14);
    //     this.service.searchCatId = 14;
    //     break;
    //   }

    //   case "تكنولوجيا المعلومات والشبكات": {
    //     this.service.pushCategoryFilter("تكنولوجيا المعلومات والشبكات", 16);
    //     this.service.searchCatId = 16;
    //     break;
    //   }
    //   case "هندسة": {
    //     this.service.pushCategoryFilter("هندسة", 17);
    //     this.service.searchCatId = 17;
    //     break;
    //   }
    //   case "التصميم": {
    //     this.service.pushCategoryFilter("التصميم", 18);
    //     this.service.searchCatId = 18;
    //     break;
    //   }
    //   case "تسويق": {
    //     this.service.pushCategoryFilter("تسويق", 21);
    //     this.service.searchCatId = 21;
    //     break;
    //   }
    //   case "بيوت وصيانة المباني": {
    //     this.service.pushCategoryFilter("بيوت وصيانة المباني", 24);
    //     this.service.searchCatId = 24;
    //     break;
    //   }
    // }

    const categoryId = this.getCategoryId(val);
    this.service.pushCategoryFilter(val, categoryId);
    this.service.searchCatId = categoryId;

  }
  categoryLength: any;
  getAllCategory() {
    
    this.service
      .serverGetRequest("", "custom/home/commonHeaders.json")
      .subscribe(
        res => {
          if (res["status"].responseCode == 20) {
            this.category = res["result"].commonCategories;            
            const count: any = parseInt(this.category.length);
            if(count %2 ==0)
            this.limitCount =10;
            else 
            this.limitCount=9
            this.commonHeaders = res["result"].commonHeaders;
            this.service.categoryArray = this.category;
            this.service.categoryLength = this.category.length / 4;
            this.categoryLength = this.category.length / 4;            
          }
        },
        err => {}
      );
  }

  getCategoryId(categoryName){
    let categoryId;
    for(let i =0; i<this.category.length; i++){      
      if(categoryName.toUpperCase() === this.category[i].name.toUpperCase()){
        categoryId =  this.category[i].id;
        break;
      }
    }
    return categoryId;
  }

  onKeyDown(event) {
    if (event.key === "Enter") this.allSearchPerson();
  }

  allSearchPerson() {
    this.service.fetchAllJobDetailsFromApi(0);
    if (this.selectBoxSearch == "employee") {
      //this.employee_login ||
      this.router.navigate(["employee_joblist"]);
    }
    if (this.selectBoxSearch == "employer") {
      //this.employer_login ||
      this.router.navigate(["/search_all_person"]);
    }
  }
  selectBoxSearch = "employer";
  selectSearch(val) {
    if (val != this.selectBoxSearch) this.service.search = null;
    this.selectBoxSearch = val;

    if (val == "employee") this.placeHolder = "searchjob";
    else this.placeHolder = "find_employee";
  }
  placeHolder = "find_employee";
}
