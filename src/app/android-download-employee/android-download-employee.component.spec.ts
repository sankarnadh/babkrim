import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AndroidDownloadEmployeeComponent } from './android-download-employee.component';

describe('AndroidDownloadEmployeeComponent', () => {
  let component: AndroidDownloadEmployeeComponent;
  let fixture: ComponentFixture<AndroidDownloadEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AndroidDownloadEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AndroidDownloadEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
