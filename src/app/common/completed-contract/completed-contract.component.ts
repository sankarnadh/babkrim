import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServerService } from '../../service/server.service'
import { Router } from "@angular/router";
import '../../../assets/js/js/chat.js';

declare var customealert: any;
@Component({
  selector: 'app-completed-contract',
  templateUrl: './completed-contract.component.html',
  styleUrls: ['./completed-contract.component.css'],

})
export class CompletedContractComponent implements OnInit ,OnDestroy{

  constructor(public service: ServerService, 
   
    private router: Router) { }
  componyLogo = "assets/images/userarabic.jpg"
  endDateCalculate(date, noOfDays) {
    let data = new Date(date.split('.')[0])

    return data.setDate(data.getDate() + noOfDays)
  }
  ngOnInit() {
    window.scrollTo(0, 0)
  }
   gotoMyProfile(userId){
    if (this.service.isEmployeeLogin()) {
      this.service.setStorage('employerTempId', userId);
     this.router.navigate(['/view_company_details'])
     }
     else 
     {
       this.service.setStorage('employeeTempId', userId);
       this.router.navigate(['/hire_person_direct'])
     }
  }
  setValues(val) {
    this.router.navigate(['/jobtimeline/'+val.contractId])
    this.service.setStorage('completed', '1')
    this.service.setStorage('val', JSON.stringify(val));
    this.service.setStorage('totalTime', val.totalTime)
    this.service.setStorage('contractId', val.contractId);
    if (this.service.isEmployeeLogin())
      this.service.setStorage('tempName', val.employerDetails.employerName);
    if (this.service.isEmployerLogin())
      this.service.setStorage('tempName', val.employeeDetails.employeeName);
    this.service.setStorage('jobtitle', val.jobDetails.jobTitle);
    this.service.setStorage('jobDescription', val.jobDetails.jobDescription)
  }
  returnFloat(val){return parseFloat(val);}
  ngOnDestroy(){
    this.service.completedContracts=[]
  }
}
