import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomegraphwidgetComponent } from './incomegraphwidget.component';

describe('IncomegraphwidgetComponent', () => {
  let component: IncomegraphwidgetComponent;
  let fixture: ComponentFixture<IncomegraphwidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomegraphwidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomegraphwidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
