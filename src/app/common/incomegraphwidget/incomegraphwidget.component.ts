import { Component, OnInit } from "@angular/core";
import { ServerService } from "src/app/service/server.service";
import { DatePipe } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-incomegraphwidget",
  templateUrl: "./incomegraphwidget.component.html",
  styleUrls: ["./incomegraphwidget.component.css"]
})
export class IncomegraphwidgetComponent implements OnInit {
  constructor(
    public service: ServerService,
    private translate: TranslateService,
    private datepipe: DatePipe
  ) {}
  data = [0, 0, 0];
  toDate: any = new Date();
  fromDate: any = new Date();
  params: any = {
    value: ""
  };
  ngOnInit() {
    let month: any = +this.datepipe.transform(this.toDate, "M");
    this.params.value = this.service.isEmployeeLogin()
      ? this.translate.instant("income")
      : this.translate.instant("expense");

    switch (month) {
      case 1:
        this.lineChartLabels = ["November", "December", "January"];
        break;
      case 2:
        this.lineChartLabels = ["December", "January", "February"];
        break;
      case 3:
        this.lineChartLabels = ["January", "February", "March"];
        break;
      case 4:
        this.lineChartLabels = ["February", "March", "April"];

        break;
      case 5:
        this.lineChartLabels = ["March", "April", "May"];

        break;
      case 6:
        this.lineChartLabels = ["April", "May", "June"];

        break;
      case 7:
        this.lineChartLabels = ["May", "June", "July"];

        break;
      case 8:
        this.lineChartLabels = ["June", "July", "August"];

        break;
      case 9:
        this.lineChartLabels = ["July", "August", "September"];

        break;
      case 10:
        this.lineChartLabels = ["August", "September", "October"];

        break;
      case 11:
        this.lineChartLabels = ["September", "October", "November"];

        break;
      case 12:
        this.lineChartLabels = ["October", "November", "December"];
        break;
    }

    this.fromDate = this.fromDate.setDate(this.toDate.getDate() - 90);
    this.fromDate = this.datepipe.transform(
      this.fromDate,
      "yyyy-MM-dd hh:mm:ss",
      "UTC"
    );
    this.toDate = this.datepipe.transform(
      this.toDate,
      "yyyy-MM-dd hh:mm:ss",
      "UTC"
    );
    this.getSummery();
  }
  /********************** */
  // lineChart
  public lineChartData: Array<any> = [
    {
      data: this.data,
      label: this.service.isEmployeeLogin() ? this.translate.instant("income")
      : this.translate.instant("expense")
    }
  ];
  public lineChartLabels: Array<any> = ["January", "February", "March"];
  public lineChartOptions: any = {
    responsive: true,

    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            min: 0
          }
        }
      ]
    }
  };
  public lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)"
    },
    {
      // dark grey
      backgroundColor: "rgba(77,83,96,0.2)",
      borderColor: "rgba(77,83,96,1)",
      pointBackgroundColor: "rgba(77,83,96,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(77,83,96,1)"
    },
    {
      // grey
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)"
    }
  ];
  public lineChartLegend: boolean = false;
  public lineChartType: string = "line";

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
  /********************* */
  incomeDetails;
  getSummery() {
    // this.lineChartLabels=[]
    this.service
      .serverGetRequest(
        "",
        "/statistics/summary.json?registerType=" +
          this.service.getStorage("registerType") +
          "&&fromTime=" +
          this.fromDate +
          "&toTime=" +
          this.toDate
      )
      .subscribe(
        res => {
          if (res["status"].responseCode == 20) {
            this.service.summeryDetails = res["result"].summary;

            this.incomeDetails = res["result"].summary.incomeDetails
              ? res["result"].summary.incomeDetails
              : res["result"].summary.spendDetails;
            this.incomeDetails.forEach((element, index) => {
              console.log(this.data);
              let i;
              switch (+element.month) {
                case 1:
                  i = this.lineChartLabels.findIndex(
                    record => record === "January"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 2:
                  i = this.lineChartLabels.findIndex(
                    record => record === "February"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 3:
                  i = this.lineChartLabels.findIndex(
                    record => record === "March"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 4:
                  i = this.lineChartLabels.findIndex(
                    record => record === "April"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 5:
                  i = this.lineChartLabels.findIndex(
                    record => record === "May"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 6:
                  i = this.lineChartLabels.findIndex(
                    record => record === "June"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 7:
                  i = this.lineChartLabels.findIndex(
                    record => record === "July"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 8:
                  i = this.lineChartLabels.findIndex(
                    record => record === "August"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 9:
                  i = this.lineChartLabels.findIndex(
                    record => record === "September"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 10:
                  i = this.lineChartLabels.findIndex(
                    record => record === "October"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 11:
                  i = this.lineChartLabels.findIndex(
                    record => record === "November"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
                case 12:
                  i = this.lineChartLabels.findIndex(
                    record => record === "December"
                  );
                  this.data[i] = element.income
                    ? element.income
                    : element.spend;
                  break;
              }
            });

            // if( this.incomeDetails.length<3){

            // }
            this.lineChartData = [
              {
                data: this.data,
                label: this.service.isEmployeeLogin() ? "Income" : "Expense"
              }
            ];
          }
        },
        er => this.service.serviceErrorResponce(er)
      );
  }
}
