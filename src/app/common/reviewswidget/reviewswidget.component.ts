import { Component, OnInit } from '@angular/core';
import { ServerService } from 'src/app/service/server.service';

@Component({
  selector: 'app-reviewswidget',
  templateUrl: './reviewswidget.component.html',
  styleUrls: ['./reviewswidget.component.css']
})
export class ReviewswidgetComponent implements OnInit {

  constructor(public service :ServerService) { }

  ngOnInit() {
  }

}
