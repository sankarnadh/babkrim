import { Component, OnInit } from '@angular/core';
import {ChatService} from '../../../service/chat/chat-service.service'
import {ServerService} from '../../../service/server.service'
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-recent-mesage',
  templateUrl: './recent-mesage.component.html',
  styleUrls: ['./recent-mesage.component.css']
})
export class RecentMesageComponent implements OnInit {

  constructor(public chatService : ChatService,
    private datePipe: DatePipe,
              public service : ServerService) { }
              searchText
  ngOnInit() {
    // setInterval(()=>{
    //   this.chatService.chatRecentContact()
    // },30000)
  }
  checkTime(val)
  {
    var date = new Date();
    if(this.datePipe.transform(date, "dd-MM-yyyy")==val)
   return 1;
   return 0;
  }
}
