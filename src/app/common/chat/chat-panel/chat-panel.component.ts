import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../../service/chat/chat-service.service'
import { ServerService } from '../../../service/server.service'
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-chat-panel',
  templateUrl: './chat-panel.component.html',
  styleUrls: ['./chat-panel.component.css']
})
export class ChatPanelComponent implements OnInit {

  constructor(public chatService: ChatService,
    private datePipe: DatePipe,
    public service: ServerService) {
    chatService.messages.subscribe(msg => {
      if (msg.receiveFrom == this.chatService.receiverId 
        && msg.status == 20 && 
        msg.proposalId == this.chatService.proposalId) {


        this.chatService.failed = false
     
console.info("message "+msg.messageType =='file'?msg.fileName :msg.message)
        this.chatService.message.push(
          {
            receive: {
              message: msg.message,
              messageType: msg.messageType,
              receiveFrom: msg.receiveFrom,
              proposalId: msg.proposalId,
              createdTime: msg.createdTime,
              fileName:msg.fileName
            }
          }
        )
      }
      else
        if (msg.status == 40) {
          this.chatService.failed = true;
        }
    });
  }

  ngOnInit() {

  }
  checkTime(val) {
    var date = new Date();
    if (this.datePipe.transform(date, "dd-MM-yyyy") == val)
      return 1;
    return 0;
  }
  dateFormat(value) {

    var date = new Date();

    let serverDate = this.datePipe.transform(value, "dd-MM-yyyy")
    let today = this.datePipe.transform(date, "dd-MM-yyyy");
    if (serverDate == today)
      return this.datePipe.transform(value, "h:s:a")
    else
      return this.datePipe.transform(value, "dd-MM-yyyy h:s:a")
  }
  /********************************************************************** */
  throttle = 3;
  scrollDistance = 1;
  scrollUpDistance = 8;
  onScrollup() {
    // if (this.chatService.historyPageNo != 1 && !this.service.loader && this.chatService.message.length <= this.chatService.historyTotalCount) {
    //   this.chatService.getChatHistory(this.chatService.receiverId,this.chatService.proposalId);
    //   console.log('scrolled down!!', this.chatService.historyPageNo + " ");
    // }
  }
  /*********************************************************************** */
}
