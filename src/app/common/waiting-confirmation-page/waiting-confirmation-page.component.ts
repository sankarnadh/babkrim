import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServerService } from 'src/app/service/server.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-waiting-confirmation-page',
  templateUrl: './waiting-confirmation-page.component.html',
  styleUrls: ['./waiting-confirmation-page.component.css']
})
export class WaitingConfirmationPageComponent implements OnInit,OnDestroy {

  constructor(private service: ServerService,private router:Router) { }
  interval;
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // this.interval.clearin
    clearInterval(this.interval)
  }
  ngOnInit() {
    this.service.checkLogin()
    this.interval=setInterval(()=>{
      this.verificationCheck();
    },10000)
  }
  verificationCheck() {
    let response: any;
// customealert.loaderShow('html')
    this.service.serverGetRequest('', 'otp/varification/check.json?registerType=' + this.service.getStorage('registerType')).subscribe(
      (res: Response) => {
       

        response = res['status'];
        if (response.responseCode == 20) {

          this.service.setStorage('emailVerify', res['result']['emailVerify'])
          if (res['result']['emailVerify'] == 'true')
            this.router.navigate(['/home'])
        }
        else {
          this.service.apiErrorHandler(res);
          //if(response.responseCode==30)
          //window.location.reload()
        }

      }
    );
  }
}
