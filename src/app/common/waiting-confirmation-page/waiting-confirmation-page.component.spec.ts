import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingConfirmationPageComponent } from './waiting-confirmation-page.component';

describe('WaitingConfirmationPageComponent', () => {
  let component: WaitingConfirmationPageComponent;
  let fixture: ComponentFixture<WaitingConfirmationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitingConfirmationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingConfirmationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
