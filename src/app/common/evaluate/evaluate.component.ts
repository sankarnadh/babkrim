import { Component, OnInit, OnDestroy } from '@angular/core';
import { CdTimerComponent } from 'angular-cd-timer';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { ServerService } from 'src/app/service/server.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ValidationMessageService } from 'src/app/service/validation-message.service';
import '../../../assets/js/js/chat.js';

declare var customealert;
@Component({
  selector: 'app-evaluate',
  templateUrl: './evaluate.component.html',
  styleUrls: ['./evaluate.component.css'],
 
})
export class EvaluateComponent implements OnInit,OnDestroy {

  constructor(
 
    public timer: CdTimerComponent,
    public config: NgbRatingConfig,
    public service: ServerService,
    private spinner: Ng4LoadingSpinnerService,
    public validationMsg: ValidationMessageService
  ) { }
public showNext=this.service.showNext;
 public buttonClick=this.service.buttonClick; 
  employera1Event(event) {
    this.service.A1 = (event.target.value)
  }
  employera3Event(event) {
    this.service.A3 = (event.target.value)
  }
  employera2Event(event) {
    this.service.A2 = (event.target.value)
  }
  employera4Event(event) {
    this.service.A4 = (event.target.value)
  }
  employerA6Event(event) {
    this.service.A6 = (event.target.value)
  }
  employerA5Event(event) {
    this.service.A5 = (event.target.value)
  }
  
  ngOnInit() {
    this.invoiceError=false
    // this.addInvoiceList();
    this.service.loader = true
    this.config.max = 5;
   
  }

  rating: number;
  submitEvaluation() {
    var data
 
    if (!this.service.employercomment) {

      this.service.error = 1
      return false
    }
    this.service.error = 0
    if (this.service.isEmployeeLogin())
      data = {

        "answer1": this.service.A1 == 1 ? '1' : '0',
        "answer2": this.service.A2 == 1 ? '1' : '0',
        "answer3": this.service.A3 == 1 ? '1' : '0',
        "answer4": this.service.A4 == 1 ? '1' : '0',
        "answer5": this.service.A5 == 1 ? '1' : '0',
        "answer6": this.service.A6 == 1 ? '1' : '0',
        "answer7": this.service.A7,
        "contractId": this.service.getStorage('contractTempId'),
        "comment": this.service.employercomment,

        "rating": this.service.currentRate
      }
    if (this.service.isEmployerLogin())
      data = {

        "answer1": this.service.A1,
        "answer2": this.service.A2,
        "answer3": this.service.A3,
        "answer4": this.service.A4,
        "answer5": '-1',
        "answer6": '-1',
        "answer7": '-1',
        "contractId": this.service.getStorage('contractTempId'),
        "comment": this.service.employercomment,

        "rating": this.service.currentRate
      }

    customealert.loaderShow('html')
    this.service.serverRequest(data, 'evaluation/' + this.service.getStorage('contractTempId') + '.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.employercomment = ''
      
        this.showNext=0;
        this.service.recordList = [
          { 'myList': [false, false, false, false, false] },


        ];
        this.service.getCompletedContractsFromApi();
        this.service.getContractDetailsById(this.service.getStorage('contractTempId'))
        customealert.hideAllModal();
        customealert.hideModal('exampleModal')
        this.service.showSuccess(this.validationMsg.success)
      } else
        this.service.apiErrorHandler(res)
    },
      err => {
        customealert.hideModal('exampleModal')
        this.service.serviceErrorResponce(err)
      })
  }
  invoiceError = false

  ngOnDestroy(){
  this.service.resetData()
   
  }
  
 
  returnParseFloat(val) {
    return parseFloat(val)
  }
  
  setStarTable(record: any, data: any) {
    this.service.currentRate = data + 1;
    var tableList = this.service.recordList.find(function (obj: any) { return obj.Id === record.Id });
    for (var i = 0; i <= 4; i++) {
      if (i <= data) {
        tableList.myList[i] = false;
      }
      else {
        tableList.myList[i] = true;
      }
    }
  }
}
