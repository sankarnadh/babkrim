import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../service/server.service';
@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.css']
})
export class CookiesComponent implements OnInit {

  constructor(public service: ServerService) { }

  ngOnInit() {
  }

}
