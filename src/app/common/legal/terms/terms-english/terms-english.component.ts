import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../../service/server.service';

@Component({
  selector: 'app-terms-english',
  templateUrl: './terms-english.component.html',
  styleUrls: ['./terms-english.component.css']
})
export class TermsEnglishComponent implements OnInit {
     
  constructor(public service: ServerService) { 
    
  }

  ngOnInit() {
  }

}
