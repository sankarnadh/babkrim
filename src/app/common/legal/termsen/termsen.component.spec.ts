import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsenComponent } from './termsen.component';

describe('TermsenComponent', () => {
  let component: TermsenComponent;
  let fixture: ComponentFixture<TermsenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
