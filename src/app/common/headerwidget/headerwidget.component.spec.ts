import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderwidgetComponent } from './headerwidget.component';

describe('HeaderwidgetComponent', () => {
  let component: HeaderwidgetComponent;
  let fixture: ComponentFixture<HeaderwidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderwidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderwidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
