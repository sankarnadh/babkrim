import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ServerService } from 'src/app/service/server.service';
import { Router } from '@angular/router';
import { NotificationComponent } from '../notification/notification.component';
import '../../../assets/js/js/chat.js'
declare var customealert
@Component({
  selector: 'app-headerwidget',
  templateUrl: './headerwidget.component.html',
  styleUrls: ['./headerwidget.component.css']
})
export class HeaderwidgetComponent implements OnInit,OnDestroy {
  @ViewChild('app') appNotification :NotificationComponent

  constructor(public service:ServerService,private router:Router) { }
today=new Date();
params = {
  x:this.service.runningContract?this.service.runningContract.length:' no '
}
  ngOnInit() {
   
  }
  tempTaskStartTime=[]
 
  getRunningContract(){
   this.tempTaskStartTime=[]
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'notifications.json?registerType=' + this.service.getStorage('registerType') + '&pageNo=1').subscribe(res => {
      //this.loader=false;
      if (res['status'].responseCode == 20) {
        /**Create clock */
        customealert.loaderHide('html')
        let val = []
        val = res['result'].runningContract;
        
        if (val.length != 0)

          if (val[0].lastAction != 'default') {


              if (val[0].lastAction == 'RESUME' || val[0].lastAction == 'START') {
                let totalTime = val[0].totalTime.split(':');//new Date(totalTime)
                var seconds = +totalTime[2];
                var minutes = +totalTime[1]
                var hours = +totalTime[0]
                totalTime = (60 * 60 * hours) + (minutes * 60) + seconds
                this.tempTaskStartTime.push(totalTime) //Math.abs(totalTime.getTime());
                
 
              }

          }
          
        /***** */

       
     
       

      }
      else
        this.service.apiErrorHandler(res)
    },
      er => {

        // this.service.serviceErrorResponce(er)
      })
  }
 
  gotoTimeLine(){
    this.router.navigate(['/job-time-line/'+this.service.runningContract[0].contractId])
  }

  gotoMyProfile(){
    let userId=this.service.getStorage('userIdentifier')
    if (this.service.isEmployerLogin()) {
      this.service.setStorage('employerTempId', userId);
     this.router.navigate(['/view_company_details'])
     }
     else 
     {
       this.service.setStorage('employeeTempId', userId);
       this.router.navigate(['/hire_person_direct'])
     }
  }
  ngOnDestroy(){
    //  this.service.tempTaskStartTime=0
    }
}
