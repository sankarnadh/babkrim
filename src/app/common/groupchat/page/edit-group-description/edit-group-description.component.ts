import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../../service/server.service'
import  '../../../../../assets/js/js/chat.js'
import { ValidationMessageService } from 'src/app/service/validation-message.service';
declare var customealert
@Component({
  selector: 'app-edit-group-description',
  templateUrl: './edit-group-description.component.html',
  styleUrls: ['./edit-group-description.component.css']
})
export class EditGroupDescriptionComponent implements OnInit {

  constructor(public service: ServerService,
    public validationMsg : ValidationMessageService) { }
  groupDescription:any=this.service.groupDescription
  loading: boolean = false
  ngOnInit() {
  }
  updateGroupDescription() {
    let data = {
      "description": this.groupDescription,
      "groupId": this.service.groupGroupId,
      "groupName": this.service.groupName,
      "memberId": ""
    }
    this.loading=true
    this.service.serverPutRequest(data, 'chat/groups/'+this.service.groupGroupId+'.json').subscribe(res => {
      this.loading=true
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success);
        this.service.groupActiveTab = 3
      }
      else
        this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }

}
