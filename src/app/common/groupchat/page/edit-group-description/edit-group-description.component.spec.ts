import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupDescriptionComponent } from './edit-group-description.component';

describe('EditGroupDescriptionComponent', () => {
  let component: EditGroupDescriptionComponent;
  let fixture: ComponentFixture<EditGroupDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGroupDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGroupDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
