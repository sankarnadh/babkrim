import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../../service/server.service'
import  '../../../../../assets/js/js/chat.js'
import { ValidationMessageService } from 'src/app/service/validation-message.service';
declare var customealert
@Component({
  selector: 'app-groupinfo',
  templateUrl: './groupinfo.component.html',
  styleUrls: ['./groupinfo.component.css'],

})
export class GroupinfoComponent implements OnInit {

  constructor(public service: ServerService, public validationMsg : ValidationMessageService) { }

  ngOnInit() {
    this.service.groupActiveTab = 3
    this.getGroupInfo()
  }
  groupInfo: any = [];
  members = []
  request = []
  getGroupInfo() {
    this.service.serverGetRequest('', 'chat/groups/info/{groupId}.json?groupId=' + this.service.groupGroupId).subscribe(res => {
      if (res['status'].responseCode == 20) {
        {
          this.groupInfo = res['result'];
          this.members = this.groupInfo.members;

          this.members.push({
            'memberName':this.service.getStorage('userIdentifier')==this.groupInfo.adminId?'You':this.groupInfo.adminName,
            'memberId':this.service.getStorage('userIdentifier')==this.groupInfo.adminId?this.service.getStorage('userIdentifier'):this.groupInfo.adminId
          })
          this.request = this.groupInfo.newrequest
        }
      } else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))
  }
  memberId
  aproveRequest() {
    let data = {
      "description": "",
      "groupId": this.service.groupGroupId,
      "groupName": "",
      "memberId": this.memberId
    }
    this.service.serverRequest(data, 'chat/groups/approve.json').subscribe(res => {
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.getGroupInfo()
      }
      else
        this.service.apiErrorHandler(res)
    },
      er => this.service.serviceErrorResponce(er))
  }
  rejectRequest()
  {
    let data = {
      "description": "",
      "groupId": this.service.groupGroupId,
      "groupName": "",
      "memberId": this.memberId
    }
    this.service.serverRequest(data, 'chat/groups/reject.json').subscribe(res => {
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.getGroupInfo()
      }
      else
        this.service.apiErrorHandler(res)
    },
      er => this.service.serviceErrorResponce(er))
  }

  removeMember()
  {
    let data = {
      "description": "",
      "groupId": this.service.groupGroupId,
      "groupName": "",
      "memberId": this.memberId
    }
    this.service.serverRequest(data, 'chat/groups/remove.json').subscribe(res => {
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.getGroupInfo()
      }
      else
        this.service.apiErrorHandler(res)
    },
      er => this.service.serviceErrorResponce(er))
  }
  exitGroup() {
    this.service.serverRequest('', 'chat/groups/exit/{groupId}.json?groupId=' + this.service.groupGroupId).subscribe(res => {
      if (res['status'].responseCode == '20') {
        this.service.showSuccess(this.validationMsg.success)
        this.service.groupActiveTab = 1
      }
      else
        this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
}
