import { Component, OnInit, ViewChild } from '@angular/core';
import { GroupChatService } from '../../../../service/groupChat/group-chat.service'
import { WebsocketService } from '../../../../service/websocket.service'
import { ServerService } from '../../../../service/server.service'
import { DatePipe } from '@angular/common';
import '../../../../../assets/js/js/chat.js'
declare var customealert;
import { ValidationMessageService } from '../../../../service/validation-message.service'



@Component({
  selector: 'app-groupmessage',
  templateUrl: './groupmessage.component.html',
  styleUrls: ['./groupmessage.component.css'],
  providers: [WebsocketService, GroupChatService]
})
export class GroupmessageComponent implements OnInit {
  @ViewChild('scrollMe')
  scrollMe: any
  constructor(public groupService: GroupChatService,
    public ws: WebsocketService,
    private datePipe: DatePipe,
    public service: ServerService,
    public validationMsg: ValidationMessageService) {
    groupService.messages.subscribe(msg => {

      if (msg.grpCreatedBy != this.service.getStorage('userIdentifier') && msg.status == 20) {
        this.groupService.failed = false

        this.groupService.message.push(
          {
            receive: {
              message: msg.message,
              messageType: msg.messageType,
              receiveFrom: msg.receiveFrom,
              proposalId: msg.proposalId,
              createdTime: new Date(),
              profileImage: msg.profileImage,
              createdUserName: msg.createdUserName
            }
          }
        )
      }
      else
        if (msg.status == 40) {
          this.groupService.failed = true;
        }
    });
  }
  ngOnInit() {
   
    this.loadGroupChatHistoryFromApi();
  }
  pageNo = 1;
  loadMoreButton = 0;
  showLoadmore = 0;
  chatTotalCount = 0;
  loadGroupChatHistoryFromApi() {

    this.service.groupChatLoader = true;
    if (this.loadMoreButton == 0) {

      this.groupService.message = [];
      this.chatTotalCount = 0;
    }
    this.service.serverGetRequest('', 'chat/group/history.json?registerType=' + this.service.getStorage('registerType') + '&groupChatId=' + this.service.groupGroupId + '&page=' + this.pageNo).subscribe(res => {
      if (res['status'].responseCode == 20) {

        if (res['result'].chatHistory.length != 0)
          if (this.loadMoreButton == 1)
            this.groupService.message.reverse()
        res['result'].chatHistory.forEach(val => {
          this.chatTotalCount++;
          if (val.createdUserId == this.service.getStorage('userIdentifier')) {
            this.groupService.message.push(
              {
                send: {
                  message: val.message,
                  messageType: val.messageType,
                  receiveFrom: val.receiveFrom,
                  proposalId: val.proposalId,
                  createdTime: new Date(val.createdTime + 'GMT'),
                  profileImage: val.profileImage,
                  createdUserName: this.service.getStorage('userName')
                }
              }
            )
          }
          else {
            this.groupService.message.push(
              {
                receive: {
                  message: val.message,
                  messageType: val.messageType,
                  receiveFrom: val.receiveFrom,
                  proposalId: val.proposalId,
                  createdTime: new Date(val.createdTime + 'GMT'),
                  profileImage: val.profileImage,
                  createdUserName: val.createdUserName
                }
              }
            )
          }
        });

        this.service.groupChatLoader = false

        if (res['result'].chatHistory.length == 0 || this.chatTotalCount >= res['result'].totalCount)
          this.showLoadmore = 1
        else {
          this.loadMoreButton = 0
          // this.groupService.message.reverse()
        }
        if (res['result'].chatHistory.length > 0)
          this.groupService.message.reverse()
        console.log('reversed')
      }
      else
        this.service.apiErrorHandler(res)
    },

      err => {
        this.service.groupChatLoader = false
        this.service.serviceErrorResponce(err)
      })
  }
  checkTime(val) {
    var date = new Date();
    if (this.datePipe.transform(date, "dd-MM-yyyy") == val)
      return 1;
    return 0;
  }

  dateFormat(value) {

    var date = new Date();
    value = new Date(value + 'GMT')
    let serverDate = this.datePipe.transform(value, "dd-MM-yyyy")
    let today = this.datePipe.transform(date, "dd-MM-yyyy");
    if (serverDate == today)
      return this.datePipe.transform(value, "h:s:a")
    else
      return this.datePipe.transform(value, "dd-MM-yyyy h:s:a")
  }
  /********************************************************************** */
  throttle = 3;
  scrollDistance = 1;
  scrollUpDistance = 2;
  onScrollDown(ev) {
    console.log(this.scrollMe.scrollHeight)
    console.log(ev);
  }
  exitGroup() {
    this.service.serverRequest('', 'chat/groups/exit/{groupId}.json?groupId=' + this.service.groupGroupId).subscribe(res => {
      if (res['status'].responseCode == '20') {
        this.service.showSuccess(this.validationMsg.success)
        this.service.groupActiveTab = 1
      }
      else
        this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
}
