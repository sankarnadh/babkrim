import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageverificationComponent } from './messageverification.component';

describe('MessageverificationComponent', () => {
  let component: MessageverificationComponent;
  let fixture: ComponentFixture<MessageverificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageverificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageverificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
