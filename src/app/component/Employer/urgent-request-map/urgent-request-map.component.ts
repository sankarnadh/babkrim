import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { MapsAPILoader } from "@agm/core";
import { ServerService } from "../../../service/server.service";
import "../../../../assets/js/js/chat.js";
import { ValidationMessageService } from "../../../service/validation-message.service";
import { lang } from "moment";
import { AgmCoreModule, GoogleMapsAPIWrapper } from "@agm/core";
import { isArray } from "util";
import { NgbRatingConfig } from "@ng-bootstrap/ng-bootstrap";
import { NgProgress } from "ngx-progressbar";
import { TranslateService } from "@ngx-translate/core";

declare var customealert: any;
declare var google :any;
@Component({
  selector: "app-urgent-request-map",
  templateUrl: "./urgent-request-map.component.html",
  styleUrls: ["./urgent-request-map.component.css"],
  providers: [GoogleMapsAPIWrapper]
})
export class UrgentRequestMapComponent implements OnInit, OnDestroy {
  @ViewChild("hourlyRateInput")
  hourlyRateInput: any;
  @ViewChild("attachment")
  attachment: any;
  @ViewChild("cover")
  cover: any;
  urgentMapViewStatus: any = 0;
  constructor(
    public ngProgress: NgProgress,
    public config: NgbRatingConfig,
    private router: Router,
    public service: ServerService,
    private mapsAPILoader: MapsAPILoader,
    public validationMsg: ValidationMessageService,
    private translate:TranslateService
  ) {
    config.max = 5;
    config.readonly = true;
  }

  jobId;
  mapShow = 1;
  currentIcon = {
    url: this.service.urgentEmployerIcon,
    scaledSize: new google.maps.Size(60, 60)
  };
  currentIconEmployee = {
    url: this.service.currentIconEmployee,
    scaledSize: new google.maps.Size(60, 60)
  };
  /******************************** */
  title: any = "endjob";
  text: any = "cantbeundone";
  /******************************* */
  test(val) {

  }
  ngOnDestroy() {
    clearTimeout(this.markerTimeOut);
    clearInterval(this.interval);
    clearInterval(this.progressTimer)
    customealert.hideAllModal()
    this.flag = 1;
  }
  interval;
  testval = 0
  timeLimit: any = 600;
  timeLeft: any = '10:0';
  stopTimer = this.timeLimit;
  progressTimer: any = 0.16666666666;
  flag = 0
  publicListingStatus = 0;
  retryAttemptCount = 0;
  progresstimer;
  ngOnInit() {
    this.mapsAPILoader.load().then(() => {
      try {
        let v: any = new google.maps.Point(0, 0);

      } catch (e) {
        window.location.reload();
      }
    });

    if (!this.service.isEmployerLogin()) this.router.navigate(["/login"]);
    this.jobId = this.service.getStorage("tempJobId");
    this.fetchProposalFromApi(0);
    this.getDetailsByJobId();
    this.markers.push({
      lat: this.lat,
      lng: this.lng,
      draggable: false
    });

    this.interval = setInterval(() => {
      if (this.service.isEmployerLogin()) this.fetchProposalFromApi(0);

    }, 10000);

    this.progresstimer = setInterval(() => {
      const currentTime: any = new Date().getTime();
      const diff = (currentTime - new Date(this.retryTime).getTime())
      const sec = (this.timeLimit - (diff / 1000));
      if (sec > 0)
        this.timeLeft = (this.secondsToHms(Math.abs(sec)))
      else
        this.timeLeft = 0
      if (this.testval > 100) {
        this.testval = 100;
        if (this.flag == 0 && this.publicListingStatus == 0 && this.retryAttemptCount < 2)
          customealert.showModal('jobpostmodal')
        this.flag = 1;
      } else {
        // this.flag=0;
        this.testval = this.testval < 101 ? this.testval + this.progressTimer : 100;

      }
    }, 1000);
  }

  zoom: number = 10;
  lat: any = parseFloat(this.service.getStorage("lat"));
  lng: any = parseFloat(this.service.getStorage("lng"));
  markers: marker[] = [];
  returnLatLang(lat, lang) {

    // return {lat:parseFloat(lat),lng:parseFloat(lang)}
  }
  returnFloatValue(value) {
    return parseFloat(value);
  }
  viewWindow = 0;
  setValues() {

    if (this.proposalDetails.wageMode == "hourly") {
      // this.minHour=this.proposalDetails.
      this.hourlyRate = this.proposalDetails.hourlyWages;
    } else this.fixedRate = this.proposalDetails.fixedWage;
  }
  /******************** */
  applicationLength = 0;
  newApplication: any = [];
  mapViewIcon: any = [];
  prevLat = [];
  prevLng = [];
  i = 0;
  deltaLat = [];
  deltaLng = [];
  numDeltas = 1000;
  dealy = 50;
  tempLat;
  tempLng;
  markerTimeOut;
  transitionMap() {
    this.mapViewIcon.forEach((element, key) => {
      this.deltaLat[key] =
        (element.employeeDetails.lastSyncLatitude -
          element.employeeDetails.prevSyncLatitude) /
        this.numDeltas;
      this.deltaLng[key] =
        (element.employeeDetails.lastSyncLongitude -
          element.employeeDetails.prevSyncLongitude) /
        this.numDeltas;
    });
    this.moveMarker();
  }
  moveMarker() {
    this.mapViewIcon.forEach((element, key) => {
      element.employeeDetails.lastSyncLatitude = parseFloat(
        element.employeeDetails.lastSyncLatitude + this.deltaLat[key]
      );
      element.employeeDetails.lastSyncLongitude = parseFloat(
        element.employeeDetails.lastSyncLongitude + this.deltaLng[key]
      );
    });
    if (this.i != this.numDeltas) {
      if (this.i != this.numDeltas)
        this.markerTimeOut = setTimeout(() => {
          this.moveMarker();
        }, this.dealy);
      this.i++;
    }
  }

  fetchProposalFromApi(pageNo) {
    pageNo = +(pageNo + 1);
    this.service.loader = true;

    this.service
      .serverGetRequest(
        "",
        "employer/jobDetails/proposal/" + this.jobId + ".json?pageNo=" + pageNo
      )
      .subscribe(
        res => {
          if (res["status"].responseCode == 20) {
            if (this.viewWindow == 0) {
              this.i = 0;
              this.mapViewIcon = res["result"].proposalDetails;
              //  this.transitionMap()
            }
            if (this.applicationLength != res["result"].totalCount) {
              this.newApplication = [];
              this.applicationLength = res["result"].totalCount;
              res["result"].proposalDetails.forEach(element => {
                this.newApplication.push(element);
                // this.markers.push(element)
              });
            }

            this.service.loader = false; //Hide loader
          }

          // else
          //   this.service.apiErrorHandler(res)
        },
        err => {
          // this.service.serviceErrorResponce(err)
        }
      );
  }
  /************************ */
  endJobFlag = 0;
  endJob() {
    this.endJobFlag = 1;
    this.service
      .serverPutRequest("", "jobs/complete/" + this.jobId + ".json")
      .subscribe(
        res => {
          this.endJobFlag = 0;
          if (res["status"].responseCode == 20) {
            this.service.showSuccess(this.validationMsg.success);
            customealert.hideAllModal()
            this.router.navigate(["/my_jobs"]);
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);
          this.endJobFlag = 0;
        }
      );
  }
  skillFormat(skill) {
    let data = skill.split(",");
    if (isArray(data)) return data;
    else return [];
  }

  hourlyRate: any = 1;
  submitError = false;
  minHour: any = 1;
  fixedRate: any = 1;
  counter: number = 0;
  fileName: any = [];
  error: string = null;
  coverLetter: any = "";
  proposalAttachments = [];
  page = 0; // to approve proposal
  currency = this.service.defaultCurrency;
  loaderShow = false;
  proposalId;
  proposalDetails: any = [];
  tempDetails: any;

  approveProposal() {
    this.submitError = true;
    if (!this.hourlyRate) {
      return false;
    }
    if (this.proposalDetails.wageMode == 'hourly') {
      if (this.hourlyRate < this.standardWage) {
        return;
      }
    }
    else {
      if (this.fixedRate < this.fixedStandardWage) {
        return;
      }
    }
    if (!this.fixedRate) return;
    if (!this.minHour) return;
    if (this.minHour > 24) return;
    // if (this.proposalAttachments.length == 0) {
    //   this.error = "Attachment required"
    //   this.attachment.nativeElement.focus()
    //   return false
    // }
    this.coverLetter = this.cover.nativeElement.value;
    if (!this.coverLetter) {
      this.error = this.validationMsg.coverLetter;
      this.cover.nativeElement.focus();
      return false;
    }

    this.error = null;

    this.loaderShow = true;

    let data = {
      proposalId: this.proposalId,
      hourlyWages: this.hourlyRate,
      currency: this.currency,
      comment: this.coverLetter,
      contractAttachments: this.proposalAttachments,
      wageMode: this.proposalDetails.wageMode,
      fixedWage: this.fixedRate,
      minHour: this.minHour
    };
    this.service.serverRequest(data, "contracts.json").subscribe(res => {
      if (res["status"].responseCode == 20) {
        this.fetchProposalFromApi(0);
        this.hiredWorkerFromApi(0);
        customealert.hideModal("modal_approve_proposal");
        this.service.showSuccess(this.validationMsg.success);
        this.loaderShow = false;
      } else this.service.apiErrorHandler(res);
      this.loaderShow = false;
    });
  }

  fileLoader: any = false;
  fileEvent(event: any) {
    var counter = event.target.files.length;
    if (this.counter + counter < 6) {
      for (let i = 0; i < counter; i++) {
        const fileSelected: File = event.target.files[i];
        if (
          !this.service.checkAllowedFiles(
            event.target.files[i].name.split(".")[1]
          )
        ) {
          this.service.showError(this.validationMsg.invalidFileType);
          return false;
        }
        if (fileSelected.size < this.service.maxFileSize) {
          this.fileLoader = true;
          this.service.loader = true;
          let response;
          var reader = new FileReader();
          this.ngProgress.start();
          this.service
            .uploadSingleFile(fileSelected, "upload.json?fileType=docs")
            .subscribe(
              (res: Response) => {
                response = res;
                this.ngProgress.done();

                this.fileLoader = false;
                this.service.loader = false;

                if (response.status.responseCode == 20) {
                  this.proposalAttachments.push(response.result.file1);
                  this.fileName.push(fileSelected.name);
                  this.counter++;
                } else {
                  this.service.apiErrorHandler(res);
                }
              },
              err => {
                this.service.serviceErrorResponce(err);
                this.service.loader = false;
                this.fileLoader = false;
              }
            );
        } else this.service.showError(this.validationMsg.attachmentSizeLimit);
      }
    } else this.service.showError(this.validationMsg.attachmentLimit);
  }

  deleteFileicon(index) {
    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.proposalAttachments.splice(index, 1);
      // if (this.counter != 5) {
      this.counter--;
      if (this.counter < 0) this.counter = 0;
      // }
    }
  }
  /****************************** */
  hiredWorkers = [];
  hiredWorkersLoadmore = 0;
  hiredWorkersLength = 0;
  hiredWorkerFromApi(pageNo) {
    pageNo = +(pageNo + 1);
    this.service.loader = true;
    if (this.hiredWorkersLoadmore == 0 || pageNo == 0) this.hiredWorkers = [];
    this.service
      .serverGetRequest(
        "",
        "employer/jobDetails/hired/" + this.jobId + ".json?pageNo=" + pageNo
      )
      .subscribe(
        res => {
          if (res["status"].responseCode == 20) {
            res["result"].hiredDetails.forEach(element => {
              this.hiredWorkers.push(element);
            });
            this.hiredWorkersLength = res["result"].hiredDetails.length;
            this.service.loader = false; //Hide loader
          } else this.service.apiErrorHandler(res);
        },
        err => {
          this.service.serviceErrorResponce(err);
        }
      );
  }
  proposal;
  fixedStandardWage
  standardWage
  retryTime: any;
  jobDetails: any;
  getDetailsByJobId() {
    this.service.serverGetRequest('', "jobDetails/" + this.jobId + ".json?pageNo=1").subscribe(res => {
      if (res['status'].responseCode == 20) {
        let result = res["result"];
        this.jobDetails = result;
        if (result.jobPostType != 'urgent') {
          this.fixedStandardWage = result.subCategoryDetails.fixedStandardWage
            ? result.subCategoryDetails.fixedStandardWage
            : 0;
          this.standardWage = result.subCategoryDetails.standardWage
            ? result.subCategoryDetails.standardWage
            : 0;
        } else {
          this.fixedStandardWage = result.subCategoryDetails.urgentFixedStandardWage
            ? result.subCategoryDetails.urgentFixedStandardWage
            : 0;
          this.standardWage = result.subCategoryDetails.urgentStandardWage
            ? result.subCategoryDetails.urgentStandardWage
            : 0;
        }
        /** */
        try {
          this.publicListingStatus = result.publicListingStatus;
          this.retryAttemptCount = result.retryAttemptCount;
          this.retryTime = this.service.convertDateToGmt(res['result'].retryTime);
          const currentTime: any = new Date().getTime();
          const diff = (currentTime - new Date(this.retryTime).getTime())
          const sec: any = (this.timeLimit - (diff / 1000));
          this.stopTimer = this.stopTimer - parseInt(sec);

          if (sec > 0) {
            this.timeLeft = (this.secondsToHms(sec));
            this.testval = 100 - (sec * this.progressTimer);
          }
          else {
            this.timeLeft = 0;
            this.testval = 100;
          }
        } catch (error) {

        }

      }
      else
        this.service.apiErrorHandler(res)
    },
      err => {
        this.service.serviceErrorResponce(err)
      })
  }
  retry() {
    customealert.loaderShow('html')
    this.service.serverPutRequest('', 'jobs/' + this.jobId + '/retry.json').subscribe(
      (res) => {
        customealert.loaderHide('html')

        if (res['status'].responseCode == 20) {
          this.flag = 0;
          customealert.hideAllModal()
          this.service.showSuccess(this.translate.instant('searchingemployee'))
          this.getDetailsByJobId();
        } else {
          this.service.apiErrorHandler(res);
        }
      },
      (er) => {
        this.service.serviceErrorResponce(er);
      }
    );
  }
  makeaspublic() {
    customealert.loaderShow('html')
    this.service.serverPutRequest('', 'jobs/' + this.jobId + '/listing.json').subscribe(
      (res) => {
        customealert.loaderHide('html')

        if (res['status'].responseCode == 20) {
          this.service.showSuccess(this.validationMsg.success);
          customealert.hideAllModal()
          this.router.navigate(["/my_jobs"]);
        } else {
          this.service.apiErrorHandler(res);
        }
      },
      (er) => {
        this.service.serviceErrorResponce(er);
      }
    );
  }
  showModal() {
    customealert.showModal('jobpostmodal')
  }
  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? ":" : ":") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? ":" : ":") : "0";
    var sDisplay = s > 0 ? s + (s == 1 ? "" : "") : "0";
    return hDisplay + mDisplay + sDisplay;
  }
  /**************************** */
}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable?: boolean;
}
