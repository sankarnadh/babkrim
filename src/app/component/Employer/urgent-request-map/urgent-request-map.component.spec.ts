import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrgentRequestMapComponent } from './urgent-request-map.component';

describe('UrgentRequestMapComponent', () => {
  let component: UrgentRequestMapComponent;
  let fixture: ComponentFixture<UrgentRequestMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrgentRequestMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrgentRequestMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
