import {
  Component,
  OnInit,
  ElementRef,
  Input,
  SystemJsNgModuleLoader
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Response } from "@angular/http";
import { NgProgress } from "ngx-progressbar";
import { FormBuilder, Validators } from "@angular/forms";
import { ValidationServiceService } from "../../../service/validation-service.service";
import { ServerService } from "../../../service/server.service";
import { ValidationMessageService } from "../../../service/validation-message.service";
import { Router } from "@angular/router";
import "../../../../assets/js/js/chat.js";
declare var customealert: any;
@Component({
  selector: "app-company-details",
  templateUrl: "./company-details.component.html",
  styleUrls: ["./company-details.component.css"]
})
export class CompanyDetailsComponent implements OnInit {
  signupForm: any;
  es_name: string = "";
  es_state: string = "";
  es_country: string = "";
  es_city: string = "";
  es_website: string = "";
  es_email: string = "";
  es_phone: string = "";
  es_mobile: string = "";
  es_zipcode: string = "";
  es_po: string = "";
  ma_fname: string = "";
  ma_mname: string = "";
  ma_lname: string = "";
  ma_position: string = "";
  ma_mobile: string = "";
  ma_phone: string = "";
  ma_email: string = "";
  ex_name: string = "";
  ex_mobile: string = "";
  ex_phone: string = "";
  ex_email: string = "";
  file: any;

  buttonClick = 0;
  cities: any;
  state: any;

  country = [];

  step1btn: boolean;
  step1btnValid: any;
  step2btnValid: any;
  //Select box loader
  initalLoader: any;
  stateLoader: any;
  cityLoader: any;

  constructor(
    public ngProgress: NgProgress,
    private http: HttpClient,
    private el: ElementRef,
    private router: Router,
    private formBuilder: FormBuilder,
    public service: ServerService,
    public validationMsg: ValidationMessageService
  ) {
    this.signupForm = this.formBuilder.group({
      es_name: [
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(50),
          ValidationServiceService.nameValidator
        ])
      ],
      esaddress1: ["", Validators.required],
      esaddress2: ["", Validators.required],
      es_state: ["", ValidationServiceService.notNullValidation],
      es_country: ["", ValidationServiceService.notNullValidation],
      es_city: ["", ValidationServiceService.notNullValidation],
      es_website: [
        "",
        Validators.compose([Validators.required, Validators.maxLength(50)])
      ],
      es_email: ["", ValidationServiceService.emailValidator],
      es_phone: ["", ValidationServiceService.mobileValidation],
      es_mobile: ["", ValidationServiceService.mobileValidation],
      es_zipcode: ["", Validators.required],
      es_po: ["", Validators.required],
      ma_fname: ["", Validators.required],
      ma_mname: ["", Validators.required],
      ma_lname: ["", Validators.required],
      ma_position: ["", Validators.required],
      ma_mobile: ["", ValidationServiceService.mobileValidation],
      ma_phone: ["", ValidationServiceService.mobileValidation],
      ma_email: ["", ValidationServiceService.emailValidator],
      ex_name: ["", Validators.required],
      ex_mobile: ["", ValidationServiceService.mobileValidation],
      ex_phone: ["", ValidationServiceService.mobileValidation],
      ex_email: ["", ValidationServiceService.emailValidator],
      file: ["", Validators.required],
      mobile_otp: ["", ValidationServiceService.numberValidation],
      terms: ["", Validators.required],
      map: ["", Validators.required],
      user_photo: ["", Validators.required],
      aboutme: ["", Validators.minLength(20)]
    });
  }

  countrySettings: any;
  stateSettings: any;
  citySettings: any;
  otp_valid: any = false;
  employerType: any = this.service.getStorage("employerType"); //?this.service.getStorage('employerType'):'company'
  showEmployerType: any = this.service.getStorage("registerLevel")
    ? this.service.getStorage("registerLevel")
    : 2;
  ngOnInit() {
    this.service.checkLogin(); //To change the login header
    this.service.loader = false;
    this.service.checkMobileVerification();
    this.smsOtpMsg = this.service.mobile_otp_msg;
    if (this.service.getStorage("mobileVerify") == "true")
      this.otp_valid = true;
    if (!this.service.isEmployerLogin()) this.router.navigate(["/login"]);

    this.prf_pic = "assets/images/pro_company.png"; // Profile Image
    this.initalLoader = "loadinginput";
    this.step1btnValid = "not-allowed";
    this.step2btnValid = "not-allowed";
    // this.service.verificationCheck();                         //Email verification check
    this.previousLevel2(); //Previous details submited If any
    this.service.getLevel2(); //Populate Country from Api
    this.step1btn = false;
    this.getSateFromApi(this.prcountry);
  }
  //Previously entered details
  prstate: any;

  prcity: any;
  prcountry: any = 352;

  countryId: any = 352;
  cityId: any;
  stateId: any;

  /**
   * Clearing country Id when user unselect country
   */
  onCountryItemDeselect() {
    //Unsetting State and city
    this.countryId = null;
    this.prcity = [];
    this.prstate = [];
    this.state = [];
    this.cities = [];
    this.cityId = null;
    this.stateId = null;
  }
  /**
   * When country is selected
   */
  onCountryItemSelect(item: any) {
    this.countryId = item.id;
    this.prstate = [];
    this.prcity = [];
    this.getSateFromApi(item.id);
  }

  //State Selected
  onStateItemSelect(item: any) {
    this.stateId = item.id;
    this.prcity = [];
    this.getCityFromApi(item.id);
  }
  //State deselect
  onStateItemDeselect() {
    this.stateId = null;
    this.cityId = null;
    this.cities = [];
    this.prcity = [];
  }
  //City select
  onCityItemSelect(item: any) {
    this.cityId = item.id;
  }
  //City Deselect
  onCityItemDeselect() {
    this.cityId = "";
    this.prcity = [];
  }

  //Fetching state from server
  getCityFromApi(id) {
    this.cities = [];
    this.cityLoader = "loadinginput";
    this.service
      .serverGetRequest("", "static/cities.json?state=" + id)
      .subscribe(
        res => {
          this.cityLoader = "";
          if (res["status"].responseCode == 20) {
            this.cities = res["result"].cities;
            this.cities.push({ id: 0, city: "Other" });
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {}
      );
  }
  //Fetching State from api
  getSateFromApi(id) {
    this.stateLoader = "loadinginput";
    this.state = [];
    this.cities = [];

    this.service
      .serverGetRequest("", "static/states.json?country=" + id)
      .subscribe(
        res => {
          this.stateLoader = "";
          if (res["status"].responseCode == 20)
            this.state = res["result"].states;
          else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {}
      );
  }

  /****
   * Resending Otp
   */
  smsOtpMsg: any;
  sendSms() {
    this.smsOtpMsg = "";
    customealert.loaderShow("html");
    let response: any;
    this.service
      .serverRequest(
        "",
        "/otp/sms/send.json?registerType=" +
          this.service.getStorage("registerType")
      )
      .subscribe(
        (res: Response) => {
          customealert.loaderHide("html");
          response = res["status"];
          if (response.responseCode == 20) {
            this.smsOtpMsg = res["result"].message;
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);

          customealert.loaderHide("html");
        }
      );
  }
  /******
   * Verify Sms Otp
   */
  verifyOtp() {
    this.smsOtpMsg = "";
    let otp = this.signupForm.get("mobile_otp").value;
    if (otp) {
      customealert.loaderShow("html");
      let response: any;

      this.service
        .serverRequest(
          "",
          "/otp/sms/verify.json?registerType=" +
            this.service.getStorage("registerType") +
            "&OTP=" +
            otp
        )
        .subscribe(
          (res: Response) => {
            customealert.loaderHide("html");
            response = res["status"];
            if (response.responseCode == 20) {
              this.smsOtpMsg = this.validationMsg.otpVerified;
              if (res["result"].message) {
                this.otp_valid = true;
                this.service.setStorage("mobileVerify", true);
              } else this.smsOtpMsg = this.validationMsg.invalidOtp;
            } else {
              this.service.apiErrorHandler(res);
            }
          },
          err => {
            this.service.serviceErrorResponce(err);

            customealert.loaderHide("html");
          }
        );
    } else this.smsOtpMsg = this.validationMsg.invalidOtp;
  }
  //Step 1 => Personal Information submit
  personalInformationSubmit(val) {
    /**
     * Setting default values if employer type is individual
     */
    if (this.employerType == "individual") {
      //  this.signupForm.get('es_name').setValue('BABKRIM-DEFAULT-COMPANY-NAME')
      this.signupForm
        .get("es_email")
        .setValue(this.service.getStorage("email"));
    }
    this.service.setStorage("employerType", this.employerType);
    if (!this.signupForm.get("es_name").valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    if (!this.address1) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    if (!this.profileImage) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    if (this.aboutmeModal == "") {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    // if (this.countryId == '') {
    //   this.buttonClick = 1
    //   return false;
    // }
    // else
    //   this.buttonClick = 0
    if (!this.stateId) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    if (!this.cityId && this.cityId != 0) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;
    // if (!this.signupForm.get('es_website').valid) {
    //   this.buttonClick = 1
    //   return false;
    // }
    // else
    //   this.buttonClick = 0
    if (!this.signupForm.get("es_email").valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;
    if (!this.signupForm.get("es_phone").valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;
    // if (!this.signupForm.get('es_mobile').valid) {
    //   this.buttonClick = 1
    //   return false;
    // }
    // else
    //   this.buttonClick = 0
    // if (!this.signupForm.get('es_zipcode').valid) {
    //   this.buttonClick = 1
    //   return false;
    // }
    // else
    //   this.buttonClick = 0
    if (!this.signupForm.get("es_po").valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;
    if (!this.otp_valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    customealert.loaderShow("html");

    if (!this.profileImage) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;
    let data = {
      city: this.cityId,
      companyEmail: this.signupForm.get("es_email").value
        ? this.signupForm.get("es_email").value
        : "",
      companyName: this.signupForm.get("es_name").value
        ? this.signupForm.get("es_name").value
        : "",
      companyMobile: this.signupForm.get("es_mobile").value
        ? this.signupForm.get("es_mobile").value
        : "",
      companyPOBox: this.signupForm.get("es_po").value
        ? this.signupForm.get("es_po").value
        : "",
      companyPhoneNo: this.signupForm.get("es_phone").value
        ? this.signupForm.get("es_phone").value
        : "",
      companyWebSite: this.signupForm.get("es_website").value
        ? this.signupForm.get("es_website").value
        : "",
      companyZip: this.signupForm.get("es_zipcode").value
        ? this.signupForm.get("es_zipcode").value
        : "",
      state: this.stateId,
      country: 352, //this.countryId,
      registryLicenceFile: this.registryLicenceFile,
      addressLine1: this.address1,
      addressLine2: this.address2 ? this.address2 : "",
      profileImage: this.profileImage,
      profileDescription: this.aboutmeModal,
      employerType: this.employerType //this.service.getStorage('employerType')
    };
    let response: any;

    this.service
      .serverRequest(
        data,
        "register.json?registerType=" +
          this.service.getStorage("registerType") +
          "&registerLevel=2&deviceMode=web"
      )
      .subscribe(
        (res: Response) => {
          response = res["status"];
          customealert.loaderHide("html");
          if (response.responseCode == 20) {
            if (val == "1") {
              this.service.showSuccess(this.validationMsg.success);
              // this.router.navigate(['/employer_home']);
            } else {
              if (this.service.getStorage("registerLevel") != "0")
                this.service.setStorage("registerLevel", "3");
              this.router.navigate(["/employer_managerdetails"]);
            }
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);

          customealert.loaderHide("html");
        }
      );
  }
  /****
   * Upload license
   */
  fileLoader: any;
  fileCounter = 0;
  registryLicenceFile = [];
  uploadLicense(event: any) {
    var count = event.target.files.length;
    console.log(this.fileCounter + " " + count);
    if (this.fileCounter + count < 6) {
      for (let i = 0; i < count; i++) {
        const fileSelected: File = event.target.files[i];
        if (
          !this.service.checkAllowedFiles(
            event.target.files[i].name.split(".")[1]
          )
        ) {
          this.service.showError(this.validationMsg.invalidFileType);
          return false;
        }
        if (fileSelected.size < this.service.maxFileSize) {
          if (fileSelected) {
            this.fileLoader = "loadinginput";
          }
          this.imgfileLoader = true;
          let response;
          this.ngProgress.start();

          this.service
            .uploadFile(fileSelected, "upload.json?fileType=docs")
            .subscribe(
              (res: Response) => {
                this.imgfileLoader = false;
                this.ngProgress.done();

                response = res;
                this.fileLoader = "";
                this.service.loader = false;

                if (response.status.responseCode == 20) {
                  this.dwnld_file = true;
                  this.fileCounter++;
                  this.registryLicenceFile.push(response.result.file1);
                  this.fileName.push(fileSelected.name);
                } else {
                  this.service.apiErrorHandler(res);
                }
              },
              error => {
                this.service.loader = false;
                this.imgfileLoader = false;

                this.service.serviceErrorResponce(error);
              }
            );
        } else this.service.showError(this.validationMsg.attachmentSizeLimit);
      }
    } else this.service.showError(this.validationMsg.attachmentLimit);
  }
  changeEmployerType() {
    if (this.employerType == "company") {
      this.prcompanyname = null;
    } else this.prcompanyname = this.service.getStorage("userName");
  }
  prcompanyname: any =
    this.employerType == "individual"
      ? this.service.getStorage("userName")
      : null;
  prwebsite: any;
  prcompanyEmail: any = this.service.getStorage("email");
  prPhone: any;
  prMobile: any = this.service.getStorage("mobileNo");
  prZip: any;
  prPo: any;
  fileName = [];
  fileType: any;
  address1;
  address2;
  /*****
   * Previous data uploaded
   */
  previousLevel2() {
    let response;
    customealert.loaderShow("html");
    this.service
      .serverGetRequest(
        "",
        "profile/profileDetails.json?registerType=" +
          this.service.getStorage("registerType") +
          "&registerLevel=2"
      )
      .subscribe(
        (res: Response) => {
          response = res;
          customealert.loaderHide("html");

          if (response.status.responseCode == "20") {
            if (response.result) {
              if (response.result.registryLicenceFile[0].seedName) {
                // this.fileID=response.result.registryLicenceFile.fileId;
                this.dwnld_file = true;
                let counter = response.result.registryLicenceFile.length;
                this.fileCounter = counter;
                for (let i = 0; i < counter; i++) {
                  this.fileName.push(
                    response.result.registryLicenceFile[i].seedName
                  );
                  this.registryLicenceFile.push(
                    response.result.registryLicenceFile[i].fileId
                  );
                }

                // let ext =response.result.registryLicenceFile.seedName.split('.');
                // this.fileType=ext[1];
              }

              this.aboutmeModal = response.result.profileDesc;
              if (response.result.profileImage) {
                this.prf_pic =
                  this.service.url +
                  "/download/profile.json?fileID=" +
                  response.result.profileImage +
                  "&token=" +
                  this.service.getStorage("token");
                this.profileImage = response.result.profileImage;
                this.service.setStorage("prfPicId", this.prf_pic);
              }
              this.prcompanyname =
                this.employerType == "individual"
                  ? this.service.getStorage("userName")
                  : response.result.companyName;
              // this.employerType=response.result.employerType?response.result.employerType:'company'
              this.prcompanyEmail =
                response.result.companyEmail == this.service.getStorage("email")
                  ? this.service.getStorage("email")
                  : response.result.companyEmail;
              this.prwebsite = response.result.companyWebSite;
              this.prPhone = response.result.companyPhoneNo;
              this.prMobile = response.result.companyMobile;
              this.prZip = response.result.companyZip;
              this.prPo = response.result.companyPOBox;
              this.address1 = response.result.addressLine1;
              this.address2 = response.result.addressLine2;

              this.prcountry = +response.result.country.id; //Selected Country
              this.getSateFromApi(response.result.country.id); //Fetching last entered state
              this.prstate = +response.result.state.id; //Last entered State
              this.prcity = +response.result.city.id;
              this.getCityFromApi(response.result.state.id);

              //this.picId=response.result.profile.fileId;

              this.countryId = +response.result.country.id;
              this.stateId = +response.result.state.id;
              this.cityId = +response.result.city.id;
            } else {
              // this.prcompanyname=null;
              // this.prcompanyEmail=null;
              // this.prwebsite=null;
              // this.prPhone=null;
              // this.prMobile=null;
              // this.prZip=null;
              // this.prPo=null;
              this.prcountry = []; //Selected Country
              this.prstate = []; //Last entered State
              this.prcity = [];
              this.countryId = null;
              this.stateId = null;
              this.cityId = null;
            }
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);
        }
      );
  }
  fileID: any;
  picId: any;
  dwnld_pic: boolean = false;
  dwnld_file: boolean = false;
  deleteFileicon(index) {
    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.registryLicenceFile.splice(index, 1);
      this.fileCounter--;
      if (this.fileCounter < 0) this.fileCounter = 0;
    }
  }
  downloadFile(type) {
    let fileid;
    if (type == "docs") fileid = this.fileID;
    else fileid = this.picId;
    customealert.loaderShow("html");
    this.service
      .serverGetRequest(
        "",
        "download.json?fileID=" + fileid + "&fileType=" + type
      )
      .subscribe(
        res => {
          customealert.loaderHide("html");
          if (res["status"].responseCode == 20) {
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          customealert.loaderHide("html");
          //this.initialLoader=''
         this.service.serviceErrorResponce(err)
        }
      );
  }
  profileImage;
  imgfileLoader = false;

  fileEvent(event: any) {
    let response;
    var reader = new FileReader();

    let fileType = event.target.files[0].name.split(".")[1];
    if (
      fileType != "jpeg" &&
      fileType != "jpg" &&
      fileType != "JPEG" &&
      fileType != "png" &&
      fileType != "JPG" &&
      fileType != "PNG"
    ) {
      this.service.showError(this.validationMsg.invalidFileType);
      return false;
    }
    const fileSelected: File = event.target.files[0];
    if (fileSelected.size < this.service.maxFileSize && fileSelected) {
      this.imgfileLoader = true;
      this.ngProgress.start();
      this.service
        .uploadSingleFile(fileSelected, "upload.json?fileType=profile")
        .subscribe(
          (res: Response) => {
            response = res;
            this.ngProgress.done();

            this.imgfileLoader = false;

            if (response.status.responseCode == 20) {
              this.profileImage = response.result.file1;
              let imgPath =
                this.service.url +
                "/download/profile.json?fileID=" +
                this.profileImage +
                "&token=" +
                this.service.getStorage("token");
              this.service.setStorage("prfPicId", imgPath);

              this.service.prfImg = imgPath;
            } else {
              this.service.apiErrorHandler(res);
            }
          },
          err => {
            this.imgfileLoader = false;

            this.service.serviceErrorResponce(err);
          }
        );
    } else this.service.showError(this.validationMsg.attachmentSizeLimit);
  }

  prf_pic;
  aboutmeModal = "";
}
