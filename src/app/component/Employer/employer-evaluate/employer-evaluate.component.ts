import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ServerService} from '../../../service/server.service';
@Component({
  selector: 'app-employer-evaluate',
  templateUrl: './employer-evaluate.component.html',
  styleUrls: ['./employer-evaluate.component.css']
})
export class EmployerEvaluateComponent implements OnInit {

  constructor(private router : Router,private service :ServerService) { }

  ngOnInit() {
    if(!this.service.isEmployerLogin())
    this.router.navigate(['/login']);
  }

}
