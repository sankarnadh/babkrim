import {
  Component,
  OnInit,
  ElementRef,
  NgZone,
  ViewChild,
  OnChanges
} from "@angular/core";
import { MapsAPILoader } from "@agm/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ValidationServiceService } from "../../../service/validation-service.service";
import { ServerService } from "../../../service/server.service";
import { ValidationMessageService } from "../../../service/validation-message.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbTimepickerConfig } from "@ng-bootstrap/ng-bootstrap";
import { NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";
import { DatePipe } from "@angular/common";
import { DateTimeAdapter } from "ng-pick-datetime";
import { NgProgress } from "ngx-progressbar";
import swal, { SweetAlertOptions } from "sweetalert2";

import "../../../../assets/js/js/chat.js";
// import { DatepickerOptions } from 'ng2-datepicker';
// import * as frLocale from 'date-fns/locale/fr';
declare var customealert: any;
declare var $;
declare var jobForm;

// import {} from "googlemaps";
import { transition } from "@angular/animations";
import { SwalComponent } from "@toverux/ngx-sweetalert2";
import { TranslateService } from "@ngx-translate/core";

declare var plusicon;
declare var google: any;
@Component({
  selector: "app-urgent-request",
  templateUrl: "./urgent-request.component.html",
  styleUrls: ["./urgent-request.component.css"],
  providers: [NgbTimepickerConfig]
})
export class UrgentRequestComponent implements OnInit {
  @ViewChild("search")
  public searchElementRef: ElementRef;
  @ViewChild("addAddress") addAddress: SwalComponent;
  jobForm;
  initialLoader = "loadinginput";
  gender: any = [
    { id: 1, gender: "Male" },
    { id: 2, gender: "Female" },
    { id: 3, gender: "All" }
  ];
  genderId = 3;
  nationalityId = null;
  stateId = null;
  generalSpecId = null;
  specId = null;
  currencyId = null;
  category;
  currency;
  checked: any = 1;
  toTimeModel: any;
  fromTimeModel: any;
  currencyModel = 98;
  nationalityModel = null;
  stateModel = 33;
  noOfVacancyModal = 1;
  jobTitle: any;
  noOfDay: any;
  fromDate: any;
  jobMode = 1;
  togleButton = 0;
  subCatSettings ;
  constructor(
    public ngProgress: NgProgress,
    public dateTimeAdapter: DateTimeAdapter<any>,
    public config: NgbTimepickerConfig,
    private router: Router,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    public service: ServerService,
    public validationMsg: ValidationMessageService,
    public activatedRouter: ActivatedRoute,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public translate: TranslateService
  ) {
    translate.get("income", {  }).subscribe((res: string) => {
      console.info(this.translate.instant('level1EmailExists'));
     
    });
    this.dateTimeAdapter.setLocale("en-in");
    config.seconds = true;
    config.spinners = false;

    this.jobForm = this.formBuilder.group({
      fromDate: ["", Validators.compose([Validators.required])],
      noOfDay: ["", Validators.required],
      fromTime: ["", Validators.required],
      toTime: ["", Validators.required],
      gender: ["", Validators.compose([Validators.required])],
      nationality: ["", Validators.required],
      state: ["", Validators.required],
      noOfHour: ["", Validators.required],
      currency: ["", Validators.required],
      maxexperience: [
        "",
        Validators.compose([
          Validators.required,
          ValidationServiceService.numberValidation
        ])
      ],
      minexperience: [
        "",
        Validators.compose([
          Validators.required,
          ValidationServiceService.numberValidation
        ])
      ],
      category: ["", Validators.required],
      subCategory: ["", Validators.required],
      hourlyWages: [
        "",
        Validators.compose([
          Validators.required,
          ValidationServiceService.numberValidation
        ])
      ],
      maxAge: ["", Validators.required],
      minAge: ["", Validators.required],
      minWeight: [
        "",
        Validators.compose([
          Validators.required,
          ValidationServiceService.numberValidation
        ])
      ],
      maxWeight: [
        "",
        Validators.compose([
          Validators.required,
          ValidationServiceService.numberValidation
        ])
      ],
      maxHeight: [
        "",
        Validators.compose([
          Validators.required,
          ValidationServiceService.numberValidation
        ])
      ],
      minHeight: [
        "",
        Validators.compose([
          Validators.required,
          ValidationServiceService.numberValidation
        ])
      ],
      acceptDisability: ["", Validators.required],
      jobTitle: ["", Validators.minLength(6)],
      jobDescription: ["", Validators.minLength(10)],
      no_vacancies: ["", Validators.required],
      business_location_name: ["", Validators.required],
      file: ["", Validators.required],
      educations: ["", Validators.required],
      skill: ["", Validators.required],
      noOfVacancy: ["", Validators.required],
      jobType: ["", Validators.required],
      wageMode: ["", Validators.required],
      minhour: ["", Validators.required],
      fixedWages: ["", Validators.required]
    });
  }
  plusicon = 0;
  vote() {
    plusicon = 1;
    //Cut for brevity
  }

  jobDescriptionModel: any = "";
  fromtimeTime;
  jobType: any;
  totimeTime = "am";
  fromtimeTimeHandler(event) {
    this.fromtimeTime = event.target.value;
  }
  totimeTimeHandler(event) {
    this.totimeTime = event.target.value;
  }
  fromTimeModelChange(event) {
    if (this.fromTimeModel.hour < 11) {
      this.fromtimeTime = "am";
    } else this.fromtimeTime = "pm";
  }
  toTimeModelChange(event) {
    if (this.toTimeModel.hour < 11) {
      this.totimeTime = "am";
    } else this.totimeTime = "pm";
  }
  today = new Date();
  get j() {
    return this.jobForm.controls;
  }
  s=this.translate.instant("selectall")
  ngOnInit() {
    // this.getGeoLocation(this.lat, this.lng)
    this.getAddress();
this.subCatSettings = {
    singleSelection: true,
    idField: "id",
    textField: "subCategory",
    searchPlaceholderText: this.s,
    selectAllText: this.translate.instant("selectall"),
    unSelectAllText: this.translate.instant("unselectall"),
    noDataAvailablePlaceholderText: this.translate.instant("sorrynorecordsfound"
    ),
    itemsShowLimit: 2,
    allowSearchFilter: true,
    closeDropDownOnSelection: true
  };
    this.jobForm.get("wageMode").setValue("hourly");
    this.getStateFromApi();
    this.mapsAPILoader.load().then(() => {
      let autocomplete: any;
      try {
        autocomplete = new google.maps.places.Autocomplete(
          this.searchElementRef.nativeElement
        );
      } catch (error) {
        window.location.reload();
      }
      // console.log('2')

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place:google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.business_location_name = place.formatted_address;
          this.jobForm
            .get("business_location_name")
            .setValue(place.formatted_address);
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.getGeoLocationForAddAddress(this.lat, this.lng);
          this.markers = [
            {
              lat: this.lat,
              lng: this.lng,
              draggable: true
            }
          ];
        });
      });
    });
    this.today.setDate(this.today.getDate() - 1);
    this.jobForm.get("noOfHour").value = 8;
    this.markers = [
      {
        lat: this.lat,
        lng: this.lng,
        draggable: true
      }
    ];
    if (!this.service.isEmployerLogin()) this.router.navigate(["/login"]);
    //this.getCommon();                   //Fetch comon details
    this.getCategory();
    this.getCurrency();
    this.getEducation(); //Fetch education

    this.activatedRouter.params.subscribe(params => {
      if (
        params["key"] == "nonurgentrequest" ||
        params["key"] == "non-urgent"
      ) {
        this.jobType = "non-urgent";
        this.tittle = "Non Urgent Request";
        this.resetForm();
      } else {
        this.jobType = "urgent";
        this.tittle = "urgentrequest";
        this.resetForm();
      }

      if (params["jobId"]) {
        this.editJobPage = 1;
        this.tempJobId = params["jobId"];
        this.fetchJobDetails(params["jobId"]);
      } else {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(position => {
            this.getGeoLocation(
              position.coords.latitude,
              position.coords.longitude
            );
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;
            this.markers = [
              {
                lat: parseFloat(this.lat),
                lng: parseFloat(this.lng),
                draggable: true
              }
            ];
          });
        }
        this.editJobPage = 0;
      }
    });
  }

  resetForm() {
    if (this.editJobPage == 0) {
      this.jobDescriptionModel = "";
      this.jobForm.reset();
      this.checked = 1;
      this.jobForm.get("wageMode").setValue("hourly");
      this.jobForm.get("state").setValue(33);
      this.currencyModel = 98;
      this.jobForm.get("gender").setValue(3);
      this.jobForm.get("noOfVacancy").setValue(1);
      this.jobForm.get("acceptDisability").setValue(false);
      this.jobMode = 1;
      this.genderId = 3;
      this.stateModel = 33;
      this.noOfVacancyModal = 1;
    }
  }
  tempJobId;
  editJobPage = 0; //if 0 then job post page else job edit
  noOfHour: any = 8;
  maxexperience: any;
  hourlyWages: any;
  maxAge: any;
  minAge: any;
  maxWeight: any;
  minWeight: any;
  minHeight: any;
  maxHeight: any;
  convertTimeToDateTime(time) {}
  address = [];
  limit = 4;

  getAddress() {
    this.address = [];
    this.service
      .serverGetRequest(
        "",
        "address.json?registerType=" + this.service.getStorage("registerType")
      )
      .subscribe(
        res => {
          if (res["status"].responseCode == 20) {
            this.address = res["result"].addressDetails;
            this.address.reverse();
            if (this.address.length != 0) {
              this.addressId = this.address[0].addressID;
              this.service.setStorage("lat", this.address[0].latitude);
              this.service.setStorage("lng", this.address[0].longitude);
              this.lat = this.address[0].latitude;
              this.lng = this.address[0].longitude;
            }
          } else this.service.apiErrorHandler(res);
        },
        er => {
          this.service.serviceErrorResponce(er);
        }
      );
  }
  setLangLat(lat, lng) {
    this.service.setStorage("lat", lat);
    this.service.setStorage("lng", lng);
    this.lat = lat;
    this.lng = lng;
  }
  fetchJobDetails(jobId) {
    customealert.loaderShow("html");
    this.service
      .serverGetRequest("", "jobDetails/" + jobId + ".json?pageNo=1")
      .subscribe(
        res => {
          customealert.loaderHide("html");
          if (res["status"].responseCode == 20) {
            let result = res["result"];

            this.jobTitle = result.jobTitle;
            this.jobDescriptionModel = result.jobDescription;
            this.fromDate = result.fromDate;
            this.jobForm.get("fromDate").setValue(result.fromDate);
            this.noOfDay = result.noOfDay;
            this.j.wageMode.setValue(result.wageMode);
            this.j.minhour.setValue(result.minHour);
            this.j.fixedWages.setValue(result.fixedWage);
            this.fixedWages = result.fixedWage;
            this.fromTimeModel = new Date(
              "2018 10 12 " + result.fromTime + " GMT"
            );
            this.addressId = result.addressId ? result.addressId : null;
            this.toTimeModel = new Date("2018 10 12 " + result.toTime + " GMT");
            this.genderId = result.genderDetails["genderId"];
            this.nationalityModel = result.nationalityDetails["id"];
            this.stateModel = result.state ? result.state["id"] : null;
            this.noOfHour = result.noOfHour;
            this.maxexperience = result.maxYearofExperience;
            this.noOfVacancyModal = result.noOfVacancy;
            this.fixedStandardWage = result.subCategoryDetails.fixedStandardWage
              ? result.subCategoryDetails.fixedStandardWage
              : 0;
            this.standardWage = result.subCategoryDetails.standardWage
              ? result.subCategoryDetails.standardWage
              : 0;
            this.generalSpecId = result.categoryDetails["id"];
            this.specId = result.subCategoryDetails["id"];
            this.getSubCategory(this.generalSpecId);
            this.getSkills(this.generalSpecId);
            this.jobMode = result.jobMode == "onsite" ? 1 : 0;
            let val: any = [];
            if (result.skill) {
              result.skill.forEach(element => {
                val += +element.id + ",";
              });
              this.skillModal = [parseInt(val)];
            }
            this.hourlyWages = result.hourlyWages;
            this.j.hourlyWages.setValue(result.hourlyWages);

            this.maxAge = result.maxAge;
            this.minAge = result.minAge;
            this.maxWeight = result.maxWeight;
            this.minWeight = result.minWeight;
            this.maxHeight = result.maxHeight;
            this.minHeight = result.minHeight;
            this.jobAttachments = [];
            this.fileName = [];
            this.fileCounter = result.fileDetails
              ? result.fileDetails.length
              : 0;
            result.fileDetails.forEach(element => {
              this.dwnld_file = true;
              this.jobAttachments.push(element.fileId);
              this.fileName.push(element.seedName);
            });
            this.educationId = result.education["id"];
            this.jobType = result.jobPostType;
            this.checked = result.acceptDisability;
            this.lat = parseFloat(result.latitude);
            this.lng = parseFloat(result.longitude);
            this.getGeoLocation(this.lat, this.lng);
            this.markers = [
              {
                lat: this.lat,
                lng: this.lng,
                draggable: true
              }
            ];

            // this.jobAttachments=result.jobAttachments
          } else this.service.apiErrorHandler(res);
        },
        err => this.service.serviceErrorResponce(err)
      );
  }

  timeFormat(input) {
    var hr = input.split(":")[0];
  }
  error;
  inputError = false;
  validateInputKey(event, id) {
    if (
      (event.keyCode >= 65 && event.keyCode <= 90) ||
      (event.keyCode >= 106 && event.keyCode <= 222)
    )
      event.preventDefault();
  }
  validateExperience(event) {
    if (
      (event.keyCode >= 65 && event.keyCode <= 90) ||
      (event.keyCode >= 106 &&
        event.keyCode <= 222 &&
        event.keyCode != 110 &&
        event.keyCode != 190)
    )
      event.preventDefault();
  }
  buttonClick = 0;
  wagesLengthIssue = 0;
  minheightLengthIssue = 0;
  maxheightLengthIssue = 0;
  maxweightLengthIssue = 0;
  maxexperienceLengthIssue = 0;
  minweightLengthIssue = 0;
  business_location_name = null;
  jobPost() {
    // console.log(this.datePipe.transform(this.fromTimeModel,'HH:mm a','UTC','ar-SA'))
    //Job Title
    if (!this.jobForm.get("jobTitle").valid) {
      console.log("1");
      $("#jobTitle")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#jobTitle").css("border-color", "#ced4da");
    //Job Description
    if (!this.jobForm.get("jobDescription").valid) {
      console.log("1");

      $("#jobDescription")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#jobDescription").css("border-color", "#ced4da");
    //Job fromDate

    if (!this.jobForm.get("fromDate").value) {
      console.log("1");

      $("#fromDate")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#fromDate").css("border-color", "#ced4da");
    //Wage mode
    if (this.j.wageMode.value == "fixed") {
      this.jobForm.get("hourlyWages").setValue(0);
      this.jobForm.get("minhour").setValue(0);
      //fixedWages
      if (!this.jobForm.get("fixedWages").valid) {
        $("#fixedWages")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#fixedWages").css("border-color", "#ced4da");
      if (this.jobForm.get("fixedWages").value < this.fixedStandardWage) {
        this.service.showError(
          this.validationMsg.belowStandardWage +
            " " +
            this.fixedStandardWage +
            " INR"
        );
        return;
      }
    } else {
      this.jobForm.get("fixedWages").setValue(0);
      //hourlyWages
      if (!this.jobForm.get("hourlyWages").valid) {
        $("#hourlyWages")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#hourlyWages").css("border-color", "#ced4da");
      if (this.jobForm.get("hourlyWages").value < this.standardWage) {
        this.service.showError(
          this.validationMsg.belowStandardWage +
            " " +
            this.standardWage +
            " INR"
        );

        return;
      }
      if (this.jobForm.get("hourlyWages").valid) {
        let ex: any = String(this.jobForm.get("hourlyWages").value);
        if (ex.split(".")[1])
          if (ex.split(".")[1].length > 2) {
            this.wagesLengthIssue = 1;
            return false;
          } else this.wagesLengthIssue = 0;
      }
      //minhour
      // if (!this.jobForm.get('minhour').valid) {
      //   $('#minhour').css('border-color', 'red').focus(); this.buttonClick = 1
      //   return false
      // }
      // else
      //   $('#minhour').css('border-color', '#ced4da')
    }
    //Nuber of days
    // if (!this.jobForm.get('noOfDay').valid) {
    //   console.log('1')

    //   $('#number_day').css('border-color', 'red').focus(); this.buttonClick = 1
    //   return false
    // }
    // else
    //   $('#number_day').css('border-color', '#ced4da')
    //From Time
    if (!this.fromTimeModel) {
      console.log("1");

      $("#from_time")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#from_time").css("border-color", "#ced4da");
    //To Time
    if (!this.toTimeModel) {
      console.log("1");

      $("#to_time")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#to_time").css("border-color", "#ced4da");
    // Gender
    if (!this.jobForm.get("gender").value) {
      console.log("1");

      $("#gender")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#gender").css("border-color", "#ced4da");
    //nationality
    // if (!this.jobForm.get('nationality').value) {
    //   console.log('1')

    //   $('#nationality').css('border-color', 'red').focus(); this.buttonClick = 1
    //   return false
    // }
    // else
    //   $('#nationality').css('border-color', '#ced4da')

    //state
    if (!this.jobForm.get("state").value) {
      console.log("1");

      $("#state")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#state").css("border-color", "#ced4da");

    //maxexperience
    if (this.jobForm.get("maxexperience").value)
      if (
        !this.jobForm.get("maxexperience").valid ||
        this.jobForm.get("maxexperience").value > 52 ||
        this.jobForm.get("maxexperience").value < 0
      ) {
        $("#maxexperience")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#maxexperience").css("border-color", "#ced4da");

    if (this.jobForm.get("maxexperience").valid) {
      let ex: any = String(this.jobForm.get("maxexperience").value);
      if (ex.split(".")[1])
        if (ex.split(".")[1].length > 2) {
          this.maxexperienceLengthIssue = 1;
          return false;
        } else this.maxexperienceLengthIssue = 0;
    }

    //noVacancy
    if (
      !this.jobForm.get("noOfVacancy").valid ||
      this.jobForm.get("noOfVacancy").value < 1 ||
      this.jobForm.get("noOfVacancy").value > 100
    ) {
      $("#noVacancy")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#noVacancy").css("border-color", "#ced4da");
    //general_specialization
    if (!this.jobForm.get("category").value) {
      $("#general_specialization")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#general_specialization").css("border-color", "#ced4da");
    //specialization
    if (!this.jobForm.get("subCategory").value) {
      $("#specialization")
        .css("border-color", "red")
        .focus();
      this.buttonClick = 1;
      return false;
    } else $("#specialization").css("border-color", "#ced4da");

    //maxAge
    if (this.jobForm.get("maxAge").value)
      if (
        !this.jobForm.get("maxAge").valid ||
        this.jobForm.get("maxAge").value < 12 ||
        this.jobForm.get("maxAge").value > 70
      ) {
        console.log("1");

        $("#maxAge")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#maxAge").css("border-color", "#ced4da");
    //minAge
    if (this.jobForm.get("minAge").value)
      if (
        !this.jobForm.get("minAge").valid ||
        this.jobForm.get("minAge").value < 12 ||
        this.jobForm.get("minAge").value > 70
      ) {
        console.log("1");

        $("#minAge")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#minAge").css("border-color", "#ced4da");
    if (this.jobForm.get("maxAge").value < this.jobForm.get("minAge").value) {
      console.log("1");

      this.buttonClick = 1;
      return false;
    }
    // maxWeight
    if (this.jobForm.get("maxWeight").value)
      if (
        this.jobForm.get("maxWeight").value < 20 ||
        this.jobForm.get("maxWeight").value > 1000
      ) {
        console.log("1");

        $("#maxWeight")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#maxWeight").css("border-color", "#ced4da");
    if (this.jobForm.get("maxWeight").valid) {
      let ex: any = String(this.jobForm.get("maxWeight").value);
      if (ex.split(".")[1])
        if (ex.split(".")[1].length > 2) {
          this.maxweightLengthIssue = 1;
          return false;
        } else this.maxweightLengthIssue = 0;
    }

    // minWeight
    if (this.jobForm.get("minWeight").value)
      if (
        this.jobForm.get("minWeight").value < 20 ||
        this.jobForm.get("minWeight").value > 1000
      ) {
        console.log("1");

        $("#minWeight")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#minWeight").css("border-color", "#ced4da");

    if (this.jobForm.get("minWeight").valid) {
      let ex: any = String(this.jobForm.get("minWeight").value);
      if (ex.split(".")[1])
        if (ex.split(".")[1].length > 2) {
          this.minweightLengthIssue = 1;
          return false;
        } else this.minweightLengthIssue = 0;
    }

    //maxHeight
    if (this.jobForm.get("maxHeight").value)
      if (
        this.jobForm.get("maxHeight").value < 50 ||
        this.jobForm.get("maxHeight").value > 250
      ) {
        console.log("1");

        $("#maxHeight")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#maxHeight").css("border-color", "#ced4da");
    if (this.jobForm.get("maxHeight").valid) {
      let ex: any = String(this.jobForm.get("maxHeight").value);
      if (ex.split(".")[1])
        if (ex.split(".")[1].length > 2) {
          this.maxheightLengthIssue = 1;
          return false;
        } else this.maxheightLengthIssue = 0;
    }

    // minHeight
    if (this.jobForm.get("minHeight").value)
      if (
        this.jobForm.get("minHeight").value < 50 ||
        this.jobForm.get("minHeight").value > 250
      ) {
        console.log("1");

        $("#minHeight")
          .css("border-color", "red")
          .focus();
        this.buttonClick = 1;
        return false;
      } else $("#minHeight").css("border-color", "#ced4da");

    if (this.jobForm.get("minHeight").valid) {
      let ex: any = String(this.jobForm.get("minHeight").value);
      if (ex.split(".")[1])
        if (ex.split(".")[1].length > 2) {
          this.minheightLengthIssue = 1;
          return false;
        } else this.minheightLengthIssue = 0;
    }
    //educations
    // if (!this.jobForm.get('educations').value) {
    //   $('#educations').css('border-color', 'red').focus(); this.buttonClick = 1
    //   return false
    // }
    // else
    //   $('#educations').css('border-color', '#ced4da')

    //business_location_name
    if (!this.addressId) {
      this.service.showError("Please Provide Job Location");
      return false;
    }

    //No of working hours
    // if (!this.jobForm.get('noOfHour').value) {
    //   $('#no_hours').css('border-color', 'red').focus(); this.buttonClick = 1
    //   return false
    // }
    // else
    //   $('#no_hours').css('border-color', '#ced4da')
    //maxexperience
    // if (!this.jobForm.get('maxexperience').valid) {
    //   $('#maxexperience').css('border-color', 'red').focus(); this.buttonClick = 1
    //   return false
    // }
    // else
    //   $('#maxexperience').css('border-color', '#ced4da')
    //minexperience
    // if (!this.jobForm.get('minexperience').valid) {
    //   $('#minexperience').css('border-color', 'red').focus(); this.buttonClick = 1
    //   return false
    // }
    // else
    //   $('#minexperience').css('border-color', '#ced4da')
    // if (this.jobForm.get('maxexperience').value < this.jobForm.get('minexperience').value) {
    //   this.buttonClick = 1;
    //   return false
    // }
    // if (this.jobForm.get('maxAge').value < this.jobForm.get('minAge').value) {
    //   this.buttonClick = 1;
    //   return false
    // }
    // if (this.jobForm.get('maxWeight').value < this.jobForm.get('minWeight').value) {
    //   this.buttonClick = 1;
    //   return false
    // }
    // if (this.jobForm.get('maxHeight').value < this.jobForm.get('minHeight').value) {
    //   this.buttonClick = 1;
    //   return false
    // }

    //skill
    // if (!this.jobForm.get('skill').value) {
    //   $('#skill').css('border-color', 'red').focus();this.buttonClick=1
    //   return false
    // }
    // else
    //   $('#skill').css('border-color', '#ced4da')

    // //currency
    // if (!this.jobForm.get('currency').value) {
    //   $('#currency').css('border-color', 'red').focus(); this.buttonClick = 1
    //   return false
    // }
    // else
    //   $('#currency').css('border-color', '#ced4da')

    //minWeight
    // if (!this.jobForm.get('minWeight').valid) {
    //   $('#minWeight').css('border-color', 'red').focus();this.buttonClick=1
    //   return false
    // }
    // else
    //   $('#minWeight').css('border-color', '#ced4da')
    //maxWeight
    // if (!this.jobForm.get('maxWeight').valid) {
    //   $('#maxWeight').css('border-color', 'red').focus();this.buttonClick=1
    //   return false
    // }
    // else
    //   $('#maxWeight').css('border-color', '#ced4da')
    //minHeight
    // if (!this.jobForm.get('minHeight').valid) {
    //   $('#minHeight').css('border-color', 'red').focus();this.buttonClick=1
    //   return false
    // }
    // else
    //   $('#minHeight').css('border-color', '#ced4da')
    // //maxHeight
    // if (!this.jobForm.get('maxHeight').valid) {
    //   $('#maxHeight').css('border-color', 'red').focus();this.buttonClick=1
    //   return false
    // }
    // else
    //   $('#maxHeight').css('border-color', '#ced4da')
    //file
    // if (this.jobAttachments.length == 0) {

    //   this.error = this.validationMsg.required
    //   $('#file').css('border-color', 'red').focus();this.buttonClick=1
    //   return false
    // }
    // else
    //   $('#file').css('border-color', '#ced4da')

    let disability = this.checked;
    // if (this.jobForm.get("acceptDisability").value == "") disability = true;
    // else disability = this.jobForm.get("acceptDisability").value;
    let type = this.jobType;

    let fromTime = this.datePipe.transform(
      this.fromTimeModel,
      "HH:mm:ss",
      "UTC"
    );
    let toTime = this.datePipe.transform(this.toTimeModel, "HH:mm:ss", "UTC");

    let data = {
      fromDate: this.datePipe.transform(
        this.jobForm.get("fromDate").value,
        "yyyy-MM-dd"
      ), // this.jobForm.get('fromDate').value,
      noOfDay: this.jobForm.get("noOfDay").value
        ? this.jobForm.get("noOfDay").value
        : 1,
      fromTime: fromTime,
      toTime: toTime,
      gender: this.jobForm.get("gender").value,
      nationality: this.jobForm.get("nationality").value
        ? this.jobForm.get("nationality").value
        : "86",
      state: this.jobForm.get("state").value,
      noOfHour: this.jobForm.get("noOfHour").value
        ? this.jobForm.get("noOfHour").value
        : 0,
      currency: this.jobForm.get("currency").value
        ? this.jobForm.get("currency").value
        : 98,
      minYearofExperience: this.jobForm.get("minexperience").value
        ? this.jobForm.get("minexperience").value
        : 0,
      maxYearofExperience: this.jobForm.get("maxexperience").value
        ? this.jobForm.get("maxexperience").value
        : 0,
      minWeight: this.jobForm.get("minWeight").value
        ? this.jobForm.get("minWeight").value
        : 0,
      category: this.jobForm.get("category").value,
      subCategory: this.jobForm.get("subCategory").value,
      hourlyWages: this.jobForm.get("hourlyWages").value,
      maxAge: this.jobForm.get("maxAge").value
        ? this.jobForm.get("maxAge").value
        : 0,
      minAge: this.jobForm.get("minAge").value
        ? this.jobForm.get("minAge").value
        : 0,
      maxWeight: this.jobForm.get("maxWeight").value
        ? this.jobForm.get("maxWeight").value
        : 0,
      maxHeight: this.jobForm.get("maxHeight").value
        ? this.jobForm.get("maxHeight").value
        : 0,
      minHeight: this.jobForm.get("minHeight").value
        ? this.jobForm.get("minHeight").value
        : 0,
      acceptDisability: disability,
      jobTitle: this.jobForm.get("jobTitle").value,
      jobDescription: this.jobForm.get("jobDescription").value,
      no_vacancies: this.jobForm.get("no_vacancies").value
        ? this.jobForm.get("no_vacancies").value
        : 1,
      business_location_name: this.jobForm.get("business_location_name").value,
      jobAttachments: this.jobAttachments,
      skills: ["000001"], //this.skillModal != null ? this.skillModal : ['BABKRIM-DEFAULT-SKILL'],
      noOfVacancy: this.noOfVacancyModal,
      latitude: this.lat,
      longitude: this.lng,
      education: this.educationId ? this.educationId : "9",
      jobType: this.jobType,
      wageMode: this.j.wageMode.value,
      fixedWage: this.j.fixedWages.value,
      minHour: 1,
      addressId: this.addressId,
      jobMode: this.jobMode == 1 ? "onsite" : "offsite"
    };
    this.service.setStorage("lat", this.lat);
    this.service.setStorage("lng", this.lng);

    this.service.setStorage("jobForm", JSON.stringify(data));
    let response: any;
    customealert.loaderShow("html");
    if (this.editJobPage == 1) {
      //Calling edit Job
      this.updateJob(data, type);
      return false;
    }
    this.service.serverRequest(data, "/jobs.json?jobType=" + type).subscribe(
      (res: Response) => {
        customealert.loaderHide("html");
        response = res["status"];

        if (response.responseCode == 20) {
          this.service.removeStorage("jobForm");
          if (this.service.getStorage("employeeId")) {
            //

            let response: any = JSON.stringify(res["result"]);
            let val = JSON.parse(response);

            this.inviteEmployeeForthisJob(Object.values(val)[0]);
          } else {
            this.service.showSuccess(this.validationMsg.jobpost);
            if (type != "urgent") {
              this.router.navigate(["/employer_home"]);
            } else {
              console.log(res["result"]["jobID"]);
              this.service.setStorage("tempJobId", res["result"]["jobID"]);

              this.router.navigate(["/urgentjobmap"]);
            }
          }
        } else {
          this.service.apiErrorHandler(res);
        }
      },
      err => {
        this.service.serviceErrorResponce(err);
      }
    );
  }
  updateJob(data, type) {
    let response;
    this.service
      .serverPutRequest(data, "/jobs/" + this.tempJobId + ".json")
      .subscribe(
        (res: Response) => {
          customealert.loaderHide("html");
          response = res["status"];

          if (response.responseCode == 20) {
            this.service.removeStorage("jobForm");
            if (this.service.getStorage("employeeId")) {
              //

              let response: any = JSON.stringify(res["result"]);
              let val = JSON.parse(response);

              this.inviteEmployeeForthisJob(Object.values(val)[0]);
            } else {
              this.service.showSuccess(this.validationMsg.jobEdit);
              if (type != "urgent") {
                this.router.navigate(["/employer_home"]);
              } else {
                console.log(res["result"]["jobID"]);
                this.service.setStorage("tempJobId", res["result"]["jobID"]);

                this.router.navigate(["/urgentjobmap"]);
              }
            }
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);
        }
      );
  }

  inviteEmployeeForthisJob(jobId) {
    customealert.loaderShow("html");
    let data = {
      employeeId: this.service.getStorage("employeeId"),
      jobId: jobId
    };
    this.service.loader = true;
    this.service.serverRequest(data, "invitations.json").subscribe(
      res => {
        this.service.loader = false;
        customealert.loaderHide("html");
        if (res["status"].responseCode == 20) {
          this.service.removeStorage("employeeId");
          this.service.removeStorage("employeeTempId");

          this.service.showSuccess(
            this.validationMsg.newjobwithinvitation +
              " " +
              this.service.transform(this.service.getStorage("employeeName")) +
              "."
          );
          this.router.navigate(["/employer_home"]);
        } else this.service.apiErrorHandler(res);
      },
      err => {
        customealert.loaderHide("html");
        this.service.serviceErrorResponce(err);
      }
    );
  }
  tittle;

  skillsArray;
  skillModal: any = [];
  getSkills(id) {
    this.service
      .serverGetRequest("", "/static/skills.json?category=" + id)
      .subscribe(
        res => {
          this.initialLoader = "";
          if (res["status"].responseCode == 20) {
            this.skillsArray = res["result"].skills;
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);
        }
      );
  }
  getCurrency() {
    this.service.serverGetRequest("", "/static/currencies.json").subscribe(
      res => {
        this.initialLoader = "";
        if (res["status"].responseCode == 20) {
          this.currency = res["result"].currencies;
        } else {
          this.service.apiErrorHandler(res);
        }
      },
      err => {
        this.service.serviceErrorResponce(err);
      }
    );
  }
  getCategory() {
    this.service.serverGetRequest("", "/static/categories.json").subscribe(
      res => {
        this.initialLoader = "";
        if (res["status"].responseCode == 20) {
          this.category = res["result"].categories;
        } else {
          this.service.apiErrorHandler(res);
        }
      },
      err => {
        this.service.serviceErrorResponce(err);
      }
    );
  }
  nationality;
  state;
  getStateFromApi() {
    this.service
      .serverGetRequest("", "static/states.json?country=" + 352)
      .subscribe(
        res => {
          if (res["status"].responseCode == 20)
            this.state = res["result"].states;
          else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {}
      );
  }

  getCommon() {
    this.service
      .serverGetRequest("", "/custom/employee/level/2.json")
      .subscribe(
        res => {
          this.initialLoader = "";
          if (res["status"].responseCode == 20) {
            // this.nationality = res['result'].nationalities;
            this.gender = res["result"].genders;
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);
        }
      );
  }
  subCategoryLoader;

  getSubCategory(id) {
    this.subCategoryLoader = "loadinginput";
    this.subCategory = [];
    this.nextSubCategory = [];
    this.service
      .serverGetRequest("", "static/subCategories.json?category=" + id)
      .subscribe(
        res => {
          this.subCategoryLoader = "";
          if (res["status"].responseCode == 20) {
            this.subCategory = res["result"].SubCategories;
          }
        },
        err => {
          this.subCategoryLoader = "";
          this.service.serviceErrorResponce(err);
        }
      );
  }
  /*** Drop down events */
  subCategory;
  nextSubCategory;
  categorySelect(item: any) {
    this.subCategory = [];
    this.nextSubCategory = [];
    this.skillModal = [];
    this.specId = null;
    this.getSubCategory(item.id);
    this.getSkills(item.id); // Fetch skills
  }
  categoryDeselect() {
    this.subCategory = [];
    this.nextSubCategory = [];
    this.skillsArray = [];
    this.skillModal = [];
    this.specId = null;
  }

  education: any;
  getEducation() {
    this.service.serverGetRequest("", "custom/employee/level/3.json").subscribe(
      res => {
        this.initialLoader = "";

        if (res["status"].responseCode == 20) {
          this.education = res["result"].educations;
        } else {
          this.service.apiErrorHandler(res);
        }
      },
      err => {
        this.service.serviceErrorResponce(err);
      }
    );
  }
  educationId = null;

  onEducationItemDeselect() {
    this.educationId = null;
  }
  onEducationItemSelect(item: any) {
    if (item) this.educationId = item.id;
    else this.educationId = null;
  }
  /****
   * Upload license
   */
  dwnld_file = false;
  fileLoader: any = false;
  fileName = [];
  fileCounter = 0;
  jobAttachments = [];
  deleteFileicon(index) {
    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.jobAttachments.splice(index, 1);

      this.fileCounter--;
      if (this.fileCounter < 0) this.fileCounter = 0;
    }
  }
  uploadLicense(event: any) {
    console.log(event.target.files.length);

    var count = event.target.files.length;
    if (this.fileCounter + count < 6) {
      for (let i = 0; i < count; i++) {
        const fileSelected: File = event.target.files[i];
        if (
          !this.service.checkAllowedFiles(
            event.target.files[i].name.split(".")[1]
          )
        ) {
          this.service.showError(this.validationMsg.invalidFileType);
          return false;
        }
        if (fileSelected.size < this.service.maxFileSize) {
          this.fileLoader = true;
          let response;
          this.ngProgress.start();
          this.service
            .uploadFile(fileSelected, "upload.json?fileType=docs")
            .subscribe(
              (res: Response) => {
                this.ngProgress.done();
                response = res;
                this.fileLoader = false;

                if (response.status.responseCode == 20) {
                  this.dwnld_file = true;
                  this.fileCounter++;
                  this.jobAttachments.push(response.result.file1);
                  this.fileName.push(fileSelected.name);
                } else {
                  this.service.apiErrorHandler(res);
                }
              },
              error => {
                this.fileLoader = false;
                this.service.serviceErrorResponce(error);
              }
            );
        } else this.service.showError(this.validationMsg.attachmentSizeLimit);
      }
    } else this.service.showError(this.validationMsg.attachmentLimit);
  }
  checkValidationJobpost() {}
  zoom: number = 12;
  lat: any = this.service.lat;
  lng: any = this.service.lng;

  clickedMarker(label: string, index: number) {}
  i = 0;
  delay = 3000;
  numDelay = 10;
  templat;
  templng;
  dummylat: any = 0;
  dummylng: any = 0;
  position = [0.0000000001, 0.0000000001];
  transition() {
    this.templat = (this.lat - this.position[0]) / this.numDelay;
    this.templng = (this.lng - this.position[1]) / this.numDelay;
    console.info(this.templat + " " + this.templng);

    this.move();
  }
  move() {
    this.dummylat = this.dummylat + this.templat;
    this.dummylng += this.templng;
    this.markers = [
      {
        lat: this.dummylat,
        lng: this.dummylng,
        draggable: true
      }
    ];

    if (this.i != this.numDelay) {
      this.i++;

      setTimeout(() => {
        if (this.i != this.numDelay) this.move();
      }, this.delay);
    }
    // else {
    //   this.markers = [
    //     {
    //       lat: $event['coords'].lat,
    //       lng: $event['coords'].lng,
    //       draggable: true
    //     }
    //   ]
    // }
  }
  mapClicked($event: MouseEvent) {
    this.i = 0;
    this.dummylat = 0;
    this.dummylng = 0;
    this.markers = [
      {
        lat: $event["coords"].lat,
        lng: $event["coords"].lng,
        draggable: true
      }
    ];
    this.lat = $event["coords"].lat;
    this.lng = $event["coords"].lng;
    console.info(this.lat + " " + this.lng);
    // this.addAddress.show()
    // this.transition()
    this.getGeoLocationForAddAddress(
      $event["coords"].lat,
      $event["coords"].lng
    );
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    this.lat = $event["coords"].lat;
    this.lng = $event["coords"].lng;
    this.getGeoLocationForAddAddress(
      $event["coords"].lat,
      $event["coords"].lng
    );
  }
  currentIcon = {
    url: this.service.urgentEmployerIcon,
    scaledSize: new google.maps.Size(60, 60)
  };
  markers: marker[] = [];
  /************************************************** */
  getGeoLocationForAddAddress(lat: number, lng: number) {
    if (navigator.geolocation) {
      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(lat, lng);
      let request = { latLng: latlng };

      geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          let resultLength = rsltAdrComponent.length;
          if (result != null) {
            this.business_location_name = result.formatted_address;
            this.jobForm
              .get("business_location_name")
              .setValue(result.formatted_address);
            customealert.showModal("addAddressConfirm");

            //this.addAddressToApi()
          } else {
            alert("No address available!");
          }
        }
      });
    }
  }
  addressId = null;
  doorNo = "";
  landmark = "";
  addAddressToApi() {
    /**
    Min length validation removed 
    on 10-03-2019
    informed by Jamsheer.
     */
    if (!this.doorNo) {
      this.service.showError(this.validationMsg.doornomissing);
      return;
    }
    if (!this.landmark) {
      this.service.showError(this.validationMsg.landmarkmissing);
      return;
    }
    customealert.loaderShow("html");
    let data = {
      address:
        this.doorNo + "||" + this.landmark + "||" + this.business_location_name,
      latitude: this.lat,
      longitude: this.lng
    };
    this.service
      .serverRequest(
        data,
        "address.json?registerType=" + this.service.getStorage("registerType")
      )
      .subscribe(
        res => {
          customealert.loaderHide("html");
          if (res["status"].responseCode == 20) {
            this.addressId = res["result"].addressID;
            console.log(this.addressId + " " + res["result"].addressID);
            customealert.hideModal("addAddressConfirm");
            this.address.reverse();
            this.address.push({
              address:
                this.doorNo +
                "," +
                this.landmark +
                "," +
                this.business_location_name,
              latitude: this.lat,
              addressID: this.addressId,
              longitude: this.lng
            });
            this.address.reverse();
          } else this.service.apiErrorHandler(res);
        },
        er => this.service.serviceErrorResponce(er)
      );
  }
  getGeoLocation(lat: number, lng: number) {
    if (navigator.geolocation) {
      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(lat, lng);
      let request = { latLng: latlng };

      geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          let resultLength = rsltAdrComponent.length;
          if (result != null) {
            this.business_location_name = result.formatted_address;
            this.jobForm
              .get("business_location_name")
              .setValue(result.formatted_address);
          } else {
            alert("No address available!");
          }
        }
      });
    }
  }
  standardWage: any = 0;
  fixedStandardWage = 0;
  fixedWages;
  subCategorySelect(event) {
    try {
      console.info(event);
      if (event.id) {
        if (this.jobType == "urgent") {
          this.standardWage = event.urgentStandardWage;
          this.hourlyWages = this.standardWage;
          this.fixedWages = event.urgentFixedStandardWage;
          this.fixedStandardWage = event.urgentFixedStandardWage;
        } else {
          this.standardWage = event.standardWage;
          this.hourlyWages = this.standardWage;
          this.fixedWages = event.fixedStandardWage;
          this.fixedStandardWage = event.fixedStandardWage;
        }

        // this.standardWage=this.service.getStandardWageFromSubCategory(this.subCategory, event.id)
      }
      if (!event) {
        this.standardWage = 0;
      }
    } catch (error) {}
  }
  /************************************************** */
}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
