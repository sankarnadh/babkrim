import { Component, OnInit, ViewChild } from '@angular/core';
import { ServerService } from '../../../service/server.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ValidationMessageService } from '../../../service/validation-message.service'
import { ActivatedRoute, Router } from '@angular/router'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgProgress } from 'ngx-progressbar';
import '../../../../assets/js/js/chat.js';
import { isArray } from 'util';
import { count } from 'rxjs-compat/operator/count';
declare var customealert: any;
@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit {
  @ViewChild('hourlyRateInput')
  hourlyRateInput: any;
  @ViewChild('attachment')
  attachment: any;
  @ViewChild('cover')
  cover: any;
  constructor(public service: ServerService,
    public ngProgress: NgProgress,
    public router: Router,
    public config: NgbRatingConfig,
    private spinnerService: Ng4LoadingSpinnerService,
    public validationMsg: ValidationMessageService) {
    config.max = 5;
    config.readonly = true
  }
  proposal = JSON.parse(this.service.getStorage('proposal'));
  invitationId: any;
  jobDetails = JSON.parse(this.service.getStorage('data'))
  hourlyRate: any;
  wageMode = 'hourly'
  submitError = false
  fixedWages: any;
  minhour: any;
  counter: number = 0;
  fileName: any = [];
  error: string = null;
  coverLetter: any = '';
  proposalAttachments = []
  page = 0; // to approve proposal
  ngOnInit() {
    this.service.loader = false
   
    if (this.jobDetails.wageMode == 'fixed') {
      this.wageMode=('fixed')
      this.fixedWages=(this.proposal.fixedWage)
      this.hourlyRate=(1)
      this.minhour=(1)
      
    }
    else {
      this.hourlyRate=(this.proposal.hourlyWages)
      this.minhour=(this.proposal.minHour)
      this.fixedWages=(1)
    }
    if (!this.proposal)
      this.router.navigate(['/home'])
    this.invitationId = this.proposal.invitationId
    this.getEmployeeDetails()
    let url = this.router.url;
    this.getDetailsByProposalId()
    if (url == '/view-invitation-details') {
      this.page = 1;
      // this.getDetailsByProposalId()
    }
    this.service.getCurrency()

  }
  employeeId;
  employeeDetails: any = []
  feedBack = []
  feedBackRating = []
  featuredId = null
  featuredImage = []
  imageUrl: any
  videoUrl: any
  featuredVideo = []
  getEmployeeDetails() {
    this.employeeId = this.proposal.employeeDetails.employeeId
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'employee/employeeDetails/' + this.employeeId + '.json').subscribe(res => {
      customealert.loaderHide('html')

      if (res['status'].responseCode == 20) {
        this.employeeDetails = res['result'];
        /********************Featured ********************************** */
        if (this.employeeDetails.featuredDetails) {
          this.featuredId = this.employeeDetails.featuredDetails.featuredId

          if (this.employeeDetails.featuredDetails.image1) {

            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image1,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image1 + '&token=' + this.service.getStorage('token')
            })
          }
          if (this.employeeDetails.featuredDetails.image2) {


            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image2,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image2 + '&token=' + this.service.getStorage('token')
            })
          }
          if (this.employeeDetails.featuredDetails.image3) {


            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image3,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image3 + '&token=' + this.service.getStorage('token')
            })
          }
          if (this.employeeDetails.featuredDetails.image4) {


            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image4,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image4 + '&token=' + this.service.getStorage('token')
            })
          }
          if (this.employeeDetails.featuredDetails.image5) {


            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image5,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image5 + '&token=' + this.service.getStorage('token')
            })
          }
          this.imageUrl = this.featuredImage[this.featuredImage.length - 1].url

          if (this.employeeDetails.featuredDetails.video1) {


            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video1,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video1 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video1
            })
          }
          if (this.employeeDetails.featuredDetails.video2) {


            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video2,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video2 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video2
            })
          }
          if (this.employeeDetails.featuredDetails.video3) {


            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video3,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video3 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video3
            })
          }
          if (this.employeeDetails.featuredDetails.video4) {


            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video4,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video4 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video4
            })
          }
          if (this.employeeDetails.featuredDetails.video5) {


            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video5,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video5 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video5
            })
          }
          this.videoUrl = this.featuredVideo[this.featuredVideo.length - 1].url
          console.log(this.videoUrl)
        }
        /******************************************************** */
        this.employeeDetails = res['result'];
        if (res['result'].feedback) {
          this.feedBack = res['result'].feedback
          this.feedBack.forEach(element => {
            this.feedBackRating.push(element)
          });
        }
      }
      else this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
  getDetailsByProposalId() {
    this.service.serverGetRequest('', 'proposalDetails/' + this.proposal.proposalId + '.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      if (res['status'].responseCode == 20) {

        this.proposal = res['result'].proposal
        // this.service.setStorage('data',JSON.stringify(this.proposal.jobDetails))
        console.info(this.proposal)
      }
      else
        this.service.apiErrorHandler(res)
    },
      err => {
        this.service.serviceErrorResponce(err)
      })
  }
  loaderShow = false
  currency = this.service.defaultCurrency
  approveProposal() {
    this.submitError = true
    let categoryId=this.jobDetails.categoryId?this.jobDetails.categoryId:16
  
    if (this.wageMode == 'hourly') {
      this.hourlyRate = this.hourlyRateInput.nativeElement.value;
      if (!this.hourlyRate) {
        this.hourlyRateInput.nativeElement.focus()
        return false
      }
      if (!Number(this.currency)) {
        this.error = this.validationMsg.currencyInvalid
        return false
      }
      if (!this.minhour)
        return false
      if(this.minhour>24)
      return   
      this.fixedWages = 0
      if(this.hourlyRate < this.proposal.jobDetails.standardWage) {
        return;
      }
    }
    else {
      if (!this.fixedWages)
        return
        if(this.fixedWages < this.proposal.jobDetails.standardWage) {
          return;
        }
      this.minhour = 0
      this.hourlyRate = 0
    }

    this.coverLetter = this.cover.nativeElement.value;
    if (!this.coverLetter) {
      this.error = this.validationMsg.coverLetter
      this.cover.nativeElement.focus()
      return false
    }

    this.error = null;
   customealert.loaderShow('html');  //To show loader
    this.loaderShow = true

    let data = {
      "proposalId": this.proposal.proposalId,
      "hourlyWages": this.hourlyRate,
      "currency": this.currency,
      "comment": this.coverLetter,
      "contractAttachments": this.proposalAttachments,
      "wageMode": this.wageMode,
      "fixedWage": this.fixedWages,
      "minHour": this.minhour
    }
    this.service.serverRequest(data, 'contracts.json').subscribe((res) => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        customealert.hideModal('modal_approve_proposal')
        this.service.showSuccess('success')
        this.router.navigate(['/all-employee-list/'+this.jobDetails.jobId+'/'+categoryId])
       // window.history.back()
      }
      else
        this.service.apiErrorHandler(res)
      this.loaderShow = false

        //To hide loader
    },
    er=>this.service.serviceErrorResponce(er))
  }
  approveInvitation() {
   
    if (this.wageMode == 'hourly') {
      this.hourlyRate = this.hourlyRateInput.nativeElement.value;
      if (!this.hourlyRate) {
        this.hourlyRateInput.nativeElement.focus()
        return false
      }
      if (!Number(this.currency)) {
        this.error = this.validationMsg.currencyInvalid
        return false
      }
      if (!this.minhour)
        return false
      this.fixedWages = 0
    }
    else {
      if (!this.fixedWages)
        return
      this.minhour = 0
      this.hourlyRate = 0
    }


    this.coverLetter = this.cover.nativeElement.value;
    if (!this.coverLetter) {
      this.error = this.validationMsg.coverLetter
      this.cover.nativeElement.focus()
      return false
    }

    this.error = null;
    customealert.loaderShow('html');  //To show loader
    this.loaderShow = true
    let categoryId=this.jobDetails.categoryId?this.jobDetails.categoryId:16
    let data = {
      "invitationId": this.invitationId,
      "hourlyWages": this.hourlyRate,
      "currency": this.currency,
      "comment": this.coverLetter,
      "invitationAttachments": this.proposalAttachments,
      "wageMode": this.wageMode,
      "fixedWage": this.fixedWages,
      "minHour": this.minhour
    }
    this.service.serverRequest(data, 'hires.json').subscribe((res) => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        customealert.hideModal('modal_approve_proposal')
        this.service.showSuccess('success')
        this.router.navigate(['/all-employee-list/'+this.jobDetails.jobId+'/'+categoryId])
      }
      else
        this.service.apiErrorHandler(res)
      this.loaderShow = false

      
    },
    er=>this.service.serviceErrorResponce(er))
  }
  fileLoader: any=false;
  fileEvent(event: any) {


    var counter = event.target.files.length
    if (this.counter + counter < 6) {
      for (let i = 0; i < counter; i++) {
        const fileSelected: File = event.target.files[i];
        if (!this.service.checkAllowedFiles(event.target.files[i].name.split('.').pop())) {
          this.service.showError(this.validationMsg.invalidFileType)
          return false
        }
        if (fileSelected.size < this.service.maxFileSize) {

          this.fileLoader = true;

          let response;
          var reader = new FileReader();
          this.ngProgress.start();
          this.service.uploadSingleFile(fileSelected, 'upload.json?fileType=docs')
            .subscribe((res: Response) => {
              response = res;
              this.ngProgress.done();
              this.spinnerService.hide()
              this.fileLoader = false
              this.service.loader = false

              if (response.status.responseCode == 20) {

                this.proposalAttachments.push(response.result.file1);
                this.fileName.push(fileSelected.name)
                this.counter++;
              }
              else {
                this.service.apiErrorHandler(res);

              }
            },
              (err) => {
                this.service.serviceErrorResponce(err)
                this.service.loader = false
                this.fileLoader = false
              });
        } else this.service.showError(this.validationMsg.attachmentSizeLimit)
      }
    }
    else
      this.service.showError(this.validationMsg.attachmentLimit)
  }


  deleteFileicon(index) {

    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.proposalAttachments.splice(index, 1);
      // if (this.counter != 5) {
      this.counter--;
      if (this.counter < 0)
        this.counter = 0
      // } 
    }

  }
  skillFormat(skill) {

    let data
    if (skill)
      try {
        data = skill.split(',');
      }
      catch (er) {
        data = []
      }
    if (isArray(data))
      return data
    else return []
  }
  rejectProposals() {
    let categoryId=this.jobDetails.categoryId?this.jobDetails.categoryId:16
    customealert.loaderShow('html');
    this.service.serverDeleteRequest('proposalDetails/reject/' + this.proposal.proposalId + ".json?registerType=" + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess('success')
        this.router.navigate(['/all-employee-list/'+this.jobDetails.jobId+'/'+categoryId])

      } else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))
  }
}