import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerMyJobsComponent } from './employer-my-jobs.component';

describe('EmployerMyJobsComponent', () => {
  let component: EmployerMyJobsComponent;
  let fixture: ComponentFixture<EmployerMyJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerMyJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerMyJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
