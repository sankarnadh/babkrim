import { Component, OnInit, HostListener, Input } from '@angular/core';
import { ServerService } from '../../../service/server.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router'
import { ValidationMessageService } from '../../../service/validation-message.service'
import '../../../../assets/js/js/chat.js'
declare var customealert;
@Component({
  selector: 'app-employer-my-jobs',
  templateUrl: './employer-my-jobs.component.html',
  styleUrls: ['./employer-my-jobs.component.css']
})

export class EmployerMyJobsComponent implements OnInit {
  searchText

  constructor
    (
      public service: ServerService,
      private spinnerService: Ng4LoadingSpinnerService,
      protected router: Router,
      public validationMsg: ValidationMessageService
    ) {
    service.loader = true
  }
  @Input() showClosedJob: any = true;
  myJobs: any = [];                      //To store all job details 
  p: any;                      //Page Counter
  pageCount = 0;
  buttonShow = true; //Same page for hire employee direct (modal) so hide / show button
  /******************************** */
  title: any = 'close_job';
  text: any = 'cantbeundone'
  /******************************* */
  endJobFlag = 0;
  jobid: any;
  endJob() {
    this.endJobFlag = 1;
    this.service.serverPutRequest('', 'jobs/complete/' + this.jobid + '.json').subscribe(res => {
      this.endJobFlag = 0;
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.getAllJobOfEmployer(0);
      }
      else {
        this.service.apiErrorHandler(res)
      }
    }, err => {
      this.service.serviceErrorResponce(err)
      this.endJobFlag = 0;
    })
  }
  editJob(value) {
    value.jobPostType == 'urgent' ? value.jobPostType = 'Urgent requests' : 'non-urgentrequest'
    this.router.navigate(['/editjob/' + value.jobPostType + '/' + value.jobId])
  }
  ngOnInit() {

    // if(!this.service.isEmployerLogin()||this.service.getRegisterLevel()!='0')
    // this.router.navigate(['/login']);
    this.getAllJobOfEmployer(0);
    let url = this.router.url;
    if (url != '/my-jobs')
      this.buttonShow = false
  }


  pageNo = 0
  totalJobs = 0
  getAllJobOfEmployer(pageCount) {
    if (pageCount == 0) {
      window.scrollTo(0, 0)
      this.myJobs = []
      this.pageCount = 0
      this.totalJobs = 0
    }
    this.service.loader = true;         //Loading Spinner
    pageCount = +(pageCount) + 1
    this.service.serverGetRequest('', '/employer/jobDetails.json?pageNo=' + pageCount).subscribe(
      (res) => {
        if (res['status'].responseCode == 20) {
          this.pageNo++
          this.service.loader = false; //Hiding spinner
          res['result'].jobDetails.forEach(element => {
            this.myJobs.push(element)
          });
          const url=this.router.url
         
          if (res['result'].totalCount > this.myJobs.length && url!='/my-jobs') {
            this.getAllJobOfEmployer(pageCount)
          }
          this.pageCount += res['result'].count
          this.totalJobs = res['result'].totalCount


        }
        else
          this.service.apiErrorHandler(res)
      }
    );
  }
  viewJobDetails(data) {
    this.service.setStorage('data', JSON.stringify(data));
    if(this.buttonShow)
    this.router.navigate(['/all_employee_list/' + data.jobId + '/' + data.categoryId]);
  }

  inviteToJob(jobId) {
    customealert.loaderShow('html')
    let data = {
      "employeeId": this.service.getStorage('employeeTempId'),
      "jobId": jobId
    }
    this.service.loader = true
    this.service.serverRequest(data, 'invitations.json').subscribe((res) => {
      this.service.loader = false
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        customealert.hideModal('allJobsOfMe')
      }
      else
        this.service.apiErrorHandler(res)
    }, err => {
      this.service.serviceErrorResponce(err)
    })
  }
  /********************************************************************** */
  throttle = 3;
  scrollDistance = 1;
  scrollUpDistance = 2;
  onScrollDown() {
     if (this.pageNo != 0 && !this.service.loader && this.pageCount < this.totalJobs) {
       this.getAllJobOfEmployer(this.pageNo);
    console.log('scrolled down!!', this.service.jobListPageNo + " ");
    }
  }
  /*********************************************************************** */
}
