import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VieweEmployeeDetailsComponent } from './viewe-employee-details.component';

describe('VieweEmployeeDetailsComponent', () => {
  let component: VieweEmployeeDetailsComponent;
  let fixture: ComponentFixture<VieweEmployeeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VieweEmployeeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VieweEmployeeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
