import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HireSearchEmployeeComponent } from './hire-search-employee.component';

describe('HireSearchEmployeeComponent', () => {
  let component: HireSearchEmployeeComponent;
  let fixture: ComponentFixture<HireSearchEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HireSearchEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HireSearchEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
