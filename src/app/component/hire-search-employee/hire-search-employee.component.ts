import { Component, OnInit, Output, HostListener } from "@angular/core";
import { Router } from "@angular/router";
import { ServerService } from "../../service/server.service";
import { ValidationMessageService } from "../../service/validation-message.service";
import { NgbRatingConfig } from "@ng-bootstrap/ng-bootstrap";

import "../../../assets/js/js/chat.js";
import { EventEmitter } from "events";
import { TranslateService } from "@ngx-translate/core";
import { TitleCasePipe } from "@angular/common";
declare var customealert;
@Component({
  selector: "app-hire-search-employee",
  templateUrl: "./hire-search-employee.component.html",
  styleUrls: ["./hire-search-employee.component.css"]
})
export class HireSearchEmployeeComponent implements OnInit {
  employeeId = this.service.getStorage("employeeTempId"); // Value is set by Search employee page

  constructor(
    public service: ServerService,
    public router: Router,
    public config: NgbRatingConfig,
    private translate: TranslateService,
    private titlepipe: TitleCasePipe,
    public validationMsg: ValidationMessageService
  ) {
    config.max = 5;
    config.readonly = true;
  }
  page = 0;
  aboutSplice =350;
  ngOnInit() {
    if (!this.employeeId) this.router.navigate(["/home"]);
    this.getEmployeeDetailsByEmployeeId();
    this.service.getemployeeDetails(this.page);
  }
  removeItem(index) {
    this.service.getemployeeDetails(this.page);
  }

  feedBack = [];
  feedBackRating = [];
  employeeDetails: any = [];
  items: any = [];
  userImg: any;
  featuredId = null;
  featuredImage = [];
  imageUrl: any;
  videoUrl: any = null;
  featuredVideo = [];
  loadNewemployee(id) {
    this.service.setStorage("employeeTempId", id);
    this.getEmployeeDetailsByEmployeeId();
    this.service.getemployeeDetails(this.page);
  }
  getEmployeeDetailsByEmployeeId() {
    customealert.loaderShow("html");

    this.service
      .serverGetRequest(
        "",
        "employee/employeeDetails/" +
          this.service.getStorage("employeeTempId") +
          ".json"
      )
      .subscribe(
        res => {
          customealert.loaderHide("html");

          if (res["status"].responseCode == 20) {
            this.featuredVideo = [];
            this.feedBack = [];
            this.feedBackRating = [];
            this.featuredImage = [];
            this.imageUrl = this.videoUrl = null;
            this.employeeDetails = res["result"];
            console.info(this.employeeDetails.disabilityStatus);
            if (this.employeeDetails.profileImage)
              this.userImg =
                this.service.url +
                "/download/profile.json?fileID=" +
                this.employeeDetails.profileImage +
                "&token=" +
                this.service.getStorage("token");
            /********************Featured ********************************** */
            if (this.employeeDetails.featuredDetails) {
              this.featuredId = this.employeeDetails.featuredDetails.featuredId;

              if (this.employeeDetails.featuredDetails.image1) {
                this.featuredImage.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.image1,
                  url:
                    this.service.url +
                    "/download/docs.json?fileID=" +
                    this.employeeDetails.featuredDetails.image1 +
                    "&token=" +
                    this.service.getStorage("token")
                });
              }
              if (this.employeeDetails.featuredDetails.image2) {
                this.featuredImage.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.image2,
                  url:
                    this.service.url +
                    "/download/docs.json?fileID=" +
                    this.employeeDetails.featuredDetails.image2 +
                    "&token=" +
                    this.service.getStorage("token")
                });
              }
              if (this.employeeDetails.featuredDetails.image3) {
                this.featuredImage.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.image3,
                  url:
                    this.service.url +
                    "/download/docs.json?fileID=" +
                    this.employeeDetails.featuredDetails.image3 +
                    "&token=" +
                    this.service.getStorage("token")
                });
              }
              if (this.employeeDetails.featuredDetails.image4) {
                this.featuredImage.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.image4,
                  url:
                    this.service.url +
                    "/download/docs.json?fileID=" +
                    this.employeeDetails.featuredDetails.image4 +
                    "&token=" +
                    this.service.getStorage("token")
                });
              }
              if (this.employeeDetails.featuredDetails.image5) {
                this.featuredImage.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.image5,
                  url:
                    this.service.url +
                    "/download/docs.json?fileID=" +
                    this.employeeDetails.featuredDetails.image5 +
                    "&token=" +
                    this.service.getStorage("token")
                });
              }
              this.imageUrl = this.featuredImage[
                this.featuredImage.length - 1
              ].url;

              if (this.employeeDetails.featuredDetails.video1) {
                this.featuredVideo.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.video1,
                  url:
                    this.service.s3bucketurl +
                    this.service.bucketFolder +
                    this.employeeDetails.featuredDetails.video1 +
                    this.service.bucketExtension,
                  thumbnail:
                    this.service.url +
                    "/download/thumbnail.json?fileID=" +
                    this.employeeDetails.featuredDetails.video1
                });
              }
              if (this.employeeDetails.featuredDetails.video2) {
                this.featuredVideo.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.video2,
                  url:
                    this.service.s3bucketurl +
                    this.service.bucketFolder +
                    this.employeeDetails.featuredDetails.video2 +
                    this.service.bucketExtension,
                  thumbnail:
                    this.service.url +
                    "/download/thumbnail.json?fileID=" +
                    this.employeeDetails.featuredDetails.video2
                });
              }
              if (this.employeeDetails.featuredDetails.video3) {
                this.featuredVideo.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.video3,
                  url:
                    this.service.s3bucketurl +
                    this.service.bucketFolder +
                    this.employeeDetails.featuredDetails.video3 +
                    this.service.bucketExtension,
                  thumbnail:
                    this.service.url +
                    "/download/thumbnail.json?fileID=" +
                    this.employeeDetails.featuredDetails.video3
                });
              }
              if (this.employeeDetails.featuredDetails.video4) {
                this.featuredVideo.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.video4,
                  url:
                    this.service.s3bucketurl +
                    this.service.bucketFolder +
                    this.employeeDetails.featuredDetails.video4 +
                    this.service.bucketExtension,
                  thumbnail:
                    this.service.url +
                    "/download/thumbnail.json?fileID=" +
                    this.employeeDetails.featuredDetails.video4
                });
              }
              if (this.employeeDetails.featuredDetails.video5) {
                this.featuredVideo.push({
                  name: "",
                  fileId: this.employeeDetails.featuredDetails.video5,
                  url:
                    this.service.s3bucketurl +
                    this.service.bucketFolder +
                    this.employeeDetails.featuredDetails.video5 +
                    this.service.bucketExtension,
                  thumbnail:
                    this.service.url +
                    "/download/thumbnail.json?fileID=" +
                    this.employeeDetails.featuredDetails.video5
                });
              }
              if (this.featuredVideo[this.featuredVideo.length - 1])
                this.videoUrl = this.featuredVideo[
                  this.featuredVideo.length - 1
                ].url;
            }
            /******************************************************** */
            if (res["result"].feedback) {
              this.feedBack = res["result"].feedback;

              this.feedBack.forEach(element => {
                this.feedBackRating.push(element);
              });
            }
            this.items = this.employeeDetails.skill.split(",");
          } else this.service.apiErrorHandler(res);
        },
        er => {
          this.service.serviceErrorResponce(er);
        }
      );
  }
  dateTransform(date) {
    return new Date(date + "GMT");
  }
  formatLamguageLevel(value) {
    switch (value) {
      case 1:
        return this.translate.instant("weak");
      case 5:
        return this.translate.instant("medium");
      case 10:
        return this.translate.instant("strong");
      default:
        return this.translate.instant("weak");
    }
  }

  checkValueAvilable(value) {
    if (value) {
    try {
      return   this.titlepipe.transform(value);
    } catch (error) {
      return value;
    }
    }
    return this.translate.instant("notspecified");
  }
}
