import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeViewjoblistComponent } from './employee-viewjoblist.component';

describe('EmployeeViewjoblistComponent', () => {
  let component: EmployeeViewjoblistComponent;
  let fixture: ComponentFixture<EmployeeViewjoblistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeViewjoblistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeViewjoblistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
