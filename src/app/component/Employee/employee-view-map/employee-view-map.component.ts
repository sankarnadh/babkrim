import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { MapsAPILoader } from '@agm/core';
import { ServerService } from '../../../service/server.service';
import '../../../../assets/js/js/chat.js'
import{ValidationMessageService} from '../../../service/validation-message.service'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
declare var customealert: any;
declare var google :any
@Component({
  selector: 'app-employee-view-map',
  templateUrl: './employee-view-map.component.html',
  styleUrls: ['./employee-view-map.component.css']
})
export class EmployeeViewMapComponent implements OnInit {
  viewMoreDiv = 0;
  constructor(private router: Router, 
     public config: NgbRatingConfig,
     public service: ServerService, 
     private mapsAPILoader: MapsAPILoader,
     public validationMsg : ValidationMessageService) { 
    config.max=5;
    config.readonly=true
  }
  currentIcon = {
    url: this.service.currentIcon,
   scaledSize: new google.maps.Size(50, 50)
  }
  urgentIcon={
    url: this.service.urgentEmployerIcon,
    scaledSize: new google.maps.Size(50, 50)
  }
  ngOnInit() {
    if (!this.service.isEmployeeLogin())
      this.router.navigate(['/login']);
    this.setCurrentPosition();
    this.mapsAPILoader.load().then(() => {
      this.service.fetchAllJobDetailsForMap(0);
    });

  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 10;


      });
    }
  }
  zoomEvent(event){
    this.zoom=event
  }
  /*****
   * Getting all job list (No filter)
   */
  jobDetails=this.service.jobDetails;
  // fetchAllJobDetailsFromApi() {
  //   this.service.serverRequest('{}', 'jobDetails.json?pageNo=1').subscribe(
  //     (res) => {

  //       if (res['status'].responseCode == 20) {
  //         this.jobDetails = res['result'].jobDetails

  //         let count = this.jobDetails.length;

  //         for (let i = 0; i < count; i++) {
          
  //           this.markers.push({
  //             lat: parseFloat(this.jobDetails[i].latitude),
  //             lng: parseFloat(this.jobDetails[i].longitude),
  //             label: '',
  //             draggable: false,
  //             companyName: this.jobDetails[i].employerDetails.companyName,
  //             rating: this.jobDetails[i].employerDetails.rating,
  //             employerId :this.jobDetails[i].employerDetails.employerId,
  //             companyImg:this.jobDetails[i].employerDetails.profileImage,
  //             fromDate:this.jobDetails[i].fromDate,
  //             jobTitle:this.jobDetails[i].jobTitle,
  //             jobDescription:this.jobDetails[i].jobDescription,
  //             jobPostType:this.jobDetails[i].jobPostType,
  //             noOfVacancy:this.jobDetails[i].noOfVacancy,
  //             hourlyWages:this.jobDetails[i].hourlyWages,
  //             category:this.jobDetails[i].category,
  //             subCategory:this.jobDetails[i].subCategory,
  //             allDetails:this.jobDetails[i]
  //           })

  //         }


  //       }
  //       else
  //         this.service.apiErrorHandler(res)
  //     },
  //     err => {

  //     }
  //   );
  // }
  getEmployerDetailsById(employerId)
  {
    this.service.serverGetRequest('','').subscribe(res=>{

    },
    er=>{
      this.service.serviceErrorResponce(er)
    })
  }
  /*******
   * Map 
   */

  zoom:number=10;
  lat: any = this.service.lat;
  lng: any =  this.service.lng;


  mouseoverEvent(val,index){
    this.lat=val.lat;
    this.lng=val.lng;
    this.zoom=16;
    this.service.markers.forEach((element,key) => {
      if(key==index){
        element.isOpen=true
      }
      else {
        element.isOpen=false
      }
    });
  }
  /*****
   * viewJobList another page
   */
  viewJobList(val) {

    this.service.setStorage('data', JSON.stringify(val))
    this.router.navigate(['/employee_viewjoblist' + '/' + val.jobId])
  }
  jobMoreDetails :any;
  viewMore(value)
{
  this.jobMoreDetails=value;
  this.viewJobList(value)
  // console.log(this.jobMoreDetails)
  // this.viewMoreDiv=0
}
}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  companyName?: String,
  rating?: string,
  employerId?: any,
  fromDate ? :any,
  jobTitle ? :any,
  jobDescription ? :any,
  jobPostType ? :any,
  noOfVacancy? :any,
  hourlyWages ? :any,
  category?:any,
  subCategory?:any,
  companyImg?:any,
  allDetails?:any
  wageMode?:any
  
}