import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeViewMapComponent } from './employee-view-map.component';

describe('EmployeeViewMapComponent', () => {
  let component: EmployeeViewMapComponent;
  let fixture: ComponentFixture<EmployeeViewMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeViewMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeViewMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
