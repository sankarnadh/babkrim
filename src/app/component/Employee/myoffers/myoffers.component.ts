import { ServerService } from '../../../service/server.service';
import { ValidationMessageService } from '../../../service/validation-message.service';

import { ValidationServiceService } from '../../../service/validation-service.service';

import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Response } from '@angular/http';

import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';

import { NgProgress } from 'ngx-progressbar';

import '../../../../assets/js/js/chat.js';

declare var customealert: any;
declare var google: any;
@Component({
  selector: 'app-myoffers',
  templateUrl: './myoffers.component.html',
  styleUrls: ['./myoffers.component.css'],
})

export class MyoffersComponent implements OnInit {
  jobForm;
  offer;

  currency = this.service.defaultCurrency;
  constructor(
    public ngProgress: NgProgress,
    private http: HttpClient,
    public router: Router,
    public config: NgbRatingConfig,
    public validator: ValidationServiceService,
    private formBuilder: FormBuilder, public service: ServerService,
    public validationMsg: ValidationMessageService,
    public activatedRoute: ActivatedRoute,
  ) {
    this.jobForm = this.formBuilder.group
      ({
        'hourlyWages': ['', Validators.compose([Validators.required, ValidationServiceService.numberValidation])],
        'currency': ['', Validators.required],
        'coverLetter': ['', Validators.required],
        'attachment': ['', ''],
        'wageMode': ['', Validators.required],
        'minhour': ['', Validators.required],
        'fixedWages': ['', Validators.required]

      });
    config.max = 5;
    config.readonly = true
  }
  title: any;
  proposalType: number;
  login = false;
  get j() { return this.jobForm.controls }
  currentIcon = {
    url: this.service.currentIcon,
    scaledSize: new google.maps.Size(60, 60)
  }
  urgentIcon = {
    url: this.service.urgentEmployerIcon,
    scaledSize: new google.maps.Size(60, 60)
  }
  standardWage
  ngOnInit() {
    this.service.loader = false;
    this.jobForm.get('wageMode').setValue('hourly');
    const url = this.router.url;
    switch (url) {
      case '/my-offers-employee': {
        this.title = 'my_offers';
        this.proposalType = 1;
        break;
      }
      case '/invite-interview-employee': this.title = 'interview'
        this.proposalType = 2
        break;

    }
    if (!this.service.isEmployeeLogin())
      this.router.navigate(['/login']);                       //Authentication check

    this.offer = JSON.parse(this.service.getStorage('data'))
    this.standardWage=this.offer.jobDetails.jobPostType!='urgent'?this.offer.jobDetails.wageMode=='hourly'?
                this.standardWage : this.offer.jobDetails.fixedStandardWage : 
                this.offer.jobDetails.wageMode=='hourly'?
                this.offer.jobDetails.urgentStandardWage : this.offer.jobDetails.urgentFixedStandardWage
    console.info(this.offer)
    if (this.offer.jobDetails.wageMode == 'fixed') {
      this.jobForm.get('wageMode').setValue('fixed')
      this.jobForm.get('fixedWages').setValue(this.offer.jobDetails.fixedWage)
      this.jobForm.get('hourlyWages').setValue(1)
      this.jobForm.get('minhour').setValue(1)
    }
    else {
      this.jobForm.get('fixedWages').setValue(1)
      this.jobForm.get('hourlyWages').setValue(this.offer.jobDetails.hourlyWages)
      this.jobForm.get('minhour').setValue(1)
    }
    if (!this.offer)
      this.router.navigate(['/home'])
    this.service.getCurrency();                            //Fetch Currency from API
  }
  upparttimefee: number = 0;
  employeeReceive: number = 0;


  public calculateFee() {

    let hourlyFee = this.jobForm.get('hourlyWages').value;
    if (hourlyFee) {
      this.upparttimefee = (hourlyFee * 10) / 100;

      this.employeeReceive = (hourlyFee * 90) / 100;
    }
    else {
      this.upparttimefee = 0;

      this.employeeReceive = 0;
    }


  }

  submitOffer() {
    customealert.loaderShow('html')


    let response: any;
    this.service.serverRequest('', 'contract/approve/' + this.offer.contractId + '.json').subscribe(
      (res: Response) => {
        customealert.loaderHide('html')
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.showSuccess(this.validationMsg.offeraccepted)
          window.history.back()
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide('html')
      }
    );
  }
  fileLoader: any = false;
  proposalAttachments = [];
  counter = 0;
  fileName = [];
  fileEvent(event: any) {
    var count = event.target.files.length;

    if (this.counter + count < 6) {
      for (let i = 0; i < count; i++) {
        const fileSelected: File = event.target.files[i];
        if (!this.service.checkAllowedFiles(event.target.files[i].name.split('.').pop())) {
          this.service.showError(this.validationMsg.invalidFileType)
          return false
        }
        if (fileSelected.size < this.service.maxFileSize) {
          if (fileSelected) {


          }
          let response;
          var reader = new FileReader();
          this.fileLoader = true
          this.ngProgress.start()

          this.service.uploadSingleFile(fileSelected, 'upload.json?fileType=docs')
            .subscribe((res: Response) => {
              this.ngProgress.done()

              response = res;
              this.fileLoader = false;
              this.service.loader = false
              if (response.status.responseCode == 20) {

                this.proposalAttachments.push(response.result.file1);
                this.fileName.push(fileSelected.name)
                this.counter++;
              }
              else {
                this.service.apiErrorHandler(res);

              }
            },
              (err) => {
                this.service.loader = false
                this.fileLoader = false
                this.service.serviceErrorResponce(err)
              });
        } else
          this.service.showError(this.validationMsg.attachmentSizeLimit)
      }
    }
    else
      this.service.showError(this.validationMsg.attachmentLimit)
  }
  /*****
   * Submiting Proposal in to API
   */
  submitError = false;
  submit() {
    this.submitError = true
    if (!this.jobForm.get('hourlyWages').value) {
      return false;
    }
    if (!this.jobForm.get('minhour').value) {
      return false;
    }
    if (this.jobForm.get('minhour').value > 24) {
      return false;
    }
    if (!this.jobForm.get('fixedWages').value) {
      return false;
    }
    if (!this.currency) {
      this.submitError = true
      return false;
    }

    if (this.j.wageMode.value == 'fixed') {

      if (this.jobForm.get('fixedWages').value < this.standardWage) {
        return
      }
    } else {
      if (this.jobForm.get('hourlyWages').value < this.standardWage) {
        return
      }

    }


    if (!this.jobForm.get('coverLetter').valid) {
      this.submitError = true
      return false;
    }

    else {
      this.acceptInvitation();
      this.submitError = false
    }




  }
  deleteFileicon(index) {
    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.proposalAttachments.splice(index, 1);

      this.counter--;
      if (this.counter < 0)
        this.counter = 0
    }

  }
  acceptInvitation() {
    customealert.loaderShow('html')
    let data = {
      'jobId': this.offer.jobDetails.jobId,
      'hourlyWages': this.jobForm.get('hourlyWages').value,
      'currency': this.currency,
      'coverLetter': this.jobForm.get('coverLetter').value,
      'proposalAttachments': this.proposalAttachments,
      'wageMode': this.jobForm.get('wageMode').value,
      'minHour': this.jobForm.get('minhour').value,
      'fixedWage': this.jobForm.get('fixedWages').value

    };

    let response: any;
    this.service.serverRequest(data, 'invitation/approve/' + this.offer.invitationId + '.json').subscribe(
      (res: Response) => {
        customealert.loaderHide('html')
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.showSuccess(this.validationMsg.invitationAcepted)
          customealert.hideModal('Modalinviteinterview')
          this.router.navigate(['/proposals']);
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide('html')
      }
    );
  }
  swaltitle
  text
  rejectProposals() {
    customealert.loaderShow('html');
    let url: any;
    this.offer.contractId ? url = 'contractDetails/offers/reject/' : url = 'invitation/reject/';
    let contractId = this.offer.contractId ? this.offer.contractId : this.offer.invitationId


    this.service.serverDeleteRequest(url + contractId + '.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.router.navigate(['/proposals']);
      } else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))
  }
}
