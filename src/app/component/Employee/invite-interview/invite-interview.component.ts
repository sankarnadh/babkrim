import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators,FormsModule,FormControl} from '@angular/forms';
import {ValidationServiceService} from '../../../service/validation-service.service';
import {ServerService} from '../../../service/server.service';
import {ValidationMessageService} from '../../../service/validation-message.service'

@Component({
  selector: 'app-invite-interview',
  templateUrl: './invite-interview.component.html',
  styleUrls: ['./invite-interview.component.css']
})
export class InviteInterviewComponent implements OnInit {

  constructor( private formBuilder : FormBuilder,public service: ServerService, public validationMsg : ValidationMessageService) { 
    this.jobForm = this.formBuilder.group
    ({
      'hourlyWages': '',
      'currency':['',Validators.required],
      'coverLetter':['',Validators.required],
      'nationality':['',Validators.required],
      'dob':['',Validators.required],
      'country':['',ValidationServiceService.notNullValidation],
      'state':['',ValidationServiceService.notNullValidation],
      'city':['',ValidationServiceService.notNullValidation],
      
    });
  }
  jobForm;
  ngOnInit() {
  }

}
