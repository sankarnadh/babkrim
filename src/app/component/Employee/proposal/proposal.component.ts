import { Component, OnInit } from '@angular/core';
import { ServerService} from '../../../service/server.service'
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router'
import '../../../../assets/js/js/chat.js'
import { ValidationMessageService } from 'src/app/service/validation-message.service';
declare var customealert : any
@Component({
  selector: 'app-proposal',
  templateUrl: './proposal.component.html',
  styleUrls: ['./proposal.component.css']
 
})
export class ProposalComponent implements OnInit {
currentTab:number=1; // for offer 
  constructor(public config: NgbRatingConfig,public service : ServerService,
    private spinner : Ng4LoadingSpinnerService,public router : Router,
    private validationMsg: ValidationMessageService) { 
      config.max = 5;
      config.readonly = true;
    }
    activeRating=[]
  ngOnInit() {
  
  this.getOffers();
  this.getInvitationToInterviewFromApi();
  this.getActiveProposalFromApi();
  this.getSubmitedProposalFromApi()
  }
offers          : any =null; 

gotoEditProposalPage(proposalId)
{
  this.router.navigate(['/editproposal/'+proposalId])
}
  getOffers()
  {
    window.scrollTo(0,0)
    this.service.loader=true 
    this.service.serverGetRequest('','contractDetails/offers.json').subscribe((res)=>{
      this.service.loader=false
      if(res['status'].responseCode==20)
      { 
          this.service.offers=res['result'].offers
          this.service.offers.forEach(element => {
            this.service.offerRating.push(
              element.employerDetails.rating/element.employerDetails.ratingCount
            )
          });
          this.service.offerLength=this.service.offers.length
          this.service.loader=false
      }
      else 
      this.service.apiErrorHandler(res)
    })
  }
  getInvitationToInterviewFromApi()
  {
    window.scrollTo(0,0)
    this.service.loader=true
    this.service.serverGetRequest('','invitationDetails.json').subscribe((res)=>{
      this.service.loader=false
      if(res['status'].responseCode==20)
      { 
          this.service.invitation=res['result'].invitations
          this.service.invitationLength=res['result'].invitations.length
          this.service.loader=false
         
      }
      else 
      this.service.apiErrorHandler(res)
    })
  }
  title: any = 'withdrawproposal';
  text: any = 'cantbeundone'
  tempProposalId:any;
  withDrawProposal(proposalId)
  {
    
    let response: any;
    this.service.serverPutRequest('', 'proposals/withdraw/' + proposalId + '.json').subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.showSuccess(this.validationMsg.success)
          
          if(this.currentTab==3)
          this.getActiveProposalFromApi()
          if(this.currentTab==4)
          this.getSubmitedProposalFromApi()
        }
        else {
          this.service.apiErrorHandler(res);
         
        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }

  activeLength  : number = 0;
  activeProposal: any =null; 
  getActiveProposalFromApi()
  {
    this.service.loader=true
    window.scrollTo(0,0)
    this.service.serverGetRequest('','proposalDetails/active.json?registerType=employee').subscribe((res)=>{
      if(res['status'].responseCode==20)
      { 
          this.activeProposal=res['result'].proposal
          this.activeProposal.forEach(element => {
            if(element.employerDetails.rating!=0 || element.employerDetails.ratingCount!=0)
            this.activeRating.push(
             (+(element.employerDetails.rating)/+(element.employerDetails.ratingCount))
            )
            else 
            this.activeRating.push(0)
          });
         
          this.activeLength=this.activeProposal.length
          this.service.loader=false
      }
      else 
      this.service.apiErrorHandler(res)
    })
  }
  submitedProposal : any =null;
  submitedProposalLength :any =0;
  submitedRating=[]
  getSubmitedProposalFromApi()
  {
    window.scrollTo(0,0)
    this.service.loader=true
    this.service.serverGetRequest('','proposalDetails/submit.json?registerType=employee').subscribe((res)=>{
      if(res['status'].responseCode==20)
      { 
          this.submitedProposal=res['result'].proposal
          this.submitedProposalLength=this.submitedProposal.length
          this.submitedProposal.forEach(element => {
            this.submitedRating.push(
              element.employerDetails.rating/element.employerDetails.ratingCount
            )
          });
          this.service.loader=false
      }
      else 
      this.service.apiErrorHandler(res)
    })
  }
  navigateMsg(receiver, proposal) {
    this.router.navigate(['/message/' + receiver + '/' + proposal])
  }
}
