import { Component, OnInit } from "@angular/core";
import { ServerService } from "../../../service/server.service";
import "../../../../assets/js/js/chat.js";
import { NgbRatingConfig } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { TitleCasePipe } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";
declare var customealert: any;
declare var google: any;
@Component({
  selector: "app-view-company-details",
  templateUrl: "./view-company-details.component.html",
  styleUrls: ["./view-company-details.component.css"]
})
export class ViewCompanyDetailsComponent implements OnInit {
  constructor(
    public router: Router,
    private titlepipe: TitleCasePipe,
    private translate: TranslateService,
    public config: NgbRatingConfig,
    public service: ServerService
  ) {
    config.max = 5;
    config.readonly = true;
  }
  dateTransform(date) {
    return this.service.convertDateToGmt(date);
  }
  ngOnInit() {
    if (!this.service.getStorage("employerTempId"))
      this.router.navigate(["/home"]);
    customealert.loaderShow("html");
    this.getEmployerDetails();
  }
  // currentIcon = {
  //   url: this.service.currentIcon,
  //   scaledSize: new google.maps.Size(60, 60)
  // }
  // urgentIcon = {
  //   url: this.service.urgentEmployerIcon,
  //   scaledSize: new google.maps.Size(60, 60)
  // }
  employer: any = [];
  currentRate = 0;
  feedBack = [];
  feedBackRating = [];
  aboutLimit = 350;
  currentIcon = {
    url: this.service.currentIcon,
    scaledSize: new google.maps.Size(60, 60)
  };
  getEmployerDetails() {
    var empId = this.service.getStorage("employerTempId");

    this.service
      .serverGetRequest("", "employee/employerDetails/" + empId + ".json")
      .subscribe(
        res => {
          setTimeout(() => {
            customealert.loaderHide("html");
          }, 3000);
          if (res["status"].responseCode == 20) {
            this.employer = res["result"];
            if (res["result"].feedback) {
              this.feedBack = res["result"].feedback;
              this.feedBack.forEach(element => {
                this.feedBackRating.push(element.rating);
              });
            }
            this.currentRate = this.employer.rating / this.employer.ratingCount;
          } else this.service.apiErrorHandler(res);
        },
        err => {
          this.service.serviceErrorResponce(err);
        }
      );
  }
  checkValueAvilable(value) {
    if (value) {
      try {
        return this.titlepipe.transform(value);
      } catch (error) {
        return value;
      }
    }
    return this.translate.instant("notspecified");
  }

  getfloatValue(value){
    return parseFloat(value);
  }
  gotoWegsite(url) : void {
    console.log(url.includes('www'))
    if(url.includes('http://') || url.includes('https://')) {
      window.open(url, "_blank")
    }else {
      window.open('http://'+url, "_blank")
    }
    // window.open(url, "_blank");
}
}
