import { Component, OnInit, ElementRef, Input, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { FormBuilder, Validators, FormsModule, FormControl } from '@angular/forms';
import { ValidationServiceService } from '../../../service/validation-service.service';
import { ServerService } from '../../../service/server.service';
import { ValidationMessageService } from '../../../service/validation-message.service';
import { Router } from "@angular/router";
import '../../../../assets/js/js/chat.js';
import { DatePipe } from '@angular/common'
import { DateTimeAdapter } from 'ng-pick-datetime';
import { NgProgress } from 'ngx-progressbar';
import { count } from 'rxjs/operators';
declare var customealert: any;
@Component({
  selector: 'app-education-setup',
  templateUrl: './education-setup.component.html',
  styleUrls: ['./education-setup.component.css']
})
export class EducationSetupComponent implements OnInit {
  today = new Date()
  bottonClick = 0
  signupForm: any;
  motherTongue: any = [];
  identityTypes: any;
  education: any;
  levels: any;
  step1btn: boolean;
  btnValid: any;
  educationCertificate = [];
  dwnld_file: any = false;
  fileName = [];

  mother_tongueId: any = '';
  id_typeId: any = '';
  educationId: any = '';
  tongue_1Id: any = '';
  reading_1Id: any = '';
  conversation_1Id: any = '';
  tongue_2Id: any = '';
  reading_2Id: any = '';
  conversation_2Id: any;
  initialLoader: string;
  fileLoader: any = false;
  public language = []
  public languageKey = 0
  constructor(
    public ngProgress: NgProgress,
    public dateTimeAdapter: DateTimeAdapter<any>,
    private datePipe: DatePipe,
    private el: ElementRef,
    public validationMsg: ValidationMessageService,
    public router: Router,
    private formBuilder: FormBuilder, public service: ServerService
  ) {
    this.dateTimeAdapter.setLocale('en-in');
    this.signupForm = this.formBuilder.group(
      {
        'user_photo': ['', Validators.required],
        'mother_tongue': ['', Validators.required],
        'id_type': ['', ValidationServiceService.notNullValidation],
        'id_no': ['', ValidationServiceService.notNullValidation],
        'id_date': ['', Validators.required],
        'education': ['', ValidationServiceService.notNullValidation],
        'collage': ['', ValidationServiceService.notNullValidation],
        'education_date': ['', Validators.required],
        'upload_file': ['', Validators.required],
        'tongue_1': ['', ValidationServiceService.notNullValidation],
        'reading_1': ['', ValidationServiceService.notNullValidation],
        'conversation_1': ['', ValidationServiceService.notNullValidation],
        'tongue_2': ['', Validators.required],
        'reading_2': ['', Validators.required],
        'conversation_2': ['', ValidationServiceService.notNullValidation]
      });
  }

  deleteFileicon(index) {

    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.educationCertificate.splice(index, 1);
      this.fileCounter--;
      if (this.fileCounter < 0)
        this.fileCounter = 0
    }


  }


  minDate: any;

  genderId: any;
  ngOnInit() {
    var minDate = new Date();
    var dd: any = '01'
    var mm: any = '01'
    var yyyy: any = minDate.getFullYear() - 70;
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }

    this.minDate = yyyy + '-' + mm + '-' + dd;

    if (!this.service.isEmployeeLogin())         //Authantication check
      this.router.navigate(['/login']);
    this.service.checkLogin(); //To change the login header
    customealert.loaderShow('html')
    this.btnValid = "not-allowed"                 //Button Style
    this.initialLoader = "loadinginput"           //Initial Loader
    this.getLevel3();                           //Comon details of level3
    //this.service.verificationCheck();           //Email verification check

    this.service.loader = false
  }
  prMothertoungue: any;
  prIdType: any;
  prEducation: any;
  prTongue_1: any;
  prReading1: any;
  prConversation_1: any;
  prTongue_2: any;
  prReading_2: any;
  prConversation_2: any;
  prcollegeName: any;
  prIdno: any;
  prgraduationDate: any;
  pridIssueDate: any;
  fileID: any;

  previousLevel3() {
    let response;
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'profile/profileDetails.json?registerType=' + this.service.getStorage('registerType') + '&registerLevel=3').subscribe(
      (res: Response) => {
        response = res;
        customealert.loaderHide('html')

        if (response.status.responseCode == "20") {

          this.prMothertoungue = parseInt(response.result.motherTongue.id);
          this.prIdType = parseInt(response.result.idType.id);
          this.prEducation = parseInt(response.result.education.id);
          this.prTongue_1 = response.result.firstOtherLanguage.id != 0 ? parseInt(response.result.firstOtherLanguage.id) : null;
          this.prReading1 = response.result.firstOtherLanguageReadingWritingLevel.id != 0 ? parseInt(response.result.firstOtherLanguageReadingWritingLevel.id) : null;
          this.prConversation_1 = response.result.firstOtherLanguageConversationLevel.id != 0 ? parseInt(response.result.firstOtherLanguageConversationLevel.id) : null;

          this.prTongue_2 = response.result.secondOtherLanguage.id != 0 ? parseInt(response.result.secondOtherLanguage.id) : null;
          this.prReading_2 = response.result.secondOtherLanguageReadingWritingLevel.id != 0 ? parseInt(response.result.secondOtherLanguageReadingWritingLevel.id) : null;
          this.prConversation_2 = response.result.secondOtherLanguageConversationLevel.id != 0 ? parseInt(response.result.secondOtherLanguageConversationLevel.id) : null;

          this.motherTongue.forEach(element => {
            if (element.id == this.prTongue_1 &&this.prTongue_1 ) {

              this.language[0] = {
                'langauge': element.language,
                'read': this.prReading1,
                'speak': this.prConversation_1,
                'index': 0
              }
            }
            if (element.id == this.prTongue_2 &&this.prTongue_2) {
              this.language[1] = {
                'langauge': element.language,
                'read': this.prReading_2,
                'speak': this.prConversation_2,
                'index': 1
              }
            }
          });
          this.languageKey = this.language.length - 1 < 0 ? 0 : 1;

          if (response.result.educationCertificate) {
            this.dwnld_file = true//to display file icon
            let counter = response.result.educationCertificate.length;
            this.fileCounter = counter;
            for (let i = 0; i < counter; i++) {
              this.educationCertificate.push(response.result.educationCertificate[i].fileId)
              this.fileName.push(response.result.educationCertificate[i].seedName);
            }
          }
          else {
            this.fileID = [];
            this.fileName = [];
          }
          this.prcollegeName = response.result.collegeName;
          this.prIdno = String(response.result.idNo);
          this.prgraduationDate = response.result.graduationDate;
          this.pridIssueDate = response.result.idIssueDate;
          this.mother_tongueId = parseInt(response.result.motherTongue.id);
          this.id_typeId = parseInt(response.result.idType.id);
          this.educationId = parseInt(response.result.education.id);

          // if (response.result.firstOtherLanguage.id != 0) {
          //   this.tongue_1Id = parseInt(response.result.firstOtherLanguage.id);
          //   this.conversation_1Id = parseInt(response.result.firstOtherLanguageConversationLevel.id);
          //   this.prConversation_1 = parseInt(response.result.firstOtherLanguageConversationLevel.id)
          //   this.reading_1Id = parseInt(response.result.firstOtherLanguageReadingWritingLevel.id);
          //   this.prReading1 = parseInt(response.result.firstOtherLanguageReadingWritingLevel.id);
          // }
          // if (response.result.secondOtherLanguage.id != 0) {
          // this.tongue_2Id = parseInt(response.result.secondOtherLanguage.id);
          //   this.conversation_2Id = parseInt(response.result.secondOtherLanguageConversationLevel.id);
          //   this.prReading_2 = parseInt(response.result.secondOtherLanguageReadingWritingLevel.id);
          //   this.reading_2Id = parseInt(response.result.secondOtherLanguageReadingWritingLevel.id);
          //   this.prConversation_2 = parseInt(response.result.secondOtherLanguageConversationLevel.id)
          // }
        }
        else {
          this.service.apiErrorHandler(res);
         
        }
      },
      err => {
        this.service.serviceErrorResponce(err)

      }
    );
  }
  conversation_2Select(item: any) {
    if (item)
      this.conversation_2Id = item.id
  }
  conversation_2Deselect() {
    this.conversation_2Id = ''
  }
  reading_2Deselect() {
    this.reading_2Id = '';
  }
  reading_2Select(item: any) {
    if (item)
      this.reading_2Id = item.id
  }
  tongue_2Deselect() {
    this.tongue_2Id = '';
    this.prReading_2 = null
    this.reading_2Id = ''
    this.conversation_2Id = ''
    this.prConversation_2 = null
  }

  tongue_2Select(item: any) {
    if (item)
      this.tongue_2Id = item.id;
  }
  conversation_1Deselect() {
    this.conversation_1Id = '';
  }
  conversation_1Select(item: any) {
    if (item)
      this.conversation_1Id = item.id;
  }
  onMotherToungueItemDeselect() {
    this.mother_tongueId = '';
  }
  onMotherToungueItemSelect(item: any) {
    if (item)
      this.mother_tongueId = item.id;
  }
  onIdTypeItemDeselect() {
    this.id_typeId = '';

  }
  onIdTypeItemSelect(item: any) {
    if (item)
      this.id_typeId = item.id;
  }
  onEducationItemDeselect() {
    this.educationId = ''
  }
  onEducationItemSelect(item: any) {
    if (item) {
      this.educationId = item.id
      if (this.prEducation == '9' || this.prEducation == '8') {
        this.prcollegeName = ''
        this.prgraduationDate = ''
      }
    }
  }
  tongue_1Deselect() {
    this.tongue_1Id = '';
    this.prReading1 = null
    this.reading_1Id = ''
    this.prConversation_1 = null
    this.conversation_1Id = ''
  }
  tongue_1Select(item: any) {
    if (item)
      this.tongue_1Id = item.id
  }
  reading_1Deselect() {
    this.reading_1Id = '';
  }
  reading_1Select(item: any) {
    if (item)
      this.reading_1Id = item.id;
  }
  isBtnValid() {
    if (this.mother_tongueId != '')
      if (this.id_typeId != '')
        if (this.signupForm.get('id_no').valid)
          if (this.signupForm.get('id_date').valid)
            if (this.educationId != '')
              if (this.signupForm.get('collage').valid)
                if (this.signupForm.get('education_date').valid)

                // if(this.signupForm.get('tongue_1').valid)
                //   if(this.signupForm.get('reading_1').valid)
                //    if(this.signupForm.get('conversation_1').valid)
                //     if(this.signupForm.get('tongue_2').valid)
                //      if(this.signupForm.get('reading_2').valid)
                //       if(this.signupForm.get('conversation_2').valid)
                {
                  this.btnValid = "pointer"
                  return false;
                }
    this.btnValid = "not-allowed"
    return true;
  }
  getLevel3() {
    this.service.serverGetRequest('', 'custom/employee/level/3.json').subscribe(
      (res) => {
        this.initialLoader = ""

        if (res['status'].responseCode == 20) {
          this.motherTongue = res['result'].languages;
          this.identityTypes = res['result'].identityTypes;
          this.education = res['result'].educations;
          this.levels = res['result'].lanLevels;
          this.previousLevel3();                       //Previously submited data If any
        }
        else {
          this.service.apiErrorHandler(res);
         
        }

      },
      err => {
        //this.initialLoader=""
        this.service.serviceErrorResponce(err)

      }
    );
  }
  removeLanguage(index) {
    this.language.splice(index, 1);

    if (this.languageKey == 0) {
      this.prTongue_1 = null
      this.prReading1 = 1;
      this.prConversation_1 = 1

    }
    else {

      this.prTongue_2 = null
      this.prReading_2 = 1;
      this.prConversation_2 = 1

    }
    if (!this.prTongue_1)
      this.languageKey = 0
    else
      if (this.prTongue_1 && !this.prTongue_2)
        this.languageKey = 1

  }
  addLanguage() {
    let flag = 0
    let temp = []

    this.motherTongue.forEach(element => {
      if (element.id == this.prTongue_1 && this.languageKey == 0) {
        if (this.language.length == 0) {
          this.language[0] = {
            'langauge': element.language,
            'read': this.prReading1,
            'speak': this.prConversation_1,
            'index': 0
          }
        }
        if (this.language[this.language.length - 1].index == 0) {
          this.language[0] = {
            'langauge': element.language,
            'read': this.prReading1,
            'speak': this.prConversation_1,
            'index': 0
          }
        }
        else {
          flag = 1;
          temp = this.language[1];
          this.language[1] = {
            'langauge': element.language,
            'read': this.prReading1,
            'speak': this.prConversation_1,
            'index': 0
          }
        }

      }
      if (element.id == this.prTongue_2 && this.languageKey == 1) {
        this.language[1] = {
          'langauge': element.language,
          'read': this.prReading_2,
          'speak': this.prConversation_2,
          'index': 1
        }
      }
    });
    if (flag == 1) {
      this.language.reverse()
    }
    // if(this.language.length==0){

    //   return;
    // }
    this.languageKey = 1

    if(!this.prTongue_1)
      this.languageKey = 0

  }


  educationSetupApi(val) {

    if (!this.signupForm.get('mother_tongue').valid) {
      this.bottonClick = 1
      return false
    }
    else
      this.bottonClick = 0
    if (!this.signupForm.get('id_type').value) {
      this.bottonClick = 1
      return false
    }
    else
      this.bottonClick = 0
    if (!this.signupForm.get('id_no').valid) {
      this.bottonClick = 1
      return false
    }
    else
      this.bottonClick = 0

    if (!this.signupForm.get('id_date').valid) {
      this.bottonClick = 1
      return false
    }
    else
      this.bottonClick = 0

    if (!this.signupForm.get('education').value) {
      this.bottonClick = 1
      return false
    }
    else
      this.bottonClick = 0
    if (this.prEducation != 9 && this.prEducation != 8)
      if (!this.signupForm.get('collage').valid) {
        this.bottonClick = 1
        return false
      }
      else
        this.bottonClick = 0
    if (this.prEducation != 9 && this.prEducation != 8)
      if (!this.signupForm.get('education_date').valid) {
        this.bottonClick = 1
        return false
      }
      else
        this.bottonClick = 0
    // if (!this.prTongue_1) {
    //   this.service.showError(this.validationMsg.other_langInvalid)
    //   this.bottonClick = 1
    //   return false
    // }
    // else
    //   this.bottonClick = 0

    if (this.prTongue_1)
      if (!this.prReading1) {
        this.service.showError(this.validationMsg.r_wInvalid)
        this.bottonClick = 1
        return false
      }
      else
        this.bottonClick = 0

    if (this.prTongue_1)
      if (!this.prConversation_1) {
        this.service.showError(this.validationMsg.conversation_levelInvalid)
        this.bottonClick = 1
        return false
      }
      else
        this.bottonClick = 0
    if (this.prTongue_2)
      if (!this.prReading_2) {
        this.bottonClick = 1
        return false
      }
      else
        this.bottonClick = 0
    if (this.prTongue_2)
      if (!this.prConversation_2) {
        this.bottonClick = 1
        return false
      }
      else
        this.bottonClick = 0
    if (this.prTongue_2 != '' && this.prTongue_2!=undefined && this.prTongue_2!=null) {
      if (this.prTongue_2 == this.prTongue_1) {
        console.log(this.prTongue_2+'  '+this.prTongue_1)
        this.service.showError(this.validationMsg.samesecondLanguage)
        this.bottonClick = 1
        return
      }
      else
        this.bottonClick = 0
    }

    if((this.prTongue_2==this.mother_tongueId) || this.prTongue_1==this.mother_tongueId) {
      this.bottonClick=1;
      this.service.showError(this.validationMsg.samemotherToungueandotherLang)
      return 
    }else {
      this.bottonClick=0;
    }

    customealert.loaderShow("html")
    let data = {
      'motherTongue': this.mother_tongueId,
      'idType': this.id_typeId,
      'idNo': this.signupForm.get('id_no').value,
      'idIssueDate': this.datePipe.transform(this.signupForm.get('id_date').value, 'yyyy-MM-dd'),
      'education': this.educationId,
      'collegeName': this.signupForm.get('collage').value ? this.signupForm.get('collage').value : ' ',
      'graduationDate': this.signupForm.get('education_date').value ? this.datePipe.transform(this.signupForm.get('education_date').value, 'yyyy-MM-dd') : '',
      'firstOtherLanguage': this.prTongue_1,
      'firstOtherLanguageReadingWritingLevel': this.prReading1,
      'firstOtherLanguageConversationLevel': this.prConversation_1,
      'secondOtherLanguage': this.prTongue_2 ? this.prTongue_2 : '',
      'secondOtherLanguageReadingWritingLevel': this.prReading_2 ? this.prReading_2 : null,
      'secondOtherLanguageConversationLevel': this.prConversation_2 ? this.prConversation_2 : null,
      'educationCertificate': this.educationCertificate

    };

    let response: any;

    this.service.serverRequest(data, 'register.json?registerType=' + this.service.getStorage('registerType') + '&registerLevel=3&deviceMode=web').subscribe(
      (res: Response) => {

        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          if (val == '1') {
            this.service.showSuccess(this.validationMsg.success);
            // this.router.navigate(['/employee_home']);
          }
          else {
            if (this.service.getStorage('registerLevel') != '0')
              this.service.setStorage('registerLevel', '4');
            this.router.navigate(['/employee_profetionalsetup']);
          }
        }
        else {

          this.service.apiErrorHandler(res);
          
        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }
  //level2 employee 
  previous() {

    this.router.navigate(['/employee_accountsetup'])
  }
  /**
   * 
   * educationCertificate Upload on file change 
   */
  fileCounter: any = 0;
  fileEvent(event: any) {


    let response;
    let fileSelected;
    var count = event.target.files.length
    if (this.fileCounter + count < 6) {
      for (let i = 0; i < count; i++) {
        fileSelected = event.target.files[i];
        if (!this.service.checkAllowedFiles(event.target.files[i].name.split('.').pop())) {
          this.service.showError(this.validationMsg.invalidFileType)
          return false
        }
        if (fileSelected.size < this.service.maxFileSize) {
          if (fileSelected) {
            this.fileLoader = true;

          }
          this.dwnld_file = true
          this.ngProgress.start()


          this.service.uploadMultipleFile(fileSelected, 'upload.json?fileType=docs', this.fileCounter + 1)
            .subscribe((res: Response) => {
              this.ngProgress.done()


              response = res;
              this.fileLoader = false;
              this.service.loader = false

              if (response.status.responseCode == 20) {
                this.educationCertificate.push(response.result.file1);
                this.fileName.push(fileSelected.name)
                this.fileCounter++;
              }
              else {
                this.service.apiErrorHandler(res);

              }
            },
              (err) => {
                this.ngProgress.done()
                this.service.loader = false
                this.fileLoader = false;
                this.service.serviceErrorResponce(err)
              });
          // }

        } else
          this.service.showError(this.validationMsg.attachmentSizeLimit)
      }
    }
    else {
      this.service.showError(this.validationMsg.attachmentLimit)
    }

  }

}
