import { Component, AfterViewChecked, ElementRef, ViewChild, OnInit, Testability } from '@angular/core';
import '../../../assets/js/js/chat.js';
import { Router, ActivatedRoute } from '@angular/router'
import { ServerService } from '../../service/server.service'
import { WebsocketService } from '../../service/websocket.service'
import { ChatService } from '../../service/chat/chat-service.service'
import { UploadEvent, UploadFile } from 'ngx-file-drop';
import { NgProgress } from 'ngx-progressbar';
declare var customealert: any;
declare var $;
declare var Jquery;
declare var updateChat;
@Component({
  selector: 'app-employeemessage',
  templateUrl: './employeemessage.component.html',
  styleUrls: ['./employeemessage.component.css'],
  providers: [WebsocketService, ChatService]
})
export class EmployeemessageComponent implements OnInit, AfterViewChecked {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  flag = 0
  constructor(public router: Router,
    public ngProgress: NgProgress,
    public queryParam: ActivatedRoute,
    public service: ServerService,
    public ws: WebsocketService,
    public chatService: ChatService) {

  }
  receiverId = null

  proposalId = null

  ngOnInit() {
    this.service.messageBadgeIcon = 0;
    this.service.loader = true
    this.chatService.chatRecentContact()
    this.chatService.chatContactList()
    this.queryParam.params.subscribe((res) => {

      if (res['receiver'])
        this.chatService.receiverId = res['receiver'];

      if (res['proposal'])
        this.chatService.proposalId = res['proposal']

    })
    if (this.chatService.receiverId && this.chatService.proposalId) {
      this.chatService.flag = 1
      this.chatService.getChatHistory(this.chatService.receiverId, this.chatService.proposalId)
      //this.chatBox()
    }

  }


  ngAfterViewChecked() {
    this.chatService.restartConnection()
  }

  message = []
  messageSendCount = 0
  receiveMsgCount = 0
  failed = false


  scrollTopPosition
  //Recent chat




  dwnld_file = false;
  fileLoader: any;
  fileName
  fileUploadLoader = false;

  public files: UploadFile[] = [];
  public mystyle = { height: '100px' };
  public dropped(event: UploadEvent) {
    this.files = event.files;
    for (const droppedFile of event.files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // Here you can access the real file

          let response;
          const fileSelected: File = file;
          this.ngProgress.start()
          this.service.uploadSingleFile(fileSelected, 'upload.json?fileType=docs')
            .subscribe((res: Response) => {
              this.ngProgress.done()
              response = res;
              // this.fileLoader='';
              if (response.status.responseCode == 20) {
                //  this.profileImage=response.result.file1;
                //  this.profileImage=response.result.file1;
                console.log(res['result'])
                //this.service.prfImg=this.service.url+"/download/profile.json?fileID="+this.profileImage+"&token="+this.service.getStorage('token')
              }
              else {
                this.service.apiErrorHandler(res);

              }
            },
              (error) => {

              });


        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(fileEntry);
      }
    }
  }

  public fileOver(event) {
    $('.drop-zone').css('height', '100px')
    $('.content[_ngcontent-c4]').css('height', '100px')

    console.log(event);
  }

  public fileLeave(event) {
    $('.drop-zone').css('height', '100px')
    $('.content[_ngcontent-c4]').css('height', '100px')
    // console.log(event);
  }
}


interface FileSystemEntry {
  name: string,
  isDirectory: boolean
  isFile: boolean
}

interface FileSystemEntryMetadata {
  modificationTime?: Date,
  size?: number
}
interface FileSystemDirectoryReader {
  readEntries(
    successCallback: (result: FileSystemEntry[]) => void,
    errorCallback?: (error: DOMError) => void,
  ): void
}

interface FileSystemFlags {
  create?: boolean
  exclusive?: boolean
}

interface FileSystemDirectoryEntry extends FileSystemEntry {
  isDirectory: true
  isFile: false
  createReader(): FileSystemDirectoryReader
  getFile(
    path?: USVString,
    options?: FileSystemFlags,
    successCallback?: (result: FileSystemFileEntry) => void,
    errorCallback?: (error: DOMError) => void,
  ): void
  getDirectory(
    path?: USVString,
    options?: FileSystemFlags,
    successCallback?: (result: FileSystemDirectoryEntry) => void,
    errorCallback?: (error: DOMError) => void,
  ): void
}

interface FileSystemFileEntry extends FileSystemEntry {
  isDirectory: false
  isFile: true
  file(callback: (file: File) => void): void
}
