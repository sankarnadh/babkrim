import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeemessageComponent } from './employeemessage.component';

describe('EmployeemessageComponent', () => {
  let component: EmployeemessageComponent;
  let fixture: ComponentFixture<EmployeemessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeemessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeemessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
