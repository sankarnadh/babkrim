import { Component, OnInit, ElementRef, } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { Router } from "@angular/router";
import { FormBuilder, Validators, FormsModule, FormControl, MaxLengthValidator } from '@angular/forms';
import { ValidationServiceService } from '../../service/validation-service.service';
import { ServerService } from '../../service/server.service';
import { ValidationMessageService } from '../../service/validation-message.service'
import '../../../assets/js/js/chat.js';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Alert } from 'selenium-webdriver';
declare var customealert: any;
declare var require: any;
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: any;
  pwdmissmatch: any;
  phoneNumber: any ;
  timezonesjson = require('angular-timezone/timezones.json');
  constructor(private http: HttpClient,
    private el: ElementRef, public snackBar: MatSnackBar,
    private router: Router, private formBuilder: FormBuilder,
    public service: ServerService,
    public validationMsg: ValidationMessageService
  ) {
    this.signupForm = this.formBuilder.group({
      'name': ['', Validators.compose([Validators.required, Validators.maxLength(50), ValidationServiceService.nameValidator])],
      'email': ['', Validators.compose([ValidationServiceService.emailValidator, Validators.maxLength(50)])],
      'mobile': ['', Validators.compose([Validators.required, Validators.minLength(10),Validators.maxLength(15)])],
      'password': ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20)])],
      'password_confirm': ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20)])],
      'choice': ['', Validators.required],
      'timezone': ['', Validators.required]
    });

    if (!this.signupForm.get('choice').value)
      this.signupForm.get('choice').value = "employee"


  }
  validateInputKey(event) {

    if (event.key == '`' || event.key == '!' || event.key == '@' || event.key == '#' || event.key == '$' || event.key == '%' || event.key == '^' || event.key == '&' || event.key == '*' || event.key == '(' || event.key == ')' || event.key == '_' || event.key == '+') { event.preventDefault(); }
    else
      if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 108 && event.keyCode <= 222) {
        event.preventDefault();
      }
    // if (!this.phoneNumber)
    //   this.phoneNumber = '+91'
    // this.phoneNumber.replace(/\s/g, "")

  }
  isdisable() {
    if (this.signupForm.get('name').valid)
      if (this.signupForm.get('email').valid)
        if (this.signupForm.get('mobile').valid)
          if (this.signupForm.get('password').valid)
            if (this.signupForm.get('password').valid)
              if (this.signupForm.get('password_confirm').valid)
                //if(this.signupForm.get('choice').valid)
                return false
              else return true;
    return true;
  }
  emailExists = false
  timeZones
  myTimeZone: any = 0
  ngOnInit() {
    this.service.setStorage('timeZone', '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi')
   
    if (this.service.getStorage('timeZone'))
      this.myTimeZone = this.service.getStorage('timeZone');
    this.timeZones = this.timezonesjson
    this.service.checkLogin(); //To change the login header

    if (this.service.isEmployerLogin())
      this.router.navigate(['/employer_home']);

    if (this.service.isEmployeeLogin())
      this.router.navigate(['/employee_home']);
  }
  httpOptions = {
    headers: new HttpHeaders({

      'Authorization': 'my-auth-token',
      'Access-Control-Allow-Origin': '*'
    })
  };
  demo(val) {
    console.log(val)
  }

  resetError() {
    this.pwdmissmatch = ''
  }
  staticAlertClosed = false;
  private _success = new Subject<string>();
  submit = false
  signup() {
    // let mobile = this.phoneNumber.split('+');
    // this.submit = true;
    // console.log(mobile[1].length)

    let status, result, message, responseCode, responseStatus;
    let data = {
      "fullName": HTMLInputElement = this.el.nativeElement.querySelector('#name').value,
      "email": HTMLInputElement = this.el.nativeElement.querySelector('#email').value,
      "mobNo": '91'+this.phoneNumber,
      "userName": HTMLInputElement = this.el.nativeElement.querySelector('#email').value,
      "password": HTMLInputElement = this.el.nativeElement.querySelector('#password').value,
      "confirmPassword": HTMLInputElement = this.el.nativeElement.querySelector('#password_confirm').value,
      "localTimeZone": this.myTimeZone,
      "iosDeviceId": '',
      "androidDeviceId": '',
      "deviceType": 'web'

    };

    if (this.signupForm.get('password').value == this.signupForm.get('password_confirm').value) {
    this.pwdmissmatch = ''
      customealert.loaderShow("html")
      let choice = this.signupForm.get('choice').value


      this.service.setStorage('registerType', choice);

      this.service.serverRequest(data, 'register.json?registerType=' + choice + '&registerLevel=1&deviceMode=web').subscribe(
        (res: Response) => {
          customealert.loaderHide("html")
          result = res['result'];
          this.service.setStorage('mobile_no', this.signupForm.get('mobile').value)
          this.service.setStorage('mobileNo', this.signupForm.get('mobile').value)
          this.service.setStorage('email', this.signupForm.get('email').value)
          responseCode = res['status']['responseCode'];
          responseStatus = res['status']['responseStatus'];
          if (responseCode == 20 && responseStatus == 'Success') {
            this.service.checkLogin(); //To change the login header
            this.service.logoutClick = 0
            this.service.setStorage('userName', this.el.nativeElement.querySelector('#name').value); //User First Name 

            this.service.userName = this.service.getStorage('userName');
            this.service.showMenu = false
            this.service.setStorage('registerLevel', 2);
            this.service.setStorage('token', res['result']['token'])
            if (choice == 'employer') {
              this.service.setStorage('employer_login', 'true');
              this.service.setStorage('employerType','company')
              this.router.navigate(['/message-verification']);

              // this.router.navigate(['/employer_companydetails']);
            }
            else {
              this.service.setStorage('employee_login', 'true');
             
              this.router.navigate(['/message-verification']);
              // this.router.navigate(['/employee_accountsetup']);
            }
            
          }
          else {
            let msg = res['result']['message'];
            if (msg == 'Email already exists.') {
              this.emailExists = true;
              customealert.emailExist()
              // setTimeout(() => this.emailExists = false, 3000);

            }

            else
              this.service.showError(msg)
          }

        },
        err => {
          this.service.serviceErrorResponce(err)
        }
      );
    }
    else {
      this.pwdmissmatch = this.validationMsg.level1PasswordNotMatch
      //this.el.nativeElement.querySelector('#pwdmissmatch').html="Password and confirm password do not match"
    }
  }

}
