import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { ValidationServiceService } from '../../service/validation-service.service';
import { ValidationMessageService } from '../../service/validation-message.service';

import '../../../assets/js/js/chat.js';
import { ActivatedRoute } from '@angular/router';
declare var customealert: any;
declare var require: any;
@Component({
  selector: 'app-user-data-settings',
  templateUrl: './user-data-settings.component.html',
  styleUrls: ['./user-data-settings.component.css']
})
export class UserDataSettingsComponent implements OnInit {
  hide = true;
  timezonesjson = require('angular-timezone/timezones.json');
  constructor(private activated: ActivatedRoute, public service: ServerService, public validation: ValidationServiceService, public validationMsg: ValidationMessageService) { }
  timeZones
  accountHolder: any = this.service.getStorage('userName');
  accountNumber: any;
  email: any = this.service.getStorage('email');
  paymentType: any;
  phoneNumber: any = this.service.getStorage('mobileNo');
  ifscCode: any;
  myTimeZone = 0;
  tab = 1;
  regexNumberAndCharOnly = '[^A-Za-z0-9]+';

  ngOnInit() {
    this.activated.params.subscribe(res => {
      if (!res['tab'])
        this.tab = 1;
      else
        this.tab = res['tab'];

    })
    this.getLevel4();
    if (this.service.isEmployeeLogin())
      this.loadRegisterLevel();
    this.timeZones = this.timezonesjson
    if (this.service.getStorage('timeZone'))
      this.myTimeZone = parseInt(this.service.getStorage('timeZone'));

    this.getBankDetails();
    this.getWalletHistory();
    // this.getTransactionHistory()
  }
  paymentDetails = []
  getBankDetails() {
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'payment/bankDetails.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.paymentDetails = res['result'].paymentDetails

        this.service.paymentDetails = this.paymentDetails
        if (this.service.paymentDetails.length > 0) {
          this.accountNumber = (this.service.paymentDetails[0].accountNumber)
          this.ifscCode = this.service.paymentDetails[0].routingNumber
          this.accountHolder=this.service.paymentDetails[0].accountHolderName
        }
      } else
        this.service.apiErrorHandler(res)
    },
      err => {
        this.service.serviceErrorResponce(err)
      })
  }
  updateTimeZone() {
    if (this.myTimeZone != 0) {
      let data = {
        'timeZone': this.myTimeZone
      }
      this.service.serverPutRequest(data, 'settings.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
        if (res['status'].responseCode == 20) {
          this.service.setStorage('timeZone', this.myTimeZone)
          this.service.showSuccess('Timezone Updated')
        }
        else
          this.service.apiErrorHandler(res)
      })
    }
    else {
      this.service.showError('Invalid Timezone')
    }
  }
  button = 0
  resetBankDetails(){
    if(this.paymentDetails.length>0){
    this.accountHolder= this.paymentDetails[0].accountHolderName;
    this.ifscCode= this.paymentDetails[0].routingNumber;
    this.accountNumber= this.paymentDetails[0].accountNumber;
    } else {
    this.accountHolder=null;
    this.ifscCode=null;
    this.accountNumber=null;
    }
  }
  addPayment(paymentType) {
         if (!this.accountHolder || !this.ifscCode || !this.accountNumber || (this.regexNumberAndCharOnly && this.ifscCode.match(this.regexNumberAndCharOnly))) {
      
      this.button = 1;
      return false
    }
    let data = {
      "accountHolderName": this.accountHolder,
      "accountNumber": this.accountNumber,

      "paymentType": paymentType,

      "routingNumber": this.ifscCode
    }
    customealert.loaderShow('html')
    
  
    if(this.paymentDetails.length != 0){
      this.editBankDetails(data)
      return false
    
    }
    this.service.serverRequest(data, 'payment/bankDetails.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')

      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.resetBankDetails()
        this.getWalletHistory();
        this.getBankDetails();
        this.getTransactionHistory()

        customealert.hideModal('modalpayment')
      }
      else {
        this.service.apiErrorHandler(res)
      }
    },
      err => {
        this.service.serviceErrorResponce(err)
        customealert.hideModal('modalpayment')

      })


  }
  editBankDetails(data){
    this.service.serverPutRequest(data, '/payment/bankDetails/'+this.paymentDetails[0].paymentDetailsId+'.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')

      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.resetBankDetails()
        this.getWalletHistory();
        this.getBankDetails();
        this.getTransactionHistory()

        customealert.hideModal('modalpayment')
      }
      else {
        this.service.apiErrorHandler(res)
      }
    },
      err => {
        this.service.serviceErrorResponce(err)
        customealert.hideModal('modalpayment')

      })
  }
  requestPayment(paymentType) {
    let data = {
      "amount": this.service.walletAmount,
      "paymentType": paymentType,

    }
    customealert.loaderShow('html')
    this.service.serverRequest(data, 'payment/request.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')

      if (res['status'].responseCode == 20) {
        this.service.walletAmount = null
        this.service.showSuccess(this.validationMsg.success)
        customealert.hideModal('modalwalletadd')
        customealert.hideAllModal();
        this.getWalletHistory()
        this.getTransactionHistory()
      }
      else {
        this.service.apiErrorHandler(res)
      }
    },
      err => {
        this.service.serviceErrorResponce(err)
        customealert.hideModal('modalwalletadd')

      })

  }

  getWalletHistory() {
    customealert.loaderShow('html')
    this.service.serverGetRequest('', '/payment/wallet/history.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.paymentRequestList = res['result'].walletDetails
        customealert.hideAllModal();
        // this.accountNumber=this.service.paymentRequestList
      } else
        this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
  getTransactionHistory() {
    customealert.loaderShow('html')
    this.service.serverGetRequest('', '/payment/request.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        customealert.hideAllModal();
        this.service.walletDetailsList = res['result'].paymentDetails
      } else
        this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
  prExp: any = 0;
  prDisability: any = 0;
  prTypeofdisability;
  prGeneralspec;
  prSpec;
  prMorespec;
  prMinwage;
  prCurrency = this.service.defaultCurrency;
  prMaxwrk = 8;
  prMinwrk = 1;
  prOtherwrk = 0;
  prSms = 0;
  prVoice = 0;

  fileName = [];
  dwnld_file: any = false;
  categoryId = null;
  subcateId = null;;
  moresubId = null;;
  skillModal = []
  prHeadline;
  tempSpecialization = []
  experienceCertificate = []
  availability = 'Any'
  loadRegisterLevel() {
    this.tempSpecialization = []
    this.experienceCertificate = []
    this.fileName = []
    this.skillModal = []
    let response;
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'profile/profileDetails.json?registerType=' + this.service.getStorage('registerType') + '&registerLevel=4').subscribe(
      (res: Response) => {
        response = res;
        customealert.loaderHide('html')
        let result = response.result;

        if (response.status.responseCode == "20") {
          if (!result)
            return false;
          this.prExp = result.yearOfExperience;//Previous Experience
          this.prDisability = result.disabilityStatus == 1 ? true : false; // Radio btn value 0 (no) / 1 (yes)
          this.prTypeofdisability = result.disabilityDescription; // Type of disability
          this.prGeneralspec = result.category.categoryId; //General specialization 

          this.prSpec = result.subCategory.categoryId;// Specialization

          this.prMorespec = result.nextSubCategory.subSubCategoryId; //More specialization
          this.categoryId = this.prGeneralspec;
          this.subcateId = this.prSpec;
          this.moresubId = this.prMorespec;


          if (result.skillDetails.length != 0) {
            let ar = [], res = [];

            res = (result.skillDetails)
            res.forEach(element => {
              this.skillModal.push((element.skill))
            });

          }
          this.availability = result.subCategory.availability
          this.prMinwage = result.minimumHourlyWage // Minimum wages
          this.prHeadline = result.profileTitle; //Profile tittle
          this.prCurrency = result.currency.currencyId; // Currency id
          this.prMaxwrk = result.maxHourPerDay;
          this.prMinwrk = result.minHourPerDay
          this.prOtherwrk = result.acceptOtherWork // Accept other works 
          this.prSms = result.smsNotification
          this.prVoice = result.voiceNotification
          if (result.category) {
            this.tempSpecialization.push({
              category: result.category.categoryName,
              categoryId: result.category.categoryId,
              primary: true,
              subCategory: result.subCategory.subCategoryName,
              subCategoryId: result.subCategory.categoryId
            })
          }
          if (result.secCategories) {
            result.secCategories.forEach(element => {
              this.tempSpecialization.push({
                category: element.categoryName,
                categoryId: element.categoryId,
                primary: false,
                subCategory: element.subCategoryName,
                subCategoryId: element.subCategoryId
              })
            });
          }

          if (result.experienceCertificate) {


            let count = result.experienceCertificate.length

            for (let i = 0; i < count; i++) {
              if (!result.experienceCertificate[i].fileId)
                break;
              this.experienceCertificate.push(result.experienceCertificate[i].fileId)
              this.fileName.push(result.experienceCertificate[i].seedName);

            }

          }


        }
        else {
          this.service.apiErrorHandler(res);

        }
      },
      err => {
        this.service.serviceErrorResponce(err)

      }
    );
  }
  submitPersonalData() {

    let category;
    let subCategory;
    let secCategories = [];
    this.tempSpecialization.forEach(element => {
      if (element.primary) {

        category = element.categoryId;
        subCategory = element.subCategoryId;
      }
      else {
        secCategories.push({
          category: element.categoryId,
          subCategory: element.subCategoryId
        })
      }
    });

    let data = {
      'yearOfExperience': this.prExp,
      'category': category,
      'idNo': this.prSpec,
      'subCategory': subCategory,
      'nextSubCategory': '0000001',//this.signupForm.get('more_specialization').value,
      'secCategories': secCategories,
      'disabilityStatus': this.prDisability,
      'disabilityDescription': this.prTypeofdisability,
      'minimumHourlyWage': this.prMinwage,
      'currency': this.prCurrency,
      'maxHourPerDay': this.prMaxwrk,
      'minHourPerDay': this.prMinwrk,
      'acceptOtherWork': this.prOtherwrk,
      'smsNotification': this.prSms,
      'voiceNotification': this.prVoice,
      'experienceCertificate': this.experienceCertificate,
      'skills': ['BABKRIM-DEFAULT-SKILL'],//this.skillModal,
      'profileTitle': this.prHeadline,
      'availability': this.availability
    };
    customealert.loaderShow('html')

    this.service.serverRequest(data, 'register.json?registerType=' + this.service.getStorage('registerType') + '&registerLevel=4&deviceMode=web').subscribe(res => {
      customealert.loaderHide('html')

      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
      } else {
        this.service.apiErrorHandler(res)
      }
    }, er => {
      this.service.serviceErrorResponce(er)
    })
  }
  setAvailablity(item) {
    console.log(item.target.value)
    this.availability = item.target.value;
    this.submitPersonalData();
  }

  removeSpecialization(index) {
    this.tempSpecialization.splice(index, 1);
    var flag = false;
    this.tempSpecialization.forEach(element => {
      if (!element.primary && !flag) {
        this.tempSpecialization[0].primary = true;
        flag = true
      }
    });
    this.resetModal();
  }
  makeAsPrimary(index) {
    this.tempSpecialization.forEach(element => {
      if (element.primary) {
        element.primary = false;

      }
    });
    this.tempSpecialization[index].primary = true;
  }
  subCategory = []
  tempData = []
  standardWage = 0
  showAddButton = 0
  category = []
  currency = []
  resetModal() {
    this.prGeneralspec = null
    this.subCategory = [];
    this.tempData = [];
    this.prMorespec = null
    this.prSpec = null;
    this.standardWage = 0
    this.showAddButton = 0;
  }
  getLevel4() {
    this.service.serverGetRequest('', 'custom/employee/level/4.json').subscribe(
      (res) => {

        if (res['status'].responseCode == 20) {

          this.category = res['result'].categories;
          this.currency = res['result'].currencies;
        }
        else {
          this.service.apiErrorHandler(res);

        }
      },
      err => {
        this.service.serviceErrorResponce(err)

      }
    );
  }
  onCategorySelect(item: any) {
    var primary = false;
    this.tempData = [];
    if (this.tempSpecialization.length == 0 && this.tempData.length == 0) {

      primary = true;
    }
    var flag = true;
    this.tempSpecialization.forEach(element => {
      if (element.categoryId == this.prGeneralspec && element.subCategoryId == item.id) {
        flag = false;
        return false;
      }
    });
    this.tempData.push({
      category: item.category,
      categoryId: item.id,
      primary: primary,
      subCategory: null,
      subCategoryId: null
    })

  }
  categorySelect(item: any) {
    this.prSpec = null
    this.prMorespec = null
    this.skillModal = []
    this.subCategory = [];
    //Fetching Skills from API
    if (item) {
      this.getSubCategory(item.id)

    }
  }
  categoryDeselect() {
    this.prSpec = null
    this.subCategory = []

    this.showAddButton = 0;
    this.tempData = []
    this.prGeneralspec = null
    this.resetModal()
  }
  subcategoryDeselect() {
    // this.subCategory = [];

    this.prMorespec = null
    this.standardWage = 0
    this.showAddButton = 0;
  }
  onSubCategorySelected(item: any) {
    var flag = true;
    this.showAddButton = 1;
    this.tempSpecialization.forEach(element => {
      if (element.categoryId == this.prGeneralspec && element.subCategoryId == item.id) {
        flag = false;
        this.tempData.splice(this.tempData.length - 1, 1)
        return false;
      }
    });
    if (flag) {

      this.tempData.forEach((element, key) => {

        if (element.subCategory == null && element.subCategoryId == null && element.categoryId == this.prGeneralspec) {
          element.subCategory = item.subCategory;
          element.subCategoryId = item.id
        }
      });
    }
  }
  nextSubCategory = []
  getSubCategory(id) {


    this.subCategory = []

    this.nextSubCategory = []
    this.service.serverGetRequest('', 'static/subCategories.json?category=' + id).subscribe(
      (res) => {

        if (res['status'].responseCode == 20) {
          this.subCategory = res['result'].SubCategories;
        }

      },
      err => {

        this.service.serviceErrorResponce(err)

      }
    );
  }
  pushItemToCategory() {
    var flag = 1;
    console.log(this.tempData)
    this.tempData.forEach((element, key) => {
      if (element.subCategory == null && element.subCategoryId == null) {

        this.tempSpecialization.splice(key, 1);
      }
    });
    if (this.tempData[0]) {
      this.tempSpecialization.forEach((element, key) => {

        if (element.categoryId == this.tempData[0].categoryId && element.subCategoryId == this.tempData[0].subCategoryId && flag == 1) {
          flag = 0;

        }
      })
      if (flag) {
        this.tempSpecialization.push({
          category: this.tempData[0].category,
          categoryId: this.tempData[0].categoryId,
          primary: this.tempData[0].primary,
          subCategory: this.tempData[0].subCategory,
          subCategoryId: this.tempData[0].subCategoryId
        })
      }
    }
    this.resetModal();
  }
  deleteBanckDetails() {
    customealert.loaderShow('html');
    this.service.serverDeleteRequest('/payment/bankDetails/'+this.paymentDetails[0].paymentDetailsId+'.json?registerType='+this.service.getStorage('registerType')).subscribe(res => {
      if (res['status'].responseCode == 20) {
        customealert.loaderHide('html');
        this.getBankDetails()
      } else {
        this.service.apiErrorHandler(res)
      }
    }, er => { this.service.serviceErrorResponce(er) })
  }
}
