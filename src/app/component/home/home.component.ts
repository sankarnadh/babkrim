import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ServerService} from '../../service/server.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css',
   
    ]
})
export class HomeComponent implements OnInit {

  topCategories : Array<Object> = [];
  
  constructor(private router : Router,public service :ServerService) {
    this.getTopCategories();
   }

  ngOnInit() {
    if(this.service.isEmployerLogin())
    this.router.navigate(['/employer_home']);

    if(this.service.isEmployeeLogin())
    this.router.navigate(['/employee_home']);
  }

  
  getTopCategories(){
    this.service.serverGetRequest("", "/custom/topCategories.json").subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.topCategories = res['result']['topCategories'];          
        } else {
          this.service.apiErrorHandler(res);
        }
      },
      err => {}
    );
  }

  updateCategoryFilter(categoryDetails) {
    
    this.service.resetDefault();
    if(categoryDetails){
      this.service.pushCategoryFilter(categoryDetails.name, categoryDetails.id);
      this.service.searchCatId = categoryDetails.id;
    }

  }
  
}
