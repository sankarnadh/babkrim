import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { ServerService } from '../../service/server.service'
import { ValidationMessageService } from '../../service/validation-message.service'
import { FormBuilder, Validators, FormsModule, FormControl } from '@angular/forms';
import '../../../assets/js/js/chat.js'
declare var customealert: any;
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  public passwordForm;
  constructor(public service: ServerService,
    private router: Router,
    public validationMsg: ValidationMessageService,
    private formBuilder: FormBuilder,
    public activatedRouter: ActivatedRoute) {
    this.passwordForm = this.formBuilder.group({


      'confirmPassword': ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])],
      'newPassword': ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])]
    });
  }

  public key;
  ngOnInit() {
    this.activatedRouter.queryParams.subscribe(params => {
      this.key = params['key'];


    });

  }
  buttonClick = 0
  forgotPasswordReset() {
  if(!this.key)
  {
    this.service.showError(this.validationMsg.invalidOtp)
    return false
  }
    if (!this.passwordForm.get('confirmPassword').valid) {
      this.buttonClick = 1;
      return false
    } else this.buttonClick = 0;
    if (!this.passwordForm.get('newPassword').valid) {
      this.buttonClick = 1;
      return false
    } else this.buttonClick = 0;
    if (this.passwordForm.get('newPassword').value != this.passwordForm.get('confirmPassword').value) {
      this.buttonClick = 1;
      return false
    }
    let data = {}
    let newPassword = this.passwordForm.get('newPassword').value;
    let confirmPassword = this.passwordForm.get('confirmPassword').value;
    customealert.loaderShow('html')
    this.service.serverRequest(data, 'forgotPassword/verify.json?OTP=' + this.key + '&password=' + newPassword + "&confirmPassword=" + confirmPassword).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.passwordUpdated+' '+this.validationMsg.loginToContinue)
        this.router.navigate(['/login'])

      } else
        this.service.apiErrorHandler(res)
    }, er => {
      this.service.serviceErrorResponce(er)
    })
  }
}
