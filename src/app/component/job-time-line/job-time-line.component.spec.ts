import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTimeLineComponent } from './job-time-line.component';

describe('JobTimeLineComponent', () => {
  let component: JobTimeLineComponent;
  let fixture: ComponentFixture<JobTimeLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTimeLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTimeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
