import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { FormBuilder, Validators, FormsModule, FormControl } from '@angular/forms';
import { ValidationServiceService } from '../../service/validation-service.service';
import { ServerService } from '../../service/server.service';
import { ValidationMessageService } from '../../service/validation-message.service'

import { Router, ActivatedRoute } from "@angular/router";
import '../../../assets/js/js/chat.js';
declare var customealert: any;
declare var google:any;
@Component({
  selector: 'app-view-proposal',
  templateUrl: './view-proposal.component.html',
  styleUrls: ['./view-proposal.component.css']
})
export class ViewProposalComponent implements OnInit {

  constructor(
    private http: HttpClient,
    public router: Router,

    public validator: ValidationServiceService,
    private formBuilder: FormBuilder, public service: ServerService,
    public validationMsg: ValidationMessageService,
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.service.loader = true
    let url = this.router.url.split('/');
    if (url[1] == 'view-contract')
      this.getContractDetails()
    else
      this.getProposalDetails()
  }
  offer: any;
  currentIcon = {
    url: this.service.currentIcon,
   scaledSize: new google.maps.Size(60, 60)
  }
  urgentIcon={
    url: this.service.urgentEmployerIcon,
    scaledSize: new google.maps.Size(60, 60)
  }
  offerLength = 0
  getProposalDetails() {
    this.service.loader = true
    let proposalId
    this.activatedRoute.params.subscribe((res) => {
      proposalId = res['proposalId'];

    });
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'proposalDetails/' + proposalId + '.json?registerType=' + this.service.getStorage('registerType')).
      subscribe(res => {
        customealert.loaderHide('html')

        if (res['status'].responseCode == 20) {
          this.service.loader = false
          this.offer = res['result'].proposal;
          this.offerLength = this.offer.length;
         

          this.service.setStorage('data', JSON.stringify(res['result'].proposal))
        }
        else
          this.service.apiErrorHandler(res)
      },
        er => {
          this.service.serviceErrorResponce(er)
        })
  }
  getContractDetails() {
    customealert.loaderShow('html')
    this.service.loader = true
    let contractId
    this.activatedRoute.params.subscribe((res) => {
      contractId = res['contractId'];

    });

    this.service.serverGetRequest('', 'contractDetails/' + contractId + '.json?registerType=' + this.service.getStorage('registerType')).
      subscribe(res => {
        customealert.loaderHide('html')

        if (res['status'].responseCode == 20) {
          this.service.loader = false
          console.log(res['result'].contract)
          this.offer = res['result'].contract;
          this.offerLength = this.offer.length;
          // console.log(this.offer.jobDetails)
          this.service.setStorage('data', JSON.stringify(res['result'].contract))
        }
        else
          this.service.apiErrorHandler(res)
      },
        er => {
          this.service.serviceErrorResponce(er)
        })
  }
}
