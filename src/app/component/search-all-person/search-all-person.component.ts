import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { isArray } from 'util';
import { Router, NavigationEnd } from '@angular/router'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-search-all-person',
  templateUrl: './search-all-person.component.html',
  styleUrls: ['./search-all-person.component.css'],
})
export class SearchAllPersonComponent implements OnInit ,OnDestroy{
  employee_login;
  employer_login;
  noLogin;

  constructor(public service: ServerService, public router: Router,
     public config: NgbRatingConfig) {
    config.max = 5;
    config.readonly = true
  }
  pageCount = 0;
ngOnDestroy(){
  this.service.search=null
}
  ngOnInit() {
    this.service.removeStorage('employeeTempId')
    this.service.employeeListPageNo = 0
    window.scrollTo(0, 0);
    this.service.employeeList = []
    this.service.emplength = 0;
    this.service.pageCounter = 0

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    this.service.employeeList = []
    this.service.getemployeeDetails(0)
  }
  pageChange() {
    // if (this.service.isEmployerLogin())
      this.router.navigate(['/hire_person_direct'])
  }
  onKeyDown(event) {
    if (event.key === "Enter")
      this.service.getemployeeDetails(0)
  }
  p: any; //PageCounter
  pageno = 0;
  skillFormat(skill) {

    let data
    if (skill)
      data = skill.split(',');
    if (isArray(data))
      return data
    else return []
  }
  /********************************************************************** */
  throttle = 3;
  scrollDistance = 1;
  scrollUpDistance = 2;
  onScrollDown() {
    console.log('scrolled down!!', this.service.employeeListPageNo + " ");
    if (this.service.employeeListPageNo != 0 && !this.service.loader && this.service.emplength <= this.service.pageCounter) {
      this.service.getemployeeDetails(this.service.employeeListPageNo);
      console.log('scrolled down!!', this.service.employeeListPageNo + " ");
    }
  }
  /*********************************************************************** */
}
