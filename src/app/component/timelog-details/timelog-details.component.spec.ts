import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelogDetailsComponent } from './timelog-details.component';

describe('TimelogDetailsComponent', () => {
  let component: TimelogDetailsComponent;
  let fixture: ComponentFixture<TimelogDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelogDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelogDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
