import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ServerService } from '../../service/server.service';
import { ChatService } from '../../service/chat/chat-service.service'

import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import '../../../assets/js/js/chat.js'
declare var customealert;
declare var System: any;
@Component({
  selector: 'app-empolyeehome',
  templateUrl: './empolyeehome.component.html',
  styleUrls: ['./empolyeehome.component.css',
  ],
 
})
export class EmpolyeehomeComponent implements OnInit {


  constructor(private router: Router, public service: ServerService,
    public config: NgbRatingConfig,
   ) {
    window.scrollTo(0, 0)
    config.max = 5;
    config.readonly = true
  }
  limit = 3
  returnParseInt(value){
    return parseInt(value)
  }
  ngOnInit() {
    this.service.getRunningContract()
    this.service.loader = true;
    this.service.jobDetails = []
    this.service.pageCounter = 0
    this.service.jobListPageNo = 0
    this.service.fetchAllJobDetailsFromApi(0);

    if (!this.service.isEmployeeLogin() || this.service.getStorage('registerLevel') != '0')
      this.router.navigate(['/login']);

    this.service.checkLogin(); //To change the login header

    if ( this.service.getStorage('tempData')) {
      let val=JSON.parse(this.service.getStorage('tempData'))
      this.service.removeStorage('tempData') 
      this.viewJobList(val)
    }
  }
  viewJobList(val) {

    this.service.setStorage('data', JSON.stringify(val))
    this.router.navigate(['/employee_viewjoblist' + '/' + val.jobId])
  }
  /********************************************************************** */
  throttle = 3;
  scrollDistance = 1;
  scrollUpDistance = 2;
  onScrollDown() {
    if (this.service.jobListPageNo != 0 && !this.service.loader && this.service.jobDetails.length <= this.service.pageCounter) {
      this.service.fetchAllJobDetailsFromApi(this.service.jobListPageNo);

    }
  }
  /*********************************************************************** */
}
