import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpolyeehomeComponent } from './empolyeehome.component';

describe('EmpolyeehomeComponent', () => {
  let component: EmpolyeehomeComponent;
  let fixture: ComponentFixture<EmpolyeehomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpolyeehomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpolyeehomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
