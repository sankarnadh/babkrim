import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service'
import { ValidationMessageService } from '../../service/validation-message.service'
import { ValidationServiceService } from '../../service/validation-service.service'
import { Router } from '@angular/router'
import '../../../assets/js/js/chat.js'
declare var customealert;
@Component({
  selector: 'app-contactsus',
  templateUrl: './contactsus.component.html',
  styleUrls: ['./contactsus.component.css']
})
export class ContactsusComponent implements OnInit {

  constructor(public service: ServerService,
    public validationMsg: ValidationMessageService,
    public validatorservice: ValidationServiceService,
    public router: Router) { }
  name: any = null;
  email: any = null;
  mobile: any = '';
  subject: any = null
  message: any = null;

  ngOnInit() {
  }
  buttonClick = 0
  emailValid = false;
  contactUs() {
    if (!this.name) {
      this.buttonClick = 1;
      return false
    }
    if (!this.validatorservice.emailValidation(this.email)) {

      this.buttonClick = 1;
      this.emailValid = false;
      return false;
    }
    else
      this.emailValid = true
    if (!this.mobile || this.mobile.length!=10) {
      this.buttonClick = 1;
      return false
    }

    if (!this.subject) {
      this.buttonClick = 1;
      return false
    }
    if (!this.message) {
      this.buttonClick = 1;
      return false
    }
    let data = {
      "comment": this.message,
      "email": this.email,
      "mobileNumber": this.mobile,
      "name": this.name,
      "subject": this.message
    }
    customealert.loaderShow('html')
    this.service.serverRequest(data, 'contactUs.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        this.router.navigate(['/home'])
      }
      else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))
  }
}
