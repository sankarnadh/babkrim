import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-android-download-employer',
  templateUrl: './android-download-employer.component.html',
  styleUrls: ['./android-download-employer.component.css']
})
export class AndroidDownloadEmployerComponent implements OnInit {

  constructor() { window.location.href= "https://play.google.com/store/apps/details?id=com.babkrim.employer_job_portal&hl=en"; }

  ngOnInit() {
  }

}
