import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AndroidDownloadEmployerComponent } from './android-download-employer.component';

describe('AndroidDownloadEmployerComponent', () => {
  let component: AndroidDownloadEmployerComponent;
  let fixture: ComponentFixture<AndroidDownloadEmployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AndroidDownloadEmployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AndroidDownloadEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
