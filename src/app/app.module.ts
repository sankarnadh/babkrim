import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';


import { AppRoutingModule } from './app-routing.module';
import { SignupComponent } from './component/signup/signup.component';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatSnackBarModule } from "@angular/material";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';
import { InternationalPhoneModule } from 'ng4-intl-phone';
import { SelectModule } from 'ng2-select';
import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core'; //GOOGLE MAP
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner'; // Spinner
import { Ng5SliderModule } from 'ng5-slider';
import { NgxPaginationModule } from 'ngx-pagination'; //pagination
import { CdTimerModule, CdTimerComponent } from 'angular-cd-timer'; //countdowntimer
// File uploader
import { PaginatorModule } from 'primeng/paginator';
/****
 * Language translation
 */
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
/*
Home page services
Login, sign up , Validation
*/
import { ValidationServiceService } from './service/validation-service.service';
import { ServerService, SafePipe } from './service/server.service';
import { LoginComponent } from './component/login/login.component';
/**
 * Employer Pages
 */
import { EmpolyerhomeComponent } from './component/empolyerhome/empolyerhome.component';
import { NonUrgentRequestComponent } from './component/Employer/non-urgent-request/non-urgent-request.component';
import { UrgentRequestComponent } from './component/Employer/urgent-request/urgent-request.component';
import { NewapplicationJobsComponent } from './component/Employer/newapplication-jobs/newapplication-jobs.component';
import { EmployerEvaluateComponent } from './component/Employer/employer-evaluate/employer-evaluate.component';
import { JobAnnouncedComponent } from './component/Employer/job-announced/job-announced.component';
import { ProfileComponent } from './component/Employer/profile/profile.component';
import { ViewApplicantsComponent } from './component/Employer/view-applicants/view-applicants.component';
import { ViewApplicantProfileComponent } from './component/Employer/view-applicant-profile/view-applicant-profile.component';
import { EmployerEditJobComponent } from './component/Employer/employer-edit-job/employer-edit-job.component';
import { CompanyDetailsComponent } from './component/Employer/company-details/company-details.component';
import { ManagerDetailsComponent } from './component/Employer/manager-details/manager-details.component';

/*****
 * EMPLOYEE PAGES
 */
import { PersonalSetupComponent } from './component/Employee/personal-setup/personal-setup.component';

import { EmpolyeehomeComponent } from './component/empolyeehome/empolyeehome.component';
import { EmployeeJoblistComponent } from './component/Employee/employee-joblist/employee-joblist.component';
import { EmployeeViewjoblistComponent } from './component/Employee/employee-viewjoblist/employee-viewjoblist.component';
import { EmployeeEditComponent } from './component/Employee/employee-edit/employee-edit.component';
import { EmployeeEvaluateComponent } from './component/Employee/employee-evaluate/employee-evaluate.component';
import { EmployeeViewMapComponent } from './component/Employee/employee-view-map/employee-view-map.component';
import { AccountSetupComponent } from './component/Employee/account-setup/account-setup.component';
import { EducationSetupComponent } from './component/Employee/education-setup/education-setup.component';
import { ViewEmployeeComponent } from './component/Employer/view-employee/view-employee.component';
import { HowItWorkComponent } from './component/how-it-work/how-it-work.component';
import { ActivateEmailComponent } from './component/activate-email/activate-email.component';
import { ProposalComponent } from './component/Employee/proposal/proposal.component';
import { MyoffersComponent } from './component/Employee/myoffers/myoffers.component';
import { EmployeemessageComponent } from './component/employeemessage/employeemessage.component';
import { VieweEmployeeDetailsComponent } from './component/Employer/viewe-employee-details/viewe-employee-details.component';
import { InviteInterviewComponent } from './component/Employee/invite-interview/invite-interview.component';
import { EmployerMyJobsComponent } from './component/Employer/employer-my-jobs/employer-my-jobs.component';
import { AllEmployeeListComponent } from './component/Employer/all-employee-list/all-employee-list.component';
import { SearchAllPersonComponent } from './component/search-all-person/search-all-person.component';
import { HireSearchEmployeeComponent } from './component/hire-search-employee/hire-search-employee.component';
import { UserDataSettingsComponent } from './component/user-data-settings/user-data-settings.component';
import { FaqComponent } from './component/faq/faq.component';
import { UserNotificationsComponent } from './component/user-notifications/user-notifications.component';
import { JobTimeLineComponent } from './component/job-time-line/job-time-line.component';
import { JobTimeLineChartComponent } from './component/job-time-line-chart/job-time-line-chart.component';
import { ActiveContractComponent } from './common/active-contract/active-contract.component';
import { CompletedContractComponent } from './common/completed-contract/completed-contract.component';
import { ContractsComponent } from './common/contracts/contracts.component';
import { TimelogDetailsComponent } from './component/timelog-details/timelog-details.component';
import { FilterComponent } from './layout/filter/filter.component';
import { SideBarfilterComponent } from './layout/side-barfilter/side-barfilter.component';
import { EmployerFilterComponent } from './layout/employer-filter/employer-filter.component';
import { RightsidebarComponent } from './layout/rightsidebar/rightsidebar.component';
import { FileDropModule } from 'ngx-file-drop';
import { NorocordsfoundComponent } from './layout/norocordsfound/norocordsfound.component';
import { ViewProposalComponent } from './component/view-proposal/view-proposal.component';
import { RecentMesageComponent } from './common/chat/recent-mesage/recent-mesage.component';
import { ChatPanelComponent } from './common/chat/chat-panel/chat-panel.component';
import { ChatContactComponent } from './common/chat/chat-contact/chat-contact.component';
import { ChatFileComponent } from './common/chat/chat-file/chat-file.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
// import { NotificationComponent } from './common/notification/notification.component';
import { WalletComponent } from './common/wallet/wallet.component';
import { PasswordComponent } from './common/password/password.component';
import { AppSettingsComponent } from './common/app-settings/app-settings.component';
import { NotificationSettingsComponent } from './common/notification-settings/notification-settings.component';
import { GroupChatComponent } from './common/group-chat/group-chat.component'
import { NotsureComponent } from './component/notsure/notsure.component';
import { DisputesComponent } from './component/disputes/disputes.component';
import { ContactsusComponent } from './component/contactsus/contactsus.component';
import { PrivacyEnglishComponent } from './common/legal/privacy/privacy-english/privacy-english.component';
import { TermsEnglishComponent } from './common/legal/terms/terms-english/terms-english.component'
import {TimeAgoPipe} from 'time-ago-pipe';
import { DatePipe, AsyncPipe, TitleCasePipe } from '@angular/common';
import { ForgotPasswordComponent } from './component/forgot-password/forgot-password.component';
import { TermsenComponent } from './common/legal/termsen/termsen.component';
import { TermsarComponent } from './common/legal/termsar/termsar.component';
import { CookiesComponent } from './common/legal/cookies/cookies.component';
import { AppleidFilterComponent } from './layout/appleid-filter/appleid-filter.component';
import { JobListComponent } from './layout/job-list/job-list.component';
import { NewproposalComponent } from './layout/employee/proposal/newproposal/newproposal.component';
import { InvitationComponent } from './layout/employee/proposal/invitation/invitation.component';
import { UrgentRequestMapComponent } from './component/Employer/urgent-request-map/urgent-request-map.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { LazyloaderComponent } from './layout/lazyloader/lazyloader.component';
import { DxSchedulerModule } from "devextreme-angular";

import { ViewCompanyDetailsComponent } from './component/Employee/view-company-details/view-company-details.component';
import { PaymentProcessingComponent } from './component/layout/payment-processing/payment-processing.component';
import { PaymentSuccessComponent } from './component/layout/payment-success/payment-success.component';
import { PaymentFailedComponent } from './component/layout/payment-failed/payment-failed.component';
import { AboutCompanyComponent } from './component/about-company/about-company.component';
// import { CompanydetailsComponent } from './component/employee/companydetails/companydetails.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { PlansAndServicesComponent } from './common/plans-and-services/plans-and-services.component';
import { IconsetComponent } from './layout/iconset/iconset.component';
import { FeaturedworksComponent } from './layout/employee/featuredworks/featuredworks.component';

import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';
import { LOCALE_ID } from '@angular/core';
import { PlayerComponent } from './layout/player/player.component';
import { SidebarfiltermobileComponent } from './layout/sidebarfiltermobile/sidebarfiltermobile.component';
import { EmployeefeaturedworkComponent } from './layout/employer/employeefeaturedwork/employeefeaturedwork.component';
import { AgmDirectionModule } from 'agm-direction'
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { GooglePlacesDirective } from './google-places.directive';
import { EditProposalComponent } from './component/Employee/edit-proposal/edit-proposal.component';
import { GroupchatcontainerComponent } from './common/groupchat/groupchatcontainer/groupchatcontainer.component';
import { GrouplistComponent } from './common/groupchat/page/grouplist/grouplist.component';
import { GroupmessageComponent } from './common/groupchat/page/groupmessage/groupmessage.component';
import { GroupinfoComponent } from './common/groupchat/page/groupinfo/groupinfo.component';
import { GroupsearchComponent } from './common/groupchat/page/groupsearch/groupsearch.component';
import { GroupcreateComponent } from './common/groupchat/page/groupcreate/groupcreate.component';
import { EmployerotherjobsComponent } from './layout/employer/employerotherjobs/employerotherjobs.component';
import { EditGroupNameComponent } from './common/groupchat/page/edit-group-name/edit-group-name.component';
import { EditGroupDescriptionComponent } from './common/groupchat/page/edit-group-description/edit-group-description.component';
import { AtachmentviewComponent } from './layout/atachmentview/atachmentview.component';

import { OWL_DATE_TIME_LOCALE, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';
import { EditattachmentComponent } from './layout/editattachment/editattachment.component';
import { ChartComponent } from './layout/chart/chart.component';
import { LeaderboardComponent } from './common/leaderboard/leaderboard.component';
import { ReviewswidgetComponent } from './common/reviewswidget/reviewswidget.component';
import { IncomegraphwidgetComponent } from './common/incomegraphwidget/incomegraphwidget.component';

import { ChartsModule } from 'ng2-charts';
import { HeaderwidgetComponent } from './common/headerwidget/headerwidget.component';
import { MaptrackComponent } from './common/maptrack/maptrack.component';
import {DatePipes} from './service/datepipe'


import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { MessageverificationComponent } from './common/messageverification/messageverification.component';

import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment, MessagingService } from './service/message.service';

import { EvaluateComponent } from './common/evaluate/evaluate.component';
import { WaitingConfirmationPageComponent } from './common/waiting-confirmation-page/waiting-confirmation-page.component';
import { TimelineJobdetailsComponent } from './common/timeline-jobdetails/timeline-jobdetails.component';
import { SharedModule } from './shared/shared.module';
import { EmployeeiconComponent } from './layout/employeeicon/employeeicon.component';
import { RatecardComponent } from './common/ratecard/ratecard.component';
import { JobmodalComponent } from './common/jobmodal/jobmodal.component';


export const MY_NATIVE_FORMATS = {
  fullPickerInput: {year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric'},
  datePickerInput: {year: 'numeric', month: 'numeric', day: 'numeric'},
  timePickerInput: {hour: 'numeric', minute: 'numeric'},
  monthYearLabel: {year: 'numeric', month: 'long'},
  
  dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
  monthYearA11yLabel: {year: 'numeric', month: 'long'},
};
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
 import { ToastrModule } from 'ng6-toastr-notifications';
 import {ProgressBarModule} from "angular-progress-bar";
import { AgmdirectionComponent } from './layout/modal/agmdirection/agmdirection.component'
import localear from '@angular/common/locales/ar'
import localeAr from '@angular/common/locales/ar';
import { registerLocaleData } from '@angular/common';
import { JoblistnologinComponent } from './common/joblistnologin/joblistnologin.component';
import { DemocodeComponent } from './layout/democode/democode.component';
import { NgxStripeModule } from '@nomadreservations/ngx-stripe';
import { DownloadappComponent } from './component/downloadapp/downloadapp.component';
// import { EmployeeComponent } from './component/AndroidDownload/employee/employee.component';
import { AndroidDownloadEmployeeComponent } from './android-download-employee/android-download-employee.component';
import { AndroidDownloadEmployerComponent } from './component/android-download-employer/android-download-employer.component';


registerLocaleData(localeAr);
@NgModule({
  providers: [
    DatePipes,
   MessagingService,
 TitleCasePipe,
    AsyncPipe,
    ValidationServiceService,
    CdTimerComponent, DatePipe,
    { provide: LOCALE_ID, useValue: 'en-IN' },
   
    // { provide: LOCALE_ID, useValue: 'ar'},
    { provide: LOCALE_ID, useValue: 'en' },
    // { provide: OWL_DATE_TIME_LOCALE, useValue: 'us' },
    
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS},
    GoogleMapsAPIWrapper,
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
    SnotifyService,
    
  ],
  imports: [
    SharedModule,
    
    NgxStripeModule.forRoot('pk_test_U4CiS23E5wkBliM1oEuuEzb9001BdF50ey'),
    ProgressBarModule,
    SnotifyModule,
    ToastrModule.forRoot(),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ChartsModule,
    AgmDirectionModule,
    SweetAlert2Module.forRoot(),
    NgProgressModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    DxSchedulerModule,
    InfiniteScrollModule,
    BrowserModule, Ng2SearchPipeModule,
    InternationalPhoneModule,
    NgSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
    PaginatorModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule, MatSnackBarModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      //  apiKey: 'AIzaSyDV3kwvlzDzAXf0qJcdzBlRIFsYVJqB1nA',
      apiKey: 'AIzaSyDpQa7ReG-1ILPrVOnHuEO-fEOxG9EJ8e4',
      libraries: ["geometry"]
    }), AgmJsMarkerClustererModule,
    CdTimerModule,

    Ng4LoadingSpinnerModule.forRoot(),
    NgbModule.forRoot(),
    Ng5SliderModule,
    NgxPaginationModule,
    IonRangeSliderModule,
    FileDropModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),


  ],

  declarations: [
    DatePipes,
    SafePipe,
   // NotificationComponent,
    AppComponent,
    HomeComponent,
    SignupComponent,
    TimeAgoPipe,
    LoginComponent,

    EmpolyeehomeComponent,
    EmpolyerhomeComponent,
    NonUrgentRequestComponent,
    UrgentRequestComponent,
    NewapplicationJobsComponent,
    EmployerEvaluateComponent,
    JobAnnouncedComponent,
    ProfileComponent,
    EmployeeJoblistComponent,
    EmployeeViewjoblistComponent,
    EmployeeEditComponent,
    EmployeeEvaluateComponent,
    EmployeeViewMapComponent,
    ViewApplicantsComponent,
    ViewApplicantProfileComponent,
    EmployerEditJobComponent,
    AccountSetupComponent,
    EducationSetupComponent,
    PersonalSetupComponent,
    CompanyDetailsComponent,
    ManagerDetailsComponent,
    ViewEmployeeComponent,
    HowItWorkComponent,
    ActivateEmailComponent,
    ProposalComponent,
    MyoffersComponent,
    EmployeemessageComponent,
    VieweEmployeeDetailsComponent,
    InviteInterviewComponent,
    EmployerMyJobsComponent,
    AllEmployeeListComponent,
    SearchAllPersonComponent,
    HireSearchEmployeeComponent,
    UserDataSettingsComponent,
    FaqComponent,
    UserNotificationsComponent,
    JobTimeLineComponent,
    JobTimeLineChartComponent,
    ActiveContractComponent,
    CompletedContractComponent,
    ContractsComponent,
    TimelogDetailsComponent,
    FilterComponent,
    SideBarfilterComponent,
    EmployerFilterComponent,
    RightsidebarComponent,
    NorocordsfoundComponent,
    ViewProposalComponent,
    RecentMesageComponent,
    ChatPanelComponent,
    ChatContactComponent,
    ChatFileComponent,
    WalletComponent,
    PasswordComponent,
    AppSettingsComponent,
    NotificationSettingsComponent,
    GroupChatComponent,
    NotsureComponent,
    DisputesComponent,
    ContactsusComponent,
    PrivacyEnglishComponent,
    TermsEnglishComponent,
    ForgotPasswordComponent,
    TermsenComponent,
    TermsarComponent,
    CookiesComponent,
    AppleidFilterComponent,
    JobListComponent,
    NewproposalComponent,
    InvitationComponent,
    UrgentRequestMapComponent,
    LazyloaderComponent,
    ViewCompanyDetailsComponent,
    PaymentProcessingComponent,
    PaymentSuccessComponent,
    PaymentFailedComponent,
    AboutCompanyComponent,
    PlansAndServicesComponent,
    IconsetComponent,
    FeaturedworksComponent,
    PlayerComponent,
    SidebarfiltermobileComponent,
    EmployeefeaturedworkComponent,
    HeaderComponent,
    FooterComponent,
    GooglePlacesDirective,
    EditProposalComponent,
    GroupchatcontainerComponent,
    GrouplistComponent,
    GroupmessageComponent,
    GroupinfoComponent,
    GroupsearchComponent,
    GroupcreateComponent,
    EmployerotherjobsComponent,
    EditGroupNameComponent,
    EditGroupDescriptionComponent,
    AtachmentviewComponent,
    EditattachmentComponent,
    ChartComponent,
    LeaderboardComponent,
    ReviewswidgetComponent,
    IncomegraphwidgetComponent,
    HeaderwidgetComponent,
    MaptrackComponent,
    MessageverificationComponent,
    EvaluateComponent,
    WaitingConfirmationPageComponent,
    TimelineJobdetailsComponent,
    EmployeeiconComponent,
    RatecardComponent,
    JobmodalComponent,
    AgmdirectionComponent,
    JoblistnologinComponent,
    DemocodeComponent,
    DownloadappComponent,
    AndroidDownloadEmployeeComponent,
    AndroidDownloadEmployerComponent,



    // CompanydetailsComponent,


  ],
  exports: [

  ],


  bootstrap: [AppComponent]
})
export class AppModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
