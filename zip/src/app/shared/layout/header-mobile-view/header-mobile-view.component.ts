import { Component, OnInit, Input } from '@angular/core';
import { ServerService } from '../../../service/server.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header-mobile-view',
  templateUrl: './header-mobile-view.component.html',
  styleUrls: ['./header-mobile-view.component.css']
})
export class HeaderMobileViewComponent implements OnInit {

  constructor(public service: ServerService,
    private router : Router) { }
//title = 'app';
employee_login: any;//Employee Login
employer_login: any;//Employer Login
noLogin: any;        //No logins
category: any;
loop: any = [0, 1, 2, 3];
@Input() commonHeaders: any;
baseurl: any;
langSelected: boolean = this.service.langSelected();

//Test
all = "";
other = "Other Countries";
most = "Most Popular Countries";
disabled = false;

selectedItems = [];
dropdownSettings = {};
showMenu = false;
    ngOnInit() {
  
  
      this.service.checkLogin()
    }
  
    bodyscrolltop(){
      window.scrollTo(0,0)
    }
  
    onSelectAll(items: any) {
      // console.log(items);
    }
    /***
     * Logout & destroy all storage
     */
    logout() {
      let res;
  
      res = this.service.logout();
  
      this.service.checkLogin(); //To change the header
  
      //this.router.navigate(['/home']);
    }
    /***
     * Get all category
     */
    ar: any;
    returnCategoryName(i,k){
      try {
       return this.category[this.categoryIndex(i,k)].name
      } catch (error) {
      //  console.info(error) 
      }
     
    }
    returnSubCategories(i,k){
      try {
        return this.category[this.categoryIndex(i,k)].subCategories
      } catch (error) {
        
      }
    }
    categoryIndex(i, k) {
      //IMPORTANT
      let index;
      if (i == 0 && k == 0)
        return i;
      else {
        if (k == 0)
          return parseInt(i + 1);
        else
          return +(k * 10 + i);
      }
    }
  
    isSub(val) {
     
      if (val)
        return true;
      else return false;
    }
    checkCat(val) {
      switch (val) {
        case 'Human Medicine' :
          {
            this.service.pushCategoryFilter('Human Medicine', 15)
            this.service.searchCatId=15
            break;
          }
          case 'Legal' :
          {
            this.service.pushCategoryFilter('Legal', 12)
            this.service.searchCatId=12
            break;
          } 
          case 'Web & Mobile Development' :
          {
            this.service.pushCategoryFilter('Web & Mobile Development', 14)
            this.service.searchCatId=14
            break;
          } 
  
          case 'IT & Networking' :
          {
            this.service.pushCategoryFilter('IT & Networking', 16)
            this.service.searchCatId=16
            break;
          }
          case 'Engineering' :
          {
            this.service.pushCategoryFilter('Engineering', 17)
            this.service.searchCatId=17
            break;
          } 
          case 'Design' :
          {
            this.service.pushCategoryFilter('Design', 18)
            this.service.searchCatId=18
            break;
          } 
          case 'Marketing' :
          {
            this.service.pushCategoryFilter('Marketing', 21)
            this.service.searchCatId=21
            break;
          } 
          case 'Houses & Building Maintenance' :
          {
            this.service.pushCategoryFilter('Houses & Building Maintenance', 24)
            this.service.searchCatId=24
            break;
          } 
  
  
  
  
  
          case 'الطب البشري' :
          {
            this.service.pushCategoryFilter('الطب البشري', 15)
            this.service.searchCatId=15
            break;
          }
          case 'قانوني' :
          {
            this.service.pushCategoryFilter('قانوني', 12)
            this.service.searchCatId=12
            break;
          } 
          case 'تطوير الويب والجوال' :
          {
            this.service.pushCategoryFilter('تطوير الويب والجوال', 14)
            this.service.searchCatId=14
            break;
          } 
  
          case 'تكنولوجيا المعلومات والشبكات' :
          {
            this.service.pushCategoryFilter('تكنولوجيا المعلومات والشبكات', 16)
            this.service.searchCatId=16
            break;
          }
          case 'هندسة' :
          {
            this.service.pushCategoryFilter('هندسة', 17)
            this.service.searchCatId=17
            break;
          } 
          case 'التصميم' :
          {
            this.service.pushCategoryFilter('التصميم', 18)
            this.service.searchCatId=18
            break;
          } 
          case 'تسويق' :
          {
            this.service.pushCategoryFilter('تسويق', 21)
            this.service.searchCatId=21
            break;
          } 
          case 'بيوت وصيانة المباني' :
          {
            this.service.pushCategoryFilter('بيوت وصيانة المباني', 24)
            this.service.searchCatId=24
            break;
          }
      }
    }
   

}
