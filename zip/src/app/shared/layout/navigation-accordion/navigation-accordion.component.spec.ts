import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationAccordionComponent } from './navigation-accordion.component';

describe('NavigationAccordionComponent', () => {
  let component: NavigationAccordionComponent;
  let fixture: ComponentFixture<NavigationAccordionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationAccordionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
