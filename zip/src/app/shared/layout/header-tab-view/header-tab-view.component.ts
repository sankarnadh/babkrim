import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../service/server.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-tab-view',
  templateUrl: './header-tab-view.component.html',
  styleUrls: ['./header-tab-view.component.css']
})
export class HeaderTabViewComponent implements OnInit {

  constructor(public service: ServerService,
    private router : Router) { }

  ngOnInit() {
    setInterval(() => {
        
      if(this.service.getStorage('registerType'))
      this.selectSearch(this.service.getStorage('registerType'))
    }, 10);
  }
  onKeyDown(event) {
    if (event.key === "Enter")
      this.allSearchPerson()
  }

  allSearchPerson() {
this.service.fetchAllJobDetailsFromApi(0)
    if (this.selectBoxSearch == 'employee') {//this.employee_login || 
      this.router.navigate(['employee_joblist']);
    }
    if (this.selectBoxSearch == 'employer') { //this.employer_login || 
      this.router.navigate(['/search_all_person']);
    }

  }
  selectBoxSearch = this.service.isEmployerLogin()?"employer":'employee';
  selectSearch(val) {
    if (val != this.selectBoxSearch)
      this.service.search = null
    this.selectBoxSearch = val;

    if (val == 'employee')
      this.placeHolder = 'searchjob'
    else
      this.placeHolder = 'find_employee';
  }
  placeHolder =!this.service.isEmployeeLogin()? 'find_employee':'searchjob';
}
