import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployerheaderComponent } from './layout/employerheader/employerheader.component';
/****
 * Language translation
 */
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NotificationComponent } from '../common/notification/notification.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import { EmployeeheaderComponent } from './layout/employeeheader/employeeheader.component';
import { HeaderTabViewComponent } from './layout/header-tab-view/header-tab-view.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SelectModule } from 'ng2-select';
import { HeaderMobileViewComponent } from './layout/header-mobile-view/header-mobile-view.component';
import { NavigationAccordionComponent } from './layout/navigation-accordion/navigation-accordion.component';

@NgModule({
  exports:[
    EmployerheaderComponent,
    EmployeeheaderComponent,
    HeaderMobileViewComponent,
    HeaderTabViewComponent,
    NavigationAccordionComponent,
    NotificationComponent],
  imports: [
    NgbModule,
    SelectModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    CommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  declarations: [EmployerheaderComponent,NotificationComponent,
     EmployeeheaderComponent, HeaderTabViewComponent,
     HeaderMobileViewComponent,
     NavigationAccordionComponent ]
})
export class SharedModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}