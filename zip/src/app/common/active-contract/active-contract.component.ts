import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  OnDestroy
} from "@angular/core";
import { ServerService } from "../../service/server.service";
import { ValidationMessageService } from "../../service/validation-message.service";

import { AgmCoreModule, MapsAPILoader } from "@agm/core";

import { Router } from "@angular/router";
import "../../../assets/js/js/chat.js";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { TranslateService } from "@ngx-translate/core";
declare var customealert: any;

@Component({
  selector: "app-active-contract",
  templateUrl: "./active-contract.component.html",
  styleUrls: ["./active-contract.component.css"]
})
export class ActiveContractComponent implements OnInit, OnDestroy {
  @Output() messageEvent = new EventEmitter<string>();
  tempData: any = [];
  sendMessage(val) {
    this.messageEvent.emit(val);
  }
  constructor(
    public service: ServerService,
    private router: Router,
    public validationMsg: ValidationMessageService,
    private translate: TranslateService
  ) {}
  emptyInvoiceList() {
    this.service.invoiceList = [];
    this.service.invoiceTotal = 0;
    this.service.invoiceSubTotal = 0;
    this.addInvoiceList();
  }

  setTrackdata(val) {
    this.service.setStorage("tempData", JSON.stringify(val));
  }
  formatValue = "default";
  addInvoiceList() {
    if (this.service.invoiceList.length == 0)
      this.service.invoiceList.push({
        item: "",
        quantity: "",
        price: ""
      });

    this.service.invoiceTotal = 0;
    this.service.invoiceList.forEach(element => {
      if (element.quantity != "" && element.price != "") {
        this.service.invoiceTotal += element.quantity * element.price;
      }
    });
  }
  ngOnInit() {
    window.scrollTo(0, 0);
    if (!this.service.isEmployeeLogin() && !this.service.isEmployerLogin())
      //Checking authentication
      this.router.navigate(["/login"]);

    this.service.getActiveContracts();
  }
  ngOnDestroy() {
    this.service.activeContract = [];
  }
  componyLogo: any = "assets/images/userarabic.jpg";
  test = 110;
  endDateCalculate(date, noOfDays) {
    let data = new Date(date.split(".")[0]);

    return data.setDate(data.getDate() + noOfDays);
  }
  resetModal() {
    this.service.taskDescription = "";
    this.service.taskName = "";
    this.service.securityKey = "";
    this.service.invoiceList = [];
  }
  gotoMyProfile(userId) {
    if (this.service.isEmployeeLogin()) {
      this.service.setStorage("employerTempId", userId);
      this.router.navigate(["/view_company_details"]);
    } else {
      this.service.setStorage("employeeTempId", userId);
      this.router.navigate(["/hire_person_direct"]);
    }
  }
  setValue(val, i) {
    this.router.navigate(["/jobtimeline/" + val.contractId]);
    var time: any = 0;
    // if(this.service.taskStartTime[this.service.getStorage('i')])
    // {
    var seconds: any = +this.service.taskStartTime[
      this.service.getStorage("i")
    ]; //parseInt(, 10);

    var days = Math.floor(seconds / (3600 * 24));
    seconds -= days * 3600 * 24;
    var hrs = Math.floor(seconds / 3600);
    seconds -= hrs * 3600;
    var mnts = Math.floor(seconds / 60);
    seconds -= mnts * 60;
    if (!isNaN(seconds)) time = days + ":" + hrs + ":" + mnts + ":" + seconds;
    else time = "00:00:00:00";
    //}
    this.service.setStorage(
      "timmer",
      this.service.taskStartTime[this.service.getStorage("i")]
    );
    this.service.setStorage("i", i);
    this.service.setStorage("val", JSON.stringify(val));
    this.service.setStorage("contractId", val.contractId);

    if (this.service.isEmployerLogin())
      this.service.setStorage("tempName", val.employeeDetails.employeeName);
    if (this.service.isEmployeeLogin())
      this.service.setStorage("tempName", val.employerDetails.employerName);
    this.service.setStorage("jobtitle", val.jobDetails.jobTitle);
    this.service.setStorage("jobDescription", val.jobDetails.jobDescription);
    this.service.setStorage("completed", "0");
    if (val.lastAction == "default") {
      console.log("1");
      this.service.setStorage("totalTime", "00:00:00:00");
    } else {
      this.service.setStorage(
        "totalTime",
        val.totalTime ? val.totalTime : time
      );
    }
  }
  getInvoiceDetails() {
    customealert.loaderShow("html");
    this.service
      .serverGetRequest(
        "",
        "contractDetails/" +
          this.service.taskProposal +
          "/invoice.json?registerType=" +
          this.service.getStorage("registerType")
      )
      .subscribe(
        res => {
          customealert.loaderHide("html");

          if (res["status"].responseCode == 20) {
            this.service.invoiceDetails = res["result"].invoice;
            this.service.invoiceTotal = this.service.invoiceDetails.invoiceDetails.totalFare;
            customealert.showModal("modal_job_invoice");
          } else this.service.apiErrorHandler(res);
        },
        er => this.service.serviceErrorResponce(er)
      );
  }
  calculateHourlyWages(totalTime, hourlyWages, minHour) {
    if (!totalTime) return 0;
    var time = totalTime.split(":");
    var hour = time[0];
    var mint = time[1];
    var sec = time[2];
    if (hour < minHour) return minHour * hourlyWages;
    return this.returnFloat(
      Math.round(hour * hourlyWages) +
        (mint / 60) * hourlyWages +
        (sec / 3600) * hourlyWages
    );
  }
  returnFloat(val) {
    return parseFloat(val);
  }
  changeStatusText(status) {
    switch (status) {
      case "default":
        return this.translate.instant('notstarted');

      case "START":
        return this.translate.instant('Ongoing');

      case "PAUSE":
        return this.translate.instant('Paused')

      case "STOP":
        return this.translate.instant('Stopped');

      case "RESUME":
        return this.translate.instant('Ongoing');

      default:
        return this.translate.instant('notstarted');
    }
  }

  tempsLat;
  tempsLan;
  tempdLan;
  tempdLat;
  tempDestination
  tempOrigin
  openTrackModal() {
    this.tempsLat=parseFloat(this.tempData.employerDetails.latitude);
    this.tempsLan=parseFloat(this.tempData.employerDetails.longitude);
    this.tempdLan=parseFloat(this.tempData.jobDetails.longitude );
    this.tempdLat=parseFloat(this.tempData.jobDetails.latitude);
    this.tempOrigin={ "lat": this.tempsLat, "lng":this.tempsLan}
    this.tempDestination={ "lat": this.tempdLat, "lng":this.tempdLan}

  }
}
