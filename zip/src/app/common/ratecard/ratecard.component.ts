import { Component, OnInit, Input } from '@angular/core';
import { ServerService } from 'src/app/service/server.service';

@Component({
  selector: 'app-ratecard',
  templateUrl: './ratecard.component.html',
  styleUrls: ['./ratecard.component.css']
})
export class RatecardComponent implements OnInit {

  constructor(public serveice: ServerService) { }
  @Input() budget: any = 0;
  @Input() minWage: any = 0;
  @Input() proposal: any = 0;
  @Input() wageMode: any = 0;
  @Input() minHour: any = 0;

  curency = this.wageMode == 'hourly' ? 'INR/Hr' : 'INR';
  ngOnInit() {
  }

}
