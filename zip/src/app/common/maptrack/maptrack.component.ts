import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { ServerService } from 'src/app/service/server.service';
import { AgmMap } from '@agm/core';

@Component({
  selector: 'app-maptrack',
  templateUrl: './maptrack.component.html',
  styleUrls: ['./maptrack.component.css']
})
export class MaptrackComponent implements OnInit,OnDestroy {
  @Input() showJobDetails
  @ViewChild(AgmMap)
  public agmMap: AgmMap
  constructor(public service: ServerService) { }
  public lat: Number = 10.0050
  public lng: Number = 76.3130
  zoom: number = 14;
  public origin: any
  public destination: any
  public data;
  trackInterval
  ngOnDestroy(){
    this.showJobDetails=0;
    this.zoom=14
    this.trackDetails=null
    clearTimeout( this.markerTimeOut)
    clearInterval(this.trackInterval)
  }
  ngOnInit() {
  
if(this.showJobDetails==1)
this.zoom=18
else 
this.zoom=14
this.agmMap.triggerResize();
    try {
      this.data = (JSON.parse(this.service.getStorage('tempData')))
      this.origin = { lat: parseFloat(this.data.jobDetails.latitude), lng: parseFloat(this.data.jobDetails.longitude) }
      this.trackEmployee()
      console.info(this.data)
    } catch (error) {

    }
this.trackInterval=setInterval(() => {
  if(this.service.isEmployerLogin())
  this.trackEmployee()
}, 10000);
  }
  trackDetails:any
  trackEmployee() {
    this.service.serverGetRequest('', 'contractDetails/' + this.data.contractId + '/tracking.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      if (res['status'].responseCode == 20) {
       
       this.trackDetails = res['result'].location
        // var latlng = new google.maps.LatLng(this.trackDetails.lastSyncLatitude, this.trackDetails.lastSyncLongitude);
       
     //  this.zoom=14
      //  this.trackDetails.prevSyncLatitude=this.trackDetails.lastSyncLatitude
      //  this.trackDetails.prevSyncLongitude=this.trackDetails.lastSyncLongitude
      //  this.trackDetails.lastSyncLatitude=this.trackDetails.prevSyncLatitude+0.1
      //  this.trackDetails.lastSyncLongitude=this.trackDetails.prevSyncLongitude+0.1
        this.transition()
      } else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))
  }
  i=0
  deltaLat
  deltaLng
  numDeltas=1000
  dealy=50

  markerTimeOut
   transition(){
    this.i = 0;
    if(!this.destination)
    this.destination = {
        lat: parseFloat(this.trackDetails.prevSyncLatitude),
        lng: parseFloat(this.trackDetails.prevSyncLongitude)
      }
   
    this.deltaLat = (this.trackDetails.lastSyncLatitude-this.trackDetails.prevSyncLatitude)/ this.numDeltas;
    this.deltaLng = (this.trackDetails.lastSyncLongitude-this.trackDetails.prevSyncLongitude)/ this.numDeltas;
    if(this.deltaLat!=0 || this.deltaLng!=0)
    this. moveMarker();
    }


  getDirection() {
    
    this.trackDetails.prevSyncLatitude == 0 ? this.trackDetails.lastSyncLatitude : this.trackDetails.prevSyncLatitude
    this.trackDetails.prevSyncLongitude == 0 ? this.trackDetails.lastSyncLongitude : this.trackDetails.prevSyncLongitude


    if (this.trackDetails.lastSyncLatitude != this.trackDetails.prevSyncLatitude ||
      this.trackDetails.lastSyncLongitude != this.trackDetails.prevSyncLongitude) {

      // this.destination = {
      //   lat: parseFloat(this.trackDetails.prevSyncLatitude),
      //   lng: parseFloat(this.trackDetails.prevSyncLongitude)
      // }
    }
   this.transition()

  }

  moveMarker() {
    console.log(this.i)
    try {
     // console.info(this.destination)
      this.destination.lat+=this.deltaLat
    this.destination.lng+=this.deltaLng
    } catch (error) {
      
    }
    var lat=this.destination.lat
    var lng= this.destination.lng
    this.destination = {
      lat:lat,
      lng:lng
    }
   console.info(this.destination)
 
    if(this.i!=this.numDeltas){
      if(this.i!=this.numDeltas)
    this.markerTimeOut=setTimeout(() => {
        this.moveMarker()
      }, this.dealy);
      this. i++;
    
  }
    // if (this.trackDetails.lastSyncLatitude != this.trackDetails.prevSyncLatitude ||
    //   this.trackDetails.lastSyncLongitude != this.trackDetails.prevSyncLongitude) {

    //   this.trackDetails.prevSyncLatitude += Math.abs(this.trackDetails.lastSyncLatitude-this.trackDetails.prevSyncLatitude)
    //   this.trackDetails.prevSyncLongitude += Math.abs(this.trackDetails.lastSyncLongitude-this.trackDetails.prevSyncLongitude)
    // }
  }
  public renderOptions = {
    suppressMarkers: true,
  }

  public markerOptions = {
    origin: {
      icon: '/assets/images/employermaplogo48.png',

      draggable: false,
    },
    destination: {
      icon: '/assets/images/mapemployer48.png',


    },
  }
}
