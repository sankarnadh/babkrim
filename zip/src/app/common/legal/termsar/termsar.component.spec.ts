import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsarComponent } from './termsar.component';

describe('TermsarComponent', () => {
  let component: TermsarComponent;
  let fixture: ComponentFixture<TermsarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
