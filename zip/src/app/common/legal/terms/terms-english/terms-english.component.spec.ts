import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsEnglishComponent } from './terms-english.component';

describe('TermsEnglishComponent', () => {
  let component: TermsEnglishComponent;
  let fixture: ComponentFixture<TermsEnglishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsEnglishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsEnglishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
