import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyEnglishComponent } from './privacy-english.component';

describe('PrivacyEnglishComponent', () => {
  let component: PrivacyEnglishComponent;
  let fixture: ComponentFixture<PrivacyEnglishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacyEnglishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyEnglishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
