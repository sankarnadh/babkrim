import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../../service/server.service';

@Component({
  selector: 'app-privacy-english',
  templateUrl: './privacy-english.component.html',
  styleUrls: ['./privacy-english.component.css']
})
export class PrivacyEnglishComponent implements OnInit {

  constructor(public service: ServerService) { }

  ngOnInit() {
  }

}
