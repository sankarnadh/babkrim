import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { ServerService } from '../../service/server.service';
import { FormBuilder, Validators, FormGroup, FormsModule, FormControl } from '@angular/forms';
import { ValidationServiceService } from '../../service/validation-service.service';
import { ValidationMessageService } from '../../service/validation-message.service'
import { NgProgress } from 'ngx-progressbar';
import '../../../assets/js/js/chat.js';
declare var google: any
declare var customealert: any;
@Component({
  selector: 'app-jobmodal',
  templateUrl: './jobmodal.component.html',
  styleUrls: ['./jobmodal.component.css']
})
export class JobmodalComponent implements OnInit {

  jobForm: any;
  viewJobModal;
  constructor(
    public ngProgress: NgProgress,
    private router: Router,
    public service: ServerService,
    public activatedRoute: ActivatedRoute,
    public validator: ValidationServiceService,
    private formBuilder: FormBuilder,
    public validationMsg: ValidationMessageService
  ) {
    this.jobForm = this.formBuilder.group
      ({
        'hourlyWages': ['', Validators.compose([Validators.required, ValidationServiceService.numberValidation])],
        'currency': ['', ValidationServiceService.numberValidation],
        'coverLetter': ['', Validators.required],
        'attachment': ['', ''],
        'wageMode': ['', Validators.required],
        'minhour': ['', Validators.compose([Validators.required, Validators.max(24)])],
        'fixedWages': ['', Validators.required]
      });
    router.events.subscribe((val) => {
      this.viewJobModal = ''
      this.viewJobModal = JSON.parse(this.service.getStorage('data'))
    });
  }

  ngOnInit() {
    this.jobForm.get('wageMode').setValue('hourly')

    if (!this.service.isEmployeeLogin())
      this.router.navigate(['/login']);
    this.service.getCurrency();                            //Fetch Currency from API
    this.jobForm.get('currency').setValue(this.service.defaultCurrency)
    if (!this.service.getStorage('data'))
      this.router.navigate(['/home']);
    customealert.loaderShow('html')
    let data = this.service.getStorage('data')
    this.service.loader = false
    this.activatedRoute.params.subscribe((res) => {

      // this.getJobDetailsById(res['data'])

      this.viewJobModal = null

      this.viewJobModal = JSON.parse(data)
      if (!this.viewJobModal)
        this.router.navigate(['/home']);
      if (this.viewJobModal.wageMode == 'fixed')
        this.jobForm.get('wageMode').setValue('fixed')

      this.jobForm.get('hourlyWages').setValue(this.viewJobModal.hourlyWages ? this.viewJobModal.hourlyWages : 1)
      this.jobForm.get('minhour').setValue(this.viewJobModal.minHour ? this.viewJobModal.minHour : 1)
      this.jobForm.get('fixedWages').setValue(this.viewJobModal.fixedWage ? this.viewJobModal.fixedWage : 1)

    });
  }

}
