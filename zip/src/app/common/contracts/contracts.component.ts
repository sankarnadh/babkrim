import { Component, OnInit, OnDestroy } from '@angular/core';
import { CdTimerComponent } from 'angular-cd-timer';
import { ServerService } from '../../service/server.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner'
import { ValidationMessageService } from '../../service/validation-message.service'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { ConditionalExpr } from '@angular/compiler';
import '../../../assets/js/js/chat.js'
declare var customealert;
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.css'],
  providers: [CdTimerComponent]
})
export class ContractsComponent implements OnInit,OnDestroy {
  /********************************************************* */
  ngOnDestroy(){
    this.t=0;
  }
  t=0;
  recordList: any[] = [
    { 'myList': [false, false, false, false, false] },
  ];
  rating: number;
  returnParseFloat(val) {
    return parseFloat(val)
  }
  resetInvoice() {
    this.tempTotal = 0
  }
  setStarTable(record: any, data: any) {
    this.currentRate = data + 1;
    var tableList = this.recordList.find(function (obj: any) { return obj.Id === record.Id });
    for (var i = 0; i <= 4; i++) {
      if (i <= data) {
        tableList.myList[i] = false;
      }
      else {
        tableList.myList[i] = true;
      }
    }
  }
  /********************************************************* */
  constructor(public timer: CdTimerComponent,
    public config: NgbRatingConfig,
    public service: ServerService,
    private spinner: Ng4LoadingSpinnerService,
    public validationMsg: ValidationMessageService) {

  }

  removeInvoiceList(val) {
    this.service.invoiceList.forEach((element, index) => {
      if (element == val) {
        console.log(this.service.invoiceList)
        console.log(Math.random())
        this.service.invoiceList.splice(index, 1)
      }
    });
    this.service.invoiceSubTotal = 0
    this.service.invoiceList.forEach(element => {
      if (element.quantity != '' && element.price != '') {
        this.service.invoiceSubTotal = Math.abs(this.service.invoiceSubTotal - (element.quantity * element.price))
      }
    });
    this.tempTotal = this.service.invoiceTotal
    this.tempTotal -= this.service.invoiceSubTotal;

  }
  emptyInvoiceList() {
    this.service.invoiceList = []
    this.service.invoiceTotal = 0
    // this.addInvoiceList()
  }
  tempTotal = this.service.invoiceTotal

  addInvoiceList() {
    if (this.service.invoiceList.length == 0) {
      
      this.service.invoiceList.push({
        item: '',
        quantity: '',
        price: ''
      })
    }
    else {
      this.invoiceError = false
      let flag = 0
      this.service.invoiceList.forEach(element => {
        if (element.item == '' && element.quantity == '' && element.price == '') {
          flag = 1
          this.invoiceError = true
          return false
        }
      });
      if (flag == 0)
        this.service.invoiceList.push({
          item: '',
          quantity: '',
          price: ''
        })
    }
    this.calculateTotal()
  }
  calculateTotal() {
    this.service.invoiceSubTotal = 0
    this.tempTotal = this.service.invoiceTotal
    this.service.invoiceList.forEach(element => {
      if (element.quantity != '' && element.price != '') {
        this.service.invoiceSubTotal += element.quantity * element.price
      }
    });
    console.log(this.service.invoiceSubTotal)
    console.log(this.service.invoiceTotal)
    this.tempTotal += this.service.invoiceSubTotal;
  }
  employera1Event(event) {
    this.A1 = (event.target.value)
  }
  employera3Event(event) {
    this.A3 = (event.target.value)
  }
  employera2Event(event) {
    this.A2 = (event.target.value)
  }
  employera4Event(event) {
    this.A4 = (event.target.value)
  }
  employerA6Event(event) {
    this.A6 = (event.target.value)
  }
  employerA5Event(event) {
    this.A5 = (event.target.value)
  }
  itemList = []
  ngOnInit() {
    this.invoiceError=false
    // this.addInvoiceList();
    this.service.loader = true
    this.config.max = 5;
    this.tempTotal = this.service.invoiceTotal
  }
  optionArray: any = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  A1 = 10
  A2 = 1
  A3 = 1
  A4 = 10
  A5 = 1
  A6 = 1
  A7 = 10;
  currentRate = 5
  employercomment: any = null;
  error = 0;
  submitEvaluation() {
    var data
    // alert(this.service.getStorage('tempTaskId'))
    // if(!this.service.getStorage('contractTempId') || this.service.getStorage('contractTempId')=='undefined'||this.service.getStorage('contractTempId')==undefined || this.service.getStorage('contractTempId')==null)
    // {
    //   customealert.hideModal('exampleModal');
    //   this.service.showError(this.validationMsg.networkError)
    //   return false
    // }
    if (!this.employercomment) {

      this.error = 1
      return false
    }
    this.error = 0
    if (this.service.isEmployeeLogin())
      data = {

        "answer1": this.A1 == 1 ? '1' : '0',
        "answer2": this.A2 == 1 ? '1' : '0',
        "answer3": this.A3 == 1 ? '1' : '0',
        "answer4": this.A4 == 1 ? '1' : '0',
        "answer5": this.A5 == 1 ? '1' : '0',
        "answer6": this.A6 == 1 ? '1' : '0',
        "answer7": this.A7,
        "contractId": this.service.getStorage('contractTempId'),
        "comment": this.employercomment,

        "rating": this.currentRate
      }
    if (this.service.isEmployerLogin())
      data = {

        "answer1": this.A1,
        "answer2": this.A2,
        "answer3": this.A3,
        "answer4": this.A4,
        "answer5": '-1',
        "answer6": '-1',
        "answer7": '-1',
        "contractId": this.service.getStorage('contractTempId'),
        "comment": this.employercomment,

        "rating": this.currentRate
      }

    customealert.loaderShow('html')
    this.service.serverRequest(data, 'evaluation/' + this.service.getStorage('contractTempId') + '.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.employercomment = ''
        this.recordList = [
          { 'myList': [false, false, false, false, false] },


        ];
        this.service.getCompletedContractsFromApi();
        customealert.hideModal('exampleModal')
        this.service.showSuccess(this.validationMsg.success)
      } else
        this.service.apiErrorHandler(res)
    },
      err => {
        customealert.hideModal('exampleModal')
        this.service.serviceErrorResponce(err)
      })
  }
  invoiceError = false
  submitInvoice() {
    let flag = 0

    this.invoiceError = false
    this.service.invoiceList.forEach((element, index) => {
      if (element.item == '' && element.quantity == '' && element.price == '') {
        if (index != 0) {
          flag = 1
          if(this.service.invoiceList.length==0)
          {this.invoiceError = true
          return false}
        }
        else
          this.service.invoiceList = []
      }
    });

    if (flag == 0)
      this.service.serverRequest({ 'invoice': this.service.invoiceList }, 'contractDetails/' + this.service.taskProposal + '/invoice.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
        this.addInvoiceList()
        if (res['status'].responseCode == 20) {
          this.service.showSuccess(this.validationMsg.success)
          customealert.hideModal('modal_job_invoice')

          this.service.getActiveContracts();
        }
        else
          this.service.apiErrorHandler(res)
      }, er => { this.service.serviceErrorResponce(er);this.addInvoiceList() })
    console.log(this.service.invoiceList)
  }
}
