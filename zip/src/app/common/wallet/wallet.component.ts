import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ServerService } from '../../service/server.service'
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from "@nomadreservations/ngx-stripe";

import '../../../assets/js/js/chat.js'
import { UserDataSettingsComponent } from 'src/app/component/user-data-settings/user-data-settings.component';
declare var customealert;
@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit, OnDestroy {
  stripeKey = '';
  error: any;
  transactionLimit=5;
  walletHistoryLimit=5;
  limit=5
  complete = false;
  element;
  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#276fd3',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };

  elementsOptions: ElementsOptions = {
    locale: 'en'
  };


  constructor(public service: ServerService, private _stripe: StripeService, private user: UserDataSettingsComponent) {

  }
  cardUpdated(result) {
    this.element = result.element;
    this.complete = result.card.complete;
    this.error = undefined;
  }

  keyUpdated() {
    this._stripe.changeKey(this.stripeKey);
  }

  getCardToken() {
    customealert.loaderShow("html");
    this._stripe.createToken(this.element, {

    }).subscribe((res) => {
      // Pass token to service for purchase.
      this.addMoney(res)
    }, er => console.log(er));
  }

  ngOnInit() {
    this.user.getTransactionHistory()
    this.user.getWalletHistory()
  }
  addMoney(token: any) {
    customealert.loaderShow("html");
    // console.info(JSON.parse(token))
    const data = {
      "amount": this.service.walletAmount,

      "cardToken": token.token.card.id,
      "paymentType": "card",
      "sourceType": token.token.id
    }

    this.service.serverRequest(data, '/payment/wallet/sendMoney.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide("html");

      if (res['status'].responseCode == 20) {
        customealert.hideAllModal();
        this.user.getWalletHistory()
        this.user.getTransactionHistory()
      } else {
        this.service.apiErrorHandler(res)
      }
    }, er => { this.service.serviceErrorResponce(er) })
  }

  ngOnDestroy(): void {
    this.service.walletAmount = null

  }
}
