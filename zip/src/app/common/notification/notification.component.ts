import { Component, OnInit } from "@angular/core";
import { ServerService } from "../../service/server.service";
import { Router } from "@angular/router";
import { ValidationMessageService } from "../../service/validation-message.service";
import "../../../assets/js/js/chat.js";
declare var customealert: any;
@Component({
  selector: "app-notification",
  templateUrl: "./notification.component.html",
  styleUrls: ["./notification.component.css"]
})
export class NotificationComponent implements OnInit {
  constructor(
    public service: ServerService,
    private router: Router,
    public validationMsg: ValidationMessageService
  ) {}

  ngOnInit() {
    this.loadNotification();
    setInterval(() => {
      if (this.service.logoutClick == 0)
        if (this.service.isEmployeeLogin() || this.service.isEmployerLogin()) {
          if (this.service.getRegisterLevel() == "0") {this.loadNotification();
          this.service.getChatUnreadCount();}
        }
    }, 10000);
  }

  loader = true;
  notification: any = [];

  notificationLength: number = 0;
  pageNo = 1;
  lastAction: any = "";
  lastUrl: any;
  loadNotification() {
    var timeString = new Date().getTime();
    this.service
      .serverGetRequest(
        "",
        "notifications.json?registerType=" +
          this.service.getStorage("registerType") +
          "&pageNo=" +
          this.pageNo +
          "&timeStamp=" +
          timeString
      )
      .subscribe(
        res => {
          //this.loader=false;
          if (res["status"].responseCode == 20) {
            this.service.wallet = res["result"].walletBalance;
            this.service.runningContract = res["result"].runningContract;
            let url = this.router.url.split("/")[1];

            if (
              url == "employer-contracts" &&
              this.service.previousRunningContract.length !=
                this.service.runningContract.length
            ) {
              this.service.previousRunningContract = this.service.runningContract;

              this.service.getActiveContracts();
            }
            if (
              this.service.runningContract.length == 0 &&
              this.service.previousRunningContract.length == 0
            ) {
              this.service.previousRunningContract =
                res["result"].runningContract;
            }

            this.service.runningContractCount = this.service.runningContract.length;
            if (
              url == "employer-contracts" &&
              this.service.runningContract.length != 0
            ) {
              let flag = 0;
              this.service.runningContract.forEach((element, key) => {
                if (
                  element.lastAction !=
                  this.service.previousRunningContract[key].lastAction
                ) {
                  flag = 1;
                }
              });
              if (
                flag == 1 ||
                this.service.previousRunningContract.length !=
                  this.service.runningContract.length
              ) {
                this.service.previousRunningContract = this.service.runningContract;
                this.service.getActiveContracts();
              }
            }

            /***** */

            this.notification = res["result"].notificationDetails;
            this.service.summery = res["result"].summary;
            if (this.notification)
              this.service.notificationLength = res["result"].totalCount;
          } else this.service.apiErrorHandler(res);
        },
        er => {
          // this.service.serviceErrorResponce(er)
        }
      );
  }

  checkType(val) {
    if (val.status == 0) {
      val.status = 1;
      this.service.notificationLength =
        this.service.notificationLength == 0
          ? 0
          : this.service.notificationLength - 1;
      this.readNotification(val.notificationId);
    }
    if (val.notificationType == "invitation" && this.service.isEmployeeLogin())
      this.getInvitationDetails(val);
    if (
      val.notificationType == "invitation" &&
      this.service.isEmployerLogin()
    ) {
      this.getInvitationDetails(val);
    }
    if (val.notificationType == "offers" && this.service.isEmployeeLogin())
      this.getOfferDetails(val);
    if (val.notificationType == "offers" && this.service.isEmployerLogin())
      this.router.navigate(["/viewContract/" + val.notificationTypeId]);
    if (val.notificationType == "proposal" && this.service.isEmployerLogin()) {
      // this.service.showError('Failed to load notification')
      this.getProposalDetails(val); //API is missing data so
    }
    if (val.notificationType == "contract") {
      this.service.setStorage("contractId", val.notificationTypeId);
      this.router.navigate(["/jobtimeline/" + val.notificationTypeId]);
    }
  }
  getInvitationDetails(val) {
    customealert.loaderShow("html");

    let invitationId = val.notificationTypeId;

    this.service
      .serverGetRequest(
        "",
        "invitationDetails/" +
          invitationId +
          ".json?registerType=" +
          this.service.getStorage("registerType")
      )
      .subscribe(res => {
        if (res["status"].responseCode == 20) {
          this.loader = false;
          if (this.service.isEmployeeLogin()) {
            this.service.setStorage(
              "data",
              JSON.stringify(res["result"].invitation)
            );
            this.router.navigate(["/inviteinterview_employee"]);
          }
          if (this.service.isEmployerLogin()) {
            this.service.setStorage(
              "data",
              JSON.stringify(res["result"].invitation["jobDetails"])
            );
            // console.log(res['result'].invitation['jobDetails'])
            this.service.setStorage(
              "proposal",
              JSON.stringify(res["result"].invitation)
            );
            console.log(JSON.parse(this.service.getStorage("proposal")));
            this.router.navigate(["/viewInvitationDetails"]);
          }
          customealert.loaderHide("html");
        } else this.service.apiErrorHandler(res);
      });
  }
  getOfferDetails(val) {
    customealert.loaderShow("html");

    let proposalId = val.notificationTypeId;

    this.service
      .serverGetRequest(
        "",
        "contractDetails/" +
          proposalId +
          ".json?registerType=" +
          this.service.getStorage("registerType")
      )
      .subscribe(res => {
        customealert.loaderHide("html");
        if (res["status"].responseCode == 20) {
          this.loader = false;

          this.service.setStorage(
            "data",
            JSON.stringify(res["result"].contract)
          );

          this.router.navigate(["/myoffers-employee"]);
        } else this.service.apiErrorHandler(res);
      });
  }
  getProposalDetails(val) {
    let proposalId;

    customealert.loaderShow("html");
    this.service
      .serverGetRequest(
        "",
        "proposalDetails/" +
          val.notificationTypeId +
          ".json?registerType=" +
          this.service.getStorage("registerType")
      )
      .subscribe(
        res => {
          customealert.loaderHide("html");

          if (res["status"].responseCode == 20) {
            this.loader = false;
            this.service.setStorage(
              "proposal",
              JSON.stringify(res["result"].proposal)
            );
            this.service.setStorage(
              "data",
              JSON.stringify(res["result"].proposal["jobDetails"])
            );
            this.router.navigate(["/view_employee"]);
          } else this.service.apiErrorHandler(res);
        },
        er => {
          customealert.loaderHide("html");

          this.service.serviceErrorResponce(er);
        }
      );
  }
  readNotification(notificationId) {
    
    this.service
      .serverPutRequest(
        "",
        "notifications/" +
          notificationId +
          ".json?registerType=" +
          this.service.getStorage("registerType")
      )
      .subscribe(res => {
        if (res["status"].responseCode == 20) {
        } else this.service.apiErrorHandler(res);
      });
  }
}
