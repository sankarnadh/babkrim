import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentMesageComponent } from './recent-mesage.component';

describe('RecentMesageComponent', () => {
  let component: RecentMesageComponent;
  let fixture: ComponentFixture<RecentMesageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentMesageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentMesageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
