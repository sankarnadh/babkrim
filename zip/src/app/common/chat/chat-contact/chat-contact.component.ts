import { Component, OnInit } from '@angular/core';
import {ChatService} from '../../../service/chat/chat-service.service'
import {ServerService} from '../../../service/server.service'
@Component({
  selector: 'app-chat-contact',
  templateUrl: './chat-contact.component.html',
  styleUrls: ['./chat-contact.component.css']
})
export class ChatContactComponent implements OnInit {

  constructor(public chatService : ChatService,
    public service : ServerService) { }

  ngOnInit() {
  }

}
