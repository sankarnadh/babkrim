import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoblistnologinComponent } from './joblistnologin.component';

describe('JoblistnologinComponent', () => {
  let component: JoblistnologinComponent;
  let fixture: ComponentFixture<JoblistnologinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoblistnologinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoblistnologinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
