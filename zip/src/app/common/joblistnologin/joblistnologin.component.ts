import { Component, OnInit } from '@angular/core';
import { NgProgress } from 'ngx-progressbar';
import { Router, ActivatedRoute } from '@angular/router';
import { ServerService } from 'src/app/service/server.service';
import { ValidationServiceService } from 'src/app/service/validation-service.service';
import { FormBuilder } from '@angular/forms';
import { ValidationMessageService } from 'src/app/service/validation-message.service';
import '../../../assets/js/js/chat.js'
declare var customealert ;
@Component({
  selector: 'app-joblistnologin',
  templateUrl: './joblistnologin.component.html',
  styleUrls: ['./joblistnologin.component.css']
})
export class JoblistnologinComponent implements OnInit {

  constructor(
    public ngProgress: NgProgress,
    private router: Router,
    public service: ServerService,
    public activatedRoute: ActivatedRoute,
    public validator: ValidationServiceService,
    private formBuilder: FormBuilder,
    public validationMsg: ValidationMessageService
  ) { }
  viewJobModal: any = [];
  offer:any=[];
  ngOnInit() {
    if(this.service.isEmployeeLogin()){
      this.router.navigate(['/home']);
    }
    this.activatedRoute.params.subscribe((res) => {

      this.getJobById(res['data'])

    });
  }
  getJobById(id) {
    customealert.loaderShow("html")
    this.service.serverGetRequest({}, 'jobDetails/' + id + '.json').subscribe(res => {
      customealert.loaderHide("html")
      if (res['status'].responseCode == 20) {
        this.viewJobModal = res['result'];
        this.offer=this.viewJobModal;
      } else {
        this.service.apiErrorHandler(res)
      }
    }, er => {
      this.service.serviceErrorResponce(er);
    })
  }
}
