import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineJobdetailsComponent } from './timeline-jobdetails.component';

describe('TimelineJobdetailsComponent', () => {
  let component: TimelineJobdetailsComponent;
  let fixture: ComponentFixture<TimelineJobdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineJobdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineJobdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
