import { Component, Input, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';

@Component({
  selector: 'app-timeline-jobdetails',
  templateUrl: './timeline-jobdetails.component.html',
  styleUrls: ['./timeline-jobdetails.component.css'],
})
export class TimelineJobdetailsComponent implements OnInit {
  @Input() showNumber: number;
  @Input() proposalMode: any='grab';
  @Input() type: any; //proposal or contract or edit 
  @Input() data: any;
  constructor(public service : ServerService) { }

  ngOnInit() {
  }

}
