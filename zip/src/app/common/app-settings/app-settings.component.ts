import { Component, OnInit } from '@angular/core';
import {ServerService} from '../../service/server.service';
import {ValidationMessageService} from '../.././service/validation-message.service'
import '../../../assets/js/js/chat.js';
declare var customealert: any;
declare var require : any;
@Component({
  selector: 'app-app-settings',
  templateUrl: './app-settings.component.html',
  styleUrls: ['./app-settings.component.css']
})
export class AppSettingsComponent implements OnInit {

  hide = true;
  timezonesjson = require('angular-timezone/timezones.json');
constructor(public service : ServerService, public validationMsg :ValidationMessageService) { }
timeZones

myTimeZone: any=0;
ngOnInit() {


if(this.service.getStorage('timeZone'))
  this.myTimeZone=this.service.getStorage('timeZone');
  this.timeZones = this.timezonesjson
}

updateTimeZone()
{
if(this.myTimeZone!=0)
{
  let data={
    'timeZone':this.myTimeZone
  }
  console.log(this.myTimeZone)
  customealert.loaderShow('html')
  this.service.serverPutRequest(data,'settings.json?registerType='+this.service.getStorage('registerType')).subscribe(res=>{
  customealert.loaderHide('html')
    
    if(res['status'].responseCode==20)
    {
      this.service.setStorage('timeZone',this.myTimeZone)
    
      this.service.showSuccess('Timezone Updated')
    }
    else
    this.service.apiErrorHandler(res)
  },
  err=>{
  customealert.loaderHide('html')

    this.service.serviceErrorResponce(err)
  }
  )
}
else 
{
  this.service.showError('Invalid Timezone')
}
}

}
