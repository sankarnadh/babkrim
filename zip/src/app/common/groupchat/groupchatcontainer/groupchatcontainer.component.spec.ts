import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupchatcontainerComponent } from './groupchatcontainer.component';

describe('GroupchatcontainerComponent', () => {
  let component: GroupchatcontainerComponent;
  let fixture: ComponentFixture<GroupchatcontainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupchatcontainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupchatcontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
