import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../../service/server.service'
import '../../../../../assets/js/js/chat.js'
import { ValidationMessageService } from 'src/app/service/validation-message.service';
declare var customealert;
@Component({
  selector: 'app-groupsearch',
  templateUrl: './groupsearch.component.html',
  styleUrls: ['./groupsearch.component.css']
})
export class GroupsearchComponent implements OnInit {

  constructor(public service: ServerService, public validationMsg : ValidationMessageService) { }

  ngOnInit() {
    this.searchGroup()
  }
  groupList: any = []
  groupName = ''
  loader: boolean = false
  tempGroupName
  tempGroupId: any
  tempGroupDescription:any
  searchGroup() {
    this.loader = true
    this.groupList = []
    this.service.serverGetRequest('', 'chat/groups/search.json?groupName=' + this.groupName).subscribe(res => {
      this.loader = false
      if (res['status'].responseCode == 20) {
        this.groupList = res['result'].groupNames
      }
      else
        this.service.apiErrorHandler(res)
    }, er => {
      this.service.serviceErrorResponce(er);
      this.loader = false
    })
  }
  joinGroup() {
    this.service.serverRequest('', 'chat/groups/join/{groupId}.json?groupId='+this.tempGroupId).subscribe(res => {
      if (res['status'].responseCode == 20)
      {
        this.service.showSuccess(this.validationMsg.success)
        this.service.groupName=this.tempGroupName
        this.service.groupGroupId=this.tempGroupId
        this.service.groupDescription=this.tempGroupDescription
        this.service.groupActiveTab=1;
      }
      else 
      this.service.apiErrorHandler(res)
    }, er => { this.service.serviceErrorResponce(er) })
  }
}
