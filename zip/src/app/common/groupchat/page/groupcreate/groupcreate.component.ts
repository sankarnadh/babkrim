import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../../service/server.service'
import '../../../../../assets/js/js/chat.js'
import { ValidationMessageService } from 'src/app/service/validation-message.service';
declare var customealert;
@Component({
  selector: 'app-groupcreate',
  templateUrl: './groupcreate.component.html',
  styleUrls: ['./groupcreate.component.css']
})
export class GroupcreateComponent implements OnInit {

  constructor(public service: ServerService
    , public validationMsg : ValidationMessageService) { }
  groupName: any;
  loading:boolean=false 
  groupDescription: any;
  ngOnInit() {
  }
  createGroup() {

    let data={
      "description": this.groupDescription,
      "groupId": "",
      "groupName":this.groupName,
      "memberId": ""
    }
    this.loading=true
    this.service.serverRequest(data, 'chat/groups.json').subscribe(res => {
      this.loading=false
      if (res['status'].responseCode == 20) { 
        this.service.showSuccess(this.validationMsg.success)
        this.service.groupGroupId=res['result'].groupID
        this.service.groupDescription=this.groupDescription
        this.service.groupName=this.groupName
        this.service.groupActiveTab=1
      }
      else
      this.service.apiErrorHandler(res)
    },
      er => this.service.serviceErrorResponce(er))
  }
}
