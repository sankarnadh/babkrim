import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../../service/server.service'
import{DatePipes} from '../../../../service/datepipe'
@Component({
  selector: 'app-grouplist',
  templateUrl: './grouplist.component.html',
  styleUrls: ['./grouplist.component.css'],
  providers:[DatePipes]
})
export class GrouplistComponent implements OnInit {

  constructor(public service: ServerService,public datepipes:DatePipes) {

  }

  ngOnInit() {
    this.getMyGroupList()
    this.service.groupActiveTab = 1

  }
  ownGroup = []
  joinedGroup = []
  getMyGroupList() {
    this.service.serverGetRequest('', 'chat/groups/me.json').subscribe(res => {
      if (res['status'].responseCode == 20) {
        this.ownGroup = res['result'].ownGroup
        console.log(this.ownGroup)
        this.joinedGroup = res['result'].joinedGroup
      }
      else
        this.service.apiErrorHandler(res)
    },
      er => this.service.serviceErrorResponce(er))
  }
  setValues(val, adminStatus) {
    this.service.groupGroupId = val.groupId
    this.service.groupDescription = val.groupDescription
    this.service.groupName = val.groupName
    adminStatus == 1 ? this.service.isGroupAdmin = 1 : this.service.isGroupAdmin = 0
    this.service.groupActiveTab = 2
  }
  setDefaultGroupValues() {
    this.service.groupGroupId = this.service.getStorage('groupChatId')
    this.service.groupDescription = this.service.getStorage('groupChatGroupDescription')
    this.service.groupName = this.service.getStorage('groupChatGroupName')
   this.service.isGroupAdmin =2 // To not show group info 
    this.service.groupActiveTab = 2
  }
}
