import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../../service/server.service'
import '../../../../../assets/js/js/chat.js'
import { ValidationMessageService } from 'src/app/service/validation-message.service';
declare var customealert
@Component({
  selector: 'app-edit-group-name',
  templateUrl: './edit-group-name.component.html',
  styleUrls: ['./edit-group-name.component.css']
})
export class EditGroupNameComponent implements OnInit {

  constructor(public service: ServerService,
    public validationMsg : ValidationMessageService) { }
  groupName: any = this.service.groupName;
  loading: boolean = false
  ngOnInit() {
  }
  updateGroupName() {
    let data = {
      "description": this.service.groupDescription,
      "groupId": this.service.groupGroupId,
      "groupName": this.groupName,
      "memberId": ""
    }
    this.loading=true
    this.service.serverPutRequest(data, 'chat/groups/'+this.service.groupGroupId+'.json').subscribe(res => {
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success);
        this.service.groupActiveTab = 3
      }
      else
        this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
}
