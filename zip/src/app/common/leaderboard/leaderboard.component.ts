import { Component, OnInit } from '@angular/core';
import { ServerService } from 'src/app/service/server.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  constructor(public service: ServerService,
    private datepipe: DatePipe,
    private router: Router) { }
  toDate: any = new Date();
  fromDate: any = new Date();
  ngOnInit() {
    this.fromDate = this.fromDate.setDate(this.toDate.getDate() - 90)
    this.fromDate = this.datepipe.transform(this.fromDate, 'yyyy-MM-dd hh:mm:ss', 'UTC')
    this.toDate = this.datepipe.transform(this.toDate, 'yyyy-MM-dd hh:mm:ss', 'UTC')
    this.getSummery()

  }
  leaderboards: any
  getSummery() {
    this.service.serverGetRequest('', '/statistics/summary.json?registerType=' +
      this.service.getStorage('registerType') + '&&fromTime=' + this.fromDate + '&toTime=' + this.toDate).subscribe(res => {
        if (res['status'].responseCode == 20) {
          this.service.summeryDetails = res['result'].summary

          this.service.positive = (this.service.summeryDetails['reviews'].positive / this.service.summeryDetails['reviews'].total) * 100;
          this.service.neutral = (this.service.summeryDetails['reviews'].neutral / this.service.summeryDetails['reviews'].total) * 100;
          this.service.negative = (this.service.summeryDetails['reviews'].negative / this.service.summeryDetails['reviews'].total) * 100;

          this.leaderboards = res['result'].summary.leaderboards
          console.log(this.leaderboards)
        }
       
      },
        er => this.service.serviceErrorResponce(er))
  }
  gotoEmployerPage(userId) {
    if (this.service.isEmployerLogin()) {
       this.service.setStorage('employerTempId', userId);
      this.router.navigate(['/view_company_details'])
      }
      else 
      {
        this.service.setStorage('employeeTempId', userId);
        this.router.navigate(['/hire_person_direct'])
      }
  }
}
