import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service'
import { FormBuilder, Validators, FormsModule, FormControl } from '@angular/forms';
import { ValidationMessageService } from '../../service/validation-message.service'
import '../../../assets/js/js/chat.js'
declare var customealert: any;
@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {
  passwordForm: any;
  constructor(
    public service: ServerService,
    private formBuilder: FormBuilder,
    public validationMsg: ValidationMessageService
  ) {
    this.passwordForm = this.formBuilder.group({

      'currentPassword': ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])],
      'confirmPassword': ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])],
      'newPassword': ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])]
    });
  }
  buttonClick: any = 0;
  ngOnInit() {
  }
  curpwd : any ;
  changePassword() {
   if (!this.passwordForm.get('currentPassword').valid) {
      this.buttonClick = 1;
      return false
    } else this.buttonClick = 0;
    if (!this.passwordForm.get('confirmPassword').valid) {
      this.buttonClick = 1;
      return false
    } else this.buttonClick = 0;
    if (!this.passwordForm.get('newPassword').valid) {
      this.buttonClick = 1;
      return false
    } else  this.buttonClick = 0;
    if (this.passwordForm.get('newPassword').value!=this.passwordForm.get('confirmPassword').value) {
      this.buttonClick = 1;
      return false
    } else
    {
      this.buttonClick = 0;
      customealert.loaderShow("html")
      let data = {
        currentPassword: this.passwordForm.get('currentPassword').value,
        confirmPassword: this.passwordForm.get('confirmPassword').value,
        newPassword: this.passwordForm.get('newPassword').value,
      }
      this.service.serverPutRequest(data, 'settings/changePassword.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
        customealert.loaderHide("html")
        if (res['status'].responseCode == 20) {
          this.service.showSuccess(this.validationMsg.passwordUpdated)
          this.passwordForm.reset();
        }
        else this.service.apiErrorHandler(res)
      },err=>{
        this.service.serviceErrorResponce(err)
      })
    }
  }
}
