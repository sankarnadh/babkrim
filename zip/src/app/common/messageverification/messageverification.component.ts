import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { ValidationMessageService } from '../../service/validation-message.service'
import { Router, ActivatedRoute } from "@angular/router";
import '../../../assets/js/js/chat.js';
import { TranslateService } from '@ngx-translate/core';
declare var customealert;
@Component({
  selector: 'app-messageverification',
  templateUrl: './messageverification.component.html',
  styleUrls: ['./messageverification.component.css']
})
export class MessageverificationComponent implements OnInit {

  constructor(public service: ServerService,
    public validationMsg: ValidationMessageService,
    private router: Router,
    private translate:TranslateService,
    public activatedRouter: ActivatedRoute) {
      translate.get("income", {  }).subscribe((res: string) => {
        console.info(this.translate.instant('level1EmailExists'));
       
      });
     }
  timmer = 120;
  timeCount = 120;
  resendtimeleft: any = this.timmer;
  callOtp: any = 0;
  showMessage: any =0;
  ngOnInit() {
    setInterval(() => {
      this.timmer = this.timmer - 1;
      if (this.resendtimeleft != 0 && this.service.getStorage("resendOtpFlag") != "true") {
        this.resendtimeleft = this.secondsToHms(this.timmer);
      } else {
       
        if (this.callOtp != 0 && this.service.getStorage('callOtpFlag') != 'true') {
          console.log(this.secondsToHms(this.timmer))
          this.callOtp = this.secondsToHms(this.timmer);
        }
        else{
         
          if (this.service.getStorage('callOtpFlag') == 'true') {
           
            this.showMessage = this.secondsToHms(this.timmer);
          }
          if(this.service.getStorage('callOtpFlag') == 'true'&&  this.showMessage ==0) {
            this.smsOtpMsg= this.translate.instant('contactinformation')//'If you have any queries please drop a mail to hello@babkrim.com or call us +91 7511 184 184 (9 AM - 5 PM IST)'
          }
        }
      }
    }, 1000);
    this.service.checkLogin(); //To change the login header
    if (this.service.getStorage('mobileVerify') == 'true') {
      if (this.service.isEmployeeLogin())
        this.service.employeeNavigation()
      else
        this.service.employerNavigation()
    }
    let mobile = this.service.getStorage('mobileNo');
    if (mobile) {
      let star = '';
      for (let i = 0; i < (mobile.length) - 2; i++) {

        star += '*';
      }
      this.translate.get('otpsendmsg', {x: star + mobile[(mobile.length) - 2] + mobile[(mobile.length) - 1]}).subscribe((res: string) => {
        this.smsOtpMsg=(res);
        //=> 'hello world'
    });
      // this.smsOtpMsg=this.translate.instant('otpsendmsg',{x:star + mobile[(mobile.length) - 2] + mobile[(mobile.length) - 1]})
      // this.smsOtpMsg = "OTP has been send to " + star + mobile[(mobile.length) - 2] + mobile[(mobile.length) - 1]

    }
  }
  otp;
  bottonClick = 0
  /****
   * Resending Otp 
   */
  smsOtpMsg: any
  sendSms() {
    this.smsOtpMsg = null
    customealert.loaderShow("html")
    let response: any;
    this.service.serverRequest('', '/otp/sms/send.json?registerType=' + this.service.getStorage('registerType')).subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.smsOtpMsg = res['result'].message
          this.timmer = this.timeCount;
          this.service.setStorage('resendOtpFlag', 'true');
          this.callOtp = this.timmer;
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }
  callSmsOtp() {
    this.smsOtpMsg = null
    customealert.loaderShow("html")
    let response: any;
    this.service.serverRequest('', '/otp/call/send.json?registerType=' + this.service.getStorage('registerType')).subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.smsOtpMsg = res['result'].message
          this.timmer = this.timeCount;
          this.service.setStorage('callOtpFlag', 'true');
          this.showMessage=this.timmer;
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }
  /******
   * Verify Sms Otp
   */
  otp_valid = false
  verifyOtp() {
    this.smsOtpMsg = ''
    let otp = this.otp;
    if (otp) {
      customealert.loaderShow("html")
      let response: any;

      this.service.serverRequest('', '/otp/sms/verify.json?registerType=' + this.service.getStorage('registerType') + '&OTP=' + otp).subscribe(
        (res: Response) => {
          customealert.loaderHide("html")
          response = res['status'];
          if (response.responseCode == 20) {

            if (res['result'].message) {
              this.smsOtpMsg = this.validationMsg.otpVerified;
              this.service.setStorage('mobileVerify', 'true');
              this.otp_valid = true;
              if (this.service.isEmployeeLogin()) {
                this.router.navigate(['/employee_accountsetup'])
              }
              else
                this.router.navigate(['/employer_companydetails'])
            }

            else
              this.smsOtpMsg = this.validationMsg.invalidOtp;

          }
          else {
            this.service.apiErrorHandler(res);


          }

        },
        err => {
          this.service.serviceErrorResponce(err)
          customealert.loaderHide("html")
        }
      );
    }
    else
      this.smsOtpMsg = this.validationMsg.invalidOtp
  }

  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? ":" : ":") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? ":" : ":") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? "" : "") : "";
    return hDisplay + mDisplay + sDisplay;
  }
}
