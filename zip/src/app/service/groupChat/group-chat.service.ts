import { Injectable } from '@angular/core';
import { WebsocketService } from '../websocket.service';
import { Observable, Subject } from 'rxjs/Rx';
import { ServerService } from '../server.service'
import { DatePipe } from '@angular/common';
import {url} from '../url';
declare var $;
export interface Message {
  fromType: String,
  message: String,
  messageType: String,
  to?: String,
  proposalId?: String,
  status: number,
  receiveFrom: String,
  createdTime: any,
  profileImage: String,
  grpCreatedBy: String,
  createdUserName?: String
}
const CHAT_URL = url.chatUrl;
@Injectable({
  providedIn: 'root'
})
export class GroupChatService {


  public messages: Subject<Message>;
  failed = false;
  date = new Date();
  tempId = this.service.groupGroupId
  chatId: any = 'grp-' + this.service.groupGroupId + 'KUID' + Math.floor((Math.random() * 999999) + 1)
  constructor(public wsService: WebsocketService, public service: ServerService, private datePipe: DatePipe, ) {

    setInterval(() => { 
      if(this.tempId!=this.service.groupGroupId)
      {
        this.tempId=this.service.groupGroupId;
        this.chatId='grp-' + this.service.groupGroupId + 'KUID' + Math.floor((Math.random() * 999999) + 1)
      }
    }, 1)
    this.messages = <Subject<Message>>wsService
      .connect(CHAT_URL + this.chatId)
      .map((response: MessageEvent): Message => {
        let data = JSON.parse(response.data);

        return {
          fromType: data.fromType,
          message: data.message,
          messageType: data.messageType,
          to: data.to,
          profileImage: data.profileImage,
          proposalId: data.proposalId,
          status: data.status,
          receiveFrom: data.receiveFrom,
          createdTime: this.date,
          grpCreatedBy: data.grpCreatedBy,
          createdUserName: data.createdUserName
        }
      });
  }
  restartConnection() {
    this.wsService.connect(CHAT_URL + this.chatId)
  }

  messageBox: any;
  receiverId = null

  proposalId = null
  message = []
  sendMsg() {

    if (this.messageBox != ' ' && this.messageBox) {

      let chatmessage = {
        fromType: this.service.getStorage('registerType'),
        message: this.messageBox,
        messageType: "text",
        to: this.chatId,
        proposalId: this.proposalId,
        status: 20,
        createdUserName: this.service.getStorage('userName'),
        receiveFrom: this.service.getStorage('userIdentifier'),
        grpCreatedBy: this.service.getStorage('userIdentifier'),
        profileImage: this.service.getStorage('profileImageGrpChat'),
        createdTime: this.date
      }

      console.log(JSON.stringify(chatmessage))
      $("#message").val("");
      this.messageBox = null
      this.messages.next(chatmessage);
      this.message.push(
        {
          send: {
            message: chatmessage.message,
            messageType: chatmessage.messageType,
            receiveFrom: chatmessage.receiveFrom,
            proposalId: chatmessage.proposalId,
            grpCreatedBy: chatmessage.grpCreatedBy,
            profileImage: chatmessage.profileImage,
            createdTime: chatmessage.createdTime,
            createdUserName: this.service.getStorage('userName')
          }
        }
      )
    }
    //this.chatmessage.message = null;
  }
  onKeydown(event) {
    if (event.key === "Enter") {

      this.sendMsg()
    }
  }
  dateFormat(value) {

    var date = new Date();
    let res: any;
    let serverDate = this.datePipe.transform(value, "dd-MM-yyyy")
    let today = this.datePipe.transform(date, "dd-MM-yyyy");
    if (serverDate == today)
      res = this.datePipe.transform(value, "h:s:a")
    else
      res = this.datePipe.transform(value, "dd-MM-yyyy h:s:a")

    return res
  }

}
