import { Injectable } from '@angular/core';
import {FormBuilder, Validators,FormsModule,FormControl,AbstractControl} from '@angular/forms';
@Injectable({
  providedIn: 'root'
})
export class ValidationServiceService {

  constructor() { }
  //Email Validation
  static emailValidator(control) {
    if(control.value)
    if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
      {  return null;
        
      }
    } else {
       
        return { 'invalidEmailAddress': true };
    }
} 
//Website validator

//Name Validation
 static nameValidator(control)
 {
    if(control.value)
   if(control.value.length!=0){
       
          return null;
        
    }
   else {
     
      return { 'invalidName': false };
  }
 }
 //Mobile Validation
 static mobileValidation(control)
 {
    if(control.value) 
    if (control.value.match(/^([0-9]{1,50})$/)) {
       
        return null;
    } else {
        
        return { 'invalidMobile': false };
    }
 }
 //Password Validation
 static passwordValidator(control) {
    if(control.value)
    if(control.value.length>=6){
        return null;
    } else {
        return { 'invalidPassword': true };
    }
}
//Not null validation //Drop dwon
static notNullValidation(control)
{
    if(control.value)
    if(control.value!=null ||control.value!='')
        return null;
    else
        return { 'invalidInput' :true};
}
//Number Validator
static numberValidation(control)
 {
     try{
    if(control.value) 
    if (control.value.match(/^([0-9]{1,50})$/)) {
      
        return null;
    } else {
        
        return { 'invalidMobile': false };
    }
}
catch(er){
    
}
 }
 static floatValidation(control)

 {
    
    if(control.value) 
    if (String(control.value.match(/^[0-9]+(\.[0-9]{1,2})?$/))) {
   
        return null;
    } else {
        
        return { 'invalidMobile': false };
    } 
 }

 static inputNumber(value)
 {
    if(value) 
    if (value.match(/^([0-9]{1,50})$/)) {
       
        return true;
    } else {
        
        return false;
    }
 }
  emailValidation(val)
 {
     
     if(val)
    if (val.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
        {  return true;
          
        }
      } else {
         
          return false;
      }
      return false; 
 }
}
