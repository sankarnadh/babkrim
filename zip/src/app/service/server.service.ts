import { Injectable, ɵConsole } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  Headers,
  Http,
  Request,
  RequestOptions,
  Response,
  XHRBackend
} from "@angular/http";
import { HttpHeaders } from "@angular/common/http";
import "../../assets/js/js/chat.js";
import * as moment from "moment";
import { ValidationMessageService } from "../service/validation-message.service";
import { Router, NavigationEnd } from "@angular/router";
import { DatePipe } from "@angular/common";
import { catchError, map, tap } from "rxjs/operators";
import { Options, LabelType, CustomStepDefinition } from "ng5-slider";
import { TranslateService } from "@ngx-translate/core";
import { Component, OnInit, ElementRef } from "@angular/core";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { DomSanitizer } from "@angular/platform-browser";
import { Pipe, PipeTransform } from "@angular/core";
import { NgProgress } from "ngx-progressbar";
import { ToastrManager } from "ng6-toastr-notifications";
// import { } from 'googlemaps'
// import { } from '@types/googlemaps';
// import { Alert } from 'selenium-webdriver';
import { url } from "./url";
import { registerLocaleData } from '@angular/common';
import localeAr from '@angular/common/locales/ar';
import localeIn from '@angular/common/locales/en-IN'
declare var google: any;

@Pipe({ name: "safe" })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

declare var customealert: any;

@Injectable({
  providedIn: "root"
})
export class ServerService {
  constructor(
    public toastr: ToastrManager,
    public ngProgress: NgProgress,
    private spinner: Ng4LoadingSpinnerService,
    private http: HttpClient,
    private router: Router,
    private datePipe: DatePipe,
    private validationMsg: ValidationMessageService,
    private translate: TranslateService
  ) {
    if (this.getStorage("userName"))
      this.userName = this.getStorage("userName");
    if (this.getStorage("prfPicId")) this.prfImg = this.getStorage("prfPicId");

    if (this.getStorage("language") == "ar") {
      translate.setDefaultLang("ar");
    } 
    if (this.getStorage("language") == "en") {
      this.setStorage("language", "en");
      translate.setDefaultLang(this.getStorage("language"));
    }
    if (this.getStorage("language") == "hi") {
      this.setStorage("language", "hi");
      translate.setDefaultLang(this.getStorage("language"));
    }
    if(!this.getStorage("language")){
      this.setStorage("language", "en");
      translate.setDefaultLang(this.getStorage("language"));
    }


    /*****
     * Otp Error msg
     *
     */
    let mobile = this.getStorage("mobile_no");
    if (mobile) {
      let star = "";
      for (let i = 0; i < mobile.length - 2; i++) {
        star += "*";
      }
      this.mobile_otp_msg =
        "OTP has been send to " +
        star +
        mobile[mobile.length - 2] +
        mobile[mobile.length - 1];

      //this.removeStorage('mobile_no');
    }

    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
      customealert.hideAllModal();
      this.ageMax = 0;
      this.ageMin = 0;
      this.jobType = "";
      this.heightMax = 0;
      this.heightMin = 0;
      this.weightMax = 0;
      this.expMax = 0;
      this.expMin = 0;
      this.rating = 0;
      this.buttonFlag = false; //Contract submit
      this.jobmode = null;
      //  this.search=null
      // this.tempTaskStartTime=0
      this.sideBarCheckBoxStatus=0;
      this.hourWrkMin = 0;
      this.hourWrkMax = 0;
      this.hourlyWrkWagesMin = 0;
      this.hourlyWrkWagesMax = 0;
      this.availability = "";
      this.filterGender = null;
      this.distanceMin = 0;
      this.groupActiveTab = null;
      this.selectedSidebarCountry = [];
      this.selectedSidebarState = [];
      this.selectedSidebarCity = [];
    });
  }
  url: string = url.apiUrl;
  public timeOut: any = 35000;
  public version: any = url.version;
  public s3bucketurl = url.s3bucketurl;
  public bucketFolder = url.bucketFolder;
  public bucketvideoUrl = url.bucketvideoUrl;
  public bucketExtension = url.bucketExtension;
  public WebLogoUrl = url.webLogoUrl;
  coutryMobCode = url.coutryMobCode;
  public link1 = url.linkUrl1;
  public link2 = url.linkUrl2;
  currentIcon = "/assets/images/map_employere2.png";
  urgentEmployerIcon = "/assets/images/employerrmapurgent.png";
  currentIconEmployee = "/assets/images/map_employee_icon2.png";
  public categoryArray = [];
  categoryLength;
  public video =
    this.s3bucketurl +
    this.bucketFolder +
    this.bucketvideoUrl +
    this.bucketExtension;
  public notificationLength = 0;
  public dataStorage: any;
  public mobile_otp_msg;
  allCountryId = [];
  public fileFormat =
    ".doc,.pdf,.docx,.png,.jpeg,.jpg,.ppt,.odt,.xlsx,.ods,.pptx,.jpp";
  public videoFormat = ".mp4";
  public imageFormat = ".png,.jpeg,.jpg,.jpp,.tiff";
  public maxFileSize = 10 * 1024 * 1024;
  public videoSize = 100 * 1024 * 1024;
  public lat = 9.9312;
  lng = 76.2673;
  employee_login = false;
  employer_login = false;
  noLogin = true;
  public search: any = null;
  public jobmode = null;
  public userName: any;
  groupChatLoader = false;
  public prfImg = "assets/images/userpropic.png";
  public prfImgTemp = "assets/images/userpropic.png";
  public loader = true;
  public wallet: any = 0;
  public runningContractCount = 0;
  public runningContract: any = [];
  public whoesLogin = 1; // default is employee
  public previousRunningContract: any = [];
  public defaultCurrency = url.defaultCurrency;
  mapStyle = [
    {
      elementType: "geometry",
      stylers: [
        {
          color: "#f5f5f5"
        }
      ]
    },
    {
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#616161"
        }
      ]
    },
    {
      elementType: "labels.text.stroke",
      stylers: [
        {
          color: "#f5f5f5"
        }
      ]
    },
    {
      featureType: "administrative.land_parcel",
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#bdbdbd"
        }
      ]
    },
    {
      featureType: "poi",
      elementType: "geometry",
      stylers: [
        {
          color: "#eeeeee"
        }
      ]
    },
    {
      featureType: "poi",
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#757575"
        }
      ]
    },
    {
      featureType: "poi.park",
      elementType: "geometry",
      stylers: [
        {
          color: "#e5e5e5"
        }
      ]
    },
    {
      featureType: "poi.park",
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#9e9e9e"
        }
      ]
    },
    {
      featureType: "road",
      elementType: "geometry",
      stylers: [
        {
          color: "#ffffff"
        }
      ]
    },
    {
      featureType: "road.arterial",
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#757575"
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry",
      stylers: [
        {
          color: "#dadada"
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#fef8d0"
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#616161"
        }
      ]
    },
    {
      featureType: "road.local",
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#9e9e9e"
        }
      ]
    },
    {
      featureType: "transit.line",
      elementType: "geometry",
      stylers: [
        {
          color: "#e5e5e5"
        }
      ]
    },
    {
      featureType: "transit.station",
      elementType: "geometry",
      stylers: [
        {
          color: "#eeeeee"
        }
      ]
    },
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [
        {
          color: "#c9c9c9"
        }
      ]
    },
    {
      featureType: "water",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#aadaff"
        }
      ]
    },
    {
      featureType: "water",
      elementType: "labels.text.fill",
      stylers: [
        {
          color: "#9e9e9e"
        }
      ]
    }
  ];

  checkMobileVerification() {
    if (this.getStorage("mobileVerify") != "true") {
      this.router.navigate(["/message-verification"]);
      return;
    }
  }
  showMenu = false; // This is to check whether to show menus or not, menus are hidden in register page
  checkLogin() {
    this.employee_login = this.isEmployeeLogin();
    this.employer_login = this.isEmployerLogin();

    if (!this.employee_login && !this.employer_login) this.noLogin = true;
    else this.noLogin = false;

    if (
      this.getRegisterLevel() == "0" &&
      this.getStorage("mobileVerify") == "true" &&
      this.getStorage("emailVerify") == "true" &&
      this.getStorage("userVerify") == "true"
    )
      this.showMenu = true;
    else this.showMenu = false;
  }
  // 'Authorization': this.getStorage('token')
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: "bearer " + this.getStorage("token")
    })
  };
  /***
   * Download file
   */
  downloadFile(type, fileId) {
    customealert.loaderShow("html");
    this.serverGetRequest(
      "",
      "download.json?fileID=" + fileId + "&fileType=" + type
    ).subscribe(
      res => {
        customealert.loaderHide("html");
        if (res["status"].responseCode == 20) {
        } else {
          this.apiErrorHandler(res);
          // if(res['status'].responseCode==30)
          //window.location.reload()
        }
      },
      err => {
        this.serviceErrorResponce(err);
      }
    );
  }
  /******
   * Resend email
   */

  sendEmail() {
    customealert.loaderShow("html");
    let response: any;
    this.serverRequest(
      "",
      "/otp/email/send.json?registerType=" + this.getStorage("registerType")
    ).subscribe(
      (res: Response) => {
        customealert.loaderHide("html");
        response = res["status"];
        if (response.responseCode == 20) {
          this.showSuccess(res["result"].message);
          // this.smsOtpMsg=res['result'].message
        } else {
          this.apiErrorHandler(res);
          // if(response.responseCode==30)
          //window.location.reload()
        }
      },
      err => {
        this.serviceErrorResponce(err);

        customealert.loaderHide("html");
      }
    );
  }

  /**
   * Verify Email
   */
  verifyEmail(key) {
    customealert.loaderShow("html");
    let response: any;
    this.serverGetRequest("", "/otp/email/verify.json?OTP=" + key).subscribe(
      (res: Response) => {
        customealert.loaderHide("html");
        response = res["status"];

        if (response.responseCode == 20) {
          if (res["result"].message) {
            this.setStorage("emailVerify", true);

            if (
              this.getStorage("registerLevel") == "3" &&
              this.isEmployerLogin()
            )
              this.setStorage("registerLevel", "0");
            if (
              this.getStorage("registerLevel") == "4" &&
              this.isEmployeeLogin()
            )
              this.setStorage("registerLevel", "0");
            this.router.navigate(["/home"]);
            this.showSuccess(this.validationMsg.emailVerify);
          } else {
            this.showError(this.validationMsg.emailVerifyInvalid);
            this.router.navigate(["/activate_email"]);
          }
        } else {
          this.apiErrorHandler(res);
          // if(response.responseCode==30)
          //window.location.reload()
        }
      },
      err => {
        this.serviceErrorResponce(err);

        customealert.loaderHide("html");
      }
    );
  }
  /*******
   * Check verification status
   * for both mobile and email
   */

  verificationCheck() {
    let response: any;
    this.serverGetRequest(
      "",
      "otp/varification/check.json?registerType=" +
        this.getStorage("registerType")
    ).subscribe((res: Response) => {
      response = res["status"];
      if (response.responseCode == 20) {
        // this.smsOtpMsg=res['result'].message
        this.setStorage("emailVerify", res["result"]["emailVerify"]);
        if (
          res["result"]["emailVerify"] == "true" &&
          this.getStorage("registerLevel") == "0"
        )
          this.router.navigate(["/home"]);
        else this.router.navigate(["/activate-email"]);
      } else {
        this.apiErrorHandler(res);
        //if(response.responseCode==30)
        //window.location.reload()
      }
    });
  }
  /**
   *
   * @param data server
   * @param url server
   */
  serverRequest(data, url) {
    let token = "";
    if (this.getStorage("token")) token = this.getStorage("token");
    let httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + token,
        "content-type": "Application/json",
        deviceMode: "web",
        Locale: this.getStorage("language"),
        version: this.version
      })
    };
    try {
      return this.http
        .post(this.url + "/" + url, data, httpOptions)
        .timeout(this.timeOut)
        .pipe(tap(res => {}));
    } catch (er) {}
  }
  serverDeleteRequest(url) {
    let token = "";
    if (this.getStorage("token")) token = this.getStorage("token");
    let httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + token,
        "content-type": "Application/json",
        deviceMode: "web",
        Locale: this.getStorage("language"),
        version: this.version
      })
    };
    try {
      return this.http
        .delete(this.url + "/" + url, httpOptions)
        .timeout(this.timeOut)
        .pipe(tap(res => {}));
    } catch (er) {}
  }
  serverPutRequest(data, url) {
    let httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + this.getStorage("token"),
        "content-type": "Application/json",
        deviceMode: "web",
        Locale: this.getStorage("language"),
        version: this.version
      })
    };
    return this.http
      .put(this.url + "/" + url, data, httpOptions)
      .timeout(this.timeOut)
      .pipe(tap(res => {}));
  }
  checkAllowedFiles(fileExe) {
    //.doc,.pdf,.docx,.png,.jpeg,.jpg
    if (
      fileExe == "doc" ||
      fileExe == "docx" ||
      fileExe == "pdf" ||
      fileExe == "png" ||
      fileExe == "jpeg" ||
      fileExe == "jpg" ||
      fileExe == "PNG" ||
      fileExe == "JPEG" ||
      fileExe == "JPG" ||
      fileExe == "DOC" ||
      fileExe == "docx" ||
      fileExe == "jpp" ||
      fileExe == "ods" ||
      fileExe == "pptx" ||
      fileExe == "xlsx" ||
      fileExe == "odt" ||
      fileExe == "ppt" ||
      fileExe == "tiff" ||
      fileExe == "DOCX" ||
      fileExe == "PDF"
    )
      return true;
    else return false;
  }

  /******
   * Single File Upload
   */
  public uploadSingleFile(fileToUpload: File, url) {
    let httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + this.getStorage("token"),
        deviceMode: "web",
        Locale: this.getStorage("language"),
        version: this.version
      })
    };
    const _formData = new FormData();
    _formData.append("file1", fileToUpload, fileToUpload.name);
    return this.http
      .post(this.url + "/" + url, _formData, httpOptions)
      .timeout(this.timeOut);
  }
  /******
   * Multi File Upload
   */
  public uploadMultipleFile(fileToUpload: File, url, count) {
    let httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + this.getStorage("token"),
        deviceMode: "web",
        Locale: this.getStorage("language"),
        version: this.version
      })
    };
    const _formData = new FormData();

    _formData.append("file1", fileToUpload, fileToUpload.name);

    return this.http
      .post(this.url + "/" + url, _formData, httpOptions)
      .timeout(this.timeOut);
  }
  public uploadFile(fileToUpload: File, url) {
    let token = "";
    if (this.getStorage("token")) token = this.getStorage("token");
    let httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + token,
        deviceMode: "web",
        Locale: this.getStorage("language"),
        version: this.version
      })
    };
    const _formData = new FormData();

    _formData.append("file1", fileToUpload, fileToUpload.name);

    return this.http.post(this.url + "/" + url, _formData, httpOptions);
  }
  /***
   * Server request Get
   *
   */
  serverGetRequest(data, url) {
    try {
      let httpOptions = {
        headers: new HttpHeaders({
          Authorization: "bearer " + this.getStorage("token"),
          deviceMode: "web",
          Locale: this.getStorage("language"),

          version: this.version
        })
      };

      return this.http
        .get(this.url + "/" + url, httpOptions)
        .timeout(this.timeOut)
        .pipe(tap(res => {}));
    } catch (e) {}
  }
  preventDefault(event) {
    event.preventDefault();
  }
  //Set Local Storage
  setStorage(key, val) {
    localStorage.setItem(key, val);
  }
  //Get localStorage
  getStorage(key) {
    return localStorage.getItem(key);
  }
  //Remove Storage
  removeStorage(key) {
    localStorage.removeItem(key);
  }
  //Empty Storage
  emptyStorage() {
    localStorage.clear();
  }
  /***
   * Checking Employee loged or not
   */
  isEmployeeLogin() {
    let res = this.getStorage("employee_login");
    if (res == "true") return true;
    else return false;
  }
  /******
   * Checking Employer Login
   */
  isEmployerLogin() {
    let res = this.getStorage("employer_login");
    if (res == "true") return true;
    else return false;
  }
  /****
   * Logout & destroy all the data
   */
  logoutClick = 0;
  logout() {
    customealert.loaderShow("html");
    this.logoutClick = 1;
    this.serverGetRequest("", "logout.json").subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          customealert.hideAllModal();
          let language = this.getStorage("language");
          this.emptyStorage();
          // this.wsservice.closeConnection()
          this.showMenu = false;
          this.runningContract = [];
          this.employee_login = false;
          this.employer_login = false;
          this.noLogin = true;
          this.messageBadgeIcon = 0;
          customealert.loaderHide("html");
          this.setStorage("language", language);
          this.prfImg = "assets/images/userpropic.png";
          this.prfImgTemp = "assets/images/userpropic.png";
          this.ngProgress.done();
          this.router.navigate(["/login"]);
          // window.location.reload()
          return true;
        } else this.apiErrorHandler(res);
      },
      er => this.serviceErrorResponce(er)
    );
  }
  /***
   * Base URL
   */
  baseUrl() {
    let baseurl;
    baseurl = "http://upparttime.tk/";
    // baseurl="http://localhost/up_part_time/";
    return baseurl;
  }
  /*****
   * Return register level
   */
  getRegisterLevel() {
    return this.getStorage("registerLevel");
  }
  /****
   * Redirect Employee to register pages
   */
  employeeNavigation() {
    let registerLevel = this.getRegisterLevel();
    this.checkMobileVerification();
    if (registerLevel == "2") {
      // this.showMenu = true
      this.router.navigate(["/employee_accountsetup"]);
    }
    if (registerLevel == "3") {
      // this.showMenu = true

      this.router.navigate(["/employee_educationsetup"]);
    }
    if (registerLevel == "4") {
      // this.showMenu = true
      this.router.navigate(["/employee_profetionalsetup"]);
    }

    if (registerLevel == "0" || registerLevel == "5") {
      if (this.getStorage("emailVerify") == "true") {
        if (this.getStorage("userVerify") == "true") {
          this.setStorage("registerLevel", 0);
          if (
            this.getStorage("urlHistory") &&
            this.getStorage("userIdentifierPrev") ==
              this.getStorage("userIdentifier")
          ) {
            this.checkLogin();
            this.router.navigate([this.getStorage("urlHistory")]);
          } else {
            this.setStorage(
              "userIdentifierPrev",
              this.getStorage("userIdentifier")
            );
            this.router.navigate(["/employee_home"]);
          }
        } else {
          this.showMenu = false;

          this.router.navigate(["/confirmation"]);
        }
      } else this.router.navigate(["/activate_email"]);
    }
    // //window.location.reload()
  }
  /*****
   * Redirect Employer to register page
   */
  employerNavigation() {
    this.checkMobileVerification();
    let registerLevel = this.getRegisterLevel();
    if (registerLevel == "2") {
      this.router.navigate(["/employer_companydetails"]);
    }
    if (registerLevel == "3") {
      this.router.navigate(["/employer_managerdetails"]);
    }
    if (registerLevel == "0" || registerLevel == "4") {
      if (this.getStorage("emailVerify") == "true") {
        if (
          this.getStorage("urlHistory") &&
          this.getStorage("userIdentifierPrev") ==
            this.getStorage("userIdentifier")
        ) {
          this.checkLogin();
          this.router.navigate([this.getStorage("urlHistory")]);
        } else {
          this.setStorage(
            "userIdentifierPrev",
            this.getStorage("userIdentifier")
          );
          this.router.navigate(["/employer_home"]);
        }
      } else this.router.navigate(["/activate_email"]);
    }

    ////window.location.reload()
  }
  //Local time zone
  addZero(number, length) {
    var str = "" + number;
    while (str.length < length) {
      str = "0" + str;
    }
    return str;
  }
  //Local time zone
  localTimeZone() {
    let offset = new Date().getTimezoneOffset();
    let timeZone =
      (offset < 0 ? "+" : "-") +
      this.addZero(Math.abs(offset / 60), 2) +
      ":" +
      this.addZero(Math.abs(offset % 60), 2);
    if (this.getStorage("timeZone") == null)
      this.setStorage("timeZone", timeZone);
    return timeZone;
  }
  //API authentication failed or network error
  apiErrorHandler(res) {
    customealert.loaderHide("html");

    customealert.hideAllModal();
    if (res["status"].responseCode == 30) {
      let jobForm = this.getStorage("jobForm");
      let userIdentifier = this.getStorage("userIdentifier");
      let language = this.getStorage("language");
      this.emptyStorage();
      this.messageBadgeIcon = 0;
      this.prfImg = "assets/images/userpropic.png";
      this.prfImgTemp = "assets/images/userpropic.png";
      this.setStorage("language", language);
      this.setStorage("jobForm", jobForm);
      this.setStorage("userIdentifierPrev", userIdentifier);
      this.setStorage("urlHistory", this.router.url);
      // if (this.logoutClick == 0)
      //   this.showError(this.validationMsg.sessionExpired);
      //setTimeout(() => {
      //  window.location.reload()
     
      this.router.navigate(["/login"]);
      // }, 1000)
    } else {
      if (res["status"].responseCode == 50) {
        this.setStorage("registerLevel", 0);
        this.router.navigate(["/activate_email"]);
      } else this.showError(res["result"]["message"]);
    }
  }

  /**
   * Language switcher
   */
  switchLanguage(language: string) {
    this.setStorage("language", language);
    
    this.translate.use(language);
    this.validationMsg.translateValues();
    this.getCountry();
    
    window.location.reload();
  }
  /**
   * To send which language is selected
   * */
  langSelected() {
    if (this.getStorage("language") == "en") return true;
    else return false;
  }
  public initialLoader = "loadinginput";
  public currency;
  public prCurrency = [];
  getCurrency() //Fetch currency from API
  {
    this.serverGetRequest("", "/static/currencies.json").subscribe(
      res => {
        this.initialLoader = "";
        if (res["status"].responseCode == 20) {
          this.currency = res["result"].currencies;
          this.prCurrency = res["result"].currencies;
        } else {
          this.apiErrorHandler(res);
          //window.location.reload()
        }
      },
      err => {}
    );
  }
  /******
   * distanceRangeSlider Events manager
   */
  distanceRangeSlider(item: any) {
    return item.to;
  }
  /****
  Height slider
   */
  heightMin = 0;
  heightMax = 0;
  heightModal;
  heightOptions: Options = {
    floor: 50,
    ceil: 250,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return "" + value;
        case LabelType.High:
          return "" + value;
        default:
          return "" + value;
      }
    }
    // stepsArray:  this.heightSteps.split(',').map((letter: string): CustomStepDefinition => {
    //     return { value: +letter};
    // })
  };
  /**
   *Weight Slider
   * */

  weightMin = 0;
  weightMax = 0;
  weightModal;
  weightOptions: Options = {
    floor: 20,
    ceil: 1000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return "" + value;
        case LabelType.High:
          return "" + value;
        default:
          return "" + value;
      }
    }
  };
  /**
   *hour of work Slider
   * */

  hourWrkMin = 0;
  hourWrkMax = 0;
  hourWrkModal;
  hourWrkOptions: Options = {
    floor: 1,
    ceil: 8,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return "" + value;
        case LabelType.High:
          return "" + value;
        default:
          return "" + value;
      }
    }
  };
  /**
   *Experience of work Slider
   * */

  expMin = 0;
  expMax = 0;
  expModal;
  expOptions: Options = {
    floor: 0,
    ceil: 52,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return "" + value;
        case LabelType.High:
          return "" + value;
        default:
          return "" + value;
      }
    }
  };
  /**
   *Hourly work wages Slider
   * */

  hourlyWrkWagesMin = 0;
  hourlyWrkWagesMax = 0;
  hourlyWrkWagesModal;
  hourlyWrkWagesOptions: Options = {
    floor: 1,
    ceil: 9999,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return "" + value;
        case LabelType.High:
          return "" + value;
        default:
          return "" + value;
      }
    }
  };
  /**
   *Age Slider
   * */

  ageMin = 0;
  ageMax = 0;
  ageModal;
  ageOptions: Options = {
    floor: 18,
    ceil: 70,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return "" + value;
        case LabelType.High:
          return "" + value;
        default:
          return "" + value;
      }
    }
  };
  /**
   *Distance Slider
   * */

  distanceMin = 0;
  distanceStep = "0,5,10,20,50,100";
  distanceModal;
  distanceOptions: Options = {
    floor: 0,
    ceil: 100,
    stepsArray: this.distanceStep.split(",").map(
      (letter: string): CustomStepDefinition => {
        return { value: +letter };
      }
    ),
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return "" + value;
        case LabelType.High:
          return "" + value;
        default:
          return "" + value;
      }
    }
  };
  rangeSliderMin = 0; //For Range slider on both employee and employer.
  rangeSliderMax = 100;
  rangeSliderFrom = 0;
  rangeSliderTo = 5;
  rangeSliderFrom_min = 0;
  rangeSliderFrom_max = 100;
  rangeSliderFrom_shadow = false;
  rangeSliderTo_min = 1;
  rangeSliderTo_max = 100;
  rangeSliderTo_shadow = false;
  rangeSliderGrid = false;
  rangeSliderGrid_num = 10;
  rangeSliderPrefix = "";
  rangeSliderPostfix = "";

  //Fetching Country
  initalLoader;
  country = [];
  countryId: any = "";

  cityId: any = [];
  stateId: any = [];
  prstate: any = [];

  prcity: any = [];
  prcountry: any;
  disabled = false;
  dropdownList = [];
  countryArray = [];

  allCountryChange(item: any) {
    console.log(item);
    this.stateId = [];
    this.multiState = [];
    this.cityId = null;
    this.prcity = [];
    if (item.length != 0) {
      this.stateLoader = "loadinginput";
      for (let i = 0; i < item.length; i++) {
        this.allCountryId[i] = item[i].id;
        if (item[i].id == 0) {
          this.disabled = true;
          this.allCountryId = [0];
        } else this.disabled = false;

        if (item.length == 1 && item[i].id != 0) {
          //Not all country , to get city
          this.getAllCountrySateFromApi("");
        } else {
          this.multiState = [];
          this.getAllCountrySateFromApi("");
        }
      }
    } else {
      this.disabled = false;
      this.multiState = [];
      this.setStorage("allCountryId", null);
    }
    let all = [];
    let disable;
    this.dropdownList = [];

    all.push(this.countryArray);
    for (let i = 0; i < this.countryArray.length; i++) {
      if (i != 0) {
        disable = all[0][i];
        disable["disabled"] = this.disabled;
        all[0][i] = disable;
      }
      this.dropdownList.push(all[0][i]);
    }
  }
  /**
   * Clearing country Id when user unselect country
   */
  onCountryItemDeselect() {
    //Unsetting State and city
    this.countryId = "";
    this.prcity = [];
    this.prstate = [];
    this.state = [];
    this.cities = [];
    this.cityId = null;
    this.stateId = null;
  }
  /**
   * When country is selected
   */
  onCountryItemSelect(item: any) {
    this.countryId = item.id;
    this.prstate = [];
    this.prcity = [];
    this.getSateFromApi(item.id);
  }

  //State Selected
  onStateItemSelect(item: any) {
    if (item) this.stateId = item.id;

    this.prcity = [];
    if (this.stateId != []) this.getCityFromApi(this.stateId);
  }
  //State deselect
  onStateItemDeselect() {
    this.stateId = "";
    this.cityId = null;
    this.cities = [];
    this.prcity = [];
  }
  //City select
  onCityItemSelect(item: any) {
    if (item) this.cityId = item.id;
  }
  //City Deselect
  onCityItemDeselect() {
    this.cityId = null;
    this.prcity = [];
  }
  multiState;
  getAllCountrySateFromApi(
    search: any //Header bar Country / State
  ) {
    if (this.allCountryId.length == 0) {
      this.stateLoader = "";
      return false;
    }
    let data;
    if (search != "")
      data = {
        countryIds: this.allCountryId,
        search: search.target.value
      };
    else
      data = {
        countryIds: this.allCountryId,
        search: ""
      };
    this.serverRequest(data, "static/stateDetails.json").subscribe(
      res => {
        this.stateLoader = "";
        if (res["status"].responseCode == 20) {
          this.multiState = res["result"].stateDetails;
        } else this.apiErrorHandler(res);
      },
      err => {
        this.serviceErrorResponce(err);
      }
    );
  }
  getCountryFromApi() {
    this.serverGetRequest("", "static/countries.json").subscribe(
      res => {
        this.initalLoader = "";
        if (res["status"].responseCode == 20) {
          this.country = res["result"].countries;
        } else {
          this.apiErrorHandler(res);
        }
      },
      err => {}
    );
  }

  //Fetching state from server
  cityLoader;
  getCityFromApi(id) {
    if (!id) return false;
    this.cities = [];
    this.cityLoader = "loadinginput";
    this.serverGetRequest("", "static/cities.json?state=" + id).subscribe(
      res => {
        this.cityLoader = "";
        if (res["status"].responseCode == 20)
          this.cities = res["result"].cities;
        else {
          this.apiErrorHandler(res);
          //window.location.reload()
        }
      },
      err => {}
    );
  }
  //Fetching State from api
  stateLoader;
  state;
  cities;
  getSateFromApi(id) {
    this.stateLoader = "loadinginput";
    this.state = [];
    this.cities = [];

    this.serverGetRequest("", "static/states.json?country=" + id).subscribe(
      res => {
        this.stateLoader = "";
        if (res["status"].responseCode == 20) this.state = res["result"].states;
        else {
          this.apiErrorHandler(res);
          //  if(res['status'].responseCode==30)
          //window.location.reload()
        }
      },
      err => {}
    );
  }
  //Fetch all Details when loading
  nationality = [];
  gender = [];
  getLevel2() {
    let all: any[] = [];
    let pop: any[] = [];

    this.serverGetRequest("", "/custom/employee/level/2.json").subscribe(
      res => {
        this.initialLoader = "";
        if (res["status"].responseCode == 20) {
          try {
            this.nationality = res["result"].nationalities;
            this.gender = res["result"].genders;

            pop.push(res["result"].mostPopular);
            all.push(res["result"].countries);
            this.country = [];
            if (res["result"].mostPopular)
              for (let i = 0; i < res["result"].mostPopular.length; i++) {
                this.country.push(pop[0][i]);
              }
            if (res["result"].countries)
              for (let i = 0; i < res["result"].countries.length; i++) {
                this.country.push(all[0][i]);
              }
          } catch (e) {}
        } else {
          //this.apiErrorHandler(res);
          //window.location.reload()
        }
      },
      err => {
        this.serviceErrorResponce(err);
      }
    );
  }
  /**************
   * Country list on Header bar , side bar
   */

  getCountry() {
    let all: any[] = [];
    let pop: any[] = [];

    this.serverGetRequest("", "static/countries.json").subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          pop.push(res["result"].mostPopular);
          all.push(res["result"].allCountries);

          this.dropdownList = [];
          this.dropdownList.push({
            id: 0,
            country: "All Countries",
            countryCode: "all"
          });
          let disable;
          for (let i = 0; i < res["result"].mostPopular.length; i++) {
            disable = pop[0][i];
            disable["disabled"] = this.disabled;
            disable["category"] = "Popular countries";
            pop[0][i] = disable;
            this.dropdownList.push(pop[0][i]);
          }
          for (let i = 0; i < res["result"].allCountries.length; i++) {
            disable = all[0][i];
            disable["disabled"] = this.disabled;
            disable["category"] = "Other countries";
            all[0][i] = disable;
            this.dropdownList.push(all[0][i]);
          }
          this.countryArray = this.dropdownList;
        }
      },
      err => {}
    );
  }
  transform(value: string): string {
    //First letter Capital
    if (value == null || value == undefined) return "";
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
  stringToNumber(string) {
    if (string == null) return 0;

    return parseFloat(string);
  }
  formatTime(time) {
    this.formatTimezone(time);
  }

  utcToGmt(time) {
    let date = new Date("2018-12-5 " + time);

    return new Date();
  }
  fulldateFormater(date){
    try {
      // var b = date.split(/\D/);
      const locale :any ='en'//this.getStorage('language')
      // var today = new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5]));
      return this.datePipe.transform(date, "EEEE d MMMM  y",'',locale);
    } catch (error) {
      //console.log(error)
      return "-";
    }
  }
  dateWithoutTime(date){
    try {
      var b = date.split(/\D/);
      const locale :any ='en'//this.getStorage('language')
      var today = new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5]));
      return this.datePipe.transform(today, "dd MMM yyyy ",'',locale);
    } catch (error) {
      //console.log(error)
      return "-";
    }
  }
  convertDateToGmt(date) {
    try {
      var b = date.split(/\D/);
      const locale :any ='en'//this.getStorage('language')
      var today = new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5]));
      return this.datePipe.transform(today, "dd MMM yyyy hh:mm a",'',locale);
    } catch (error) {
      //console.log(error)
      return "-";
    }
  }
  i = 0;
  getLanguage(){
    return this.getStorage('language');
  }
  formatTimezone(time) {
    var today: any = new Date();
    try {
      // var myToday = today.split(/\D/);

      let date: any;
      time = time.split(":");
      date = new Date(
        Date.UTC(
          today.getFullYear(),
          today.getMonth(),
          today.getDate(),
          +time[0],
          +time[1],
          +time[2]
        )
      );
const locale :any ='en'//this.getStorage('language')
      return this.datePipe.transform(date,'hh mm a','',locale); //this.getStorage('timeZone'));
    } catch (error) {
      console.info(error)
      return "-";
    }
  }
  returnDateTimeIn12Hrs(date) {
    try {
      const locale :any ='en'//this.getStorage('language')

      return this.datePipe.transform(date, "dd MMM yyyy hh:mm a",'',locale);
    } catch (error) {
      return "-";
    }
  }
  returnTimezoneOffset() {
    // let t = this.getStorage('timeZone').split(')')[0];
    // console.log(t)
    // let zone = t//.split('(')[1].substring(3)
    // var timeZone: any = zone.split(':')
    // if (timeZone[1] == '30' || timeZone[1] == '45')
    //   timeZone[1] = '5'
    // else
    //   timeZone[1] = 0
    // timeZone = parseFloat(timeZone[0] + '.' + timeZone[1])
    // let res=''+timeZone
    // return timeZone
  }
  //Get completed contracts
  completedContracts = [];
  contractActiveTab = 1; // for active contract tab
  getCompletedContractsFromApi() {
    this.completedContracts = [];
    this.loader = true;
    customealert.loaderShow("html");
    this.serverGetRequest(
      "",
      "contractDetails/complete.json?registerType=" +
        this.getStorage("registerType")
    ).subscribe(
      res => {
        this.loader = false;
        customealert.loaderHide("html");
        if (res["status"].responseCode == 20) {
          if (res["result"].contracts.length != 0)
            this.completedContracts = res["result"].contracts;
        } else this.apiErrorHandler(res);
      },
      er => {
        this.serviceErrorResponce(er);
        customealert.loaderHide("html");
      }
    );
  }

  /*****
   * Getting all job list (No filter)
   */
  jobDetails = [];
  pageCounter: any = 0;
  rating = 0;
  jobType = "";
  jobListPageNo = 0;
  availability = "";
  markers = [];
  fetchAllJobDetailsFromApi(pageNo) {

    if (pageNo == 0) {
      //ie, first time call

      window.scrollTo(0, 0);
      this.jobDetails = [];

      this.markers = [];
      this.pageCounter = 0;
      this.jobListPageNo = 0;
    }
    

    let stateId = [];
    if (this.stateId) if (this.stateId.length != 0) stateId.push(this.stateId);
    let cityId = [];
    if (this.prcity.length != 0) cityId.push(this.prcity);

    this.cat = [];
    this.categoryFilterName.forEach(element => {
      this.cat.push(element.categoryId);
    });
    this.sub = [];

    this.subCategoryFilter.forEach(element => {
     
      this.sub.push(element.subcategoryId);
    });
    let sidebarCountry = [];
    this.selectedSidebarCountry.forEach(element => {
      sidebarCountry.push(element.id);
    });
    let sidebarState = [];
    this.selectedSidebarState.forEach(element => {
      sidebarState.push(element.id);
    });
    let sidebarCity = [];
    this.selectedSidebarCity.forEach(element => {
      sidebarCity.push(element.id);
    });
    let data = {
      jobMode: this.jobmode,
      categoryIds: this.sub.length==0?this.cat:[],
      cityIds: sidebarCity,
      countryIds: sidebarCountry,
      jobType: this.jobType,
      maxAge: this.ageMax,
      maxHeight: this.heightMax,
      maxWeight: this.weightMax,
      maxYearofExperience: this.expMax,
      minAge: this.ageMin,
      minHeight: this.heightMin,
      minWeight: this.weightMin,
      minYearofExperience: this.expMin,
      rating: this.rating,
      stateIds: sidebarState,
      subCategoryIds: this.sub,
      search: this.search,
      minHourPerDay: this.hourWrkMin,
      maxHourPerDay: this.hourWrkMax,
      minimumHourlyWage: this.hourlyWrkWagesMin,
      maximumHourlyWage: this.hourlyWrkWagesMax,
      availability: this.availability,
      gender: this.filterGender,
      distance: this.distanceMin,
      wagemode: this.wageModeFilter
    };

    if (this.router.url.split("/")[1] == "search-all-person") {
      this.getemployeeDetails(pageNo);
      return true;
    }
    pageNo++; // = this.pageCounter
    this.loader = true;

    this.serverRequest(data, "jobDetails.json?pageNo=" + pageNo).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          if (pageNo == 1) {
            this.jobDetails = [];
          }
          this.pageCounter = Number(pageNo);
    this.pageCounter++;

          res["result"].jobDetails.forEach(element => {
            

            this.jobDetails.push(element);
            this.markers.push({
              lat: parseFloat(element.latitude),
              lng: parseFloat(element.longitude),
              label: "",
              draggable: false,
              companyName: element.employerDetails.companyName,
              rating: element.employerDetails.rating,
              employerId: element.employerDetails.employerId,
              companyImg: element.employerDetails.profileImage,
              fromDate: element.fromDate,
              jobTitle: element.jobTitle,
              jobDescription: element.jobDescription,
              jobPostType: element.jobPostType,
              noOfVacancy: element.noOfVacancy,
              hourlyWages: element.hourlyWages,
              category: element.category,
              subCategory: element.subCategory ? element.subCategory : "",
              allDetails: element
            });
          });

          this.pageCounter = +res["result"].totalCount;
          this.jobListPageNo++;
          console.log(this.jobListPageNo + " page no");
        } else this.apiErrorHandler(res);

        this.loader = false;
      },
      error => {
        this.serviceErrorResponce(error);
      }
    );
  }
  employeeList = [];
  emplength = 0;
  employeeListPageNo = 0;
  getemployeeDetails(pageNo) {
    console.log(this.subCategoryFilter);
    if (pageNo == 0) {
      this.employeeListPageNo = 0;
      window.scrollTo(0, 0);
      this.employeeList = [];
      this.emplength = 0;
      this.pageCounter = 0;
    }
    console.log(this.subCategoryFilter);
    console.log(this.categoryFilterName);
    this.loader = true;
    pageNo = +pageNo + 1;
    
    let stateId = [];
    if (this.stateId) if (this.stateId.length != 0) stateId.push(this.stateId);
    let cityId = [];
    if (this.prcity.length != 0) cityId.push(this.prcity);

    this.cat = [];
   
    this.categoryFilterName.forEach(element => {
    
      this.cat.push(element.categoryId);
    });
    this.sub = [];
  
    this.subCategoryFilter.forEach(element => {
     
      this.sub.push(element.subcategoryId);
    });
    let sidebarCountry = [];
    this.selectedSidebarCountry.forEach(element => {
      sidebarCountry.push(element.id);
    });
    let sidebarState = [];
    this.selectedSidebarState.forEach(element => {
      sidebarState.push(element.id);
    });
    let sidebarCity = [];
    this.selectedSidebarCity.forEach(element => {
      sidebarCity.push(element.id);
    });
    let data = {
      jobMode: this.jobmode,
      categoryIds: this.sub.length==0?this.cat:[],
      cityIds: sidebarCity,
      countryIds: sidebarCountry,
      jobType: this.jobType,
      maxAge: this.ageMax,
      maxHeight: this.heightMax,
      maxWeight: this.weightMax,
      maxYearofExperience: this.expMax,
      minAge: this.ageMin,
      minHeight: this.heightMin,
      minWeight: this.weightMin,
      minYearofExperience: this.expMin,
      rating: this.rating,
      stateIds: sidebarState,
      subCategoryIds: this.sub,
      search: this.search,
      minHourPerDay: this.hourWrkMin,
      maxHourPerDay: this.hourWrkMax,
      minimumHourlyWage: this.hourlyWrkWagesMin,
      maximumHourlyWage: this.hourlyWrkWagesMax,
      availability: this.availability,
      gender: this.filterGender,
      distance: this.distanceMin,
      wagemode: this.wageModeFilter
    };

    this.serverRequest(data, "employeeDetails.json?pageNo=" + pageNo).subscribe(
      res => {
        this.loader = false;
        if (res["status"].responseCode == 20) {
          if (res["result"].employeeDetails) {
            this.employeeListPageNo++;
            if (pageNo == 1) {
              this.employeeList = [];
            }
            res["result"].employeeDetails.forEach(element => {
              if (this.getStorage("employeeTempId") != element.employeeId)
                this.employeeList.push(element);
            });

            this.emplength = res["result"].employeeDetails.length;
            this.pageCounter = +res["result"].totalCount;
          }
        } else this.apiErrorHandler(res);
      },
      err => {
        this.serviceErrorResponce(err);
      }
    );
  }
  /****
   * JobDetails for Map
   */
  fetchAllJobDetailsForMap(pageNo) {
    if (pageNo == 0) {
      //ie, first time call
      window.scrollTo(0, 0);
      this.jobDetails = [];
      this.markers = [];
      this.pageCounter = 0;
      this.jobListPageNo = 0;
    }
    this.pageCounter = Number(pageNo);
    this.pageCounter++;
    //  this.jobDetails = []
    let stateId = [];
    if (this.stateId) if (this.stateId.length != 0) stateId.push(this.stateId);
    let cityId = [];
    if (this.prcity.length != 0) cityId.push(this.prcity);

    this.cat = [];
    this.categoryFilterName.forEach(element => {
      this.cat.push(element.categoryId);
    });
    this.sub = [];

    this.subCategoryFilter.forEach(element => {
      this.sub.push(element.subcategoryId);
    });
    let sidebarCountry = [];
    this.selectedSidebarCountry.forEach(element => {
      sidebarCountry.push(element.id);
    });
    let sidebarState = [];
    this.selectedSidebarState.forEach(element => {
      sidebarState.push(element.id);
    });
    let sidebarCity = [];
    this.selectedSidebarCity.forEach(element => {
      sidebarCity.push(element.id);
    });
    let data = {
      categoryIds: this.cat,
      cityIds: sidebarCity,
      countryIds: sidebarCountry,
      jobType: this.jobType,
      maxAge: this.ageMax,
      maxHeight: this.heightMax,
      maxWeight: this.weightMax,
      maxYearofExperience: this.expMax,
      minAge: this.ageMin,
      minHeight: this.heightMin,
      minWeight: this.weightMin,
      minYearofExperience: this.expMin,
      rating: this.rating,
      stateIds: sidebarState,
      subCategoryIds: this.sub,
      search: this.search,
      minHourPerDay: this.hourWrkMin,
      maxHourPerDay: this.hourWrkMax,
      minimumHourlyWage: this.hourlyWrkWagesMin,
      maximumHourlyWage: this.hourlyWrkWagesMax,
      availability: this.availability,
      gender: this.filterGender,
      distance: this.distanceMin,
      wagemode: this.wageModeFilter
    };
    if (this.router.url.split("/")[1] == "search-all-person") {
      this.getemployeeDetails(pageNo);
      return true;
    }
    pageNo++; // = this.pageCounter
    this.loader = true;

    this.serverRequest(data, "jobDetails/map.json?pageNo=" + pageNo).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          res["result"].jobDetails.forEach(element => {
            this.jobDetails.push(element);
            this.markers.push({
              lat: parseFloat(element.latitude),
              lng: parseFloat(element.longitude),
              label: "",
              draggable: false,
              companyName: element.employerDetails.companyName,
              rating: element.employerDetails.rating,
              employerId: element.employerDetails.employerId,
              companyImg: element.employerDetails.profileImage,
              fromDate: element.fromDate,
              jobTitle: element.jobTitle,
              jobDescription: element.jobDescription,
              jobPostType: element.jobPostType,
              noOfVacancy: element.noOfVacancy,
              hourlyWages: element.hourlyWages,
              category: element.category,
              subCategory: element.subCategory,
              allDetails: element,
              isOpen: false
            });
          });

          this.pageCounter = +res["result"].totalCount;
          this.jobListPageNo++;
          if (this.pageCounter > this.jobDetails.length) {
            this.fetchAllJobDetailsForMap(this.jobListPageNo);
          }
          console.log(this.markers);
        } else this.apiErrorHandler(res);

        this.loader = false;
      },
      error => {
        this.serviceErrorResponce(error);
      }
    );
  }
  categoryFilterName = [];
sideBarCheckBoxStatus =0; // To avoid multiple call for job list
  pushCategoryFilter(categoryName, categoryId) {
    let flag = 0;
    this.cat = [];
    this.categoryFilterName.forEach(element => {
      if (element.categoryId == categoryId && flag == 0) {
        flag = 1;
      }
    });
    if (flag == 0) {
      this.categoryFilterName.push({
        categoryName: categoryName,
        categoryId: categoryId
      });
    }
    if(this.sideBarCheckBoxStatus ==0)
    this.fetchAllJobDetailsFromApi(0);
  }
  removeCategoryFilter(categoryId) {
    this.categoryFilterName.forEach((element, index) => {
      if (element.categoryId == categoryId) {
        var idx = this.categoryFilterName.indexOf(element.categoryId);

        this.categoryFilterName.splice(index, 1);
        this.cat.splice(index, 1);
      }
    });
    if (this.cat.length == 0) {
      // this.subCategoryFilter = [];
      this.sub = [];
    }
    this.jobDetails = [];
    this.employeeList = [];
    this.fetchAllJobDetailsFromApi(0)

  }
  subCategoryFilter = [];
  pushSubCategoryFilter(subCategoryName, subCategoryId, categoryId,recommend=false) {
    let flag = 0;
    this.subCategoryFilter.forEach((element, index) => {
      if (element.subcategoryId == subCategoryId && flag == 0) flag = 1;
    });
    if (flag == 0) {
    
      this.subCategoryFilter.push({
        subcategoryName: subCategoryName,
        categoryId: categoryId,
        subcategoryId: subCategoryId,
        defaultFilter:recommend
      });
    }
    this.sideBarCheckBoxStatus=0;
    this.fetchAllJobDetailsFromApi(0);
    // if (!this.noLogin) {this.fetchAllJobDetailsFromApi(0);alert()};
  }
  emptyCategoryFilter(){
    this.subCategoryFilter=[];
    this.categoryFilterName = []
    this.cat=[];
    this.sub=[];
    this.fetchAllJobDetailsFromApi(0)
  }
  removesubCategoryFilter(subCategoryId) {
    this.subCategoryFilter.forEach((element, index) => {
      if (element.subcategoryId == subCategoryId) {
        this.subCategoryFilter.splice(index, 1);
        this.cat.splice(index, 1);
      }
    });
    if (this.subCategoryFilter.length == 0) this.categoryFilterName = [];
    this.jobDetails = [];
    this.employeeList = [];
    this.sideBarCheckBoxStatus=0;
    this.fetchAllJobDetailsFromApi(0)
  }
  cat = [];
  sub = [];
  // pushCategory(
    
  //   categoryId,
  //   categoryName,
  //   subCategoryId,
  //   subCategoryName,
  //   event
  // ) {
  
  //   console.log(categoryName);
  //   if (event == "" || !event) {
  //     //Push category
  //     let flag = 0;

  //     this.categoryFilterName.forEach(element => {
  //       if (element.categoryId == categoryId && flag == 0) {
  //         flag = 1;
  //       }
  //     });
  //     if (flag == 0) {
  //       this.categoryFilterName.push({
  //         categoryName: categoryName,
  //         categoryId: categoryId
  //       });
  //     }

  //     if (subCategoryId != "") {
  //       //push Subcategory
  //       flag = 0;
  //       this.subCategoryFilter.forEach((element, index) => {
  //         if (element.subcategoryId == subCategoryId && flag == 0) flag = 1;
  //       });
  //       if (flag == 0) {
  //         this.subCategoryFilter.push({
  //           subcategoryName: subCategoryName,
  //           subcategoryId: subCategoryId
  //         });
  //       }
  //     }
  //   }
  //   if (event != "" && event)
  //     if (event.target.checked) {
  //       let flag = 0;

  //       this.categoryFilterName.forEach(element => {
  //         if (element.categoryId == categoryId && flag == 0) {
  //           flag = 1;
  //         }
  //       });
  //       if (flag == 0) {
  //         this.categoryFilterName.push({
  //           categoryName: categoryName,
  //           categoryId: categoryId
  //         });
  //       }
  //       if (subCategoryId != "") {
  //         flag = 0;
  //         this.subCategoryFilter.forEach((element, index) => {
  //           if (element.subcategoryId == subCategoryId && flag == 0) flag = 1;
  //         });
  //         if (flag == 0) {
  //           this.subCategoryFilter.push({
  //             subcategoryName: subCategoryName,
  //             subcategoryId: subCategoryId
  //           });
  //         }
  //       }
  //     } else {
  //       const index: number = this.sub.indexOf(subCategoryId);
  //       if (index !== -1) {
  //         this.subCategoryFilter.splice(index, 1);
  //         this.categoryFilterName.splice(index, 1);
  //       }

  //       // if (this.sub.length == 0)
  //       //   this.cat = []
  //     }
  //   this.fetchAllJobDetailsFromApi(0);
  // }
 
  filter = [];
  filterPush(filter) {}
  validateInputKey(event) {
    if (
      event.key == "`" ||
      event.key == "!" ||
      event.key == "@" ||
      event.key == "#" ||
      event.key == "$" ||
      event.key == "%" ||
      event.key == "^" ||
      event.key == "&" ||
      event.key == "*" ||
      event.key == "(" ||
      event.key == ")" ||
      event.key == "_" ||
      event.key == "+"
    ) {
      event.preventDefault();
    } else if (
      (event.keyCode >= 65 && event.keyCode <= 90) ||
      (event.keyCode >= 106 && event.keyCode <= 222)
    ) {
      event.preventDefault();
    }
  }
  validateExperience(event) {
    if (
      event.key == "`" ||
      event.key == "!" ||
      event.key == "@" ||
      event.key == "#" ||
      event.key == "$" ||
      event.key == "%" ||
      event.key == "^" ||
      event.key == "&" ||
      event.key == "*" ||
      event.key == "(" ||
      event.key == ")" ||
      event.key == "_" ||
      event.key == "+"
    ) {
      event.preventDefault();
    } else if (
      (event.keyCode >= 65 && event.keyCode <= 90) ||
      (event.keyCode >= 106 &&
        event.keyCode <= 222 &&
        event.keyCode != 110 &&
        event.keyCode != 190)
    )
      event.preventDefault();

    try {
      let val = event.target.value;
      let spit = val.split(".");
      if (spit.length > 0) {
        console.log(spit[1].length);
        if (event.keyCode == 110) event.preventDefault();
        if (
          spit[1].length > 1 &&
          event.keyCode != 8 &&
          event.keyCode != 46 &&
          event.keyCode != 37 &&
          event.keyCode != 39
        ) {
          console.log("in");
          event.preventDefault();
        }
      }
    } catch (error) {}
  }

  searchCategory;
  searchCatId;
  searchSubId;
  category = [];
  commonHeaders;
  getAllCategory() {
    this.serverGetRequest("", "custom/home/commonHeaders.json").subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.category = res["result"].commonCategories;
          this.searchCategory = res["result"].commonCategories;
          this.categoryArray = res["result"].commonCategories;
          this.commonHeaders = res["result"].commonHeaders;
          console.info(this.categoryArray);
          this.categoryLength = this.category.length;
        }
      },
      err => {}
    );
  }

  //JobDescription
  jobDescriptionModal = null;
  jobTitleModal = null;
  //Get active contracts
  activeContract = [];
  calcTime(date, offset) {
    var d = date;

    var utc = d.getTime() + d.getTimezoneOffset() * 60000;
    var nd = new Date(utc + 3600000 * offset);
    return nd;
  }

  convertDateUsingPipe(date) {
    return this.datePipe.transform(date, "yyyy MM dd hh:mm:ss a ");
  }
  convertDatewithoutZone(date) {
    return this.datePipe.transform(date, "yyyy MM dd hh:mm:ss a z");
  }
  runningIndex = null;
  getActiveContracts() {
    customealert.loaderShow("html");
    this.loader = true;
    this.activeContract = [];
    this.serverGetRequest(
      "",
      "contractDetails/active.json?registerType=" +
        this.getStorage("registerType")
    ).subscribe(
      res => {
        this.loader = false;
        customealert.loaderHide("html");

        if (res["status"].responseCode == 20) {
          customealert.hideModal("modal_job_description");
          customealert.hideAllModal();
          if (res["result"].contracts.length != 0) {
            this.taskStartTime = [];
            this.taskTimmer = [];
            this.taskFlag = [];
            this.activeContract = res["result"].contracts;
            console.log(this.activeContract);
            this.activeContract.forEach((element, index) => {
              if (element.lastAction != "default") {
                let serverDate: any = element.lastActionTime;
                let currenttime = element.currentTime;
                // console.log(new Date(currenttime+"GMT"))

                /************************************************************************************************************ */
                //Fetching current date, Hours,minit,sec
                var format = currenttime.replace("-", " ");
                format = format.replace("-", " ");

                format = new String(format.split(".")[0]);
                var utc = new String(" UTC");
                var time = format.concat(utc);

                let today: any = new Date(
                  this.convertDatewithoutZone(new Date(time))
                ); //new Date();//this.datePipe.transform(date, 'yyyy-MM-dd hh:mm:ss a', timeZone.toString());
                let t = today;

                today = Date.UTC(
                  today.getUTCFullYear(),
                  today.getUTCMonth(),
                  today.getUTCDate(),
                  today.getUTCHours(),
                  today.getUTCMinutes(),
                  today.getUTCSeconds()
                );

                /********************************************************************************************************* */
                //Server lastAction Time

                //6/29/2011 4:52:48 PM UTC
                //2018 11 06 13:28:44
                format = serverDate.replace("-", " ");
                format = format.replace("-", " ");

                format = new String(format.split(".")[0]);
                utc = new String(" UTC");
                time = format.concat(utc);
                //new Date(serverDate+'GMT')
                var date2: any = new Date(
                  this.convertDatewithoutZone(new Date(time + " UTC"))
                ); //new Date(this.convertDateUsingPipe(new Date(serverDate)));

                let di = (t.getTime() - date2.getTime()) / 1000;

                // date2 = date2.getTime();
                date2 = Date.UTC(
                  date2.getUTCFullYear(),
                  date2.getUTCMonth(),
                  date2.getUTCDate(),
                  date2.getUTCHours(),
                  date2.getUTCMinutes(),
                  date2.getUTCSeconds()
                );

                /**************************************************************************************** */

                var timeDiff = Math.abs(today - date2);

                timeDiff = timeDiff / 1000;
                //Since total time from api no calculation required
                let totalTime: any;
                if (element.totalTime)
                  if (
                    element.lastAction == "RESUME" ||
                    element.lastAction == "START"
                  ) {
                    totalTime = element.totalTime.split(":"); //new Date(totalTime)

                    var seconds = +totalTime[2];
                    var minutes = +totalTime[1];
                    var hours = +totalTime[0];
                    totalTime = 60 * 60 * hours + minutes * 60 + seconds;

                    timeDiff = totalTime; //Math.abs(totalTime.getTime());

                    this.runningIndex = index;
                  }

                this.taskStartTime.push(timeDiff);
                this.taskTimmer.push(element.totalTime);
              } else {
                this.taskStartTime.push(0);
                this.taskTimmer.push("00:00:00");
              }
              this.taskFlag.push(0);
            });
          } else this.activeContract = [];
        } else this.apiErrorHandler(res);
      },
      er => {
        this.serviceErrorResponce(er);
      }
    );
  }
  taskName = "";
  securityKey = null;
  taskDescription = "";
  taskProposal: any = null;
  taskStartTime = [];
  taskType = "start";
  taskTimmer = [];
  taskFlag = [];
  tempSecurityKey: any;
  buttonFlag = false;
  startTimecontract() {
    this.buttonFlag = true;
    if (this.taskType == "stop")
      if (this.taskDescription.length > 151) return false;
    if (this.taskName.length > 71) return false;
    if (
      this.taskType == "start" &&
      this.isEmployeeLogin() &&
      !this.securityKey
    ) {
      return false;
    }
    let data = {
      comment: this.taskDescription,
      contractAttachments: [],
      currency: 0,
      hourlyWages: 0,
      proposalId: "",
      title: this.taskName
    };
    customealert.loaderShow("html");
    if (this.isEmployerLogin()) this.securityKey = "default";
    this.serverRequest(
      data,
      "contractDetails/" +
        this.taskProposal +
        "/" +
        this.taskType +
        ".json?registerType=" +
        this.getStorage("registerType") +
        "&securityKey=" +
        this.securityKey
    ).subscribe(
      res => {
        this.buttonFlag = false;
        if (res["status"].responseCode == 20) {
          this.taskDescription = "";
          this.taskName = "";
          this.securityKey = "";
          // customealert.hideModal("modal_job_description")
          this.getActiveContracts();
          this.getContractDetailsById(this.taskProposal);
        } else {
          this.apiErrorHandler(res);
          customealert.loaderHide("html");
        }
      },
      er => {
        this.buttonFlag = false;
        this.serviceErrorResponce(er);
      }
    );
  }
  public invoiceList = [];
  endContract() {
    let price = [];
    let quantity = [];
    let item = [];
    this.invoiceList.forEach(element => {
      price.push(element.price);
      quantity.push(element.quantity);
      item.push(element.item);
    });
    let data = {
      comment: this.taskDescription,
      contractAttachments: [],
      currency: 0,
      hourlyWages: 0,
      proposalId: "",
      title: "",
      price: price,
      quantity: quantity,
      item: item
    };
    customealert.loaderShow("html");
    if (this.isEmployerLogin()) {
      this.securityKey = "default";
    }
    this.serverRequest(
      data,
      "contractDetails/" +
        this.taskProposal +
        "/stop.json?registerType=" +
        this.getStorage("registerType") +
        "&securityKey=" +
        this.securityKey
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.taskDescription = "";
          this.taskName = "";
          this.securityKey = "";
          this.invoiceList = [];
          customealert.hideModal("modal_job_invoice");
          this.getActiveContracts();
        } else {
          this.apiErrorHandler(res);
          customealert.loaderHide("html");
        }
      },
      er => {
        this.serviceErrorResponce(er);
      }
    );
  }
  serviceErrorResponce(error) {
    customealert.loaderHide("html");
    customealert.hideAllModal();

    this.loader = false;
    this.ngProgress.done();
    if (error.status == 500 || error.status == 502) {
      this.showError(this.validationMsg.serverError);
    } else {
      this.showError(this.validationMsg.networkError);
    }
  }
  fileNameFormater(name) {
    if (name) {
      if (name.length > 5) {
        return name.slice(0, 5) + "****." + name.split(".")[1];
      }
      return name;
    }
  }
  public offers: any = null;
  public offerLength: any = 0;
  public offerRating = [];
  viewOffer(val, page) {
    this.setStorage("data", JSON.stringify(val));

    switch (page) {
      case "1":
        this.router.navigate(["/myoffers-employee"]);
        break;
      case "2" || 2:
        this.router.navigate(["/inviteinterview_employee"]);
        break;
    }
  }
  public paymentDetails = [];
  public walletAmount;
  public paymentRequestList = [];
  public walletDetailsList = [];
  navigateMsg(receiver, proposal) {
    this.router.navigate(["/message/" + receiver + "/" + proposal]);
  }
  public invitation: any = null;
  public invitationLength: any = 0;

  public messageBadgeIcon: any = 0;
  checkVideoType(fileExe, fileType) {
    switch (fileType) {
      case "image": {
        // alert(extention)
        if (
          fileExe == "png" ||
          fileExe == "jpeg" ||
          fileExe == "jpg" ||
          fileExe == "PNG" ||
          fileExe == "JPEG" ||
          fileExe == "JPG"
        )
          return true;
        break;
      }
      case "doc": {
        if (
          fileExe == "doc" ||
          fileExe == "docx" ||
          fileExe == "DOC" ||
          fileExe == "docx" ||
          fileExe == "jpp" ||
          fileExe == "ods" ||
          fileExe == "pptx" ||
          fileExe == "xlsx" ||
          fileExe == "odt" ||
          fileExe == "ppt" ||
          fileExe == "tiff" ||
          fileExe == "DOCX"
        )
          return true;
        break;
      }
      case "pdf": {
        if (fileExe == "pdf" || fileExe == "PDF") return true;
        break;
      }
    }
  }
  public filterGender: any = null;

  /************* */
  public groupActiveTab: any;
  /**1 -> groupList 2 -> groupMessage 3 -> group Info 4 -> group search 5 -> group create*/
  public groupGroupId = this.getStorage("groupChatId");
  public groupName = null;
  public groupDescription = null;
  public isGroupAdmin = 0;
  categorySelected = [];
  subCategoryList = [];
  selectedSubCategoryList = [];
  /***
   * Sidebar filter country
   * ***** */
  public selectedSidebarCountry = [];

  onSidebarCountrySelect(item) {
    let country = [];
    this.selectedSidebarCountry.forEach(element => {
      country.push(element.id);
    });
    let data = {
      countryIds: country,
      search: ""
    };

    this.fetchAllJobDetailsFromApi(0);
    if (country.length > 0) this.loadStateFromApi(data);
  }
  onSidebarCountryAllSelect(item) {
    let country = [];
    item.forEach(element => {
      country.push(element.id);
    });
    let data = {
      countryIds: country,
      search: ""
    };

    this.fetchAllJobDetailsFromApi(0);
    if (country.length > 0) this.loadStateFromApi(data);
  }
  onSideBarCountryRemoved(item) {
    let country = [];
    this.selectedSidebarState = [];
    this.selectedSidebarCity = [];
    this.selectedSidebarCountry.forEach(element => {
      country.push(element.id);
    });
    let data = {
      countryIds: country,
      search: ""
    };

    this.fetchAllJobDetailsFromApi(0);
    if (country.length > 0) this.loadStateFromApi(data);
  }
  onSidebarCountryAllRemoved(item) {
    this.selectedSidebarState = [];
    this.selectedSidebarCity = [];
    this.fetchAllJobDetailsFromApi(0);
  }

  public selectedSidebarState = [];
  onSidebarStateSelect(item) {
    this.getCityFromApi(item.id);
    this.fetchAllJobDetailsFromApi(0);
  }

  onSideBarStateRemoved(item) {
    this.selectedSidebarCity = [];
    this.fetchAllJobDetailsFromApi(0);
  }
  public selectedSidebarCity = [];
  onSidebarCitySelect(item) {
    this.fetchAllJobDetailsFromApi(0);
  }

  onSideBarCityRemoved(item) {
    this.fetchAllJobDetailsFromApi(0);
  }

  loadStateFromApi(data) {
    this.serverRequest(data, "static/stateDetails.json").subscribe(
      res => {
        this.stateLoader = "";
        if (res["status"].responseCode == 20) {
          this.multiState = res["result"].stateDetails;
        } else {
          this.apiErrorHandler(res);
        }
      },
      err => {
        this.serviceErrorResponce(err);
      }
    );
  }
  removeCountryFromFilter(id) {
    this.selectedSidebarCountry.forEach((element, index) => {
      if (element.id == id) {
        this.selectedSidebarCountry.splice(index, 1);
      }
    });
    const val = this.selectedSidebarCountry;
    this.selectedSidebarCountry = [];
    this.selectedSidebarState = [];
    this.selectedSidebarCity = [];
    val.forEach(element => {
      this.selectedSidebarCountry.push(element);
    });
  }
  removeStateFromFilter(id) {
    this.selectedSidebarState = [];
    this.selectedSidebarCity = [];
  }
  removeCityFromFilter(id) {
    this.selectedSidebarCity = [];
  }
  /******* */
  public invoiceDetails: any;
  public invoiceTotal: any = 0;
  public invoiceSubTotal = 0;
  public wageModeFilter: any = null;
  public val: any = [];
  public tempJobDetails: any = [];
  public tempTaskStartTime = 0;
  public tempTaskTimmer = "00:00:00";
  getContractDetailsById(contractId) {
    this.tempTaskStartTime = 0;
    customealert.loaderShow("html");
    this.serverGetRequest(
      "",
      "contractDetails/" +
        contractId +
        ".json?registerType=" +
        this.getStorage("registerType")
    ).subscribe(
      res => {
        customealert.loaderHide("html");

        if (res["status"].responseCode == 20) {
          this.tempJobDetails = res["result"].contract.jobDetails;
          this.val = res["result"].contract;
          if (this.isEmployeeLogin()) {
            this.setStorage("tempName", this.val.employerDetails.employerName);
          } else
            this.setStorage("tempName", this.val.employeeDetails.employeeName);
          if (this.val.lastAction != "default") {
            let currentTime = new Date(this.val.currentTime);
            let lastActionTime = new Date(this.val.lastActionTime);
            const diff = Math.abs(
              (currentTime.getTime() - lastActionTime.getTime()) / 1000
            );
            console.log("time diff=" + diff);

            if (this.val.totalTime) {
              if (
                this.val.lastAction == "RESUME" ||
                this.val.lastAction == "START"
              ) {
                let totalTime = this.val.totalTime.split(":"); //new Date(totalTime)
                var seconds = +totalTime[2];
                var minutes = +totalTime[1];
                var hours = +totalTime[0];
                totalTime = 60 * 60 * hours + minutes * 60 + seconds;
                this.tempTaskStartTime = totalTime; //Math.abs(totalTime.getTime());

                console.log("total time=" + this.tempTaskStartTime);
              }
            }
          } else this.tempTaskStartTime = 0;
          this.tempTaskTimmer = "00:00:00";
        } else this.apiErrorHandler(res);
      },
      er => this.serviceErrorResponce(er)
    );
  }
  summery: any = []; // Notification summery
  summeryDetails: any; //summery API

  positive = 0;
  neutral = 0;
  negative = 0;
  unreadCount = 0;
  getChatUnreadCount() {
    this.serverGetRequest(
      "",
      "/chat/unread.json?registerType=" + this.getStorage("registerType")
    ).subscribe(res => {
      if (res["status"].responseCode == 20) {
        this.unreadCount = res["result"].chatCount;
      } else this.apiErrorHandler(res);
    });
  }
  messageRead(chatSessionId) {
    this.serverPutRequest(
      "",
      "/chat/" +
        chatSessionId +
        ".json?registerType=" +
        this.getStorage("registerType")
    ).subscribe(res => {
      if (res["status"].responseCode == 20) {
        this.getChatUnreadCount();
      }
    });
  }

  headerContractTime = 0;
  headerContractDetails;
  getRunningContract() {
    //  this.headerContractTime=0
    this.headerContractDetails = null;
    customealert.loaderShow("html");
    this.serverGetRequest(
      "",
      "notifications.json?registerType=" +
        this.getStorage("registerType") +
        "&pageNo=1"
    ).subscribe(
      res => {
        //this.loader=false;
        if (res["status"].responseCode == 20) {
          /**Create clock */
          customealert.loaderHide("html");
          let val = [];
          val = res["result"].runningContract;
          let flag = 0;
          var index;
          // val.forEach((element,key) => {
          //   if(element.lastAction == 'RESUME' || element.lastAction == 'START')
          //   {
          //     flag=1;
          //     index
          //   }
          // });
          this.headerContractDetails = res["result"].runningContract;
          if (val.length != 0)
            if (val[0].lastAction != "default") {
              if (
                val[0].lastAction == "RESUME" ||
                val[0].lastAction == "START"
              ) {
                let totalTime = val[0].totalTime.split(":"); //new Date(totalTime)
                var seconds = +totalTime[2];
                var minutes = +totalTime[1];
                var hours = +totalTime[0];
                totalTime = 60 * 60 * hours + minutes * 60 + seconds;
                this.headerContractTime = totalTime; //Math.abs(totalTime.getTime());
              }
            }

          /***** */
        } else this.apiErrorHandler(res);
      },
      er => {
        // this.service.serviceErrorResponce(er)
      }
    );
  }
  public employercomment = null;
  public showNext = 0;
  public buttonClick = 0;
  optionArray: any = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  A1 = 1;
  A2 = 1;
  A3 = 1;
  A4 = 10;
  A5 = 1;
  A6 = 1;
  A7 = 10;
  currentRate = 5;

  error = 0;
  recordList: any[] = [{ myList: [false, false, false, false, false] }];
  public resetData() {
    this.employercomment = null;
    //    alert(this.employercomment)
    this.showNext = 0;
    this.buttonClick = 0;
    this.optionArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.A1 = 1;
    this.A2 = 1;
    this.A3 = 1;
    this.A4 = 10;
    this.A5 = 1;
    this.A6 = 1;
    this.A7 = 10;
    this.currentRate = 5;

    this.error = 0;
    this.recordList = [{ myList: [false, false, false, false, false] }];
  }
  getStandardWageFromSubCategory(subCategory, selectedId) {
    let flag = 0;

    console.info(selectedId + " " + subCategory);
    subCategory.forEach(element => {
      if (element.id == selectedId) {
        flag = 1;
        return element.standardWage;
      }
    });
    return false;
  }

  showSuccess(msg: any = "") {
    //  this.toastr.successToastr(msg, 'Success!',this.toastSetting);
    
    this.toastr.customToastr(
      '<div class="row ">' +
        ' <div class="col-md-1 text-right"></div>' +
        '<div class="colmd-11 text-left text-success"><i class="fa fa-check-circle-o   text-success"></i>&nbsp;<span class="notificationmsg">' +
        msg +
        "</span></div>" +
        " </div>",
      null,
      this.toastSetting
    );
  }

  showError(msg: any = "") {
    this.toastr.customToastr(
      '<div class="row text-center">' +
        ' <div class="col-md-1 text-right"></div>' +
        '<div class="colmd-11 text-left text-danger"><i class="fa fa-times-circle  danger"></i>&nbsp;<span class="notificationmsg">' +
        msg +
        "</span></div>" +
        " </div>",
      null,
      this.toastSetting
    );
    //this.toastr.errorToastr(msg, 'Oops!',this.toastSetting);
  }

  showWarning() {
    this.toastr.warningToastr("This is warning toast.", "Alert!");
  }

  showInfo() {
    this.toastr.infoToastr("This is info toast.", "Info");
  }

  toastSetting = {
    position: "top-full-width",
    showCloseButton: true,
    enableHTML: true,
    toastTimeout: 3000,
    maxShown: 1,
    animate: "slideFromBottom",
    messageClass: "toastertest"
  };
  customToastr(msg, position: any = "top-full-width") {
    this.toastr.customToastr(
      '<div class="row ">' +
        ' <div class="col-md-1 text-right"><i class="fa fa-times-circle  danger"></i></div>' +
        '<div class="colmd-11 text-left text-success"><span class="notificationmsg">' +
        msg +
        "</span></div>" +
        " </div>",
      null,
      this.toastSetting
    );
  }
  jobPostForm: any;
  contractFlag = 0;
}
