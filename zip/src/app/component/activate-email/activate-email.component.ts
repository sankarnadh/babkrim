import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { ValidationMessageService } from '../../service/validation-message.service'
import { Router, ActivatedRoute } from "@angular/router";
import '../../../assets/js/js/chat.js';
declare var customealert;
@Component({
  selector: 'app-activate-email',
  templateUrl: './activate-email.component.html',
  styleUrls: ['./activate-email.component.css']
})
export class ActivateEmailComponent implements OnInit,OnDestroy {

  constructor(
    public service: ServerService,
    public validationMsg: ValidationMessageService,
    private router: Router,
    public activatedRouter: ActivatedRoute
  ) {

  }
  public key;
  interval;
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // this.interval.clearin
    clearInterval(this.interval)
  }
  ngOnInit() {
   this.service.checkLogin(); //To change the login header
this.interval=setInterval(()=>{
  this.verificationCheck();
},10000)
    this.verificationCheck();
    this.activatedRouter.queryParams.subscribe(params => {
      this.key = params['key'];
    
      if (this.key) {
        this.service.verifyEmail(this.key);
      }

    });
  }
  verificationCheck() {
    let response: any;
// customealert.loaderShow('html')
    this.service.serverGetRequest('', 'otp/varification/check.json?registerType=' + this.service.getStorage('registerType')).subscribe(
      (res: Response) => {
        customealert.loaderHide('html')

        response = res['status'];
        if (response.responseCode == 20) {

          this.service.setStorage('emailVerify', res['result']['emailVerify'])
          if (res['result']['emailVerify'] == 'true')
            this.router.navigate(['/home'])
        }
        else {
          this.service.apiErrorHandler(res);
          //if(response.responseCode==30)
          //window.location.reload()
        }

      }
    );
  }
}
