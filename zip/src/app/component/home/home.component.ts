import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ServerService} from '../../service/server.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css',
   
    ]
})
export class HomeComponent implements OnInit {

  constructor(private router : Router,public service :ServerService) { }

  ngOnInit() {
    if(this.service.isEmployerLogin())
    this.router.navigate(['/employer_home']);

    if(this.service.isEmployeeLogin())
    this.router.navigate(['/employee_home']);
  }
  
}
