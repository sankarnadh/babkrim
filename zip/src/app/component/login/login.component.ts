import { Component, OnInit, ElementRef } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { HttpHeaders } from "@angular/common/http";

import { Router } from "@angular/router";
import {
  FormBuilder,
  Validators,
  FormsModule,
  FormControl
} from "@angular/forms";
import { ValidationServiceService } from "../../service/validation-service.service";
import { ValidationMessageService } from "../../service/validation-message.service";
import { ServerService } from "../../service/server.service";
import "../../../assets/js/js/chat.js";
import { MessagingService } from "src/app/service/message.service";
declare var customealert: any;
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: any;
  forgotForm: any;
  username: any;
  pwd: any;
  constructor(
    private http: HttpClient,
    public validationMsg: ValidationMessageService,
    private el: ElementRef,
    private fcm: MessagingService,
    private router: Router,
    private formBuilder: FormBuilder,
    public service: ServerService
  ) {
    this.loginForm = this.formBuilder.group({
      username: [
        "",
        Validators.compose([
          // ValidationServiceService.emailValidator,
         
          Validators.required
        ])
      ],
      password: [
        "",
        Validators.compose([Validators.minLength(6), Validators.maxLength(20)])
      ],
      registerType: ["", Validators.required]
    });
    this.forgotForm = this.formBuilder.group({
      email: [
        "",
        Validators.compose([
          ValidationServiceService.emailValidator,
          Validators.required
        ])
      ]
    });
    if (!this.loginForm.get("registerType").value)
      this.loginForm.get("registerType").value = "employee";
  }
  validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
  }
  /***
  Mobile validation set to 15 
  on 10-03-2019
  informed by jamsheer
   */
  validatePhone(phone) {
    var re = /^(\+\d{1,3}[- ]?)?\d{10,15}$/;
    return re.test(phone);
  }
  email;
  forgot_msg = null;
  forgotPassword() {
    this.forgot_msg = null;
    if (!this.forgotForm.valid) {
      this.forgot_msg = this.validationMsg.level1EmailInvalid;

      return false;
    }
    let data = {
      email: this.email
    };
    customealert.loaderShow("html");
    this.service.serverRequest(data, "forgotPassword/send.json").subscribe(
      res => {
        customealert.loaderHide("html");

        if (res["status"].responseCode == 20) {
          this.forgot_msg = null;
          customealert.hideAllModal();
          this.service.showSuccess(
            this.validationMsg.forgotPasswordEmailAlert + " " + this.email
          );
        } else this.service.apiErrorHandler(res);
      },
      er => {
        this.service.serviceErrorResponce(er);
      }
    );
  }

  isBtnDisabled() {
    if (this.loginForm.get("username").valid)
      if (this.loginForm.get("password").valid) return false;
    return true;
  }
  ngOnInit() {
    if (this.service.isEmployeeLogin()) this.service.employeeNavigation();
    if (this.service.isEmployerLogin()) this.service.employerNavigation();

    this.service.checkLogin(); //To change the login header
  }
  /**
   * Validating credentials
   */
  message: any = null;
  login() {
    let status;
    let choice;
    this.message = null;

 

    const username=this.loginForm.get("username").value.replace(/\s/g, "");

    let data = {
      userName:username
     ,
      password: HTMLInputElement = this.el.nativeElement.querySelector(
        "#password"
      ).value,
      iosDeviceId: "",
      androidDeviceId: "",
      deviceType: "web",
      webFbToken: this.fcm.token //this.service.getStorage('firebasetoken')
    };
    if (
      this.validateEmail(username) ||
      this.validatePhone(username)
    ) {
     if(this.validatePhone(this.loginForm.get("username").value)){
       data.userName=this.service.coutryMobCode+data.userName;
      
     }
      customealert.loaderShow("html");
      if (this.service.whoesLogin == 0)
        this.loginForm.get("registerType").value = "employer";

      this.service
        .serverRequest(
          data,
          "login.json?registerType=" + this.loginForm.get("registerType").value
        )
        .subscribe(
          (res: Response) => {
            customealert.loaderHide("html");
            // console.log(res)
            if (
              res["status"]["responseCode"] == 20 &&
              res["status"]["responseStatus"] == "Success"
            ) {
              this.service.summery = res["result"].summary;
              this.service.setStorage(
                "employerType",
                res["result"]["employerType"]
              );
              this.service.logoutClick = 0;
              this.service.setStorage(
                "userVerify",
                res["result"]["userVerify"]
              );
              this.service.wallet = res["result"]["walletBalance"];
              this.service.setStorage(
                "mobileVerify",
                res["result"]["mobileVerify"]
              ); //Mobile verification
              this.service.setStorage(
                "emailVerify",
                res["result"]["emailVerify"]
              ); //emailVerification
              this.service.setStorage("token", res["result"]["token"]); // Auth token
              this.service.setStorage(
                "registerType",
                this.loginForm.get("registerType").value
              ); // Register type employee/employer
              this.service.setStorage("userName", res["result"]["fullName"]); //User First Name
              this.service.setStorage("email", res["result"]["email"]); //User email
              this.service.setStorage("mobileNo", res["result"]["mobileNo"]); //User mobileNo
              this.service.setStorage(
                "groupChatId",
                res["result"]["groupChatId"]
              ); //User groupChatId
              this.service.setStorage("timeZone", res["result"]["timeZone"]); //Timezone
              this.service.setStorage(
                "userIdentifier",
                res["result"]["userIdentifier"]
              );
              this.service.setStorage(
                "groupChatGroupDescription",
                res["result"]["groupChatGroupDescription"]
              );
              this.service.setStorage(
                "groupChatGroupName",
                res["result"]["groupChatGroupName"]
              );

              this.service.checkMobileVerification();
              if (
                res["result"]["profileImage"] &&
                res["result"]["profileImage"] != "default"
              ) {
                this.service.setStorage(
                  "profileImageGrpChat",
                  res["result"]["profileImage"]
                );
                this.service.setStorage(
                  "prfPicId",
                  this.service.url +
                    "/download/profile.json?fileID=" +
                    res["result"]["profileImage"] +
                    "&token=" +
                    this.service.getStorage("token")
                ); //User Image
                this.service.prfImg = this.service.getStorage("prfPicId");
              }
              this.service.userName = this.service.getStorage("userName");

              if (+res["result"]["level"] != 0)
                this.service.setStorage(
                  "registerLevel",
                  +res["result"]["level"] + 1
                );

              this.service.checkLogin(); //To change the login header
              if (this.loginForm.get("registerType").value == "employee") {
                if (+res["result"]["level"] == 5)
                  this.service.setStorage("registerLevel", "0");
                this.service.setStorage("employee_login", "true");
                this.service.setStorage("defaultSubId",res["result"]['subCategoryId'])
               
                this.service.setStorage("defaultSubname",'recommended')
                if (res["result"]['secSubCategoryIds']) { 
                  this.service.setStorage("secSubCategoryIds", res["result"]['secSubCategoryIds'])
                 }
                this.service.employeeNavigation();
              }
              if (this.loginForm.get("registerType").value == "employer") {
                if (+res["result"]["level"] == 3)
                  this.service.setStorage("registerLevel", "0");
                this.service.setStorage("employer_login", "true");

                this.service.employerNavigation();
              }
            } else {
              let msg = res["result"]["message"];
              this.message = msg;
            }
          },
          err => {
            this.service.serviceErrorResponce(err);
          }
        );
    } else {
      this.message = this.validationMsg.inavlidUserName;
      return;
    }
  }
}
