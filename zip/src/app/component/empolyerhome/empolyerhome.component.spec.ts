import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpolyerhomeComponent } from './empolyerhome.component';

describe('EmpolyerhomeComponent', () => {
  let component: EmpolyerhomeComponent;
  let fixture: ComponentFixture<EmpolyerhomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpolyerhomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpolyerhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
