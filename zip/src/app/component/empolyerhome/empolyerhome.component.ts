import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ServerService} from '../../service/server.service';


import { Options, LabelType, CustomStepDefinition } from 'ng5-slider';
import { isArray } from 'util';
@Component({
  selector: 'app-empolyerhome',
  templateUrl: './empolyerhome.component.html',
  styleUrls: ['./empolyerhome.component.css',
  ],
 
})
export class EmpolyerhomeComponent implements OnInit {

  constructor(private router : Router,public service :ServerService) { }
  limit=2;
  returnParseInt(value){
    return parseInt(value)
  }
  ngOnInit() {
   
    if(!this.service.isEmployerLogin()||this.service.getStorage('registerLevel')!='0')
     { this.router.navigate(['/login']);}

      this.service.checkLogin(); //To change the login header
      this.service.getRunningContract()
      if ( this.service.getStorage('tempemployeeTempId')) {
        let val =this.service.getStorage('tempemployeeTempId')
      
        this.service.setStorage('employeeTempId',val)
        
        this.router.navigate(['/hire_person_direct'])
        this.service.removeStorage('tempemployeeTempId') 
       
      }
      // if(this.service.getStorage('emailVerify')!='true')
      // this.router.navigate(['/activate-email'])
  }

  skillFormat(skill) {

    let data
    if(skill)
    data = skill.split(',');
    if (isArray(data))
      return data
    else return []
  }
}
