import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerEvaluateComponent } from './employer-evaluate.component';

describe('EmployerEvaluateComponent', () => {
  let component: EmployerEvaluateComponent;
  let fixture: ComponentFixture<EmployerEvaluateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerEvaluateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
