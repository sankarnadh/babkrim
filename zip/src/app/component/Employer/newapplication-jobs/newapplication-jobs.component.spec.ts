import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewapplicationJobsComponent } from './newapplication-jobs.component';

describe('NewapplicationJobsComponent', () => {
  let component: NewapplicationJobsComponent;
  let fixture: ComponentFixture<NewapplicationJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewapplicationJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewapplicationJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
