import { Component, OnInit } from '@angular/core';
import { Options, LabelType, CustomStepDefinition } from 'ng5-slider';
import { ServerService } from '../../../service/server.service';
import { FormBuilder, Validators, FormsModule, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ValidationMessageService } from '../../../service/validation-message.service'
import '../../../../assets/js/js/chat.js';
import { isArray } from 'util';
import { filter } from 'rxjs-compat/operator/filter';
declare var customealert: any;
@Component({
  selector: 'app-all-employee-list',
  templateUrl: './all-employee-list.component.html',
  // template:'{{jobDetails | json}}',
  styleUrls: ['./all-employee-list.component.css']
})
export class AllEmployeeListComponent implements OnInit {
  signupForm;
  constructor(
    public service: ServerService,
    public validationMsg: ValidationMessageService,
    private router: Router,
    public queryParam: ActivatedRoute,
    private spinnerService: Ng4LoadingSpinnerService,

  ) {

  }
  jobId: any;       // Job Id  From URL
  categoryId: any = '';       // CategoryId from URL
  new: any;       //New applicatints page count
  recomendation: any;       //Recommendation page count
  invited: any;       // Invited workers page count
  jobDetails
  ngOnInit() {
    customealert.hideModal('allJobsOfMe')
    this.jobDetails = JSON.parse(this.service.getStorage('data'))
   
    window.scrollTo(0, 0)
    if (!this.jobDetails)
      this.router.navigate(['/home'])

    if (this.jobDetails.jobDetails) {
      this.router.navigate(['/my_jobs'])
    }
    this.queryParam.params.subscribe((res) => {
      this.categoryId = res['categoryId'];
      this.jobId = res['jobId']
    }
    )
    if (!this.jobId || !this.categoryId)
      this.router.navigate['/home']
    this.fetchProposalFromApi(0);
    this.recommendationFromApi(0);
    this.invitedWorkersFromApi(0);
    this.hiredWorkerFromApi(0);
  }
  /*****
 * Getting all Proposal (No filter)
 */
  applicationLoader = 0
  applicationLength = 0
  applicationTotalCount = 0
  recommendationLoader = 0
  recommendationLength = 0

  newApplication = [];    //New application for job from server
  invitedWorkers = [];    //Invited workers from API
  hiredWorkers = [];    // Hired workers from API
  recommendation = [];    //Recommended workers for job by API
  hire: any;
  fetchProposalFromApi(pageNo) {


    this.service.loader = true;
    if (this.applicationLoader == 0 || pageNo == 0) {
      this.newApplication = [];

      window.scrollTo(0, 0)
    }
    pageNo = +(pageNo + 1)
    this.applicationLoader = pageNo
    this.service.serverGetRequest('', 'employer/jobDetails/proposal/' + this.jobId + ".json?pageNo=" + pageNo).subscribe(
      (res) => {

        if (res['status'].responseCode == 20) {
          res['result'].proposalDetails.forEach(element => {
            this.newApplication.push(element)
          });
          if (res['result'].proposalDetails.length > 0) {

            this.applicationTotalCount = res['result'].totalCount ? res['result'].totalCount : 0
          }
          this.applicationLength = res['result'].proposalDetails.length
          this.service.loader = false;  //Hide loader
        }

        else
          this.service.apiErrorHandler(res)
      },
      err => {
        this.service.serviceErrorResponce(err)
      }
    );
  }
  /**
   *  Recommendation from api
   */
  recommendationTotalCount = 0
  recommendationFromApi(pageNo) {

    this.service.loader = true;
    if (this.recommendationLoader == 0 || pageNo == 0)
      this.recommendation = []
    pageNo = +(pageNo + 1)
    this.recommendationLoader = pageNo
    this.service.serverGetRequest('', 'employer/jobDetails/recommendation/' + this.jobId + ".json?categoryId=" + this.categoryId + "&pageNo=" + pageNo).subscribe(
      (res) => {

        if (res['status'].responseCode == 20) {
          res['result'].recommendationDetails.forEach(element => {
            this.recommendation.push(element)
          });
          if (res['result'].recommendationDetails.length > 0) {
            this.recommendationTotalCount = res['result'].totalCount ? res['result'].totalCount : 0

          }
          this.recommendationLength = res['result'].recommendationDetails.length;
          this.service.loader = false;  //Hide loader
        }
        else
          this.service.apiErrorHandler(res)
      },
      err => {
        this.service.serviceErrorResponce(err)
      }
    );
  }
  invitedWorkersLength = 0
  invitedworkersLoadmore = 0
  invitedWrkTotalCount = 0
  invitedWorkersFromApi(pageNo) {

    this.service.loader = true;
    if (this.invitedworkersLoadmore == 0 || pageNo == 0)
      this.invitedWorkers = []
    pageNo = +(pageNo + 1)
    this.invitedworkersLoadmore = pageNo
    if(pageNo==1)
    this.invitedWorkers=[]
  
    this.service.serverGetRequest('', 'employer/jobDetails/invitation/' + this.jobId + ".json?pageNo=" + pageNo).subscribe(
      (res) => {

        if (res['status'].responseCode == 20) {
          res['result'].invitationDetails.forEach((element, index) => {
            this.invitedWorkers.push(element)
          });
          if (res['result'].invitationDetails.length > 0) {
            this.invitedWrkTotalCount = res['result'].totalCount ? res['result'].totalCount : 0

          }
          this.invitedWorkersLength = res['result'].invitationDetails.length;
          this.service.loader = false;  //Hide loader
        }
        else
          this.service.apiErrorHandler(res)
      },
      err => {
        this.service.serviceErrorResponce(err)
      }
    );
  }
  hireThisEmployee(hireThisEmployee) {

  }
  hiredWorkersLoadmore = 0
  hiredWorkersLength = 0
  hiredTotalCount = 0
  hiredWorkerFromApi(pageNo) {

    this.service.loader = true;
    if (this.hiredWorkersLoadmore == 0 || pageNo == 0)
      this.hiredWorkers = []
    pageNo = +(pageNo + 1)
    this.hiredWorkersLoadmore = pageNo
    this.service.serverGetRequest('', 'employer/jobDetails/hired/' + this.jobId + ".json?pageNo=" + pageNo).subscribe(
      (res) => {

        if (res['status'].responseCode == 20) {
          res['result'].hiredDetails.forEach(element => {
            this.hiredWorkers.push(element)
          });
          if (res['result'].hiredDetails.length > 0) {
            this.hiredTotalCount = res['result'].totalCount ? res['result'].totalCount : 0

          }
          this.hiredWorkersLength = res['result'].hiredDetails.length;
          this.service.loader = false;  //Hide loader
        }
        else
          this.service.apiErrorHandler(res)
      },
      err => {
        this.service.serviceErrorResponce(err)
      }
    );
  }
  /**
   *Experience of work Slider
   * */

  expMin = 0;
  expMax = 0;
  expModal;
  expOptions: Options = {
    floor: 0,
    ceil: 30,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '' + value;
        case LabelType.High:
          return '' + value;
        default:
          return '' + value;
      }
    }

  };
  /********************************************************************** */
  throttle = 3;
  scrollDistance = 1;
  scrollUpDistance = 2;
  tabType = 1;
  onScrollDown() {
    if (this.tabType == 1 && this.applicationLength <= this.applicationTotalCount && this.applicationLength > 0)
      this.fetchProposalFromApi(this.applicationLoader)
    if (this.tabType == 2 && this.invitedWorkersLength <= this.invitedWrkTotalCount && this.invitedWorkersLength > 0)
      this.invitedWorkersFromApi(this.invitedworkersLoadmore)
    if (this.tabType == 3 && this.hiredWorkersLength <= this.hiredTotalCount && this.hiredWorkersLength > 0)
      this.hiredWorkerFromApi(this.hiredWorkersLoadmore)
    if (this.tabType == 4 && this.recommendationLength <= this.recommendationTotalCount && this.recommendationLength > 0)
      this.recommendationFromApi(this.recommendationLoader)
  }
  /*********************************************************************** */
  inviteToJob(employeeId) {
    customealert.loaderShow('html')
    let data = {
      "employeeId": employeeId,
      "jobId": this.jobId
    }
    this.service.loader = true
    this.service.serverRequest(data, 'invitations.json').subscribe((res) => {
      this.service.loader = false
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.invitationsent)
        this.invitedWorkersFromApi(0);
        this.recommendationFromApi(0)
      }
      else
        this.service.apiErrorHandler(res)
    }, err => {
      this.service.serviceErrorResponce(err)
    })
  }
  viewEmployee(val) {

    this.service.setStorage('proposal', JSON.stringify(val))


    // routerLink="/view_employee"
  }
  skillFormat(skill) {
    let data = skill.split(',');
    if (isArray(data))
      return data
    else return []
  }
  setValue(val) {
    this.service.setStorage('tempEmployeeId', val.employeeId)
    console.log(val)
    this.service.setStorage('data', JSON.stringify(val))
    this.router.navigate(['/viewContract/' + val.contractId])
  }
  navigateMsg(receiver, proposal) {
    this.router.navigate(['/message/' + receiver + '/' + proposal])
  }
  tempProposalId
  rejectProposals(proposalId) {
    customealert.loaderShow('html');
    this.service.serverDeleteRequest('proposalDetails/reject/' + proposalId + ".json?registerType=" + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        if (this.tabType == 1)
          this.fetchProposalFromApi(0)
        if (this.tabType == 2)
          this.invitedWorkersFromApi(0)
        if (this.tabType == 3)
          this.hiredWorkerFromApi(0)
        if (this.tabType == 4)
          this.recommendationFromApi(0)

      } else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))
  }
  tempInvitationId
  withdrawInvitation(invitationId)
  {
    customealert.loaderShow('html');
    this.service.serverDeleteRequest('invitation/reject/' + invitationId + ".json?registerType=" + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.service.showSuccess(this.validationMsg.success)
        
          this.invitedWorkersFromApi(0)
       

      } else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))
  }
  pageChange() {
    if (this.service.isEmployerLogin())
      this.router.navigate(['/hire_person_direct'])
  }
  
}
