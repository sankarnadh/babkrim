import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonUrgentRequestComponent } from './non-urgent-request.component';

describe('NonUrgentRequestComponent', () => {
  let component: NonUrgentRequestComponent;
  let fixture: ComponentFixture<NonUrgentRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonUrgentRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonUrgentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
