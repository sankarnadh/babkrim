import {
  Component,
  OnInit,
  ElementRef,
  NgZone,
  ViewChild
} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NgProgress } from "ngx-progressbar";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ValidationServiceService } from "../../../service/validation-service.service";
import { ServerService } from "../../../service/server.service";
import { ValidationMessageService } from "../../../service/validation-message.service";
// import  from "@types/googlemaps";
import { MapsAPILoader, AgmMap } from "@agm/core";
import { Router } from "@angular/router";
import "../../../../assets/js/js/chat.js";
declare var customealert: any;
// import { } from 'googlemaps';
declare const google :any;
@Component({
  selector: "app-manager-details",
  templateUrl: "./manager-details.component.html",
  styleUrls: ["./manager-details.component.css"]
})
export class ManagerDetailsComponent implements OnInit {
  @ViewChild("search")
  public searchElementRef: ElementRef;
  @ViewChild("map") map: AgmMap;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  signupForm: FormGroup;
  es_name: string = "";
  es_state: string = "";
  es_country: string = "";
  es_city: string = "";
  es_website: string = "";
  es_email: string = "";
  es_phone: string = "";
  es_mobile: string = "";
  es_zipcode: string = "";
  es_po: string = "";
  ma_fname: string = "";
  ma_mname: string = "";
  ma_lname: string = "";
  ma_position: string = "";
  ma_mobile: string = "";
  ma_phone: string = "";
  ma_email: string = "";
  ex_name: string = "";
  ex_mobile: string = "";
  ex_phone: string = "";
  ex_email: string = "";
  file: any;

  System: any;
  cities: any;
  state: any;

  country: any;

  step1btn: boolean;
  step1btnValid: any;
  step2btnValid: any;
  constructor(
    public ngProgress: NgProgress,
    private http: HttpClient,
    private el: ElementRef,
    private router: Router,
    private formBuilder: FormBuilder,
    public service: ServerService,
    public validationMsg: ValidationMessageService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {
    this.signupForm = this.formBuilder.group({
      es_name: [
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(50),
          ValidationServiceService.nameValidator
        ])
      ],
      es_state: ["", ValidationServiceService.notNullValidation],
      es_country: ["", ValidationServiceService.notNullValidation],
      es_city: ["", ValidationServiceService.notNullValidation],
      es_website: [
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(50),
          ValidationServiceService.nameValidator
        ])
      ],
      es_email: [
        "",
        Validators.compose([
          ValidationServiceService.emailValidator,
          Validators.maxLength(50)
        ])
      ],
      es_phone: [
        "",
        Validators.compose([
          ValidationServiceService.mobileValidation,
          Validators.maxLength(50)
        ])
      ],
      es_mobile: [
        "",
        Validators.compose([
          ValidationServiceService.mobileValidation,
          Validators.maxLength(50)
        ])
      ],
      es_zipcode: ["", Validators.required],
      es_po: ["", Validators.required],
      ma_fname: ["", Validators.required],
      ma_mname: ["", Validators.required],
      ma_lname: ["", Validators.required],
      ma_position: ["", Validators.required],
      ma_mobile: ["", ValidationServiceService.mobileValidation],
      ma_phone: ["", ValidationServiceService.mobileValidation],
      ma_email: [
        "",
        Validators.compose([
          ValidationServiceService.emailValidator,
          Validators.maxLength(50)
        ])
      ],
      ex_name: ["", Validators.required],
      ex_mobile: ["", ValidationServiceService.mobileValidation],
      ex_phone: ["", ValidationServiceService.mobileValidation],
      ex_email: [
        "",
        Validators.compose([
          ValidationServiceService.emailValidator,
          Validators.maxLength(50)
        ])
      ],
      file: ["", Validators.required],
      companyLocation: ["", Validators.required],
      terms: [""],
      map: [""]
    });
  }

  // google maps zoom level
  zoom: number = 16;
  lat: any = this.service.lat;
  lng: any = this.service.lng;

  clickedMarker(label: string, index: any) {
    console.log(index);
  }
  i = 0;
  deltaLat;
  deltaLng;
  numDeltas = 100;
  delay = 10;
  transition() {
    this.i = 0;
    this.deltaLat = this.lat / this.numDeltas;

    this.deltaLng = this.lng / this.numDeltas;
    this.moveMarker();
  }

  templat = 0;
  templng = 0;
  moveMarker() {
    console.log(this.deltaLat + " " + this.lat);
    this.templat += this.deltaLat;
    this.templng += this.deltaLng;

    this.markers = [
      {
        lat: this.templat,
        lng: this.templng,
        draggable: true
      }
    ];

    // this.lat = this.templat;
    //   this.lng = this.templng;
    if (this.i != this.numDeltas) {
      this.i = this.i + 1;

      this.deltaLat = this.deltaLat;
      this.i = this.i;
      this.numDeltas = this.numDeltas;
      console.log(this.deltaLat + " " + this.lat);
      setTimeout(this.moveMarker, this.delay);
    }
  }
  mapClicked($event: MouseEvent) {
    this.markers = [
      {
        lat: $event["coords"].lat,
        lng: $event["coords"].lng,
        draggable: true
      }
    ];
    this.lat = $event["coords"].lat;
    this.lng = $event["coords"].lng;
    // this.transition()
    this.getGeoLocation($event["coords"].lat, $event["coords"].lng);
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    this.lat = $event["coords"].lat;
    this.lng = $event["coords"].lng;
    this.getGeoLocation($event["coords"].lat, $event["coords"].lng);
  }
  markers: marker[] = [];

  employerType: any = this.service.getStorage("employerType")
    ? this.service.getStorage("employerType")
    : "individual";
  ngOnInit() {
    this.signupForm.get("ma_email").setValue(this.service.getStorage("email"));

    this.signupForm
      .get("ma_mobile")
      .setValue(this.service.getStorage("mobileNo"));
    // this.getGeoLocation(this.lat,  this.lng)
    this.mapsAPILoader.load().then(() => {
      let autocomplete;
      try {
        autocomplete = new google.maps.places.Autocomplete(
          this.searchElementRef.nativeElement,
          {
            types: ["address"]
          }
        );
      } catch (error) {
        console.log(error);
        window.location.reload();
      }
      google.maps.event.addListener(autocomplete, "place_changed", () => {
        this.ngZone.run(() => {
          let place:google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.signupForm
            .get("companyLocation")
            .setValue(place.formatted_address);
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.markers = [
            {
              lat: this.lat,
              lng: this.lng,
              draggable: true
            }
          ];
        });
      });
    });

    if (!this.service.isEmployerLogin()) this.router.navigate(["/login"]);

    this.previousLevel3();
    //Email verification check
  }
  getlocationDetails() //Fetching location details
  {
    let response: any;
    this.service.serverGetRequest("", "/static/locationDetails.json").subscribe(
      (res: Response) => {
        response = res;

        if (response.status.responseCode == "20") {
          this.lat = res["result"]["latitude"];
          this.lng = res["result"]["longitude"];
          this.getGeoLocation(parseFloat(this.lat), parseFloat(this.lng));
          this.markers = [
            {
              lat: this.lat,
              lng: this.lng,
              // lat :10.0236761,
              // lng:76.3116235,
              draggable: true
            }
          ];
        } else {
          this.service.apiErrorHandler(res);
        }
      },
      err => {
        this.service.serviceErrorResponce(err);
      }
    );
  }

  //Step 2 button disabled
  buttonClick = 0;
  location: any;
  get s() {
    return this.signupForm.controls;
  }
  managerDetails() {
    if (this.employerType == "individual") {
      this.s.ma_fname.setValue("BABKRIM-MANAGER-FIRST-NAME");
      this.s.ma_mname.setValue("BABKRIM-MANAGER-MIDDLE-NAME");
      this.s.ma_lname.setValue("BABKRIM-MANAGER-LAST-NAME");
      this.s.ma_email.setValue("BABKRIM@DEFAULT.VALUE");
      this.s.ma_position.setValue("BABKRIM-MANAGER-POSITION");
      this.s.ma_mobile.setValue("0000000001");
      this.s.ma_phone.setValue("0000000001");
      this.s.ma_email.setValue("BABKRIM@DEFAULT.VALUE");
      this.s.ex_name.setValue("BABKRIM-EXECUTIVE-MANAGER-NAME");
      this.s.ex_mobile.setValue("0000000001");
      this.s.ex_phone.setValue("0000000001");
      this.s.ex_email.setValue("BABKRIM@DEFAULT.VALUE");
    }
    if (!this.signupForm.get("ma_fname").valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    if (!this.signupForm.get("ma_lname").valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    if (!this.signupForm.get("ma_position").valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    // if (!this.signupForm.get('ma_phone').valid) {
    //   this.buttonClick = 1
    //   return false
    // }
    // else
    //   this.buttonClick = 0
    if (this.signupForm.get("ex_mobile").value) {
      if (this.signupForm.get("ex_mobile").value.length !=10) {
       
        this.buttonClick = 1;
        return false;
      }
    }
    if (this.employerType == "comapny")
      if (!this.signupForm.get("ma_email").valid) {
        this.buttonClick = 1;
        return false;
      } else this.buttonClick = 0;

    if (!this.signupForm.get("companyLocation").valid) {
      this.buttonClick = 1;
      return false;
    } else this.buttonClick = 0;

    customealert.loaderShow("html");
    let terms = 0;
    if (this.signupForm.get("terms").value) terms = 1;
    let data = {
      managerFirstName: this.signupForm.get("ma_fname").value
        ? this.signupForm.get("ma_fname").value
        : "",
      managerMiddleName: this.signupForm.get("ma_mname").value
        ? this.signupForm.get("ma_mname").value
        : "",
      managerLastName: this.signupForm.get("ma_lname").value
        ? this.signupForm.get("ma_lname").value
        : "",
      managerPosition: this.signupForm.get("ma_position").value
        ? this.signupForm.get("ma_position").value
        : "",
      managerMobileNo: this.signupForm.get("ma_mobile").value
        ? this.signupForm.get("ma_mobile").value
        : "",
      managerPhoneNo: this.signupForm.get("ma_phone").value
        ? this.signupForm.get("ma_phone").value
        : "",
      managerEmail: this.signupForm.get("ma_email").value
        ? this.signupForm.get("ma_email").value
        : "",

      exManagerName: this.signupForm.get("ex_name").value
        ? this.signupForm.get("ex_name").value
        : "",
      exManagerMobileNo: this.signupForm.get("ex_mobile").value
        ? this.signupForm.get("ex_mobile").value
        : "",
      exManagerPhoneNo: this.signupForm.get("ex_phone").value
        ? this.signupForm.get("ex_phone").value
        : "",
      exManagerEmail: this.signupForm.get("ex_email").value
        ? this.signupForm.get("ex_email").value
        : "",
      termAndCondition: 1,
      participateMap: this.signupForm.get("map").value,
      latitude: this.lat,
      longitude: this.lng
    };

    let response: any;

    this.service
      .serverRequest(
        data,
        "register.json?registerType=" +
          this.service.getStorage("registerType") +
          "&registerLevel=3&deviceMode=web"
      )
      .subscribe(
        (res: Response) => {
          customealert.loaderHide("html");
          response = res["status"];
          if (response.responseCode == 20) {
            this.service.setStorage("registerLevel", 0);

            if (this.service.getStorage("emailVerify") == "true") {
              this.service.showSuccess(this.validationMsg.success);
              this.router.navigate(["/employer_home"]);
            } else this.service.verificationCheck();
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);

          customealert.loaderHide("html");
        }
      );
  }
  getGeoLocation(lat: number, lng: number) {
    if (navigator.geolocation) {
      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(lat, lng);
      let request = { latLng: latlng };

      geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          let resultLength = rsltAdrComponent.length;
          if (result != null) {
            this.signupForm
              .get("companyLocation")
              .setValue(result.formatted_address);
            // this.jobForm.get('business_location_name').setValue(result.formatted_address)
          } else {
            //alert("No address available!");
          }
        }
      });
    }
  }
  previous() {
    this.service.setStorage("employerPrevious", 2);

    this.router.navigate(["/employer_companydetails"]);
  }
  /**
   *
   */
  modalPage = 0; /**0=>terms 1=>privacy 2=>cookies */
  /***
   * Previous inserted
   */
  prMfname;
  prMlname;
  prMmname;
  prMposition;
  prMmob = this.service.getStorage("mobileNo");
  prMphn;
  prMemail;

  prExname;
  prExmob;
  prExphn;
  prExemail;

  prTerm = 1;
  prMap = 0;
  previousLevel3() {
    let response;
    customealert.loaderShow("html");

    this.service
      .serverGetRequest(
        "",
        "profile/profileDetails.json?registerType=" +
          this.service.getStorage("registerType") +
          "&registerLevel=3"
      )
      .subscribe(
        (res: Response) => {
          response = res;
          customealert.loaderHide("html");

          if (response.status.responseCode == "20") {
            let result = response.result;
            if (result) {
              this.prMfname =
                result.managerFirstName == "BABKRIM-MANAGER-FIRST-NAME"
                  ? ""
                  : result.managerFirstName;
              this.prMmname =
                result.managerMiddleName == "BABKRIM-MANAGER-MIDDLE-NAME"
                  ? ""
                  : result.managerMiddleName;
              this.prMlname =
                result.managerLastName == "BABKRIM-MANAGER-LAST-NAME"
                  ? ""
                  : result.managerLastName;
              this.prMposition =
                result.managerPosition == "BABKRIM-MANAGER-POSITION"
                  ? ""
                  : result.managerPosition;
              // this.prMmob = result.managerMobileNo
              this.signupForm.get("ma_mobile").setValue(this.prMmob);
              this.prMphn =
                result.managerPhoneNo == "0000000001"
                  ? ""
                  : result.managerPhoneNo;
              this.prMemail =
                result.managerEmail == "BABKRIM@DEFAULT.VALUE"
                  ? ""
                  : result.managerEmail;
              //         this.signupForm.get('ma_email').setValue(result.managerMobileNo)

              // this.signupForm.get('ma_mobile').setValue(result.managerEmail)
              this.lat = parseFloat(result.latitude);
              this.lng = parseFloat(result.longitude);
              this.getGeoLocation(this.lat, this.lng);
              this.markers = [
                {
                  lat: this.lat,
                  lng: this.lng,

                  draggable: true
                }
              ];
              this.prExname =
                result.exManagerName == "BABKRIM-EXECUTIVE-MANAGER-NAME"
                  ? ""
                  : result.exManagerName;
              this.prExmob =
                result.exManagerMobileNo == "0000000001"
                  ? ""
                  : result.exManagerMobileNo;
              this.prExphn =
                result.exManagerPhoneNo == "0000000001"
                  ? ""
                  : result.exManagerPhoneNo;
              this.prExemail =
                result.exManagerEmail == "BABKRIM@DEFAULT.VALUE"
                  ? ""
                  : result.exManagerEmail;
              this.prTerm = result.termAndCondition;
              this.prMap = result.participateMap;

              if (navigator.geolocation && !result.latitude) {
                navigator.geolocation.getCurrentPosition(position => {
                  this.getGeoLocation(
                    position.coords.latitude,
                    position.coords.longitude
                  );
                  this.lat = position.coords.latitude;
                  this.lng = position.coords.longitude;
                  this.markers = [
                    {
                      lat: parseFloat(this.lat),
                      lng: parseFloat(this.lng),
                      draggable: true
                    }
                  ];
                });
              }
            }
          } else {
            this.service.apiErrorHandler(res);
          }
        },
        err => {
          this.service.serviceErrorResponce(err);
        }
      );
  }
}
// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
