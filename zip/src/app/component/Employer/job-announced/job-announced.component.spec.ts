import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAnnouncedComponent } from './job-announced.component';

describe('JobAnnouncedComponent', () => {
  let component: JobAnnouncedComponent;
  let fixture: ComponentFixture<JobAnnouncedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobAnnouncedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAnnouncedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
