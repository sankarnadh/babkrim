import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerEditJobComponent } from './employer-edit-job.component';

describe('EmployerEditJobComponent', () => {
  let component: EmployerEditJobComponent;
  let fixture: ComponentFixture<EmployerEditJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerEditJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerEditJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
