import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteInterviewComponent } from './invite-interview.component';

describe('InviteInterviewComponent', () => {
  let component: InviteInterviewComponent;
  let fixture: ComponentFixture<InviteInterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteInterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteInterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
