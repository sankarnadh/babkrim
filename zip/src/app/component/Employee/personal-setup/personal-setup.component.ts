import { Component, OnInit, ElementRef, Input, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { FormBuilder, Validators, FormsModule, FormControl } from '@angular/forms';
import { ValidationServiceService } from '../../../service/validation-service.service';
import { ServerService } from '../../../service/server.service';
import { ValidationMessageService } from '../../../service/validation-message.service'
import { NgProgress } from 'ngx-progressbar';
import { Router, ActivatedRoute } from "@angular/router";
import '../../../../assets/js/js/chat.js';
declare var customealert: any;

@Component({
  selector: 'app-personal-setup',
  templateUrl: './personal-setup.component.html',
  styleUrls: ['./personal-setup.component.css']
})
export class PersonalSetupComponent implements OnInit {
  disabled_text: any = 'disabled="disabled"'
  bottonClick: number = 0;
  signupForm: any;
  cities: any;
  state: any;
  gender: any;
  country: any;
  nationality: any;
  btnValid: any;
  category: any;
  currency: any;
  subCategory: any;
  nextSubCategory: any;
  fileLoader: any = false;
  experienceCertificate = [];
  prHeadline: any;
  tempSpecialization = [];
  tempPrimarySpecialization = [];
  //Select box loader
  initialLoader: any;
  subCategoryLoader: any;
  nextSubCategoryLoader: any;
  ar = Array();
  n = 8;
  constructor(private http: HttpClient, private el: ElementRef,
    public router: Router,
    public ngProgress: NgProgress,
    public validator: ValidationServiceService,
    private formBuilder: FormBuilder, public service: ServerService,
    public validationMsg: ValidationMessageService,
    public activatedRoute: ActivatedRoute
  ) {

    this.signupForm = this.formBuilder.group
      ({
        'experience': ['', Validators.compose([Validators.required, ValidationServiceService.numberValidation])],
        'upload_experience': ['', Validators.required],
        'general_specialization': ['', ValidationServiceService.notNullValidation],
        'job_specialization': ['', ValidationServiceService.notNullValidation],
        'more_specialization': ['', ValidationServiceService.notNullValidation],
        'wages': ['', Validators.compose([Validators.required, ValidationServiceService.numberValidation])],
        'currency': ['', Validators.required],
        'max_wrk_hrs': ['', ValidationServiceService.notNullValidation],
        'min_wrk_hrs': ['', ValidationServiceService.notNullValidation],
        'disability': ['', Validators.required],
        'type_disability': ['', Validators.required],
        'work_other': ['', Validators.required],
        'receive_sms': ['', Validators.required],
        'receive_voice_alert': ['', Validators.required],
        'skills': ['', Validators.required],
        'profileHeadline': ['', Validators.minLength(10)],
        'availability': ['', Validators.required]
      });


    if (!this.signupForm.get('disability').value || this.prDisability == 0)
      this.signupForm.get('disability').value = "false";         //Do you have disability
    if (this.prDisability == 1)
      this.signupForm.get('disability').value = "true";
    if (!this.signupForm.get('work_other').value)
      this.signupForm.get('work_other').value = "false";         //Accept wrk other 

    if (!this.signupForm.get('receive_sms').value)
      this.signupForm.get('receive_sms').value = "false";        //receive a message

    if (!this.signupForm.get('receive_voice_alert').value)
      this.signupForm.get('receive_voice_alert').value = "false";  //receive_voice_alert
  }
  ngOnInit() {
    if (!this.service.isEmployeeLogin())
      this.router.navigate(['/login']);                       //Authentication check
    this.service.checkLogin(); //To change the login header
    this.initialLoader = "loadinginput"                         //Loading Input
    this.btnValid = "not-allowed";                              //BUtton Style
    this.getLevel4();                                         //Common Details
    this.previousLevel4()                                     //Previously submited data
    // this.service.verificationCheck();                         //Email verification check
    this.service.loader = false
  }

  //Fetch Category
  getLevel4() {
    this.service.serverGetRequest('', 'custom/employee/level/4.json').subscribe(
      (res) => {
        this.initialLoader = '';
        if (res['status'].responseCode == 20) {

          this.category = res['result'].categories;
          this.currency = res['result'].currencies;
        }
        else {
          this.service.apiErrorHandler(res);

        }
      },
      err => {
        this.service.serviceErrorResponce(err)

      }
    );
  }
  skill = [];
  skillModal = [];

  getSkills(id) {

    this.skill = []
    this.service.serverGetRequest('', 'static/skills.json?category=' + id).subscribe(
      (res) => {
        this.subCategoryLoader = ''
        if (res['status'].responseCode == 20) {
          this.skill = res['result'].skills

        }
      },
      err => {
        this.subCategoryLoader = ''
        this.service.serviceErrorResponce(err)

      }
    );
  }

  getSubCategory(id) {

    this.subCategoryLoader = 'loadinginput'
    this.subCategory = []

    this.nextSubCategory = []
    this.service.serverGetRequest('', 'static/subCategories.json?category=' + id).subscribe(
      (res) => {
        this.subCategoryLoader = ''
        if (res['status'].responseCode == 20) {
          this.subCategory = res['result'].SubCategories;
        }

      },
      err => {
        this.subCategoryLoader = ''
        this.service.serviceErrorResponce(err)

      }
    );
  }
  getNextSubCategory(id) {
    this.nextSubCategoryLoader = 'loadinginput'
    this.nextSubCategory = []
    this.service.serverGetRequest('', 'static/nextSubCategories.json?subCategory=' + id).subscribe(
      (res) => {
        this.nextSubCategoryLoader = ''
        if (res['status'].responseCode == 20) {
          let count = res['result'].nextSubCategories.length;
          let response = res['result'].nextSubCategories;

          for (let i = 0; i < count; i++) {
            response[i].id = parseInt(response[i].id)
          }
          this.nextSubCategory = response;

        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.nextSubCategoryLoader = ''
        this.service.serviceErrorResponce(err)

      }
    );
  }
  lengthIssue = 0
  wageIssue = 0
  availability: any = 'Any'
  employeeRegister() {
    this.signupForm.get('currency').value = this.prCurrency
    if (!this.signupForm.get('more_specialization').value)
      this.signupForm.get('more_specialization').value = '';
    if (!this.signupForm.get('max_wrk_hrs').value)
      this.signupForm.get('max_wrk_hrs').value = this.prMaxwrk;

    if (!this.signupForm.get('min_wrk_hrs').value)
      this.signupForm.get('min_wrk_hrs').value = this.prMinwrk;
    // i

    if (!this.signupForm.get('experience').valid || this.signupForm.get('experience').value > 52) {
      this.bottonClick = 1
      console.log('1')
      return false
    }
    else
      this.bottonClick = 0

    if (this.signupForm.get('experience').valid) {
      let ex: any = String(this.signupForm.get('experience').value)
      if (ex.split('.').pop())
        if (ex.split('.').pop().length > 2) {
          this.lengthIssue = 1
          return false
        }
        else
          this.lengthIssue = 0
    }
    if (this.tempSpecialization.length == 0) {
      this.bottonClick = 1
      console.log('1')
      return false
    }
    if (this.prDisability == 1 && !this.prTypeofdisability)
      return false

    // if (!this.signupForm.get('general_specialization').value) {
    //   this.bottonClick = 1
    //   console.log('2')

    //   return false
    // }
    // else
    //   this.bottonClick = 0
    // if (!this.signupForm.get('job_specialization').value) {
    //   this.bottonClick = 1
    //   console.log('3')

    //   return false
    // }
    // else
    //   this.bottonClick = 0
    // if (!this.signupForm.get('more_specialization').value) {
    //   this.bottonClick = 1
    //   console.log('4')

    //   return false
    // }
    // else
    //   this.bottonClick = 0
    // if (this.skillModal.length == 0) {
    //   this.bottonClick = 1
    //   console.log('5')

    //   return false
    // }
    // else
    //   this.bottonClick = 0
    if (!this.signupForm.get('wages').value) {
      this.bottonClick = 1
      console.log('6')

      return false
    }
    else
      this.bottonClick = 0
    if (this.signupForm.get('wages').valid) {
      // let ex: any = String(this.signupForm.get('wages').value)
      // if (ex.split('.').pop())
      //   if (ex.split('.').pop().length > 2) {
         
      //     this.wageIssue = 1
      //     return false
      //   }
      //   else
      //     this.wageIssue = 0
    }
    if (this.signupForm.get('wages').value < this.standardWage) {
      // alert(this.standardWage)
      this.bottonClick = 1
      return;
    }
    if (!this.signupForm.get('currency').value) {
      this.bottonClick = 1
      console.log('7')

      return false
    }
    else
      this.bottonClick = 0
    if (!this.signupForm.get('max_wrk_hrs').value) {
      this.bottonClick = 1
      console.log('8')

      return false
    }
    else
      this.bottonClick = 0
    if (!this.signupForm.get('min_wrk_hrs').value) {
      this.bottonClick = 1
      console.log('9')

      return false
    }
    else
      this.bottonClick = 0
    if (this.prMinwrk >= this.prMaxwrk) {
      this.bottonClick = 1


      return false
    }
    else
      this.bottonClick = 0
    if (!this.signupForm.get('profileHeadline').valid) {
      this.bottonClick = 1
      console.log('10')

      return false
    }
    else
      this.bottonClick = 0
    customealert.loaderShow("html")
    this.bottonClick = 0
    let category;
    let subCategory;
    let secCategories = [];
    this.tempSpecialization.forEach(element => {
      if (element.primary) {
       
        category = element.categoryId;
        subCategory = element.subCategoryId;
      }
      else {
        secCategories.push({
          category: element.categoryId,
          subCategory: element.subCategoryId
        })
      }
    });


    if (!this.signupForm.get('disability').value || this.prDisability == 0)
      this.signupForm.get('disability').value = "false";         //Do you have disability
    if (this.prDisability == 1)
      this.signupForm.get('disability').value = "true";
    let data = {
      'yearOfExperience': this.signupForm.get('experience').value,
      'category': category,
      'idNo': this.signupForm.get('job_specialization').value,
      'subCategory': subCategory,
      'nextSubCategory': '0000001',//this.signupForm.get('more_specialization').value,
      'secCategories': secCategories,
      'disabilityStatus': this.signupForm.get('disability').value,
      'disabilityDescription': this.signupForm.get('type_disability').value,
      'minimumHourlyWage': this.signupForm.get('wages').value,
      'currency': this.signupForm.get('currency').value,
      'maxHourPerDay': this.signupForm.get('max_wrk_hrs').value,
      'minHourPerDay': this.signupForm.get('min_wrk_hrs').value,
      'acceptOtherWork': this.signupForm.get('work_other').value,
      'smsNotification': this.signupForm.get('receive_sms').value,
      'voiceNotification': this.signupForm.get('receive_voice_alert').value,
      'experienceCertificate': this.experienceCertificate,
      'skills': ['BABKRIM-DEFAULT-SKILL'],//this.skillModal,
      'profileTitle': this.prHeadline,
      'availability': this.availability
    };

    let response: any;

    this.service.serverRequest(data, 'register.json?registerType=' + this.service.getStorage('registerType') + '&registerLevel=4&deviceMode=web').subscribe(
      (res: Response) => {

        customealert.loaderHide("html")
        response = res['status'];

        if (response.responseCode == 20) {
          if (this.service.getRegisterLevel() == '0')
            this.service.showSuccess(this.validationMsg.success)
          else
            this.service.showSuccess(this.validationMsg.success)
          this.service.setStorage('registerLevel', 0);
          /*****
           * Check wheter email is verified or not
           */
          if (this.service.getStorage('emailVerify') == 'true')
            if (this.service.getStorage('userVerify') == 'true')
              this.router.navigate(['/employee_home']);
            else
              this.router.navigate(['/confirmation']);

          else
            this.router.navigate(['/activate_email'])
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)
      }
    );
  }
  /****
   * Experience Certificate file upload
   */
  fileCounter: any = 0;
  fileEvent(event: any) {


    var count = event.target.files.length
    if (this.fileCounter + count < 6) {
      for (let i = 0; i < count; i++) {
        const fileSelected: File = event.target.files[i];
        if (!this.service.checkAllowedFiles(event.target.files[i].name.split('.').pop())) {
          this.service.showError(this.validationMsg.invalidFileType)
          return false
        }
        if (fileSelected.size < this.service.maxFileSize) {

          let response;
          this.fileLoader = true
          this.ngProgress.start()


          this.service.uploadFile(fileSelected, 'upload.json?fileType=docs')
            .subscribe((res: Response) => {
              this.ngProgress.done()

              response = res;
              this.fileLoader = false;
              this.service.loader = false

              if (response.status.responseCode == 20) {
                this.dwnld_file = true;
                this.fileCounter++;
                this.experienceCertificate.push(response.result.file1);
                this.fileName.push(fileSelected.name)
              }
              else {
                this.service.apiErrorHandler(res);

              }
            },
              (err) => {
                this.ngProgress.done()

                this.fileLoader = false
                this.service.loader = false
                this.service.serviceErrorResponce(err)
              });
        } else this.service.showError(this.validationMsg.attachmentSizeLimit)
      }
    }
    else {

      this.service.showError(this.validationMsg.attachmentLimit)
    }
  }
  /********
   * Previous going back 2 level 3 employee
   * 
   */
  previous() {

    this.router.navigate(['/employee_educationsetup'])
  }
  /*****
   * Previous submited data
   */
  prExp: any = 0;
  prDisability: any = 0;
  prTypeofdisability;
  prGeneralspec;
  prSpec;
  prMorespec;
  prMinwage;
  prCurrency = this.service.defaultCurrency;
  prMaxwrk = 8;
  prMinwrk = 1;
  prOtherwrk = 0;
  prSms = 0;
  prVoice = 0;

  fileName = [];
  dwnld_file: any = false;
  categoryId = null;
  subcateId = null;;
  moresubId = null;;
  previousLevel4() {
    let response;
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'profile/profileDetails.json?registerType=' + this.service.getStorage('registerType') + '&registerLevel=4').subscribe(
      (res: Response) => {
        response = res;
        customealert.loaderHide('html')
        let result = response.result;

        if (response.status.responseCode == "20") {
          if (!result)
            return false;
          this.prExp = result.yearOfExperience;//Previous Experience
          this.prDisability = result.disabilityStatus; // Radio btn value 0 (no) / 1 (yes)
          this.prTypeofdisability = result.disabilityDescription; // Type of disability
          this.prGeneralspec = result.category.categoryId; //General specialization 

          this.prSpec = result.subCategory.categoryId;// Specialization
          this.availability=result.subCategory.availability
          this.prMorespec = result.nextSubCategory.subSubCategoryId; //More specialization
          this.categoryId = this.prGeneralspec;
          this.subcateId = this.prSpec;
          this.moresubId = this.prMorespec;
          this.getSubCategory(this.categoryId);
          this.getSkills(this.categoryId);
          if (result.skillDetails.length != 0) {
            let ar = [], res = [];
            this.skillModal = []
            res = (result.skillDetails)
            res.forEach(element => {
              this.skillModal.push((element.skill))
            });

          }
          this.getNextSubCategory(this.subcateId);
          this.prMinwage = result.minimumHourlyWage // Minimum wages
          this.prHeadline = result.profileTitle; //Profile tittle
          this.prCurrency = result.currency.currencyId; // Currency id
          this.prMaxwrk = result.maxHourPerDay;
          this.prMinwrk = result.minHourPerDay
          this.prOtherwrk = result.acceptOtherWork // Accept other works 
          this.prSms = result.smsNotification
          this.prVoice = result.voiceNotification
          if (result.category) {
            this.tempSpecialization.push({
              category: result.category.categoryName,
              categoryId: result.category.categoryId,
              primary: true,
              subCategory: result.subCategory.subCategoryName,
              subCategoryId: result.subCategory.categoryId
            })
          }
          if (result.secCategories) {
            result.secCategories.forEach(element => {
              this.tempSpecialization.push({
                category: element.categoryName,
                categoryId: element.categoryId,
                primary: false,
                subCategory: element.subCategoryName,
                subCategoryId: element.subCategoryId
              })
            });
          }

          if (result.experienceCertificate) {

            this.dwnld_file = true;
            let count = result.experienceCertificate.length
            this.fileCounter = count;
            for (let i = 0; i < count; i++) {
              if (!result.experienceCertificate[i].fileId)
                break;
              this.experienceCertificate.push(result.experienceCertificate[i].fileId)
              this.fileName.push(result.experienceCertificate[i].seedName);

            }

          }


        }
        else {
          this.service.apiErrorHandler(res);

        }
      },
      err => {
        this.service.serviceErrorResponce(err)

      }
    );
  }
  deleteFileicon(index) {

    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.experienceCertificate.splice(index, 1);
      this.fileCounter--
      if (this.fileCounter == 0)
        this.fileCounter = 0
    }


  }
  /********Drop down events */
  categorySelect(item: any) {
    this.prSpec = null
    this.prMorespec = null
    this.skillModal = []
    this.subCategory = [];
    this.nextSubCategory = [];
    if (item) {
      this.getSubCategory(item.id)
      this.getSkills(item.id);
    }      //Fetching Skills from API
  }
  categoryDeselect() {
    this.prSpec = null
    this.subCategory = []
    this.nextSubCategory = []
    this.showAddButton = 0;
    this.tempData=[]
    this.prGeneralspec=null
    this.resetModal()
  }
  subcategoryDeselect() {
    // this.subCategory = [];
    this.nextSubCategory = [];
    this.prMorespec = null
    this.standardWage = 0
    this.showAddButton = 0;
  }
  standardWage = 0
  subcategorySelect(item: any) {
    this.nextSubCategory = [];
    this.prMorespec = null;
    this.getNextSubCategory(item.id)
    this.standardWage = item.standardWage;
  }
  tempData = [];
  showAddButton = 0;
  onCategorySelect(item: any) {
    var primary = false;
    this.tempData=[];
    if (this.tempSpecialization.length == 0 && this.tempData.length == 0) {

      primary = true;
    }
    var flag = true;
    this.tempSpecialization.forEach(element => {
      if (element.categoryId == this.prGeneralspec && element.subCategoryId == item.id) {
        flag = false;
        return false;
      }
    });
    this.tempData.push({
      category: item.category,
      categoryId: item.id,
      primary: primary,
      subCategory: null,
      subCategoryId: null
    })

  }
  resetModal() {
    this.prGeneralspec = null
    this.subCategory = [];
    this.tempData = [];
    this.prMorespec = null
    this.prSpec = null;
    this.standardWage = 0
    this.showAddButton = 0;
  }
  onSubCategorySelected(item: any) {
    var flag = true;
    this.showAddButton = 1;
    this.tempSpecialization.forEach(element => {
      if (element.categoryId == this.prGeneralspec && element.subCategoryId == item.id) {
        flag = false;
        this.tempData.splice(this.tempData.length - 1, 1)
        return false;
      }
    });
    if (flag) {

      this.tempData.forEach((element, key) => {

        if (element.subCategory == null && element.subCategoryId == null && element.categoryId == this.prGeneralspec) {
          element.subCategory = item.subCategory;
          element.subCategoryId = item.id
        }
      });
    }
  }
  pushItemToCategory() {
    var flag = 1;
    console.log(this.tempData)
    this.tempData.forEach((element, key) => {
      if (element.subCategory == null && element.subCategoryId == null) {

        this.tempSpecialization.splice(key, 1);
      }
    });
    if (this.tempData[0]) {
      this.tempSpecialization.forEach((element, key) => {

        if (element.categoryId == this.tempData[0].categoryId && element.subCategoryId == this.tempData[0].subCategoryId && flag == 1) {
          flag = 0;

        }
      })
      if (flag) {
        this.tempSpecialization.push({
          category: this.tempData[0].category,
          categoryId: this.tempData[0].categoryId,
          primary: this.tempData[0].primary,
          subCategory: this.tempData[0].subCategory,
          subCategoryId: this.tempData[0].subCategoryId
        })
      }
    }
    this.resetModal();
  }
  removeSpecialization(index) {
    this.tempSpecialization.splice(index, 1);
    var flag = false;
    this.tempSpecialization.forEach(element => {
      if (!element.primary && !flag) {
        this.tempSpecialization[0].primary = true;
        flag = true
      }
    });
    this.resetModal();
  }
  makeAsPrimary(index) {
    this.tempSpecialization.forEach(element => {
      if (element.primary) {
        element.primary = false;

      }
    });
    this.tempSpecialization[index].primary = true;
  }
}
