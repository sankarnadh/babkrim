import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalSetupComponent } from './personal-setup.component';

describe('PersonalSetupComponent', () => {
  let component: PersonalSetupComponent;
  let fixture: ComponentFixture<PersonalSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
