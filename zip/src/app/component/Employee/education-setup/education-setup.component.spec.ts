import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationSetupComponent } from './education-setup.component';

describe('EducationSetupComponent', () => {
  let component: EducationSetupComponent;
  let fixture: ComponentFixture<EducationSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
