import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ServerService} from '../../../service/server.service';
@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {

  constructor(private router : Router,private service :ServerService) { }
 
  ngOnInit() {
    if(!this.service.isEmployeeLogin())
    this.router.navigate(['/login']);
  }

}
