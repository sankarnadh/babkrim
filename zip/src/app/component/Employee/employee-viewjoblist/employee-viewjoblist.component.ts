import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { ServerService } from '../../../service/server.service';
import { FormBuilder, Validators, FormGroup, FormsModule, FormControl } from '@angular/forms';
import { ValidationServiceService } from '../../../service/validation-service.service';
import { ValidationMessageService } from '../../../service/validation-message.service'
import { NgProgress } from 'ngx-progressbar';
import '../../../../assets/js/js/chat.js';
declare var google: any
declare var customealert: any;
@Component({
  selector: 'app-employee-viewjoblist',
  templateUrl: './employee-viewjoblist.component.html',
  styleUrls: ['./employee-viewjoblist.component.css']
})
export class EmployeeViewjoblistComponent implements OnInit {

  jobForm: FormGroup;
  constructor(
    public ngProgress: NgProgress,
    private router: Router,
    public service: ServerService,
    public activatedRoute: ActivatedRoute,
    public validator: ValidationServiceService,
    private formBuilder: FormBuilder,
    public validationMsg: ValidationMessageService
  ) {
    this.jobForm = this.formBuilder.group
      ({
        'hourlyWages': ['', Validators.compose([Validators.required, ValidationServiceService.numberValidation])],
        'currency': ['', ValidationServiceService.numberValidation],
        'coverLetter': ['', Validators.required],
        'attachment': ['', ''],
        'wageMode': ['', Validators.required],
        'minhour': ['', Validators.compose([Validators.required, Validators.max(24)])],
        'fixedWages': ['', Validators.required]
      });
    router.events.subscribe((val) => {
      this.viewJobModal = ''
      this.viewJobModal = JSON.parse(this.service.getStorage('data'))
    });
  }
  index;
  get j() { return this.jobForm.controls }
  validateInputKey(event) {
    if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 106 && event.keyCode <= 222) {
      event.preventDefault();

    }

  }
  standardWage

  ngOnInit() {
    this.jobForm.get('wageMode').setValue('hourly')

    if (!this.service.isEmployeeLogin())
      this.router.navigate(['/login']);
    this.service.getCurrency();                            //Fetch Currency from API
    this.jobForm.get('currency').setValue(this.service.defaultCurrency)
    if (!this.service.getStorage('data'))
      this.router.navigate(['/home']);
    customealert.loaderShow('html')
    let data = this.service.getStorage('data')
    this.service.loader = false

   
    this.activatedRoute.params.subscribe((res) => {

      // this.getJobDetailsById(res['data'])

      this.viewJobModal = null

      this.viewJobModal = JSON.parse(data)
      this.standardWage = this.viewJobModal.jobPostType != 'urgent' ? this.viewJobModal.wageMode == 'hourly' ? this.viewJobModal.standardWage :
      this.viewJobModal.fixedStandardWage : this.viewJobModal.wageMode == 'hourly' ? this.viewJobModal.urgentStandardWage :
        this.viewJobModal.urgentFixedStandardWage;
      if (!this.viewJobModal)
        this.router.navigate(['/home']);
      if (this.viewJobModal.wageMode == 'fixed')
        this.jobForm.get('wageMode').setValue('fixed')

      this.jobForm.get('hourlyWages').setValue(this.viewJobModal.hourlyWages ? this.viewJobModal.hourlyWages : 1)
      this.jobForm.get('minhour').setValue(this.viewJobModal.minHour ? this.viewJobModal.minHour : 1)
      this.jobForm.get('fixedWages').setValue(this.viewJobModal.fixedWage ? this.viewJobModal.fixedWage : 1)

    });


    setTimeout(() => {
      customealert.loaderHide('html')
    }, 3000)

  }


  viewJobModal;
  currentIcon = {
    url: this.service.currentIcon,
    scaledSize: new google.maps.Size(60, 60)
  }
  urgentIcon = {
    url: this.service.urgentEmployerIcon,
    scaledSize: new google.maps.Size(60, 60)
  }
  /*****
   * Submiting Proposal in to API
   */
  submitError = false;
  currency = this.service.defaultCurrency;
  submit() {


    this.submitError = true
    if (this.j.wageMode.value == 'fixed') {
      this.jobForm.get('hourlyWages').setValue(0)
      this.jobForm.get('minhour').setValue(0)

      if (this.jobForm.get('fixedWages').value < this.standardWage) {
        return
      }
    } else {

      this.jobForm.get('fixedWages').setValue(0)
      if (this.jobForm.get('hourlyWages').value < this.standardWage) {
        return
      }
    }

    if (this.jobForm.invalid) {
      return;
    }

    this.submitJobProposal();
  }
  deleteFileicon(index) {

    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.proposalAttachments.splice(index, 1);

      this.counter--;
      if (this.counter < 0)
        this.counter = 0
    }


  }

  submitJobProposal() {
    customealert.loaderShow("html")
    // customealert.loaderHide('html')
    //this.router.navigate(['/employee_home']);
    let data = {
      'jobId': this.viewJobModal.jobId,
      'hourlyWages': this.jobForm.get('hourlyWages').value,
      'currency': this.service.defaultCurrency,
      'coverLetter': this.jobForm.get('coverLetter').value,
      'proposalAttachments': this.proposalAttachments,
      "wageMode": this.j.wageMode.value,
      "fixedWage": this.j.fixedWages.value,
      "minHour": this.j.minhour.value
    };

    let response: any;
    this.service.serverRequest(data, 'proposals.json').subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        customealert.hideModal('ModalCenter_form')
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.showSuccess(this.validationMsg.success)
          this.router.navigate(['/employee_home']);
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        customealert.hideModal('ModalCenter_form')

        this.service.serviceErrorResponce(err)
      }
    );
  }
  fileLoader: any = false;
  proposalAttachments = [];
  counter = 0;
  fileName = [];
  fileEvent(event: any) {

    var count = event.target.files.length;

    if (this.counter + count < 6) {
      for (let i = 0; i < count; i++) {
        const fileSelected: File = event.target.files[i];
        if (!this.service.checkAllowedFiles(event.target.files[i].name.split('.').pop())) {
          this.service.showError(this.validationMsg.invalidFileType)
          return false
        }
        if (fileSelected.size < this.service.maxFileSize) {


          this.service.loader = true
          let response;
          var reader = new FileReader();
          this.fileLoader = true
          this.ngProgress.start()

          this.service.uploadSingleFile(fileSelected, 'upload.json?fileType=docs')
            .subscribe((res: Response) => {
              response = res;
              this.fileLoader = false;
              this.service.loader = false
              this.ngProgress.done()
              if (response.status.responseCode == 20) {

                this.proposalAttachments.push(response.result.file1);
                this.fileName.push(fileSelected.name)
                this.counter++;
              }
              else {
                this.service.apiErrorHandler(res);

              }
            },
              (err) => {
                this.service.loader = false
                this.fileLoader = false;

                this.service.serviceErrorResponce(err)
              });
        } else
          this.service.showError(this.validationMsg.attachmentSizeLimit)
      }
    }


    else
      this.service.showError(this.validationMsg.attachmentLimit)
  }

}
