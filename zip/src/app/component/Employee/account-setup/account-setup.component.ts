import { Component, ViewChild, OnInit, ElementRef, Input, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { FormBuilder, Validators, FormsModule, FormControl } from '@angular/forms';
import { ValidationServiceService } from '../../../service/validation-service.service';
import { ServerService } from '../../../service/server.service';
import { ValidationMessageService } from '../../../service/validation-message.service'
import { DatePipe } from '@angular/common';
import { Router } from "@angular/router";
import { NgProgress } from 'ngx-progressbar';

import { DateTimeAdapter } from 'ng-pick-datetime';
import '../../../../assets/js/js/chat.js';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
declare var customealert: any
declare var $: any;
// import { DatepickerOptions } from 'ng2-datepicker';
// import * as frLocale from 'date-fns/locale/us';
@Component({
  selector: 'app-account-setup',
  templateUrl: './account-setup.component.html',
providers:[NgbTimepickerConfig],
  styleUrls: ['./account-setup.component.css'],

})
export class AccountSetupComponent implements OnInit {

  signupForm: any;
  cities = [];
  state = [];
  gender: any;
  country = [];
  nationality: any;
  step1btn: boolean;
  step1btnValid: any;
  //Select box loader
  initialLoader: any;
  stateLoader: any;
  cityLoader: any;
  //Previously entered details
  prstate: any;
  prgender: any;
  prcity: any;
  prcountry: any;
  prnationality: any;
  countryId: any;
  cityId: any = '';
  sateId: any = '';
  genderId: any = '';
  nationalityId: any = '';
  stateId: any = ''
  prfatherName: any = '';
  prdob: any;
  bottonClick = 0


  constructor
    (
    public ngProgress: NgProgress,
    public dateTimeAdapter: DateTimeAdapter<any>,
    private datePipe: DatePipe,
    private http: HttpClient, private el: ElementRef,
    private router: Router,
    private formBuilder: FormBuilder, public service: ServerService,
    public validationMsg: ValidationMessageService
    ) {

    this.dateTimeAdapter.setLocale('en-in');

    this.signupForm = this.formBuilder.group
      ({
        'father_name': '',
        'gender': ['', Validators.required],
        'mobile_otp': ['', Validators.required],
        'nationality': ['', Validators.required],
        'dob': ['', Validators.required],
        'country': ['', ValidationServiceService.notNullValidation],
        'state': ['', ValidationServiceService.notNullValidation],
        'city': ['', ValidationServiceService.notNullValidation],
        'aboutme': ['', Validators.compose([Validators.minLength(20), Validators.required])],
        'address1': ['', Validators.required],
        'address2': ['', Validators.required],
        'height': ['', Validators.compose([Validators.required])],
        'weight': ['', Validators.compose([Validators.required])]

      });
  }
  prf_pic: any;

  today;
  minDate: any;
  aboutmeModal = '';
  prAddress1: any = '';
  prAddress2: any;
  ngOnInit() {
    var today = new Date();
    var dd: any = '31'
    var mm: any = '12'
    var yyyy = today.getFullYear() - 13;
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }

    this.today = yyyy + '-' + mm + '-' + dd;

    var minDate = new Date();
    dd = '01'
    mm = '01'
    yyyy = minDate.getFullYear() - 70;
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }

    this.minDate = yyyy + '-' + mm + '-' + dd;
    this.service.checkLogin(); //To change the login header
    this.service.checkMobileVerification()
    this.CalculateAge();
    if (!this.service.isEmployeeLogin())                   //Checking authentication
      this.router.navigate(['/login']);

    // this.service.verificationCheck();                       //checking verification status (Email)      
    this.service.getLevel2();                                       //Comon details of level 2
    this.previousLevel2();                                  //Previously submited data of if any
    this.smsOtpMsg = this.service.mobile_otp_msg;             //Mobile OTP Message
    this.step1btn = false;                                    //Submit button Disabled for 1st time
    this.prf_pic = "assets/images/userpropic.png";           // Profile Image
    this.initialLoader = "loadinginput";                      //Initial loder 
    this.step1btnValid = "not-allowed"                        //Button mouse style
    if (this.service.getStorage('mobileVerify') == 'true')     //Otp sms show/hide
      this.otp_valid = true;

      this.getSateFromApi(352)
  }

  otp_valid: any = false;                                   //Mobile otp Valid  (true / false )



  /****
   * Resending Otp 
   */
  smsOtpMsg: any;
  sendSms() {
    this.smsOtpMsg = null
    customealert.loaderShow("html")
    let response: any;
    this.service.serverRequest('', '/otp/sms/send.json?registerType=' + this.service.getStorage('registerType')).subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.smsOtpMsg = res['result'].message

        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }
  /******
   * Verify Sms Otp
   */
  verifyOtp() {
    this.smsOtpMsg = ''
    let otp = this.signupForm.get('mobile_otp').value;
    if (otp) {
      customealert.loaderShow("html")
      let response: any;

      this.service.serverRequest('', '/otp/sms/verify.json?registerType=' + this.service.getStorage('registerType') + '&OTP=' + otp).subscribe(
        (res: Response) => {
          customealert.loaderHide("html")
          response = res['status'];
          if (response.responseCode == 20) {

            if (res['result'].message) {
              this.smsOtpMsg = this.validationMsg.otpVerified;
              this.service.setStorage('mobileVerify', 'true');
              this.otp_valid = true;
            }

            else
              this.smsOtpMsg = this.validationMsg.invalidOtp;

          }
          else {
            this.service.apiErrorHandler(res);


          }

        },
        err => {
          this.service.serviceErrorResponce(err)
          customealert.loaderHide("html")
        }
      );
    }
    else
      this.smsOtpMsg = this.validationMsg.invalidOtp
  }





  //Fetching state from server
  getCityFromApi(stateId) {
    this.cityLoader = "loadinginput"
    this.cities = []

    this.service.serverGetRequest('', 'static/cities.json?state=' + stateId).subscribe(
      (res) => {

        this.cityLoader = ''
        if (res['status'].responseCode == 20)
          this.cities = res['result'].cities;
        this.cities.push({ id: 0, city: 'Other' })


      },
      err => {
        this.service.serviceErrorResponce(err)
        this.cityLoader = ''
      }
    );
  }


  //Fetching State from api
  getSateFromApi(countryId) {
    countryId=352
    this.stateLoader = "loadinginput";
    this.state = [];
    this.cities = [];

    this.service.serverGetRequest('', 'static/states.json?country=' + countryId).subscribe(
      (res) => {

        this.stateLoader = ''
        if (res['status'].responseCode == 20)
          this.state = res['result'].states;

      },
      err => {
        this.service.serviceErrorResponce(err)
        this.stateLoader = ''
      }
    );
  }
  //Personal Information submit
  height: any;
  weight: any;
  heightlengthIssue = 0
  weightlengthIssue = 0
  genderList = [
    
    {id: 1, gender: "Male"},{id: 2, gender: "Female"},{id: 4, gender: "Other"}
  ]
  personalInformationSubmit(val) {
   
    if (!this.profileImage) {
      this.bottonClick = 1
      return false
    }
    else {
      this.bottonClick = 0

    }
    // if (!this.prfatherName) {
    //   this.bottonClick = 1
    //   $('#father_name').css('border-color', 'red').focus()
    //   return false
    // }
    // else {
    //   this.bottonClick = 0
    //   $('#father_name').css('border-color', '#ced4da')
    // }
    if (!this.signupForm.get('aboutme').valid) {
      this.bottonClick = 1
      $('#aboutme').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#aboutme').css('border-color', '#ced4da')
    }
    if (!this.signupForm.get('address1').valid) {
      this.bottonClick = 1
      $('#address1').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#address2').css('border-color', '#ced4da')
    }
    // if (!this.signupForm.get('address2').valid) {
    //   this.bottonClick = 1
    //   $('#address2').css('border-color', 'red').focus()
    //   return false
    // }
    // else {
    //   this.bottonClick = 0
    //   $('#address2').css('border-color', '#ced4da')
    // }

    // if (!this.height) {
    //   this.bottonClick = 1
    //   $('#height').css('border-color', 'red').focus()
    //   return false
    // }
    // else {
    //   this.bottonClick = 0
    //   $('#height').css('border-color', '#ced4da')
    // }
    // if (this.signupForm.get('height').value < 50 || this.signupForm.get('height').value > 250) {
    //   this.bottonClick = 1
    
    //   $('#height').css('border-color', 'red').focus()
    //   return false
    // }
    // else {
    //   this.bottonClick = 0
    //   $('#height').css('border-color', '#ced4da')
    // }
    /*if (this.height) {
      let ex: any = String(this.signupForm.get('height').value)

      // return false
      if (ex.split('.').pop())

        if (ex.split('.').pop().length > 2) {
          this.bottonClick = 0
          this.heightlengthIssue = 1
          return false
        }
        else
          this.heightlengthIssue = 0

      // return false 
    }*/
   
    /*if (!this.weight) {
      this.bottonClick = 1
      $('#weight').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#weight').css('border-color', '#ced4da')
    }
    if (this.signupForm.get('weight').value < 20 || this.signupForm.get('weight').value > 1000) {
      this.bottonClick = 1

      $('#weight').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#weight').css('border-color', '#ced4da')
    }
    if (this.weight) {
      let ex: any = String(this.signupForm.get('weight').value)
      if (ex.split('.').pop())
        if (ex.split('.').pop().length > 2) {
          this.bottonClick = 0

          this.weightlengthIssue = 1
          return false
        }
        else
          this.weightlengthIssue = 0
    }
  */
    if (!this.signupForm.get('gender').value) {
      this.bottonClick = 1
      $('#gender').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#gender').css('border-color', '#ced4da')
    }
    // if (!this.signupForm.get('nationality').value) {
    //   this.bottonClick = 1
    //   $('#nationality').css('border-color', 'red').focus()
    //   return false
    // }
    // else {
    //   this.bottonClick = 0
    //   $('#nationality').css('border-color', '#ced4da')
    // }
    if (!this.signupForm.get('dob').valid) {
      this.bottonClick = 1
      $('#dob').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#dob').css('border-color', '#ced4da')
    }
    
    // if (!this.signupForm.get('country').value) {
    //   this.bottonClick = 1
    //   $('#country').css('border-color', 'red').focus()
    //   return false
    // }
    // else {
    //   this.bottonClick = 0
    //   $('#country').css('border-color', '#ced4da')
    // }
    // if (!this.signupForm.get('country').value) {

    //   this.bottonClick = 1
    //   $('#country').css('border-color', 'red').focus()
    //   return false
    // }
    // else {
    //   this.bottonClick = 0
    //   $('#country').css('border-color', '#ced4da')
    // }
    if (!this.signupForm.get('state').value) {
      this.bottonClick = 1
      $('#state').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#state').css('border-color', '#ced4da')
    }

    if (!this.cityId && this.cityId!=0) {
      this.bottonClick = 1
      $('#city').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#city').css('border-color', '#ced4da')
    }
    if (!this.otp_valid) {
      this.bottonClick = 1
      $('#otp').css('border-color', 'red').focus()
      return false
    }
    else {
      this.bottonClick = 0
      $('#otp').css('border-color', '#ced4da')
    }

    if (this.prfatherName == null || !this.prfatherName)
      this.prfatherName = ''
    if (this.cityId == null)
      this.cityId = ''
    let data = {
      'fatherName':'BABKRIM-EMPLOYEE-DEFAULT' ,//this.prfatherName,
      'city': this.cityId,
      'country': 352,//this.countryId,
      'dob': this.datePipe.transform(this.signupForm.get('dob').value, 'yyyy-MM-dd'),
      'gender': this.genderId,
      'nationality': 86,//this.nationalityId,
      'state': this.stateId,
      'profileImage': this.profileImage,
      'profileDescription': this.aboutmeModal,
      'addressLine1': this.prAddress1,
      'addressLine2': this.prAddress2 ? this.prAddress2 : '',
      'height': this.signupForm.get('height').value?this.signupForm.get('height').value:50,
      'weight': this.signupForm.get('weight').value?this.signupForm.get('weight').value:20
    };

    customealert.loaderShow("html")
    let response: any;
    this.service.serverRequest(data, 'register.json?registerType=' + this.service.getStorage('registerType') + '&registerLevel=2&deviceMode=web').subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.setStorage('prfPicId', this.service.url + "/download/profile.json?fileID=" + this.profileImage + "&token=" + this.service.getStorage('token'))
          if (val == '1') {
            this.service.showSuccess(this.validationMsg.success)
            // this.router.navigate(['/employee_home']);
          }
          else {
            if (this.service.getStorage('registerLevel') != '0')
              this.service.setStorage('registerLevel', '3')
                ; this.router.navigate(['/employee_educationsetup']);
          }
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)
        customealert.loaderHide("html")
      }
    );
  }
  /*******
   * Fetch Previously submited data 
   */
  previousLevel2() {
    let response;
   customealert.loaderShow('html')
    this.service.serverGetRequest('', 'profile/profileDetails.json?registerType=' + this.service.getStorage('registerType') + '&registerLevel=2').subscribe(
      (res: Response) => {
        response = res;
        customealert.loaderHide("html")

        if (response.status.responseCode == 20) {

          if (response.result) {
            this.prfatherName = response.result.fatherName;
            this.prdob = response.result.dob;
            this.CalculateAge()
            this.prgender = response.result.gender.id;
            this.prnationality = response.result.nationality.id
            this.prcountry = response.result.country.id;//Selected Country
            this.getSateFromApi(response.result.country.id) //Fetching last entered state 
            this.prstate = response.result.state.id;//Last entered State
            this.prcity = response.result.city.id;
            this.getCityFromApi(response.result.state.id)
            this.genderId = response.result.gender.id;
            this.nationalityId = response.result.nationality.id;
            this.countryId = response.result.country.id;
            this.stateId = response.result.state.id;
            this.cityId = response.result.city.id;
            this.prAddress1 = response.result.addressLine1;
            this.prAddress2 = response.result.addressLine2;
            this.height = (response.result.height);
            this.weight = (response.result.weight);
            this.aboutmeModal = response.result.profileDesc
            if (response.result.profileImage) {
              this.prf_pic = this.service.url + "/download/profile.json?fileID=" + response.result.profileImage + "&token=" + this.service.getStorage('token')
              this.service.prfImg = this.prf_pic
              this.profileImage = response.result.profileImage
            }
          }
          else {
            this.prfatherName = null;
            this.prdob = null;
            this.prgender = null;
            this.prnationality = null;
            this.prcountry = null;
            this.prstate = null;
            this.prcity = null;
            this.genderId = null;
            this.nationalityId = null;
            this.countryId = null;
            this.stateId = null;
            this.cityId = null;
          }
        }
        else {
          this.service.apiErrorHandler(res);

        }
      },
      err => {
        this.service.serviceErrorResponce(err)
        customealert.loaderHide("html")


      }
    );
  }
  /**
   * Clearing country Id when user unselect country
   */
  onCountryItemDeselect() {

    //Unsetting State and city

    this.countryId = null;
    this.prcity = [];
    this.prstate = [];
    this.state = [];
    this.cities = [];
    this.cityId = null;
    this.stateId = null
  }
  /**
   * When country is selected
   */
  onCountryItemSelect(item: any) {
    this.prcity = null;
    this.prstate = null
    if (item) {
      this.countryId = item.id;
      this.getSateFromApi(item.id)
    }
  }
  /***
   * Gender unselect
   */
  onGenderItemDeselect() {

    this.genderId = null;
  }
  //On gender selected
  onGenderItemSelect(item: any) {
    if (item)
      this.genderId = item.id;

  }
  //On nationality
  onNationalityItemDeselect() {
    this.nationalityId = null;
  }
  //Nationality selected
  onNationalityItemSelect(item: any) {
    if (item)
      this.nationalityId = item.id;
  }
  //State Selected
  onStateItemSelect(item: any) {

    this.prcity = null;
    if (item) {
      this.stateId = item.id;
      this.getCityFromApi(item.id)
    }
  }
  //State deselect
  onStateItemDeselect() {
    this.sateId = null;
    this.cityId = null;
    this.cities = [];
    this.prcity = [];
  }
  //City select 
  onCityItemSelect(item: any) {
    if (item)
      this.cityId = item.id;
  }
  //City Deselect
  onCityItemDeselect() {
    this.cityId = null;
    this.prcity = [];
  }
  /*****
   * Profile pic upload
   * 
   */
  fileLoader: any = false;
  profileImage;
  imageName
  fileEvent(event: any) {

    let response;
    var reader = new FileReader();


    let fileType = event.target.files[0].name.split('.').pop()
    if (fileType != 'jpeg' && fileType != 'jpg' 
    && fileType != 'JPEG' && fileType != 'png' 
    && fileType != 'JPG' && fileType != 'PNG') {
      this.service.showError(this.validationMsg.invalidFileType)
      return false
    }
    const fileSelected: File = event.target.files[0];
    if (fileSelected && fileSelected.size < this.service.maxFileSize) {
      this.fileLoader = true;
      this.ngProgress.start()

      this.service.uploadSingleFile(fileSelected, 'upload.json?fileType=profile')
        .subscribe((res: Response) => {
          response = res;
          this.fileLoader = false;
          this.ngProgress.done()

          if (response.status.responseCode == 20) {

            this.profileImage = response.result.file1;
            this.profileImage = response.result.file1;

            this.service.prfImg = this.service.url + "/download/profile.json?fileID=" + this.profileImage + "&token=" + this.service.getStorage('token')
          }
          else {
            this.service.apiErrorHandler(res);

          }
        },
          (err) => {
            this.fileLoader = false;
            this.service.serviceErrorResponce(err)

          });
    } else this.service.showError(this.validationMsg.attachmentSizeLimit)
  }
  age: any;
  public CalculateAge(): void {
    if (this.prdob) {
      console.log(this.prdob)
      var d = new Date(this.prdob)
      var timeDiff = Math.abs(Date.now() - d.getTime());
      //Used Math.floor instead of Math.ceil
      //so 26 years and 140 days would be considered as 26, not 27.
      this.age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
      console.log(timeDiff)
      if (this.age < 1 || !this.age)
        this.age = 0
    }
  }

}
