import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { FormBuilder, Validators, FormGroup, FormsModule, FormControl } from '@angular/forms';
import { ValidationServiceService } from '../../../service/validation-service.service';
import { ServerService } from '../../../service/server.service';
import { ValidationMessageService } from '../../../service/validation-message.service'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from "@angular/router";
import { NgProgress } from 'ngx-progressbar';
import '../../../../assets/js/js/chat.js';
declare var customealert: any;
declare var google:any;
import { EditattachmentComponent } from '../../../layout/editattachment/editattachment.component'
@Component({
  selector: 'app-edit-proposal',
  templateUrl: './edit-proposal.component.html',
  styleUrls: ['./edit-proposal.component.css']
})
export class EditProposalComponent implements OnInit {
  @ViewChild(EditattachmentComponent) child;

  jobForm: FormGroup;
  offer: any = [];
  currentIcon = {
    url: this.service.currentIcon,
    scaledSize: new google.maps.Size(60, 60)
  }
  urgentIcon = {
    url: this.service.urgentEmployerIcon,
    scaledSize: new google.maps.Size(60, 60)
  }
  currency = this.service.defaultCurrency
  constructor(
    public ngProgress: NgProgress,
    private http: HttpClient,
    public router: Router,
    public config: NgbRatingConfig,
    public validator: ValidationServiceService,
    private formBuilder: FormBuilder, public service: ServerService,
    public validationMsg: ValidationMessageService,
    public activatedRoute: ActivatedRoute
  ) {
    this.jobForm = this.formBuilder.group
      ({
        'hourlyWages': ['', Validators.compose([Validators.required, ValidationServiceService.numberValidation])],
        'currency': ['', Validators.required],
        'coverLetter': ['', Validators.required],
        'attachment': ['', ''],
        'wageMode': ['', Validators.required],
        'minhour': ['', Validators.compose([Validators.required, Validators.min(1)])],
        'fixedWages': ['', Validators.required]
      });
    config.max = 5;
    config.readonly = true
  }
  title: any = 'withdrawproposal';
  text: any = 'cantbeundone'

  login = false;
  get j() { return this.jobForm.controls }
  ngOnInit() {

    this.service.loader = false
    this.jobForm.get('wageMode').setValue('hourly')
    this.jobForm.get('currency').setValue(this.service.defaultCurrency)
    this.activatedRoute.params.subscribe((res) => {
      // proposalId = res['proposalId'];
      this.getProposalById(res['proposalId'])

    });                          //Fetch Currency from API
  }
  loader = false
  standardWage
  getProposalById(proposalId) {
    this.loader = true
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'proposalDetails/' + proposalId + ".json?registerType=" + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      this.loader = false

      if (res['status'].responseCode == 20) {
        this.offer = res['result'].proposal;
        this.standardWage = this.offer.jobDetails.jobPostType != 'urgent' ? this.offer.jobDetails.wageMode == 'hourly' ? this.offer.jobDetails.standardWage :
          this.offer.jobDetails.fixedStandardWage : this.offer.jobDetails.wageMode == 'hourly' ? this.offer.jobDetails.urgentStandardWage :
            this.offer.jobDetails.urgentFixedStandardWage;
        this.jobForm.get('hourlyWages').setValue(this.offer.hourlyWages);
        this.offer.attachments.forEach(element => {


          this.proposalAttachments.push(element.fileId)
          this.fileName.push(element.seedName)
        });
        this.counter = this.proposalAttachments.length
        this.jobForm.get('coverLetter').setValue(this.offer.coverLetter);
        this.jobForm.get('wageMode').setValue(this.offer.wageMode ? this.offer.wageMode : this.offer.jobDetails.wageMode)
        this.jobForm.get('minhour').setValue(this.offer.minHour ? this.offer.minHour : this.offer.jobDetails.minHour)
        this.j.fixedWages.setValue(this.offer.fixedWage ? this.offer.fixedWage : this.offer.jobDetails.fixedWage)
        customealert.showModal('Modalinviteinterview')
      }
      else
        this.service.apiErrorHandler(res)
    }, er => {
      this.service.serviceErrorResponce(er)
    })
  }

  upparttimefee: number = 0;
  employeeReceive: number = 0;




  fileLoader: any = false;
  proposalAttachments = [];
  counter = 0;
  fileName = [];
  fileEvent(event: any) {
    var count = event.target.files.length;

    if (this.counter + count < 6) {
      for (let i = 0; i < count; i++) {
        const fileSelected: File = event.target.files[i];
        if (!this.service.checkAllowedFiles(event.target.files[i].name.split('.').pop())) {
          this.service.showError(this.validationMsg.invalidFileType)
          return false
        }
        if (fileSelected.size < this.service.maxFileSize) {
          if (fileSelected) {
            this.service.loader = true
            this.fileLoader = true;
          }
          let response;
          var reader = new FileReader();

          this.ngProgress.start()

          this.service.uploadSingleFile(fileSelected, 'upload.json?fileType=docs')
            .subscribe((res: Response) => {
              this.ngProgress.done()

              response = res;
              this.fileLoader = false;
              this.service.loader = false
              if (response.status.responseCode == 20) {

                this.proposalAttachments.push(response.result.file1);
                this.fileName.push(fileSelected.name)
                this.counter++;
              }
              else {
                this.service.apiErrorHandler(res);

              }
            },
              (err) => {
                this.service.loader = false
                this.fileLoader = false;
                this.service.serviceErrorResponce(err)
              });
        } else
          this.service.showError(this.validationMsg.attachmentSizeLimit)
      }
    }
    else
      this.service.showError(this.validationMsg.attachmentLimit)
  }
  /*****
   * Submiting Proposal in to API
   */
  submitError = false;
  submit() {

    this.submitError = true
    if (this.j.wageMode.value == 'fixed') {
      this.jobForm.get('hourlyWages').setValue(1)
      this.jobForm.get('minhour').setValue(1)
      if (this.jobForm.get('fixedWages').value < this.standardWage) {
        return
      }
    } else {
      if (this.jobForm.get('hourlyWages').value < this.standardWage) {
        return
      }
      this.jobForm.get('fixedWages').setValue(1)

    }

    if (this.jobForm.invalid) {
      return;
    }

    this.editProposal();
    this.submitError = false
  }
  deleteFileicon(index) {
    if (index !== -1) {
      this.fileName.splice(index, 1);
      this.proposalAttachments.splice(index, 1);

      this.counter--;
      if (this.counter < 0)
        this.counter = 0
    }

  }
  withDrawProposal() {
    let response: any;
    this.service.serverPutRequest('', 'proposals/withdraw/' + this.offer.proposalId + '.json').subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.showSuccess(this.validationMsg.success)

          this.router.navigate(['/proposals']);
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }
  editProposal() {
    customealert.loaderShow("html")
    let data = {
      'jobId': this.offer.jobDetails.jobId,
      'hourlyWages': this.jobForm.get('hourlyWages').value,
      'currency': this.currency,
      'coverLetter': this.jobForm.get('coverLetter').value,
      'proposalAttachments': this.proposalAttachments,
      "wageMode": this.j.wageMode.value,
      "fixedWage": this.j.fixedWages.value,
      "minHour": this.j.minhour.value
    };

    let response: any;
    this.service.serverPutRequest(data, 'proposals/' + this.offer.proposalId + '.json').subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.showSuccess(this.validationMsg.proposalEdited)
          customealert.hideModal('Modalinviteinterview')
          this.router.navigate(['/proposals']);
        }
        else {
          this.service.apiErrorHandler(res);

        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }

}
