import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeJoblistComponent } from './employee-joblist.component';

describe('EmployeeJoblistComponent', () => {
  let component: EmployeeJoblistComponent;
  let fixture: ComponentFixture<EmployeeJoblistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeJoblistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeJoblistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
