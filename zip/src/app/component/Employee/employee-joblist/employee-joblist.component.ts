import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { ServerService } from '../../../service/server.service';
import { ValidationMessageService } from '../../../service/validation-message.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import '../../../../assets/js/js/chat.js';
declare var customealert: any;
declare var google:any;
@Component({
  selector: 'app-employee-joblist',
  templateUrl: './employee-joblist.component.html',
  styleUrls: ['./employee-joblist.component.css']
})
export class EmployeeJoblistComponent implements OnInit ,OnDestroy{
  constructor(
    private router: Router,
    public service: ServerService,
    public config: NgbRatingConfig,
    public validationMsg: ValidationMessageService,
    private spinnerService: Ng4LoadingSpinnerService
  ) { 
    config.max=5;
    config.readonly=true
  }
  p: any=1; //PageCounter

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.service.search=null
    this.service.subCategoryFilter=[]
    this.service.jobDetails=[]
    this.service.pageCounter = 0
    this.service.jobListPageNo = 0
  }
  ngOnInit() {
    //bug id 1432
   if(this.service.isEmployeeLogin())
   var temp :any;
   if(this.service.getStorage("secSubCategoryIds")){
    temp=this.service.getStorage("secSubCategoryIds")+','+this.service.getStorage("defaultSubId");
   }else {
    temp=this.service.getStorage("defaultSubId");
   }
   const ar=temp.split(',');
   temp='';
   ar.forEach(element => {
    
    this.service.pushSubCategoryFilter(this.service.getStorage("defaultSubname"), element, '',true)
   });
   console.info(ar.toString())

    // this.service.pushSubCategoryFilter(this.service.getStorage("defaultSubname"), this.service.getStorage("defaultSubId"), '')
    this.service.jobDetails=[]
    this.service.pageCounter = 0
    this.service.jobListPageNo = 0
    // this.service.fetchAllJobDetailsFromApi(0);                         //Getting no filter job details
  }
  onKeyDown(event)
  {
    if (event.key === "Enter") 
    this.service.fetchAllJobDetailsFromApi(0)
  }

  pageCount=0;
  direction = '';

  /*****
   * Getting all job list (No filter)
   */
  jobDetails;
  pageCounter : any;

  /******
   * Showing modal of one job details
   */
  test=new Date('06.01.2009 4:30:00')
  viewJobModal;
  fromTime;
  toTime;
  viewDetails(data) {
    this.viewJobModal = data
    this.lat = parseFloat(this.viewJobModal.latitude)
    this.lng = parseFloat(this.viewJobModal.longitude)
    this.markers=[]
    this.markers.push({
      lat: this.lat,
      lng: this.lng,

      draggable: false
    })

  }
  scrolltop(){
    window.scrollTo(0,0)
  }
  /*******
   * Map 
   */
  currentIcon = {
    url: this.service.currentIcon,
   scaledSize: new google.maps.Size(60, 60)
  }
  urgentIcon={
    url: this.service.urgentEmployerIcon,
    scaledSize: new google.maps.Size(60, 60)
  }
  zoom: number = 14;
  lat: any;
  lng: any;
  pageno : number=0
  markers: marker[] = []
  /*****
   * viewJobList another page
   */
  viewJobList(val) {

    this.service.setStorage('data', JSON.stringify(val))
    this.router.navigate(['/employee_viewjoblist' + '/' + val.jobId])
  }
  
  setTempData(val)
  {
    
    this.service.setStorage('tempData', JSON.stringify(val));
    this.router.navigate(['/view-job-details/'+val.jobId])
  }
   /********************************************************************** */
   throttle = 3;
   scrollDistance = 1;
   scrollUpDistance = 2;
   onScrollDown() {
     if (this.service.jobListPageNo != 0 && !this.service.loader && this.service.jobDetails.length <= this.service.pageCounter) {
       this.service.fetchAllJobDetailsFromApi(this.service.jobListPageNo);
       console.log('scrolled down!!', this.service.jobListPageNo + " ");
     }
   }
   /*********************************************************************** */
}
// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}