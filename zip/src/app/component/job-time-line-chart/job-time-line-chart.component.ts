import { Component, OnInit } from '@angular/core';
declare var jquery:any;
declare var $ :any;
declare var CanvasJS;

@Component({
  selector: 'app-job-time-line-chart',
  templateUrl: './job-time-line-chart.component.html',
  styleUrls: ['./job-time-line-chart.component.css']


})
export class JobTimeLineChartComponent implements OnInit {
  
  constructor() { }

  ngOnInit() {
    $(document).ready(function(){
      
     console.log("in")

        var chart = new CanvasJS.Chart("chartContainer", {
          animationEnabled: true,
          // title:{
          //   text: "Evening Sales in a Restaurant"
          // },
          axisX: {
            valueFormatString: "DDD"
          },
          axisY: {
            // prefix: "Hr"
            suffix:" Hr"
          },
          toolTip: {
            shared: true
          },
          legend:{
            cursor: "pointer",
            itemclick: toggleDataSeries
          },
          data: [{
            type: "stackedBar",
            name: "Work time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y:1},
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1},
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
        
         
          {
            type: "stackedBar",
            name: "Free time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1}
            ]
          },
          {
            type: "stackedBar",
            name: "Work time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1},
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Free time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Work time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1},
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Free time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1},
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Work time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Free time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1},
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Work time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Free time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1},
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Work time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Work time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1 },
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          },
          {
            type: "stackedBar",
            name: "Free time",
            showInLegend: "true",
            xValueFormatString: "DD, MMM",
            yValueFormatString: "$#,##0",
            dataPoints: [
              { x: new Date(2017, 0, 30), y: 1 },
              { x: new Date(2017, 0, 31), y: 1 },
              { x: new Date(2017, 1, 1), y: 1 },
              { x: new Date(2017, 1, 2), y: 1},
              { x: new Date(2017, 1, 3), y: 1 },
              { x: new Date(2017, 1, 4), y: 1 },
              { x: new Date(2017, 1, 5), y: 1 }
            ]
          }]
          
        });
        chart.render();
        
        function toggleDataSeries(e) {
          if(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          }
          else {
            e.dataSeries.visible = true;
          }
          chart.render();
        }
        
        
    })
  }

}
