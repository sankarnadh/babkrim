import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTimeLineChartComponent } from './job-time-line-chart.component';

describe('JobTimeLineChartComponent', () => {
  let component: JobTimeLineChartComponent;
  let fixture: ComponentFixture<JobTimeLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTimeLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTimeLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
