import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotsureComponent } from './notsure.component';

describe('NotsureComponent', () => {
  let component: NotsureComponent;
  let fixture: ComponentFixture<NotsureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotsureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotsureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
