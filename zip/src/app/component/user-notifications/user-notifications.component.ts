import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { ValidationMessageService } from '../../service/validation-message.service';

import { Router } from '@angular/router'
import '../../../assets/js/js/chat.js'
declare var customealert: any;
@Component({
  selector: 'app-user-notifications',
  templateUrl: './user-notifications.component.html',
  styleUrls: ['./user-notifications.component.css']
})
export class UserNotificationsComponent implements OnInit {

  constructor(public service: ServerService, public validationMsg: ValidationMessageService, private router: Router) { }
  loader=false
  ngOnInit() {
    this.loader = true
    this.loadNotification()
    // setInterval(() => {
    //   if (this.service.isEmployeeLogin() || this.service.isEmployerLogin())
    //     this.loadNotification()
    // }, 10000)
  }

  notification: any = [];
  notificationLength: number;
  pageNo = 1;
  viewMore = 0
  showloadmore = 0
  /********************************************************************** */
  throttle = 3;
  scrollDistance = 1;
  scrollUpDistance = 2;
  onScrollDown() {
    if(!this.loader &&this.notification.length <= this.notificationLength)
    this.loadNotification()
  }
  /*********************************************************************** */
  loadNotification() {
    this.loader =true
   if(this.pageNo==1)
   {
    this.notification=[];

   }
    this.service.serverGetRequest('', 'notifications.json?registerType=' + this.service.getStorage('registerType') + '&pageNo=' + this.pageNo).subscribe(res => {
      if (res['status'].responseCode == 20) {
        this.loader = false
        this.pageNo=this.pageNo+1
        res['result'].notificationDetails.forEach(element => {
          this.notification.push(element)
        });
        if (res['result'].notificationDetails) {
          if (res['result'].notificationDetails.length == 0)
            this.showloadmore = 1
          else
            this.showloadmore = 0
        }
        else
          this.showloadmore = 1

        if (this.notification)
          this.notificationLength = res['result'].totalCount

      }
      else
        this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
  checkType(val) {
    if (val.status == 0) { val.status = 1; this.service.notificationLength = this.service.notificationLength == 0 ? 0 : this.service.notificationLength - 1; this.readNotification(val.notificationId) }
    if (val.notificationType == "invitation" && this.service.isEmployeeLogin())
      this.getInvitationDetails(val)
    if (val.notificationType == "invitation" && this.service.isEmployerLogin())
    this.getInvitationDetails(val)
    if (val.notificationType == "offers" && this.service.isEmployeeLogin())
      this.getOfferDetails(val)
    if (val.notificationType == "offers" && this.service.isEmployerLogin())
      this.router.navigate(['/viewContract/' + val.notificationTypeId])
    if (val.notificationType == "proposal" && this.service.isEmployerLogin()) {
      // this.service.showError('Failed to load notification')
      this.getProposalDetails(val) //API is missing data so 
    }
    if (val.notificationType == "contract" )
    {  this.service.setStorage('contractId',val.notificationTypeId);
     this.router.navigate(['/jobtimeline/' + val.notificationTypeId])}

    
  }
  getInvitationDetails(val) {
    customealert.loaderShow('html')

    let invitationId = val.notificationTypeId

    this.service.serverGetRequest('', 'invitationDetails/' + invitationId + '.json?registerType=' + this.service.getStorage('registerType')).
      subscribe(res => {
        if (res['status'].responseCode == 20) {
          this.loader = false
          if (this.service.isEmployeeLogin()) {
            this.service.setStorage('data', JSON.stringify(res['result'].invitation));
            console.log(res['result'].invitation)
            this.router.navigate(['/inviteinterview_employee'])
          }
          if (this.service.isEmployerLogin()) {
            this.service.setStorage('data', JSON.stringify(res['result'].invitation['jobDetails']));
            console.log(res['result'].invitation['jobDetails'])
            this.service.setStorage('proposal', JSON.stringify(res['result'].invitation));
            this.router.navigate(['/viewInvitationDetails'])
          }
          customealert.loaderHide('html')
        }
        else
          this.service.apiErrorHandler(res)
      },
        er => {
          this.service.serviceErrorResponce(er)
        })
  }
  getOfferDetails(val) {
    customealert.loaderShow('html')

    let proposalId = val.notificationTypeId

    this.service.serverGetRequest('', 'contractDetails/' + proposalId + '.json?registerType=' + this.service.getStorage('registerType')).
      subscribe(res => {
        if (res['status'].responseCode == 20) {
          this.loader = false

          this.service.setStorage('data', JSON.stringify(res['result'].contract));

          this.router.navigate(['/myoffers-employee'])
          customealert.loaderHide('html')
        }
        else
          this.service.apiErrorHandler(res)
      },
        err => {
          this.service.serviceErrorResponce(err)
        })
  }
  getProposalDetails(val) {

    let proposalId

    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'proposalDetails/' + val.notificationTypeId + '.json?registerType=' + this.service.getStorage('registerType')).
      subscribe(res => {
        customealert.loaderHide('html')

        if (res['status'].responseCode == 20) {
          this.loader = false

          this.service.setStorage('proposal', JSON.stringify(res['result'].proposal))
          this.service.setStorage('data', JSON.stringify(res['result'].proposal['jobDetails']))
          this.router.navigate(['/view_employee'])
        }
        else
          this.service.apiErrorHandler(res)
      },
        er => {
          customealert.loaderHide('html')

          this.service.serviceErrorResponce(er)
        })
  }
  readNotification(notificationId) {
    this.service.serverPutRequest('', 'notifications/' + notificationId + '.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      if (res['status'].responseCode == 20) {

      }
      else this.service.apiErrorHandler(res)
    },
      err => {
        this.service.serviceErrorResponce(err)
      })
  }

}
