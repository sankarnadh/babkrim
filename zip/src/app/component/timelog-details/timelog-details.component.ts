import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, addMinutes, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import { ServerService } from '../../service/server.service'
import { Router } from '@angular/router'
import { DatePipe } from '@angular/common';
import '../../../assets/js/js/chat.js'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import localeAr from '@angular/common/locales/ar';
import localeIn from '@angular/common/locales/en-IN'
declare var customealert: any;
declare var jquery: any;
declare var $: any;
declare var window: any; //DON'T FORGET ABOUT THIS ONE!!!
import { HostListener } from "@angular/core";
import { registerLocaleData } from 'src/app/service/locale_data';

const colors: any = {
    start: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    },
    gereen: {
        primary: '#97eca2',
        secondary: '#065404'
    }
};

@Component({
    selector: 'app-timelog-details',
    templateUrl: './timelog-details.component.html',
    styleUrls: ['./timelog-details.component.css']
})
export class TimelogDetailsComponent implements OnInit {
    appointmentsData: any = [];
    currentDate: Date = new Date();
    daysViewStartDate: Date = new Date();
    weeksViewStartDate: Date = new Date();
    scrHeight: any;
    scrWidth: any;

    @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
        this.scrHeight = window.innerHeight;
        this.scrWidth = window.innerWidth;
        //   window.innerWidth=this.scrWidth-115
        //   console.log(window);
    }
    constructor(public service: ServerService,
        public router: Router,
        private datePipe: DatePipe,
        private modal: NgbModal) {
        this.getScreenSize();
    }
    ngOnInit() {
        if (!this.contractId)
            this.router.navigate(['/home'])

        // this.getTimeSheet()
    }
    timeSheet: any;
    contractId: any = this.service.getStorage('contractId') //'55b1084a40d2cb6a0bcbbe00641bd6e6140cc17f'
    onAppointmentClick(e) {
        console.log("in")
        // e.cancel = true;
    }

    onAppointmentDblClick(e) {
        e.cancel = true;
    }
    /*************** */
    test(data) {
        var that = this,
            form = data.form,
            movieInfo = {},
            startDate = data.appointmentData.startDate,
            val = data.appointmentData;
        console.log(data)
        if (val.nextAction)
            form.option("items", [
                {
                    label: {
                        text: this.changeAction(val.action) + " title"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.title,
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.action) + " comment"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.comment,
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.action) + " on"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: this.datePipe.transform(val.startDate, 'dd/MM/yyy hh:mm a', 'GMT',this.service.getStorage('language')) + ' to ' + this.datePipe.transform(val.endDate, 'dd/MM/yyy hh:mm a', 'GMT',this.service.getStorage('language')),
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.action) + " By"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.createdUser,
                        readOnly: true
                    }
                },


                {
                    label: {
                        text: this.changeAction(val.nextAction) + " title"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.nexttitle,
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.nextAction) + " comment"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.nextcomment,
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.nextAction) + " on"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: this.datePipe.transform(val.nextstartDate, 'dd/MM/yyy hh:mm a', 'GMT',this.service.getStorage('language')),
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.nextAction) + " By"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.nextcreatedUser,
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: "Duration"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.timeDifference,
                        readOnly: true
                    }
                },
            ]
            );
        else
            form.option("items", [
                {
                    label: {
                        text: this.changeAction(val.action) + " title"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.title,
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.action) + " comment"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.comment,
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.action) + " on"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: this.datePipe.transform(val.startDate, 'dd/MM/yyy hh:mm a', 'GMT',this.service.getStorage('language')) + ' to ' + this.datePipe.transform(val.endDate, 'dd/MM/yyy hh:mm a', 'GMT',this.service.getStorage('language')),
                        readOnly: true
                    }
                },
                {
                    label: {
                        text: this.changeAction(val.action) + " By"
                    },
                    name: "director",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: val.createdUser,
                        readOnly: true
                    }
                },

            ]
            );
    }
    /************ */
    calculateTimedifference(startTime, endTime) {

        var format = startTime.replace('-', ' ')
        format = format.replace('-', ' ')

        format = new String(format.split('.')[0])
        var utc = new String(' UTC')
        var time = format.concat(utc)

        var startTime1 = new Date(this.service.convertDateUsingPipe(new Date(time))).getTime()
        format = endTime.replace('-', ' ')
        format = format.replace('-', ' ')

        format = new String(format.split('.')[0])
        utc = new String(' UTC')
        time = format.concat(utc)

        var endTime1 = new Date(this.service.convertDateUsingPipe(new Date(time))).getTime()
        console.log(startTime + " " + startTime1)
        var d = Math.abs(startTime1 - endTime1) / 1000
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
        return hDisplay + mDisplay + sDisplay;
    }
    calculateStartTime(startTime) {
        // var format = startTime.replace('-', ' ')
        // format = format.replace('-', ' ')

        // format = new String(format.split('.')[0])
        // var utc = new String(' UTC')
        // var time = format.concat(utc)
       
        var time= this.service.convertDateToGmt(startTime)
      
        return new Date(this.service.convertDateUsingPipe(new Date(time)))

    }
    checkUserIdentity(createdUser) {
        var user;

        if (createdUser == this.service.getStorage('userIdentifier'))
            user = 'You'//this.service.getStorage('userName')
        else
            user = this.service.getStorage('tempName')
        return user
    }
    count: any;
    getHourFromTime(time) {
        console.warn(+this.datePipe.transform(this.calculateStartTime(time), 'H.mm'))
        return +this.datePipe.transform(this.calculateStartTime(time), 'H.mm')
    }

    getTimeSheet() {

        customealert.loaderShow('html')
        window.events = []
        window.locations = []
        this.timeSheet = []
        this.events = []
        this.appointmentsData = []
        this.service.serverGetRequest('', '/contractDetails/timesheet/' + this.contractId + '.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
            customealert.loaderHide('html')

            if (res['status'].responseCode == 20) {
                this.timeSheet = res['result'].timesheet
                let count = this.timeSheet.length
                this.count = count;

                for (let index = 0, j = count; index < count; index++) {



                    if (this.timeSheet[index + 1]) {
                        let details = this.timeSheet[index]
                        console.log(details)
                        if (details.action == 'START') { // START - STOP or START - PAUSE 
                            this.events.push({
                                start: addHours(startOfDay(this.calculateStartTime(this.timeSheet[index].createdTime)), this.getHourFromTime(this.timeSheet[index].createdTime)),
                                // end: addHours(startOfDay(this.calculateStartTime(this.timeSheet[index + 1].createdTime)), this.getHourFromTime(this.timeSheet[index + 1].createdTime)),
                                title: this.changeAction(details.action) + ' ( ' + this.calculateTimedifference(this.timeSheet[index].createdTime, this.timeSheet[index + 1].createdTime) + ' )',//this.timeSheet[index].action + ' by <br>' + this.checkUserIdentity(this.timeSheet[index].createdUser),
                                color: colors.green,
                                //
                                draggable: false,
                                actionTitle: this.timeSheet[index].title,
                                actionComment: this.timeSheet[index].comment,
                                actionName:this.changeAction(details.action),
                                actionBy: this.checkUserIdentity(this.timeSheet[index].createdUser),
                                actionTime: this.calculateStartTime(this.timeSheet[index].createdTime),
                                nextAction:this.changeAction(this.timeSheet[index+1].action),
                                nextActionBy:this.checkUserIdentity(this.timeSheet[index+1].createdUser),
                                nextActionTime: this.calculateStartTime(this.timeSheet[index+1].createdTime),
                                nextActionTitle: this.timeSheet[index+1].title,
                                nextActionComment:this.timeSheet[index+1].comment,
                                timeDifference: this.calculateTimedifference(this.timeSheet[index].createdTime, this.timeSheet[index + 1].createdTime),
                            })
                        }
                        if (details.action == 'RESUME') { // RESUME - STOP or RESUME - PAUSE 
                            this.events.push({
                                start: addHours(startOfDay(this.calculateStartTime(this.timeSheet[index].createdTime)), this.getHourFromTime(this.timeSheet[index].createdTime)),
                                end: addHours(startOfDay(this.calculateStartTime(this.timeSheet[index + 1].createdTime)), this.getHourFromTime(this.timeSheet[index + 1].createdTime)),
                                title: this.changeAction(details.action) + ' ( ' + this.calculateTimedifference(this.timeSheet[index].createdTime, this.timeSheet[index + 1].createdTime) + ' )',//this.timeSheet[index].action + ' by <br>' + this.checkUserIdentity(this.timeSheet[index].createdUser),
                                color: colors.green,
                                //
                                draggable: false,
                                actionTitle: this.timeSheet[index].title,
                                actionComment: this.timeSheet[index].comment,
                                actionName:this.changeAction(details.action),
                                actionBy: this.checkUserIdentity(this.timeSheet[index].createdUser),
                                actionTime: this.calculateStartTime(this.timeSheet[index].createdTime),
                                nextAction:this.changeAction(this.timeSheet[index+1].action),
                                nextActionBy:this.checkUserIdentity(this.timeSheet[index+1].createdUser),
                                nextActionTime: this.calculateStartTime(this.timeSheet[index+1].createdTime),
                                nextActionTitle: this.timeSheet[index+1].title,
                                nextActionComment:this.timeSheet[index+1].comment,
                                timeDifference: this.calculateTimedifference(this.timeSheet[index].createdTime, this.timeSheet[index + 1].createdTime),
                            })
                        }



                    }
                    else {
                        let details = this.timeSheet[index]
                        if (this.timeSheet[index].action == 'START' || this.timeSheet[index].action == 'RESUME')
                            this.events.push({
                                start: addHours(startOfDay(this.calculateStartTime(this.timeSheet[index].createdTime)), this.getHourFromTime(this.timeSheet[index].createdTime)),
                                // end: addHours(startOfDay(this.calculateStartTime(this.timeSheet[index + 1].createdTime)), this.getHourFromTime(this.timeSheet[index + 1].createdTime)),
                                title: this.changeAction(details.action),
                                color: colors.green,
                                
                                draggable: false,
                                actionTitle: this.timeSheet[index].title,
                                actionComment: this.timeSheet[index].comment,
                                actionName:this.changeAction(details.action),
                                timeDifference: this.calculateTimedifference(this.timeSheet[index].createdTime, this.timeSheet[index].createdTime),
                                actionBy: this.checkUserIdentity(this.timeSheet[index].createdUser),
                                actionTime: this.calculateStartTime(this.timeSheet[index].createdTime)

                            })


                        if ((this.timeSheet[index].action == 'PAUSE' && this.timeSheet[index - 1].action != 'START') || (this.timeSheet[index].action == 'STOP' && this.timeSheet[index - 1].action != 'START')) {
                            this.events.push({
                                start: addHours(startOfDay(this.calculateStartTime(this.timeSheet[index].createdTime)), this.getHourFromTime(this.timeSheet[index].createdTime)),
                                // end: addHours(startOfDay(this.calculateStartTime(this.timeSheet[index ].createdTime)), this.getHourFromTime(this.timeSheet[index ].createdTime)),
                                title: this.changeAction(details.action) + ' by <br>' + this.checkUserIdentity(this.timeSheet[index].createdUser),
                                color: colors.red,
                                draggable: false,
                                actionTitle: this.timeSheet[index].title,
                                actionComment: this.timeSheet[index].comment,
                                actionName:this.changeAction(details.action),
                                actionBy: this.checkUserIdentity(this.timeSheet[index].createdUser),
                                actionTime: this.calculateStartTime(this.timeSheet[index].createdTime),
                                timeDifference: this.calculateTimedifference(this.timeSheet[index].createdTime, this.timeSheet[index].createdTime),

                            })

                        }


                    }

                }
                console.log(this.events)
                this.refresh.next();
            }
        },
            er => {
                this.service.serviceErrorResponce(er)
            })
    }
    changeAction(value) {
        let className;
        switch (value) {
            case "START": className = 'Started'
                break;
            case "PAUSE": className = 'Paused'
                break;
            case "RESUME": className = 'Resumed'
                break;
            case "STOP": className = 'Stopped'
                break;

        }
        return className
    }

    /************************************************************************************ */
    @ViewChild('modalContent')
    modalContent: TemplateRef<any>;

    view: CalendarView = CalendarView.Week;

    CalendarView = CalendarView;

    viewDate: Date = new Date();

    modalData: {
        action: string;
        event: CalendarEvent;
    };

    actions: CalendarEventAction[] = [
        {
            label: '<i class="fa fa-fw fa-pencil"></i>',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.handleEvent('Edited', event);
            }
        },
        {
            label: '<i class="fa fa-fw fa-times"></i>',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.events = this.events.filter(iEvent => iEvent !== event);
                this.handleEvent('Deleted', event);
            }
        }
    ];

    refresh: Subject<any> = new Subject();

    events: CalendarEvent[] = [];

    activeDayIsOpen: boolean = false;

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            this.viewDate = date;
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
            }
        }
    }

    eventTimesChanged({
        event,
        newStart,
        newEnd
    }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        this.handleEvent('Dropped or resized', event);
        this.refresh.next();
    }

    handleEvent(action: string, event: CalendarEvent): void {
        this.modalData = { event, action };
        
        this.modal.open(this.modalContent, { size: 'lg' });
    }

  
    /*********************************************************************************** */

}
