import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { CdTimerComponent } from 'angular-cd-timer';
import { ValidationMessageService } from '../../service/validation-message.service'
import { ServerService } from '../../service/server.service'
import { Router, ActivatedRoute } from '@angular/router'
import '../../../assets/js/js/chat.js'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';

import { TestBed } from '@angular/core/testing';
import { TimelogDetailsComponent } from '../timelog-details/timelog-details.component';
declare var customealert: any
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-job-time-line',
  templateUrl: './job-time-line.component.html',
  styleUrls: ['./job-time-line.component.css'],
  providers: [CdTimerComponent]
})
@ViewChild('basicTimer')

export class JobTimeLineComponent implements OnInit, OnDestroy {

  @ViewChild('timeLog') timeLog: TimelogDetailsComponent
  contractId: any;
  recordList: any[] = [
    { 'myList': [false, false, false, false, false] },
  ];
  rating: number;
  constructor(public timer: CdTimerComponent,
    public router: Router,
    public validationMsg: ValidationMessageService,
    public service: ServerService,
    public activated: ActivatedRoute) { }

  public lat: Number = 10.0050
  public lng: Number = 76.3130
  flag = 0
  activateTimeLog() {
    this.timeLog.getTimeSheet()
  }
  tempsLat;
  tempsLan;
  tempdLan;
  tempdLat;
  tempDestination
  tempOrigin
  tempData: any = [];
  openTrackModal() {
    this.tempsLat=parseFloat(this.tempData.employerDetails.latitude);
    this.tempsLan=parseFloat(this.tempData.employerDetails.longitude);
    this.tempdLan=parseFloat(this.tempData.jobDetails.longitude );
    this.tempdLat=parseFloat(this.tempData.jobDetails.latitude);
    this.tempOrigin={ "lat": this.tempsLat, "lng":this.tempsLan}
    this.tempDestination={ "lat": this.tempdLat, "lng":this.tempdLan}

  }
  resetModal() {
    this.service.taskDescription = "";
    this.service.taskName = "";
    this.service.securityKey = "";
    this.service.invoiceList = [];
  }
  ngOnInit() {
    this.invoiceError = false

    this.addInvoiceList()
    this.getDirection()

    this.tempTotal = this.service.invoiceTotal
    this.activated.params.subscribe(params => {
      this.contractId = params['contractId'];
      this.service.setStorage('contractId', this.contractId)
      this.service.getContractDetailsById(this.contractId)

    })
    setTimeout(() => {
      this.flag = 1
    }, 4000)
  }
  transformData() {
    this.service.setStorage('tempData', JSON.stringify(this.service.val))
    customealert.showModal('track_modal')
    //track_modal
  }

  dispayHtml() {
    if (this.service.val) {
      let minHour = this.service.val.minHour;
      let wage = this.service.val.hourlyWages
      let totalTime = this.service.val.totalTime
      if (minHour > parseInt(totalTime.split(':')[0])) {

        return "upto"
      }
      else 
      if(this.service.isEmployerLogin())
      return 'estimatedspentafter'
      return 'estimatedearningsafter'
    }
  }
  baseFare = 0
  get calculateBaseFare() {

    if (this.service.val.wageMode == 'hourly') {
      let minHour = this.service.val.minHour;
      let wage = this.service.val.hourlyWages
      let totalTime = this.service.val.totalTime
      if (minHour < parseInt(totalTime.split(':')[0])) {
        let aditionalHour = Math.abs(minHour - parseInt(totalTime.split(':')[0]))
        this.baseFare = (aditionalHour * wage) + minHour * wage
      }
      else
        this.baseFare = minHour * wage
    }
    else
      this.baseFare = this.service.val.fixedWage
     
return this.baseFare
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.service.val = []
    this.invoiceError = false

    this.service.tempTaskStartTime = 0
    this.service.tempJobDetails = []
  }
  public origin: any
  public destination: any
  getDirection() {
    this.origin = { lat: 9.5916, lng: 76.5222 }
    this.destination = { lat: 10.0050, lng: 76.3130 }

    // this.origin = 'Taipei Main Station'
    // this.destination = 'Taiwan Presidential Office'
  }
  mapShow = 0

  public renderOptions = {
    suppressMarkers: true,
  }

  public markerOptions = {
    origin: {
      icon: 'https://i.imgur.com/7teZKif.png',

      draggable: false,
    },
    destination: {
      icon: 'https://i.imgur.com/7teZKif.png',


    },
  }
  calculateHourlyWages(totalTime, hourlyWages) {
    if (!totalTime)
      return 0
    var time = totalTime.split(':')
    var hour = time[0];
    var mint = time[1]
    if (mint > 0 && mint < 15)
      mint = 0
    if (mint > 15 && mint < 30)
      mint = .25
    if (mint > 30 && mint < 45)
      mint = .5
    if (mint > 45 && mint < 60)
      mint = .75

    return hour * hourlyWages + mint * hourlyWages
  }
  invoiceError = false
  submitInvoice() {
    let flag = 0
    this.invoiceError = false
    this.service.invoiceList.forEach((element, index) => {
      if (element.item == '' && element.quantity == '' && element.price == '') {
        if (index != 0) {
          flag = 1
          this.invoiceError = true
          return false
        }
        else
          this.service.invoiceList = []
      }
    });

    if (flag == 0)
      this.service.serverRequest({ 'invoice': this.service.invoiceList }, 'contractDetails/' + this.service.taskProposal + '/invoice.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
        this.addInvoiceList()
        if (res['status'].responseCode == 20) {

          this.service.showSuccess(this.validationMsg.success)
          customealert.hideModal('modal_job_invoice')

          this.service.getActiveContracts();
          this.service.getContractDetailsById(this.contractId)
        }
        else
          this.service.apiErrorHandler(res)
      }, er => { this.service.serviceErrorResponce(er); this.addInvoiceList() })
    console.log(this.service.invoiceList)
  }
  removeInvoiceList(val) {
    this.service.invoiceList.forEach((element, index) => {
      if (element == val) {
        console.log(this.service.invoiceList)
        console.log(Math.random())
        this.service.invoiceList.splice(index, 1)
      }
    });
    this.service.invoiceSubTotal = 0
    this.service.invoiceList.forEach(element => {
      if (element.quantity != '' && element.price != '') {
        this.service.invoiceSubTotal = Math.abs(this.service.invoiceSubTotal - (element.quantity * element.price))
      }
    });
    this.tempTotal = this.service.invoiceTotal
    this.tempTotal -= this.service.invoiceSubTotal;

  }
  emptyInvoiceList() {
    this.service.invoiceList = []
    this.service.invoiceTotal = 0
    this.tempTotal = 0
    this.service.invoiceSubTotal = 0
    // this.addInvoiceList()
  }
  tempTotal = this.service.invoiceTotal
  addInvoiceList() {
    if (this.service.invoiceList.length == 0)
      this.service.invoiceList.push({
        item: '',
        quantity: '',
        price: ''
      })
    else {
      this.invoiceError = false
      let flag = 0
      this.service.invoiceList.forEach(element => {
        if (element.item == '' && element.quantity == '' && element.price == '') {
          flag = 1
          this.invoiceError = true
          return false
        }
      });
      if (flag == 0)
        this.service.invoiceList.push({
          item: '',
          quantity: '',
          price: ''
        })
    }
    this.calculateTotal()
  }
  calculateTotal() {
    this.service.invoiceSubTotal = 0
    this.tempTotal = this.service.invoiceTotal
    this.service.invoiceList.forEach(element => {
      if (element.quantity != '' && element.price != '') {
        this.service.invoiceSubTotal += element.quantity * element.price
      }
    });
    console.log(this.service.invoiceSubTotal)
    console.log(this.service.invoiceTotal)
    this.tempTotal += this.service.invoiceSubTotal;
  }
  getInvoiceDetails() {
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'contractDetails/' + this.contractId + '/invoice.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')

      if (res['status'].responseCode == 20) {
        this.service.invoiceDetails = res['result'].invoice
        this.service.invoiceTotal = this.service.invoiceDetails.invoiceDetails.totalFare
        customealert.showModal('modal_job_invoice')
      }
      else
        this.service.apiErrorHandler(res)
    }, er => this.service.serviceErrorResponce(er))

  }
  /************************EVALUATE***** */
  employera1Event(event) {
    this.A1 = (event.target.value)
  }
  employera3Event(event) {
    this.A3 = (event.target.value)
  }
  employera2Event(event) {
    this.A2 = (event.target.value)
  }
  employera4Event(event) {
    this.A4 = (event.target.value)
  }
  employerA6Event(event) {
    this.A6 = (event.target.value)
  }
  employerA5Event(event) {
    this.A5 = (event.target.value)
  }
  optionArray: any = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  A1 = 10
  A2 = 1
  A3 = 1
  A4 = 10
  A5 = 1
  A6 = 1
  A7 = 10;
  currentRate = 5
  employercomment: any = null;
  error = 0;
  submitEvaluation() {
    var data
    // alert(this.service.getStorage('tempTaskId'))
    // if(!this.contractId || this.contractId=='undefined'||this.contractId==undefined || this.contractId==null)
    // {
    //   customealert.hideModal('exampleModal');
    //   this.service.showError(this.validationMsg.networkError)
    //   return false
    // }
    if (!this.employercomment) {

      this.error = 1
      return false
    }
    this.error = 0
    if (this.service.isEmployeeLogin())
      data = {

        "answer1": this.A1 == 1 ? '1' : '0',
        "answer2": this.A2 == 1 ? '1' : '0',
        "answer3": this.A3 == 1 ? '1' : '0',
        "answer4": this.A4 == 1 ? '1' : '0',
        "answer5": this.A5 == 1 ? '1' : '0',
        "answer6": this.A6 == 1 ? '1' : '0',
        "answer7": this.A7,
        "contractId": this.contractId,
        "comment": this.employercomment,

        "rating": this.currentRate
      }
    if (this.service.isEmployerLogin())
      data = {

        "answer1": this.A1,
        "answer2": this.A2,
        "answer3": this.A3,
        "answer4": this.A4,
        "answer5": '-1',
        "answer6": '-1',
        "answer7": '-1',
        "contractId": this.contractId,
        "comment": this.employercomment,

        "rating": this.currentRate
      }

    customealert.loaderShow('html')
    this.service.serverRequest(data, 'evaluation/' + this.contractId + '.json?registerType=' + this.service.getStorage('registerType')).subscribe(res => {
      customealert.loaderHide('html')
      if (res['status'].responseCode == 20) {
        this.employercomment = ''
        this.recordList = [
          { 'myList': [false, false, false, false, false] },
        ];
        this.service.getContractDetailsById(this.contractId)
        customealert.hideModal('evaluateModal')
        this.service.showSuccess(this.validationMsg.success)
      } else
        this.service.apiErrorHandler(res)
    },
      err => {
        customealert.hideModal('evaluateModal')
        this.service.serviceErrorResponce(err)
      })
  }
  setStarTable(record: any, data: any) {
    this.currentRate = data + 1;
    var tableList = this.recordList.find(function (obj: any) { return obj.Id === record.Id });
    for (var i = 0; i <= 4; i++) {
      if (i <= data) {
        tableList.myList[i] = false;
      }
      else {
        tableList.myList[i] = true;
      }
    }
  }
}
