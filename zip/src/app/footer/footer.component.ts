import { Component, OnInit } from '@angular/core';
import {ServerService} from '../service/server.service'
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(public service : ServerService) { }

  ngOnInit() {
  }
  bodyscrolltop(){
    window.scrollTo(0,0)
  }
}
