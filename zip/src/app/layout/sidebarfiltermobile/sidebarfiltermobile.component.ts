import { Component, OnInit } from '@angular/core';
import{ServerService} from '../../service/server.service'
import {Router} from '@angular/router'
@Component({
  selector: 'app-sidebarfiltermobile',
  templateUrl: './sidebarfiltermobile.component.html',
  styleUrls: ['./sidebarfiltermobile.component.css']
})
export class SidebarfiltermobileComponent implements OnInit {

  category : any;
  categoryLength : any ;
  constructor(public service : ServerService,public router :Router) { }
searchType=true // if job search

  ngOnInit() {

    this.service.getAllCategory() ; //getting all category
    this.service.getCountry();
   if(this.router.url.split('/')[1]=='search_all_person')
      this.searchType=false // Employee search 
  }

  checkSubcategory(subId)
  {
    let flag=0;
    this.service.subCategoryFilter.forEach(element => {
      
      if(element.subcategoryId==subId && flag==0)
          flag=1;
    });

    if(flag==0)
    return false ;
    else 
    return true
  }

  pushAndSliceFilter(categryName,categoryId,subCategoryName,subCategoryId,event)
  {
    if(event.target.checked)
     {
       this.service. pushCategoryFilter(categryName, categoryId);
       this.service.pushSubCategoryFilter(subCategoryName, subCategoryId,categoryId)
     }
     if(!event.target.checked)
     {
       this.service.removesubCategoryFilter(subCategoryId);
       let flag=0;
       this.service.subCategoryFilter.forEach(element => {
         if(element.categoryId==categoryId)
         flag=1;
       });
       if(flag==0)
       this.service. removeCategoryFilter(categoryId)
       this.service.fetchAllJobDetailsFromApi(0)
     }
  }

  getAllCategory() {

    this.service.serverGetRequest('', 'custom/home/commonHeaders.json').subscribe(
      (res) => {
        if (res['status'].responseCode == 20) {
          this.category = res['result'].commonCategories;
          this.categoryLength = (this.category.length);
        }
      },
      err => {


      }
    );
  }

}
