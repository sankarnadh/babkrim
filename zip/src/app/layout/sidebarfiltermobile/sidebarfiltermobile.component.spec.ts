import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarfiltermobileComponent } from './sidebarfiltermobile.component';

describe('SidebarfiltermobileComponent', () => {
  let component: SidebarfiltermobileComponent;
  let fixture: ComponentFixture<SidebarfiltermobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarfiltermobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarfiltermobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
