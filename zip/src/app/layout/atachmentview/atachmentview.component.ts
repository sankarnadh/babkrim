import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ServerService } from '../../service/server.service'
import '../../../assets/js/js/chat.js'
declare var customealert: any;
declare var $
@Component({
  selector: 'app-atachmentview',
  templateUrl: './atachmentview.component.html',
  styleUrls: ['./atachmentview.component.css']
})
export class AtachmentviewComponent implements OnInit, OnDestroy {

  constructor(public service: ServerService) { }
  @Input() attachments;
  ngOnInit() {
  }
  images
  hideModal() {
    customealert.hideModal('exampleModalImage')
  }
  ngOnDestroy(): void {
    this.attachments=[];
    this.images=null
this.fileId=null
  }
  fileId=null
  showModal(id){
    this.fileId=id;
    customealert.showModal(id)
  //  this.showModal(id)

  }
}
