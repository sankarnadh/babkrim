import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service'
import { FormGroup, FormControl, FormBuilder,Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  constructor(public service: ServerService,public router :Router) {
  
  }
showWageFilter=false
  ngOnInit() {
    if(this.router.url=='/employee-job-list')
    this.showWageFilter=true
    else 
    this.showWageFilter=false
  }

}
