import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service'
@Component({
    selector: 'app-job-list',
    templateUrl: './job-list.component.html',
    styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {

    constructor(public service: ServerService) { }

    ngOnInit() {
    }
    playlist = [
        {
            title: 'Pale Blue Dot',
            src: 'http://static.videogular.com/assets/videos/videogular.mp4',
            type: 'video/mp4'
        },
        {
            title: 'Big Buck Bunny',
            src: 'http://static.videogular.com/assets/videos/big_buck_bunny_720p_h264.mov',
            type: 'video/mp4'
        },
        {
            title: 'Elephants Dream',
            src: 'http://static.videogular.com/assets/videos/elephants-dream.mp4',
            type: 'video/mp4'
        }
    ];
    currentIndex = 0;
    src = "https://s3-ap-southeast-1.amazonaws.com/jb.bkt/featured-works/c3613cc7-4af8-449e-95b2-b3e99e5244af1541330191632.mp4"
    currentItem = this.playlist[this.currentIndex];

    onClickPlaylistItem(item, index) {
        this.src = 'http://static.videogular.com/assets/videos/big_buck_bunny_720p_h264.mov'
        console.log("in")
        this.currentIndex = index;
        this.currentItem = item;
    }
    test(ec:any='')
    {
        console.log("in"+ec)
    }
}
