import { Component, OnInit ,Input, ViewChild} from '@angular/core';
import { PlayerComponent } from '../../player/player.component';

@Component({
  selector: 'app-employeefeaturedwork',
  templateUrl: './employeefeaturedwork.component.html',
  styleUrls: ['./employeefeaturedwork.component.css']
})
export class EmployeefeaturedworkComponent implements OnInit {

  constructor() { }
@Input () videoUrl :any;
@Input () featuredVideo:any=[];
@Input () featuredImage :any=[]
@Input () imageUrl : any
@ViewChild('vgplayer') VgPlayer:PlayerComponent
  ngOnInit() {
  // alert(this.imageUrl)
  }
  openPlayer(){
    this.VgPlayer.pausePlayer()
  }
  images=[]
}
