import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeefeaturedworkComponent } from './employeefeaturedwork.component';

describe('EmployeefeaturedworkComponent', () => {
  let component: EmployeefeaturedworkComponent;
  let fixture: ComponentFixture<EmployeefeaturedworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeefeaturedworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeefeaturedworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
