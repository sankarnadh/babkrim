import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgmdirectionComponent } from './agmdirection.component';

describe('AgmdirectionComponent', () => {
  let component: AgmdirectionComponent;
  let fixture: ComponentFixture<AgmdirectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgmdirectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgmdirectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
