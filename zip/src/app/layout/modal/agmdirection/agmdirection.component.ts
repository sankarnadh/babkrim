import { Component, OnInit, Input, Output } from "@angular/core";
import { ServerService } from "src/app/service/server.service";
declare var google: any;
import "../../../../assets/js/js/chat.js";
declare var customealert: any;
@Component({
  selector: "app-agmdirection",
  templateUrl: "./agmdirection.component.html",
  styleUrls: ["./agmdirection.component.css"]
})
export class AgmdirectionComponent implements OnInit {
  @Input() slat: any; //Source lattitude
  @Input() slan: any; // source longitude
  @Input() dlat: any; // destination Lattitude
  @Input() dlan: any; // destination Longitude
  @Input() origin: any;
  @Input() destination: any;
  @Input() navigationStatus;
  @Input() contrctId;
  zoom = 18;
  constructor(private service: ServerService) {}

  ngOnInit() {
    this.calculateDistance();
  }
  calculateDistance() {
    if (this.slat) {
      try {
        const mexicoCity: any = new google.maps.LatLng(
          this.slat,
          this.slan,
          17
        );
        const jacksonville: any = new google.maps.LatLng(
          this.dlat,
          this.dlan,
          17
        );
        const distance: any = google.maps.geometry.spherical.computeDistanceBetween(
          mexicoCity,
          jacksonville
        );

        this.zoom = 18;
        return distance / 1000;
      } catch (error) {
        console.log(error);
      }
    }
  }
  startNavigation(url) {
    customealert.loaderShow("html");
    this.service
      .serverRequest(
        "",
        "contractDetails/" + this.contrctId + "/"+url+".json?registerType="+this.service.getStorage('registerType')
      )
      .subscribe(
        res => {
          customealert.loaderHide("html");
          if (res["status"].responseCode == 20) {
            customealert.hideAllModal()
            this.service.getActiveContracts();
          } else this.service.apiErrorHandler(res);
        },
        er => {
          this.service.serviceErrorResponce(er);
        }
      );
  }
}
