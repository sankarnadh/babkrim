import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../../service/server.service';
import { ValidationMessageService } from '../../../service/validation-message.service'
import '../../../../assets/js/js/chat.js'
import { NgProgress } from 'ngx-progressbar';
import { element } from 'protractor';
import { VgAPI } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';
declare var customealert;
@Component({
  selector: 'app-featuredworks',
  templateUrl: './featuredworks.component.html',
  styleUrls: ['./featuredworks.component.css']
})
export class FeaturedworksComponent implements OnInit {

  constructor(public ngProgress: NgProgress,public service: ServerService, public validationMsg: ValidationMessageService) {

  }
  api: VgAPI;
  ngOnInit() {
    window.scrollTo(0, 0)
    this.getEmployeeDetailsByEmployeeId();
  }
  videoUrl = null

  sources = []
  setCurrentVideo(source: string) {
    this.videoUrl = source

  }
  onPlayerReady(api: VgAPI) {
    this.api = api;
    if (this.videoUrl)
      this.setCurrentVideo(this.videoUrl)
    console.log(this.api)
  }
  uploadVideo(event) {


    if (this.videoCounter < 5) {

      const fileSelected: File = event.target.files[0];
      if (!this.checkAllowedVideo(event.target.files[0].name.split('.').pop())) {
        this.service.showError(this.validationMsg.invalidFileType)
        return false
      }
      if (fileSelected.size < this.service.videoSize) {
        if (fileSelected) {

          this.videoUpload = true
        }

        let response;
        this.ngProgress.start();
        this.service.uploadFile(fileSelected, 'upload.json?fileType=video')
          .subscribe((res: Response) => {
            response = res;
            this.ngProgress.done();
            this.videoUpload = false

            if (response.status.responseCode == 20) {
              this.featuredVideo.push({
                name: '',
                fileId: response.result.file1,
                url: this.service.s3bucketurl + this.service.bucketFolder + response.result.file1 + this.service.bucketExtension,
                thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + response.result.file1
              })
              this.videoUrl = this.service.s3bucketurl + this.service.bucketFolder + response.result.file1 + this.service.bucketExtension

              this.videoCounter++;


            }
            else {
              this.service.apiErrorHandler(res);
              if (response.responseCode == 30)
                window.location.reload()
            }
          },
            (error) => {
              this.videoUpload = false
              this.service.serviceErrorResponce(error)
            });
      } else
        this.service.showError(this.validationMsg.attachmentVideoSizeLimit)

    }
    else
      this.service.showError(this.validationMsg.attachmentLimit)
  }
  imageCounter = 0
  videoCounter = 0

  featuredImage = []
  featuredVideo = []
  imageUpload = false
  videoUpload = false
  imageUrl = null
  uploadImage(event) {
    var count = event.target.files.length;

    if (this.imageCounter + count < 6) {
      for (let i = 0; i < count; i++) {
        const fileSelected: File = event.target.files[i];
        if (!this.checkAllowedImage(event.target.files[i].name.split('.').pop())) {
          this.service.showError(this.validationMsg.invalidFileType)
          return false
        }
        if (fileSelected.size < this.service.maxFileSize) {
          if (fileSelected) {

            this.imageUpload = true
          }

          let response;
          this.ngProgress.start();
          this.service.uploadFile(fileSelected, 'upload.json?fileType=docs')
            .subscribe((res: Response) => {
              response = res;
              this.ngProgress.done();
              this.imageUpload = false

              if (response.status.responseCode == 20) {
                this.featuredImage.push({
                  name: fileSelected.name,
                  fileId: response.result.file1,
                  url: this.service.url + '/download/docs.json?fileID=' + response.result.file1 + '&token=' + this.service.getStorage('token')
                })
                this.imageUrl = this.service.url + '/download/docs.json?fileID=' + response.result.file1 + '&token=' + this.service.getStorage('token')

                this.imageCounter++;


              }
              else {
                this.service.apiErrorHandler(res);
                if (response.responseCode == 30)
                  window.location.reload()
              }
            },
              (error) => {
                this.imageUpload = false
                this.service.serviceErrorResponce(error)
              });
        } else
          this.service.showError(this.validationMsg.attachmentSizeLimit)
      }
    }
    else
      this.service.showError(this.validationMsg.attachmentLimit)
  }

  checkAllowedImage(fileExe) {
    if (fileExe == 'png' || fileExe == 'jpeg' || fileExe == 'jpg')
      return true
    return false
  }
  checkAllowedVideo(fileExe) {
    if (fileExe == 'mp4')
      return true
    return false
  }
  deleteVideo(index, fileId) {

    if (index !== -1) {
      this.featuredVideo.splice(index, 1);
      // this.experienceCertificate.splice(index, 1);
      this.videoCounter--
      if (this.videoCounter == 0) {
        this.videoCounter = 0
        this.videoUrl = null
      }
      if (this.videoCounter != 0)
      this.videoUrl = this.featuredVideo[this.featuredVideo.length - 1].url

    }
  }
  deleteImage(index, fileId) {

    if (index !== -1) {
      this.featuredImage.splice(index, 1);
      // this.experienceCertificate.splice(index, 1);
      this.imageCounter--
      if (this.imageCounter == 0) {
        this.imageCounter = 0
        this.imageUrl = null
      }
      if (this.imageCounter != 0)
        this.imageUrl = this.featuredImage[this.featuredImage.length - 1].url

    }


  }
  uploadFeaturedVideo() {
    console.log(this.featuredImage)
    let data = {
      "image1": this.featuredImage[0] ? this.featuredImage[0].fileId ? this.featuredImage[0].fileId : '' : '',
      "image2": this.featuredImage[1] ? this.featuredImage[1].fileId ? this.featuredImage[1].fileId : '' : '',
      "image3": this.featuredImage[2] ? this.featuredImage[2].fileId ? this.featuredImage[2].fileId : '' : '',
      "image4": this.featuredImage[3] ? this.featuredImage[3].fileId ? this.featuredImage[3].fileId : '' : '',
      "image5": this.featuredImage[4] ? this.featuredImage[4].fileId ? this.featuredImage[4].fileId : '' : '',
      "video1": this.featuredVideo[0] ? this.featuredVideo[0].fileId ? this.featuredVideo[0].fileId : '' : '',
      "video2": this.featuredVideo[1] ? this.featuredVideo[1].fileId ? this.featuredVideo[1].fileId : '' : '',
      "video3": this.featuredVideo[2] ? this.featuredVideo[2].fileId ? this.featuredVideo[2].fileId : '' : '',
      "video4": this.featuredVideo[3] ? this.featuredVideo[3].fileId ? this.featuredVideo[3].fileId : '' : '',
      "video5": this.featuredVideo[4] ? this.featuredVideo[4].fileId ? this.featuredVideo[4].fileId : '' : ''
    }
    console.log(data)

    customealert.loaderShow('html')
    if (!this.featuredId) {
      this.service.serverRequest(data, 'featuredWorks.json').subscribe(res => {
        customealert.loaderHide('html')
        if (res['status'].responseCode == 20) {
          this.service.showSuccess(this.validationMsg.success)
        }
        else
          this.service.apiErrorHandler(res)
      }, err => {
        this.service.serviceErrorResponce(err)
      })
    }
    else {
      this.service.serverPutRequest(data, 'featuredWorks/' + this.featuredId + '.json').subscribe(res => {
        customealert.loaderHide('html')
        if (res['status'].responseCode == 20) {
          this.service.showSuccess(this.validationMsg.success)

        }
        else
          this.service.apiErrorHandler(res)
      }, err => {
        this.service.serviceErrorResponce(err)
      })
    }
  }
  employeeDetails: any;
  featuredId = null
  getEmployeeDetailsByEmployeeId() {
    customealert.loaderShow('html')
    this.service.serverGetRequest('', 'employee/employeeDetails/' + this.service.getStorage('userIdentifier') + '.json').subscribe(res => {
      customealert.loaderHide('html')

      if (res['status'].responseCode == 20) {
        // this.service.url + "/download/profile.json?fileID=" + res['result']['profileImage'] + "&token=" + this.service.getStorage('token')
        this.employeeDetails = res['result'];
        if (this.employeeDetails.featuredDetails) {
          this.featuredId = this.employeeDetails.featuredDetails.featuredId

          if (this.employeeDetails.featuredDetails.image1) {
            this.imageCounter++
            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image1,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image1 + '&token=' + this.service.getStorage('token')
            })
          }
          if (this.employeeDetails.featuredDetails.image2) {
            this.imageCounter++

            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image2,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image2 + '&token=' + this.service.getStorage('token')
            })
          }
          if (this.employeeDetails.featuredDetails.image3) {
            this.imageCounter++

            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image3,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image3 + '&token=' + this.service.getStorage('token')
            })
          }
          if (this.employeeDetails.featuredDetails.image4) {
            this.imageCounter++

            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image4,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image4 + '&token=' + this.service.getStorage('token')
            })
          }
          if (this.employeeDetails.featuredDetails.image5) {
            this.imageCounter++

            this.featuredImage.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.image5,
              url: this.service.url + '/download/docs.json?fileID=' + this.employeeDetails.featuredDetails.image5 + '&token=' + this.service.getStorage('token')
            })
          }
          this.imageUrl = this.featuredImage[this.featuredImage.length - 1].url

          if (this.employeeDetails.featuredDetails.video1) {
            this.videoCounter++

            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video1,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video1 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video1
            })
          }
          if (this.employeeDetails.featuredDetails.video2) {
            this.videoCounter++

            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video2,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video2 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video2
            })
          }
          if (this.employeeDetails.featuredDetails.video3) {
            this.videoCounter++

            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video3,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video3 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video3
            })
          }
          if (this.employeeDetails.featuredDetails.video4) {
            this.videoCounter++

            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video4,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video4 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video4
            })
          }
          if (this.employeeDetails.featuredDetails.video5) {
            this.videoCounter++

            this.featuredVideo.push({
              name: '',
              fileId: this.employeeDetails.featuredDetails.video5,
              url: this.service.s3bucketurl + this.service.bucketFolder + this.employeeDetails.featuredDetails.video5 + this.service.bucketExtension,
              thumbnail: this.service.url + '/download/thumbnail.json?fileID=' + this.employeeDetails.featuredDetails.video5
            })
          }
          this.videoUrl = this.featuredVideo[this.featuredVideo.length - 1].url
        }
      }
      else this.service.apiErrorHandler(res)
    },
      er => {
        this.service.serviceErrorResponce(er)
      })
  }
}
