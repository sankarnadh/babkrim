import { Component, OnInit,Input, OnDestroy } from '@angular/core';
import{ServerService} from '../../service/server.service'
import {Router} from '@angular/router'
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-side-barfilter',
  templateUrl: './side-barfilter.component.html',
  styleUrls: ['./side-barfilter.component.css']
})
export class SideBarfilterComponent implements OnInit,OnDestroy {
  @Input() shadowClass=0;
  category : any;
  categoryLength : any ;
  constructor(public service : ServerService,public router :Router,private translate: TranslateService) { 
    
  }
searchType=true // if job search
  ngOnInit() {

    this.service.getAllCategory(); //getting all category
    // this.service.getCountry();
    this.service.loadStateFromApi({"countryIds":[352],"search":""});
    if (this.router.url.split('/')[1] == 'search-all-person')
      this.searchType = false // Employee search 
      this.translate.get("level1EmailExists", {}).subscribe((res: string) => {

        this.countrySettings = {
          singleSelection: false,
          idField: 'id',
          textField: 'country',
          
          itemsShowLimit: 2,
          allowSearchFilter: true,
          closeDropDownOnSelection: true,
          searchPlaceholderText:this.translate.instant('search'),
          noDataAvailablePlaceholderText:this.translate.instant('sorrynorecordsfound'),
          selectAllText:this.translate.instant('selectall'),
          unSelectAllText:this.translate.instant('unselectall')
        };
        this. StateSettings={
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          searchPlaceholderText:this.translate.instant('search'),
          selectAllText:this.translate.instant('selectall'),
          unSelectAllText:this.translate.instant('unselectall'),
          noDataAvailablePlaceholderText:this.translate.instant('sorrynorecordsfound'),
          itemsShowLimit: 2,
          allowSearchFilter: true,
          closeDropDownOnSelection: true
        }
        this.citySettings={
          singleSelection: true,
          idField: 'id',
          textField: 'city',
          searchPlaceholderText:this.translate.instant('search'),
          selectAllText:this.translate.instant('selectall'),
          unSelectAllText:this.translate.instant('unselectall'),
          noDataAvailablePlaceholderText:this.translate.instant('sorrynorecordsfound'),
          itemsShowLimit: 2,
          allowSearchFilter: true,
          closeDropDownOnSelection: true
        }
      });
     
  }
ngOnDestroy(){
  this.service.sideBarCheckBoxStatus=0
}
  checkSubcategory(subId) {
    let flag = 0;
    this.service.subCategoryFilter.forEach(element => {

      if (element.subcategoryId == subId && flag == 0)
        flag = 1;
    });

    if (flag == 0)
      return false;
    else
      return true
  }
  pushAndSliceFilter(categryName, categoryId, subCategoryName, subCategoryId, event) {
    if (event.target.checked) {
      this.service.sideBarCheckBoxStatus=1;
      this.service.pushCategoryFilter(categryName, categoryId);
      this.service.pushSubCategoryFilter(subCategoryName, subCategoryId, categoryId)
     
    
    }
    if (!event.target.checked) {
    
      let flag = 0;
      this.service.subCategoryFilter.forEach(element => {
        if (element.categoryId == categoryId)
          flag = 1;
      });
      if (flag == 0)
        this.service.removeCategoryFilter(categoryId)

        this.service.removesubCategoryFilter(subCategoryId);
      console.log( this.service.subCategoryFilter)  

    }
  }
  getAllCategory() {

    this.service.serverGetRequest('', 'custom/home/commonHeaders.json').subscribe(
      (res) => {
        if (res['status'].responseCode == 20) {
          this.category = res['result'].commonCategories;
          this.categoryLength = (this.category.length);
        }
      },
      err => {


      }
    );
  }

  countrySettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'country',
    
    itemsShowLimit: 2,
    allowSearchFilter: true,
    closeDropDownOnSelection: true,
    searchPlaceholderText:this.translate.instant('search'),
    noDataAvailablePlaceholderText:this.translate.instant('sorrynorecordsfound'),
    selectAllText:this.translate.instant('selectall'),
    unSelectAllText:this.translate.instant('unselectall')
  };
  StateSettings={
    singleSelection: true,
    idField: 'id',
    textField: 'name',
    searchPlaceholderText:this.translate.instant('search'),
    selectAllText:this.translate.instant('selectall'),
    unSelectAllText:this.translate.instant('unselectall'),
    noDataAvailablePlaceholderText:this.translate.instant('sorrynorecordsfound'),
    itemsShowLimit: 2,
    allowSearchFilter: true,
    closeDropDownOnSelection: true
  }
  citySettings={
    singleSelection: true,
    idField: 'id',
    textField: 'city',
    searchPlaceholderText:this.translate.instant('search'),
    selectAllText:this.translate.instant('selectall'),
    unSelectAllText:this.translate.instant('unselectall'),
    noDataAvailablePlaceholderText:this.translate.instant('sorrynorecordsfound'),
    itemsShowLimit: 2,
    allowSearchFilter: true,
    closeDropDownOnSelection: true
  }
}
