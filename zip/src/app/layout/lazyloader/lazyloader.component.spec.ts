import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LazyloaderComponent } from './lazyloader.component';

describe('LazyloaderComponent', () => {
  let component: LazyloaderComponent;
  let fixture: ComponentFixture<LazyloaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LazyloaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LazyloaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
