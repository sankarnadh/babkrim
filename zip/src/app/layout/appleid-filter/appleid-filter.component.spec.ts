import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppleidFilterComponent } from './appleid-filter.component';

describe('AppleidFilterComponent', () => {
  let component: AppleidFilterComponent;
  let fixture: ComponentFixture<AppleidFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppleidFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppleidFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
