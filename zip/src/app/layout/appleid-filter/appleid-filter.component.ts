import { Component, OnInit } from '@angular/core';
import {ServerService} from '../../service/server.service'
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-appleid-filter',
  templateUrl: './appleid-filter.component.html',
  styleUrls: ['./appleid-filter.component.css']
})
export class AppleidFilterComponent implements OnInit {

  constructor(public service : ServerService,public router : Router) { }

  ngOnInit() {
    console.log(this.router.url)
  }

}
