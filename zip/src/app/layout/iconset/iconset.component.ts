import { Component, OnInit,Input,LOCALE_ID } from '@angular/core';
import {ServerService} from '../../service/server.service'
@Component({
  selector: 'app-iconset',
  templateUrl: './iconset.component.html',
  styleUrls: ['./iconset.component.css']
})

export class IconsetComponent implements OnInit {

  constructor(public service : ServerService) {
    
   }
  @Input() val: any=[];
  @Input() employerDetails : any=[];
  @Input() showRating=1;
  @Input() status :any;
  ngOnInit() {
    
  }
returnFloat(val){return parseFloat(val);}
}
