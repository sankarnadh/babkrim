import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { ValidationMessageService } from '../../service/validation-message.service'
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from "@angular/router";
import '../../../assets/js/js/chat.js';
declare var customealert: any;
@Component({
  selector: 'app-rightsidebar',
  templateUrl: './rightsidebar.component.html',
  styleUrls: ['./rightsidebar.component.css']
})
export class RightsidebarComponent implements OnInit {

  constructor(
    public activatedRoute: ActivatedRoute,
    public service: ServerService,
    public router: Router,
    public config: NgbRatingConfig,
    public validationMsg: ValidationMessageService,
  ) {
    config.max = 5;
    config.readonly = true
  }
  offer;
  applyBtn = 1;
  messageButton = 1; // hide message button
  proposalId: any;
  recieverId: any;
  hideMinHourLabel=true
  ngOnInit() {
    this.offer = JSON.parse(this.service.getStorage('data'))
    console.info(this.offer)
    if (!this.offer)
      this.router.navigate(['/home'])
    if (this.offer.proposalId) {
      if (this.service.isEmployeeLogin() && this.offer.employerDetails)
        if (this.offer.employerDetails.employerId) {
          this.messageButton = 0;
          this.proposalId = this.offer.proposalId;
          this.recieverId = this.offer.employerDetails.employerId
        }
      if (this.service.isEmployerLogin()) {
        this.messageButton = 0;
        this.proposalId = this.offer.proposalId;
        this.recieverId = this.offer.employeeDetails.employeeId//this.service.getStorage('tempEmployeeId')
      }
    }
    let url = this.router.url.split('/');
    if(url[1]=='employee-view-job-list')
    {
this.hideMinHourLabel=false
    }
    if (url[1] == 'view-contract' || url[1] == 'my-offers-employee' || url[1] == 'invite-interview-employee' || url[1] == 'view-proposal')
      this.applyBtn = 0
    else
      this.applyBtn = 1

      if(url[1]=='view-contract')
      {
        this.viewContract=true;
        this.messageButton = 0;
      }
      else 
      this.viewContract=false
  }
  viewContract
  navigateMsg() {
    this.router.navigate(['/message/' + this.recieverId + '/' + this.proposalId])
  }
  withDrawProposal()
  {
    if(this.service.isEmployerLogin())
    {
      this.withdrwaContract()
      return false;
    }
    let response: any;
    this.service.serverPutRequest('', 'proposals/withdraw/' + this.proposalId + '.json').subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.showSuccess(this.validationMsg.success)
         this.router.navigate(['/proposals'])
        }
        else {
          this.service.apiErrorHandler(res);
         
        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }
  withdrwaContract()
  {
    let contractId
    this.activatedRoute.params.subscribe((res) => {
      contractId = res['contractId'];

    });
    let response: any;
    this.service.serverPutRequest('', 'contracts/withdraw/' + contractId+ '.json').subscribe(
      (res: Response) => {
        customealert.loaderHide("html")
        response = res['status'];
        if (response.responseCode == 20) {
          this.service.showSuccess('success')
         this.router.navigate(['/my_jobs'])
        }
        else {
          this.service.apiErrorHandler(res);
         
        }

      },
      err => {
        this.service.serviceErrorResponce(err)

        customealert.loaderHide("html")
      }
    );
  }
  setEmployeeTempId(id){
    this.service.setStorage('employeeTempId',id)
    this.router.navigate(['/hire_person_direct'])
  }
}
