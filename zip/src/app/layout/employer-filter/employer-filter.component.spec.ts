import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerFilterComponent } from './employer-filter.component';

describe('EmployerFilterComponent', () => {
  let component: EmployerFilterComponent;
  let fixture: ComponentFixture<EmployerFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
