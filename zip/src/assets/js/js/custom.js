
$(document).ready(function(){
 // $('#signupbtn').prop('disabled', true);//From signup page 
  $("#idsearchjobfind").on('input', function () {
    var val = this.value;
    if($('#searchjobfind option').filter(function(){
        return this.value === val;        
    }).length) {
        $("#idsearchjobfind").val('')
        $("#idsearchjobfind").attr('placeholder',val);
    }
});
//Images of playstore 
$('a[data-toggle="tooltip"]').tooltip({
  animated: 'fade',
  placement: 'left',
  html: true
}); 
//Star rating 
  // console.log( 'hello' );
  var $star_rating = $('.star-rating .fa');

  var SetRatingStar = function() {
  return $star_rating.each(function() {
    if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
      return $(this).removeClass('fa-star-o').addClass('fa-star');
    } else {
      return $(this).removeClass('fa-star').addClass('fa-star-o');
    }
  });
  };
  
  $star_rating.on('click', function() {
  $star_rating.siblings('input.rating-value').val($(this).data('rating'));
  return SetRatingStar();
  });
  
  SetRatingStar();
  $('.chosen-select').chosen();
  $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
})



$(document).on('click', '.panel-heading span.icon_minim', function (e) {
  
  var $this = $(this);
  if (!$this.hasClass('panel-collapsed')) {
  
      $this.parents('.panel').find('.panel-body').slideUp();
      $this.addClass('panel-collapsed');
      $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
  } else {

   
      $this.parents('.panel').find('.panel-body').slideDown();
      $this.removeClass('panel-collapsed');
      $this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
  }
});
$(document).on('focus', '.panel-footer input.chat_input', function (e) {
  var $this = $(this);
  if ($('#minim_chat_window').hasClass('panel-collapsed')) {
      $this.parents('.panel').find('.panel-body').slideDown();
      $('#minim_chat_window').removeClass('panel-collapsed');
      $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
  }
});
$(document).on('click', '#new_chat', function (e) {
  
  var size = $( ".chat-window:last-child" ).css("margin-left");
   size_total = parseInt(size) + 400;
  alert(size_total);
  var clone = $( "#chat_window_1" ).clone().appendTo( ".container" );
  clone.css("margin-left", size_total);
});
$(document).on('click', '.icon_close', function (e) {
 
  //$(this).parent().parent().parent().parent().remove();
  $( "#chat_window_1" ).remove();
});

