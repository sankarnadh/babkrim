import { Component, AfterViewInit, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist/dist/chartist.component';
import * as c3 from 'c3';
import { GlobalVariableService } from '@service/globalvariable/global-variable.service';
import { FunctionsService } from '@service/function/functions.service';
import { Router } from '@angular/router';
import { AdminApiService } from '@adminapi/admin-api.service';

declare var require: any;

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements AfterViewInit,OnInit {
  constructor(public variable: GlobalVariableService,
    private method: FunctionsService,
    private router: Router,
    private api: AdminApiService) { }

  // Barchart
  // barChart: Chart = {};

  // Line chart
  lineChart: Chart = {
    type: 'Line',
    data: {
      labels: ['1PM', '2PM', '3PM', '4PM', '5PM', '6PM'],
      series: [[2, 0, 5, 2, 5, 2]]
    },
    options: {
      showArea: true,
      showPoint: false,

      chartPadding: {
        left: -35
      },
      axisX: {
        showLabel: true,
        showGrid: false
      },
      axisY: {
        showLabel: false,
        showGrid: true
      },
      fullWidth: true
    }
  };
ngOnInit(){
  this.api.getJobCount();
}
  ngAfterViewInit() {

    const chart2 = c3.generate({
      bindto: '#product-sales',
      data: {
        columns: [
          ['Employee', 5, 6],
          ['Employer', 1, 2]
        ],
        type: 'spline'
      },
      axis: {
        y: {
          show: true,
          tick: {
            count: 0,
            outer: false
          }
        },
        x: {
          show: true
        }
      },
      padding: {
        top: 40,
        right: 10,
        bottom: 40,
        left: 20
      },
      point: {
        r: 0
      },
      legend: {
        hide: false
      },
      color: {
        pattern: ['#ccc', '#4798e8']
      }
    });
    console.info( this.variable.jobGraph)

    ///////////////////////////////

  }
}
