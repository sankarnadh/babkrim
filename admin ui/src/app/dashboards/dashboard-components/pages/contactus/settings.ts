import { SubjectRenderComponent } from '../subject-render/subject-render.component';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class Settings {
  constructor(private translateService: TranslateService) { }
  public settings = {
    columns: {
      fullName: {
        title: this.translateService.instant('fullname'),
        filter: false
      },
      email: {
        title:  this.translateService.instant('email'),
        filter: false
      },
      mobileNumber: {
        title:  this.translateService.instant('mobile'),
        filter: false
      },
      subject: {
        title:  this.translateService.instant('subject'),
        filter: false,
        type: 'custom',
        renderComponent: SubjectRenderComponent,
      },
      // comment: {
      //   title: 'Comment',
      //   filter: false
      // },
      // registerType:{
      //   title: 'User Type',
      //   filter: false
      // }
    },
    mode: 'external',
    actions: {
      custom: [
        {
          name: 'view',
          title: '<i class="fas fa-eye" title="view"></i>&nbsp;&nbsp;'
        }
      ],
      position: 'right',
      add: false,
      edit: false,
      delete: false
    },
    pager: {
      display: true,
      perPage: 50
    }
  };
}

