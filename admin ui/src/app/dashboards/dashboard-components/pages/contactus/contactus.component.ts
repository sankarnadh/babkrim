import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GlobalVariableService } from '@service/globalvariable/global-variable.service';
import {Settings} from './settings';
import { AdminApiService } from '@adminapi/admin-api.service';
import { FunctionsService } from '@service/function/functions.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit, OnDestroy {
  @ViewChild('resolveContact') resolveContact: any;
  constructor(public variable: GlobalVariableService,
    private method: FunctionsService,
    private router: Router,
    public api: AdminApiService,
    public setting: Settings) { }
  settings = this.setting.settings; // Smart table settings
  public event: any;
  p =1
  ngOnInit() {
    this.api.getContactDetailsAll();
    this.variable.pageNo=1;
    console.log(this.setting.settings);
  }
  ngOnDestroy(): void {
    this.variable.pageNo=1;

    this.event = null;
  }
 customEvent(event) {
    if (event.action == 'view') {
      this.event = event.data;
      this.method.setStorage('tempData', JSON.stringify(this.event));
      this.router.navigate(['/dashboard/view-contact-us']);
    }

    if (event.action == 'delete') {
    }
  }
 }
