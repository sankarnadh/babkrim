import { Component, OnInit, OnDestroy } from '@angular/core';
import { FunctionsService } from '@service/function/functions.service';
import { AdminApiService } from '@adminapi/admin-api.service';
import { MessageService } from '@service/messages/message.service';

@Component({
  selector: 'app-view-contact-us',
  templateUrl: './view-contact-us.component.html',
  styleUrls: ['./view-contact-us.component.css']
})
export class ViewContactUsComponent implements OnInit, OnDestroy {

  constructor(private method: FunctionsService,
    private message: MessageService,
    private api: AdminApiService) { }
  public data: any;
  public resolveComment: any='';

  ngOnInit() {
    this.data = (JSON.parse(this.method.getStorage('tempData')));
    if(this.data.resolveComment){
      this.resolveComment=this.data.resolveComment
    }else {
      this.resolveComment=''
    }
    }
  ngOnDestroy(): void {
    this.resolveComment = '';
    this.method.removeStorage('tempData');
    this.data = null;
  }
  resolveFeedback() {
    if (!this.resolveComment) {
      this.method.showError('Resolve Comment is required.', this.message.error);
      return;
    }
    const data = {
      'comment': this.resolveComment,
      'email': this.data.email,
      'mobileNumber': this.data.mobileNumber,
      'name': this.data.fullName,
      'subject': this.data.subject
    };
    this.api.resolveContactUs(this.data.contactUsId, data);
  }
  saveFeedback(){
    if (!this.resolveComment) {
      this.method.showError('Resolve Comment is required.', this.message.error);
      return;
    }
    const data = {
      'comment': this.resolveComment,
      'email': this.data.email,
      'mobileNumber': this.data.mobileNumber,
      'name': this.data.fullName,
      'subject': this.data.subject
    };
    this.api.saveContactUs(this.data.contactUsId, data);
  }
goBack(){ window.history.back();}
}
