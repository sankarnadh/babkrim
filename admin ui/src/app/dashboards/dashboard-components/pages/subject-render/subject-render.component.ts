import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-subject-render',
  templateUrl: './subject-render.component.html',
  styleUrls: ['./subject-render.component.css']
})
export class SubjectRenderComponent implements OnInit {
  @Input() value: string | number;
  @Input() rowData: any;
  constructor() { }

  ngOnInit() {
   
  }

}
