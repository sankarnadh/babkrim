import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRenderComponent } from './subject-render.component';

describe('SubjectRenderComponent', () => {
  let component: SubjectRenderComponent;
  let fixture: ComponentFixture<SubjectRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
