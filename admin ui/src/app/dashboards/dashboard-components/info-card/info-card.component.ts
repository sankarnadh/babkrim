import { Component, AfterViewInit } from '@angular/core';
import * as c3 from 'c3';
import { GlobalVariableService } from '@service/globalvariable/global-variable.service';
import { FunctionsService } from '@service/function/functions.service';
import { Router } from '@angular/router';
import { AdminApiService } from '@adminapi/admin-api.service';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html'
})
export class InfocardComponent implements AfterViewInit {
  constructor(public variable: GlobalVariableService,
    private method: FunctionsService,
    private router: Router,
    private api: AdminApiService) {}



  ngAfterViewInit() {
this.api.getCardCount();
  }
}
