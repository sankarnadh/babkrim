import { Routes } from '@angular/router';

import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { ContactusComponent } from './dashboard-components/pages/contactus/contactus.component';
import { ViewContactUsComponent } from './dashboard-components/pages/view-contact-us/view-contact-us.component';
import { NewcontactusComponent } from './dashboard-components/pages/newcontactus/newcontactus.component';



export const DashboardRoutes: Routes = [
  {
    path: '',
    children: [

      {
        path: '',
        component: Dashboard1Component,
        data: {
          title: 'dashboard',
          urls: [
            { title: 'dashboard', url: '/dashboard' },
            { title: 'dashboard' }
          ]
        }
      },
      {path:'contact-us',redirectTo:'allcontact-us',pathMatch:'full'},
      {
        path: 'allcontact-us',
        component: ContactusComponent,
        data: {
          title: 'allcontact-us',
          urls: [
            { title: 'dashboard', url: '/dashboard' },
            { title: 'allcontact-us' }
          ]
        }
      },

      {
        path: 'newcontact-us',
        component: NewcontactusComponent,
        data: {
          title: 'newcontact-us',
          urls: [
            { title: 'dashboard', url: '/dashboard' },
            { title: 'newcontact-us' }
          ]
        }
      },
      {
        path: 'view-contact-us',
        component: ViewContactUsComponent,
        data: {
          title: 'viewcontactus',
          urls: [
            { title: 'contactus', url: '/dashboard/contact-us' },
            { title: 'viewcontactus' }
          ]
        }
      }

    ]
  }
];
