import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { UserRoutingModule } from "./user-routing.module";
import { EmployeeComponent } from "./component/employee/employee.component";
import { EmployerComponent } from "./component/employer/employer.component";
import { VerificationRenderComponent } from "./component/verification-render/verification-render.component";
import { ViewSingleUserComponent } from "./component/view-single-user/view-single-user.component";
import { SharedModule } from "app/shared/shared.module";
import { AllemployeeComponent } from "./component/allemployee/allemployee.component";

@NgModule({
  imports: [ SharedModule, CommonModule, UserRoutingModule],
  declarations: [
    EmployeeComponent,
    EmployerComponent,

    ViewSingleUserComponent,
    AllemployeeComponent
  ],

})
export class UserModule {}
