import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './component/employee/employee.component';
import { EmployerComponent } from './component/employer/employer.component';
import { ViewSingleUserComponent } from './component/view-single-user/view-single-user.component';
import { AllemployeeComponent } from './component/allemployee/allemployee.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'newemployee',
        component: EmployeeComponent,
        data: {
          title: 'newemployee',
          urls: [
            { title: 'dashboard', url: '/dashboard' },
            { title: 'newemployee' }
          ]
        }
      },
      {
        path: 'allemployee',
        component: AllemployeeComponent,
        data: {
          title: 'allemployee',
          urls: [
            { title: 'dashboard', url: '/dashboard' },
            { title: 'allemployee' }
          ]
        }
      },
      {
        path: 'view-single-user',
        component: ViewSingleUserComponent,
        data: {
          title: 'viewuser',
          urls: [
            { title: 'user', url: '/user/newemployee' },
            { title: 'viewuser' }
          ]
        }
      },
      {
        path: 'employer',
        component: EmployerComponent,
        data: {
          title: 'employer',
          urls: [
            { title: 'dashboard', url: '/dashboard' },
            { title: 'employer' }
          ]
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
