import { VerificationRenderComponent } from '../verification-render/verification-render.component';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { DateRenderComponent } from 'app/shared/page/date-render/date-render.component';
@Injectable({
  providedIn: 'root'
})
export class Setting {
  constructor(private translateService: TranslateService) {
  //  console.info(this.translateService.instant('messageservice.userNameRequired'));

  }
  public settings = {
    columns: {
      name: {
        title: this.translateService.instant('fullname'),
        filter: false
      },
      email: {
        title: this.translateService.instant('email'),
        filter: false
      },
      mobileNumber: {
        title: this.translateService.instant('mobile'),
        filter: false
      },
      creaedTime: {
        title: this.translateService.instant('createdon'),
        type: 'custom',
        renderComponent: DateRenderComponent,
        filter: false
      },
      emailVerify: {
        title: this.translateService.instant('verification'),
        type: 'custom',
        renderComponent: VerificationRenderComponent,
        filter: false
      }
    },
    mode: 'external',
    actions: {
      custom: [
        {
          name: 'view',
          title: '<i class="fas fa-eye" title="View"></i>&nbsp;&nbsp;'
        }
      ],
      position: 'right',
      add: false,
      edit: false,
      delete: false
    },
    pager: {
      display: true,
      perPage: 20
    }
  };
}
