import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-verification-render',
  templateUrl: './verification-render.component.html',
  styleUrls: ['./verification-render.component.css']
})
export class VerificationRenderComponent implements OnInit {
  @Input() value: string | number;
  @Input() rowData: any;
  constructor() { }

  ngOnInit() {
   
  }

}
