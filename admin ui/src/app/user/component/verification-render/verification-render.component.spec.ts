import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationRenderComponent } from './verification-render.component';

describe('VerificationRenderComponent', () => {
  let component: VerificationRenderComponent;
  let fixture: ComponentFixture<VerificationRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificationRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
