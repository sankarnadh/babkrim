import { Component, OnInit, ViewChild } from "@angular/core";
import { GlobalVariableService } from "@service/globalvariable/global-variable.service";
import { Setting } from "../employee/settings";
import { AdminApiService } from "@adminapi/admin-api.service";
import { FunctionsService } from "@service/function/functions.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-allemployee",
  templateUrl: "./allemployee.component.html",
  styleUrls: ["./allemployee.component.css"]
})
export class AllemployeeComponent implements OnInit {
  @ViewChild("resolveContact") resolveContact: any;
  constructor(
    public variable: GlobalVariableService,
    private method: FunctionsService,
    private router: Router,
    public setting: Setting,
    private api: AdminApiService
  ) {}
  settings = this.setting.settings; // Smart table settings
  public event: any;
  public page: any;
  ngOnInit() {
    this.api.getAllUserFromApi();
  }
  ngOnDestroy() {
    this.event = null;
    this.variable.pageNo = 1;
    this.variable.search='';
  }
  customEvent(event) {
    if (event.action == "view") {
      this.event = event.data;
      this.method.setStorage("tempData", JSON.stringify(this.event));
      this.router.navigate(["/user/view-single-user"]);
    }
  }
  pageChange() {
    this.variable.pageNo=this.page;
    this.api.getAllUserFromApi();
  }
  search(){
    this.variable.pageNo=1;
    this.api.getAllUserFromApi();
  }
}
