import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TablesModule } from "../table/tables.module";
import { AgmCoreModule } from "@agm/core";

import { HttpClient } from "@angular/common/http";
import { HttpLoaderFactory } from "app/app.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { VerificationRenderComponent } from "app/user/component/verification-render/verification-render.component";
import { MapComponent } from "./map/map/map.component";
import { GmtpipePipe } from "@service/pipes/gmtpipe.pipe";
import { DateRenderComponent } from "./page/date-render/date-render.component";
import { NewemployeeComponent } from "./page/newemployee/newemployee.component";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    TablesModule,
    NgxDatatableModule,
    AgmCoreModule,
    Ng2SmartTableModule,TranslateModule,FormsModule,ReactiveFormsModule
  ],
  declarations: [
    VerificationRenderComponent,
    MapComponent,
    GmtpipePipe,
    DateRenderComponent,
    NewemployeeComponent
  ],
  entryComponents: [VerificationRenderComponent, DateRenderComponent],
  exports: [
    TranslateModule,
    NgxDatatableModule,
    NgxPaginationModule,
    VerificationRenderComponent,
    TablesModule,
    AgmCoreModule,
    MapComponent,
    GmtpipePipe,
    NewemployeeComponent,
    Ng2SmartTableModule,
    FormsModule,ReactiveFormsModule
  ]
})
export class SharedModule {}
