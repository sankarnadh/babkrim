import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GlobalVariableService } from '@service/globalvariable/global-variable.service';
import { Setting } from './settings';
import { AdminApiService } from '@adminapi/admin-api.service';
import { FunctionsService } from '@service/function/functions.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-newemployee',
  templateUrl: './newemployee.component.html',
  styleUrls: ['./newemployee.component.css']
})
export class NewemployeeComponent implements OnInit {

    @ViewChild('resolveContact') resolveContact: any;
  constructor(public variable: GlobalVariableService,
    private method: FunctionsService,
    private router: Router,
    public setting: Setting,
    private api: AdminApiService) { }
  settings = this.setting.settings; // Smart table settings
  public event: any;

  ngOnInit() {
    this.api.getNewUserFromApi();
  }
  ngOnDestroy() {
    this.event = null;
    this.variable.search='';
  }
  customEvent(event) {
    if (event.action == 'view') {
      this.event = event.data;
      this.method.setStorage('tempData', JSON.stringify(this.event));
      this.router.navigate(['/user/view-single-user']);
    }

  }

}
