import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-date-render',
  templateUrl: './date-render.component.html',
  styleUrls: ['./date-render.component.css']
})
export class DateRenderComponent implements OnInit {
  @Input() value: string | number;
  @Input() rowData: any;
  constructor() { }

  ngOnInit() {
  }

}
