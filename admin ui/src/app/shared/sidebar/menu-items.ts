import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
  {
    path: '/dashboard',
    title: 'dashboard',
    icon: 'icon-Car-Wheel',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '',
    title: 'user',
    icon: 'icon-Administrator',
    class: 'has-arrow',
    extralink: false,
    submenu: [
      {
        path: '/user/newemployee',
        title: 'newemployee',
        icon: '',
        class: '',
        extralink: false,
        submenu: []
      },
      {
        path: '/user/allemployee',
        title: 'allemployee',
        icon: '',
        class: '',
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '',
    title: 'Employer',
    icon: ' icon-Business-Man',
    class: 'has-arrow',
    extralink: false,
    submenu: [
      {
        path: '/employer/allemployer',
        title: 'allemployer',
        icon: '',
        class: '',
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '',
    title: 'contact',
    icon: 'icon-Mail-2',
    class: 'has-arrow',
    extralink: false,
    submenu: [
      {
        path: '/dashboard/allcontact-us',
        title: 'allcontact-us',
        icon: '',
        class: '',
        extralink: false,
        submenu: []
      },{
        path: '/dashboard/newcontact-us',
        title: 'newcontact-us',
        icon: '',
        class: '',
        extralink: false,
        submenu: []
      }
    ]
  },{
    path: '',
    title: 'Payment',
    icon: 'fa fa-credit-card',
    class: 'has-arrow',
    extralink: false,
    submenu: [
      {
        path: '/payment/allPaymentRequest',
        title: 'allPaymentRequest',
        icon: '',
        class: '',
        extralink: false,
        submenu: []
      }
    ]
  }
  // {
  //   path: '',
  //   title: 'category',
  //   icon: 'icon-Nepal',
  //   class: '',
  //   extralink: false,
  //   submenu: []
  // },
];
