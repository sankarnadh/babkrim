import { Component, AfterViewInit, OnInit } from '@angular/core';
import { ROUTES } from './menu-items';
import { RouteInfo } from './sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
declare var $: any;
import { AuthServiceService } from '@auth/auth-service.service';
import { GlobalVariableService } from '@service/globalvariable/global-variable.service';
import { FunctionsService } from '@service/function/functions.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  constructor(
    private modalService: NgbModal,
    private router: Router,
    public method: FunctionsService,
    private route: ActivatedRoute,
    public auth: AuthServiceService,
    public variable: GlobalVariableService
  ) { }
  // This is for login details
  public loginDetails = JSON.parse(this.method.getStorage('loginDetails'));
  public showMenu = '';
  public showSubMenu = '';
  public sidebarnavItems: any[];
  // End open close
  ngOnInit() {
    this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);
  }
  // this is for the open close
  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }
  addActiveClass(element: any) {
    if (element === this.showSubMenu) {
      this.showSubMenu = '0';
    } else {
      this.showSubMenu = element;
    }
  }


}
