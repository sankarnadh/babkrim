import { Input } from "@angular/core";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";
declare var google;
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  constructor() {}
  @Input() startLat: number;
  @Input() startLng: number;
  @Input() zoom: number = 16;
  @Input() markers: any = [];
  @Output() public outputEvent: any = new EventEmitter<any>();
  @Input() height = 350;
  public formatedAddress: any;
  ngOnInit() {
    // this.getGeoLocation(this.startLat, this.startLng);
  }

  markerDragEnd(label: string, index: any) {
    console.log(`clicked the marker:` + JSON.stringify(index));
    this.getGeoLocation(index.coords.lat, index.coords.lng);
  }
  getGeoLocation(lat: number, lng: number) {
    try {
      if (navigator.geolocation) {
        const geocoder = new google.maps.Geocoder();
        const latlng = new google.maps.LatLng(lat, lng);
        const request = { latLng: latlng };

        geocoder.geocode(request, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            const result = results[0];
            const rsltAdrComponent = result.address_components;
            const resultLength = rsltAdrComponent.length;
            if (result != null) {
              this.formatedAddress = result.formatted_address;
              this.outputEvent.emit(this.formatedAddress);

            } else {
              console.log("No address available!");
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

}
