import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllemployerComponent } from './allemployer.component';

describe('AllemployerComponent', () => {
  let component: AllemployerComponent;
  let fixture: ComponentFixture<AllemployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllemployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllemployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
