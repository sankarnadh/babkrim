import { Component, OnInit } from "@angular/core";
import { GlobalVariableService } from "@service/globalvariable/global-variable.service";
import { FunctionsService } from "@service/function/functions.service";
import { Router } from "@angular/router";
import { AdminApiService } from "@adminapi/admin-api.service";
// import { EmployerTableSetting } from '../employerTableSetting';
import { Setting } from "../employerTableSetting";

@Component({
  selector: "app-allemployer",
  templateUrl: "./allemployer.component.html",
  styleUrls: ["./allemployer.component.css"]
})
export class AllemployerComponent implements OnInit {
  constructor(
    public variable: GlobalVariableService,
    private method: FunctionsService,
    private router: Router,
    public setting: Setting,
    private api: AdminApiService
  ) {}
  settings = this.setting.settings; // Smart table settings
  public event: any;
  public page: any;
  ngOnInit() {
    this.variable.pageNo = 1;
    this.api.getAllEmployer();
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.variable.pageNo = 1;
    this.variable.userList = [];
    this.variable.search=''
  }
  customEvent(event) {
    if (event.action == "view") {
      this.event = event.data;
      this.method.setStorage("tempData", JSON.stringify(this.event));
      this.router.navigate(["/employer/view-single-employer"]);
    }
  }
  pageChange() {
    this.variable.pageNo = this.page;
    this.api.getAllEmployer();
  }
  search(){
    this.variable.pageNo = 1;
    this.api.getAllEmployer();
  }
}
