import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsingleemployerComponent } from './viewsingleemployer.component';

describe('ViewsingleemployerComponent', () => {
  let component: ViewsingleemployerComponent;
  let fixture: ComponentFixture<ViewsingleemployerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsingleemployerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsingleemployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
