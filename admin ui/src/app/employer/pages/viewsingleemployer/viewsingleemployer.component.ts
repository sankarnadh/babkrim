import { Component, OnInit, ViewChild } from "@angular/core";
import { FunctionsService } from "@service/function/functions.service";
import { GlobalVariableService } from "@service/globalvariable/global-variable.service";
import { AdminApiService } from "@adminapi/admin-api.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MessageService } from "@service/messages/message.service";

@Component({
  selector: "app-viewsingleemployer",
  templateUrl: "./viewsingleemployer.component.html",
  styleUrls: ["./viewsingleemployer.component.css"]
})
export class ViewsingleemployerComponent implements OnInit {
  constructor(
    public method: FunctionsService,
    public variable: GlobalVariableService,
    private api: AdminApiService,
    private modalService: NgbModal,
    private message:MessageService
  ) {}
  @ViewChild("locationModal") locationModal: any;
  public data: any;
  public userVerification: any;
  public emailearification: any;
  public mobileVerification: any;
  public lat: any;
  public lan: any;
  ngOnInit() {
    this.data = JSON.parse(this.method.getStorage("tempData"));
    if (!this.data) {
      this.method.emptyDataError();
    }

    this.emailearification = this.data.emailVerify;
    this.mobileVerification = this.data.mobileVerify;
    this.api.getEmployerById(this.data.userId);
  }
  ngOnDestroy() {
    this.variable.tempData = [];
    this.method.removeStorage("tempData");
  }
  updateUserStatus() {
    const data = {
      emailVerfiy: this.emailearification,
      mobileVerfiy: this.mobileVerification,
      userVerfiy: this.userVerification
    };
    this.api.updateEmployerStatus(this.data.userId, data);
  }
  showLocationModal() {
    try {
      this.lat = parseFloat(this.variable.tempData.lastSyncLatitude);
      this.lan = parseFloat(this.variable.tempData.lastSyncLongitude);
      if (this.lan && this.lat) {
        this.variable.modalVariable = this.modalService.open(
          this.locationModal
        );
      } else {
        this.method.showError(this.message.noLocationAvailable,this.message.error)
      }
    } catch (error) {}
  }
  agmMapOutput(event) {}
}
