import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployerRoutingModule } from './employer-routing.module';
import { AllemployerComponent } from './pages/allemployer/allemployer.component';
import { SharedModule } from 'app/shared/shared.module';
import { ViewsingleemployerComponent } from './pages/viewsingleemployer/viewsingleemployer.component';

@NgModule({
  imports: [
    CommonModule,
    EmployerRoutingModule,
    SharedModule
  ],
  declarations: [AllemployerComponent, ViewsingleemployerComponent]
})
export class EmployerModule { }
