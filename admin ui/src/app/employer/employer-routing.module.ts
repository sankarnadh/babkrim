import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AllemployerComponent } from './pages/allemployer/allemployer.component';
import { ViewsingleemployerComponent } from "./pages/viewsingleemployer/viewsingleemployer.component";

const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "allemployer",
        component: AllemployerComponent,
        data: {
          title: "allemployer",
          urls: [
            { title: "dashboard", url: "/dashboard" },
            { title: "allemployer" }
          ]
        }
      },
      {
        path: "view-single-employer",
        component: ViewsingleemployerComponent,
        data: {
          title: "viewsingleemployer",
          urls: [
            { title: "allemployer", url: "/employer/allemployer" },
            { title: "viewsingleemployer" }
          ]
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployerRoutingModule {}
