import { Component, OnDestroy } from "@angular/core";
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from "@angular/router";
import { SlimLoadingBarService } from "ng2-slim-loading-bar";
import { Spinkit } from "ng-http-loader";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnDestroy {
  private sub: any;
  public spinkit = Spinkit;
  constructor(
    private slimLoader: SlimLoadingBarService,
    private router: Router,
    private translate: TranslateService
  ) {
    // Listen the navigation events to start or complete the slim bar loading
    translate.setDefaultLang("en");
    translate.use('en');

    translate.get("messageservice.noLocationAvailable").subscribe(
      (res: string) => {
        console.log(res);
        console.log(translate.instant('messageservice.noLocationAvailable'))
      },
      er => {
        console.log(er);
      }
    );
    this.sub = this.router.events.subscribe(
      event => {
        if (event instanceof NavigationStart) {
          this.slimLoader.start();
        } else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel ||
          event instanceof NavigationError
        ) {
          this.onActivate();
          this.slimLoader.complete();
        }
      },
      (error: any) => {
        this.slimLoader.complete();
      }
    );
  }

  ngOnDestroy(): any {
    this.sub.unsubscribe();
  }
  onActivate() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }
}
