import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'gmtpipe'
})
export class GmtpipePipe extends DatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    try {
      const b = value.split(/\D/);

      const today = new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5]));
      return super.transform(today, args);
    } catch (error) {
      return "-";
    }
  }
}
