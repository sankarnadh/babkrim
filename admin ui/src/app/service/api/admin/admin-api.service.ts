
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import {
  Headers,
  Http,
  Request,
  RequestOptions,
  Response,
  XHRBackend
} from "@angular/http";
import { HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
import { GlobalVariableService } from "@service/globalvariable/global-variable.service";
import { variable } from "@angular/compiler/src/output/output_ast";
import { MessageService } from "@service/messages/message.service";
import { FunctionsService } from "@service/function/functions.service";
import { Router } from "@angular/router";
import { GmtpipePipe } from "@service/pipes/gmtpipe.pipe";
@Injectable({
  providedIn: "root"
})
export class AdminApiService {
  constructor(
    private http: HttpClient,
    private variables: GlobalVariableService,
    private message: MessageService,
    private router: Router,
    private method: FunctionsService,
    private gmtpipe:GmtpipePipe
  ) {}
  public apiUrl = this.variables.getApiUrl;
  apiPostRequest(url: String, data: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + this.method.getStorage("admintoken"),
        "content-type": "Application/json",
        locale: this.method.getStorage("language")
      })
    };
    return this.http
      .post(this.apiUrl + url, data, httpOptions)
      .pipe(tap(res => {}));
  }
  apiPutRequest(url: String, data: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + this.method.getStorage("admintoken"),
        "content-type": "Application/json",

        Locale: this.method.getStorage("language")
      })
    };
    return this.http
      .put(this.apiUrl + url, data, httpOptions)
      .pipe(tap(res => {}));
  }
  apiGetRequest(url: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: "bearer " + this.method.getStorage("admintoken"),
        "content-type": "Application/json",
        Locale: this.method.getStorage("language")
      })
    };
    return this.http.get(this.apiUrl + url, httpOptions).pipe(tap(res => {}));
  }
  /***
   * Admin Login Function
   */
  public adminLogin(data: any) {
    this.apiPostRequest("login.json?registerType=admin", data).subscribe(
      res => {
        if (res["status"].responseCode == 20 && res["result"].token) {
          // this.method.showSuccess(res['statusMessage'], this.message.success);
          this.method.setStorage("loggedStatus", true);
          this.method.setStorage("loginType", "admin");
          this.method.setStorage("admintoken", res["result"].token);
          this.method.setStorage("loginDetails", JSON.stringify(res["result"]));
          this.router.navigate(["/dashboard"]);
        } else {
          this.apiError(res);
        }
      },
      er => {
        this.errorHandler(er);
      }
    );
  }

  public processPaymentRequest(data: any) {
    this.apiPostRequest("admin/payment/process.json?registerType=admin&pageNo=" + this.variables.pageNo+'&searchKey='+this.variables.search, data).subscribe(
      res => {
        
        if (res["status"].responseCode == 20) {
          this.method.showSuccess(res['statusMessage'], this.message.success);
          this.getAllPayment();
        } else {
          this.apiError(res);
        }
      },
      er => {
        this.errorHandler(er);
      }
    );
  }

  apiError(res) {
    if (res["status"].responseCode == 30) {
      this.method.showError(this.message.plzLogin, this.message.sessionout);
      this.method.logOutSession();
    } else {
      if (res["status"].responseCode == 40) {
        this.method.showError(res["result"].message, this.message.error);
        this.method.logOutSession();
      } else {
        this.method.showError(res["result"].message, this.message.error);
      }
    }
  }
  errorHandler(er) {
    try {
      if (er.status == 503) {
        this.method.showError(this.message.serverdown, this.message.error);
      } else {
        console.log(er);
        this.method.showError(this.message.networkerror, this.message.error);
      }
      if (er.error)
        if (er.error.statusCode == 403) {
          this.method.logOutSession();
        }
    } catch (error) {
      console.log("errror=>" + error);
    }
  }

  /**
   *
   * @param data
   */
  getContactDetails() {
    this.apiGetRequest("admin/contactUs.json?pageNo="+this.variables.pageNo).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.variables.contactUsList = res["result"].contactDetails?res["result"].contactDetails:[];
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  getContactDetailsAll(){
    this.apiGetRequest("admin/contactUs/all.json?pageNo="+this.variables.pageNo).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.variables.contactUsList = res["result"].contactDetails?res["result"].contactDetails:[];
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  getNewUserFromApi() {
    this.apiGetRequest(
      "admin/newUsers.json?pageNo=" + this.variables.pageNo+'&searchKey='+this.variables.search
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.variables.newUserList = res["result"].users;
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  getAllUserFromApi() {
    this.apiGetRequest(
      "admin/employees.json?pageNo=" + this.variables.pageNo+'&searchKey='+this.variables.search
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.variables.newUserList = res["result"].userDetails;
          this.variables.totalCount = res["result"].totalCount;
          this.method.windowTopScroll();
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }

  /**
   *
   * @param contactUsId
   */
  resolveContactUs(contactUsId, data) {
    this.apiPutRequest(
      "admin/contactUs/" + contactUsId + ".json",
      data
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          window.history.back();
          this.method.showSuccess(res["result"].message, this.message.success);
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  saveContactUs(contactUsId, data) {
    this.apiPutRequest(
      "admin/contactUs/" + contactUsId + "/save.json",
      data
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          window.history.back();
          this.method.showSuccess(res["result"].message, this.message.success);
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  updateUserStatus(UserId, data) {
    this.apiPutRequest("admin/newUsers/" + UserId + ".json", data).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.router.navigate(["/user/newemployee"]);
          this.method.showSuccess(res["result"].message, this.message.success);
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  updateEmployerStatus(UserId, data) {
    this.apiPutRequest("admin/employer/" + UserId + ".json", data).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.router.navigate(["/employer/allemployer"]);
          this.method.showSuccess(res["result"].message, this.message.success);
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  getEmployeeById(UserId) {
    this.apiGetRequest(
      "employee/employeeDetails/" + UserId + ".json"
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          console.log(res["result"]);
          this.variables.tempData = res["result"];
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  getEmployerById(UserId) {
    this.apiGetRequest(
      "employee/employerDetails/" + UserId + ".json"
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          console.log(res["result"]);
          this.variables.tempData = res["result"];
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  getAllEmployer() {
    this.apiGetRequest(
      "admin/employers.json?pageNo=" + this.variables.pageNo+'&searchKey='+this.variables.search
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.variables.userList = res["result"].userDetails;
          this.variables.totalCount = res["result"].totalCount;
          this.method.windowTopScroll();
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }

  getAllPayment() {
    this.apiGetRequest(
      "admin/payment/request.json?pageNo=" + this.variables.pageNo+'&searchKey='+this.variables.search
    ).subscribe(
      res => {
        
        if (res["status"].responseCode == 20) {
          this.variables.paymentList = res["result"].paymentRequestDetails;
          this.variables.totalCount = res["result"].totalCount;
          this.method.windowTopScroll();
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }

  getJobCount(){
    this.apiGetRequest(
      "admin/job/count.json"
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.variables.jobGraph = res["result"].jobDetails;
         this.method.dashboardJob()
         const label=[];
         const createdAt=[]
         this.variables.jobGraph.forEach((element,key) => {
           label.push(element.count)
           createdAt.push(this.gmtpipe.transform(element.createdAt,'dd-MM'))

         });
         console.info(createdAt)
         this.variables.jobbarChart = {
           type: 'Bar',
           data: {
             labels:createdAt ,
             series: [label]
           },
           options: {
             chartPadding: {
               top: 15,
               left: -25
             },
             axisX: {
               showLabel: true,
               showGrid: false
             },
             axisY: {
               showLabel: false,
               showGrid: false
             },
             fullWidth: true
           }
         };
          this.method.windowTopScroll();
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
  getCardCount(){
    this.apiGetRequest(
      "admin/summary/count.json"
    ).subscribe(
      res => {
        if (res["status"].responseCode == 20) {
          this.variables.cardCount = res["result"];
         this.method.dashboardJob()

          this.method.windowTopScroll();
        } else {
          this.apiError(res);
        }
      },
      er => this.errorHandler(er)
    );
  }
}
