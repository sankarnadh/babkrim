import { Injectable } from "@angular/core";

import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { MessageService } from "@service/messages/message.service";
import { GlobalVariableService } from "@service/globalvariable/global-variable.service";
import { DatePipe } from "@angular/common";
import { GmtpipePipe } from "@service/pipes/gmtpipe.pipe";

@Injectable({
  providedIn: "root"
})
export class FunctionsService {
  constructor(
    private datePipe: DatePipe,
    private toastr: ToastrService,
    private message: MessageService,
    private variables: GlobalVariableService,
    private router: Router,
    private gmtpipe : GmtpipePipe
  ) {
    this.setStorage("language", "en");
  }

  showSuccess(message: any, title: any) {
    this.toastr.success(message, title);
  }

  showError(message: any, title: any) {
    this.toastr.error(message, title);
  }

  showWarning(message: any, title: any) {
    this.toastr.warning(message, title);
  }

  showInfo(message: any, title: any) {
    this.toastr.info(message, title);
  }
  setStorage(key: any, value: any) {
    localStorage.setItem(key, value);
  }
  removeStorage(key: any) {
    localStorage.removeItem(key);
  }
  getStorage(key: string) {
    return localStorage.getItem(key);
  }
dashboardJob(){
  this.variables.jobGraph.forEach(element => {
    this.variables.temp.count.push(element.count);
    this.variables.temp.date.push(this.gmtpipe.transform(element.createdAt,'dd-MM'))
  });
}
  logOutSession() {
    const flag = 0;
    /**
     *0->admin
     *1->superadmin
     */
    // if (this.variables.getLoginType == 'superadmin')
    //   flag = 1;
    this.removeStorage("loggedStatus");
    this.removeStorage("loginDetails");
    try {
      this.variables.modalVariable.close();
    } catch (error) {}
    this.router.navigate(["/authentication/login"]);
  }
  get whoseLogin() {
    if (this.variables.getLoginType == "superadmin") {
      return false;
    }
    return true;
  }
  convertDateToGmt(date) {
    try {
      const b = date.split(/\D/);

      const today = new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5]));
      return this.datePipe.transform(today, "dd MMM yyyy hh:mm a");
    } catch (error) {
      console.log(error);
      return "-";
    }
  }
  windowTopScroll() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }
  emptyDataError() {
    this.router.navigate(['/dashboard']);
  }
}
