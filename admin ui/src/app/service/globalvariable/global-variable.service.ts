import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})

export class GlobalVariableService {

  constructor() {

  }
  private loginType: any = 'admin'; // admin / superadmin
  public get getLoginType() {
    const val = localStorage.getItem('loginType');
    if (val) { return val; } else {
      localStorage.setItem('loginType', this.loginType);
      return this.loginType;
    }
  }



  // private url ='http://babkrim.org:8000/api/'; // BABKRIM.com
  private url = 'http://74.208.135.154:8000/api/'; // UPparttime.com
  get getApiUrl() {
    return this.url;
  }
  public contactUsList: any=[];
  public newUserList: any=[];
  public userList: any = [];
  public paymentList: any = [];
  public tempData: any = [];
  public productList: any = [];
  public modalVariable: any;
  public jobDetails: any=[];
  public cardCount:any=[];
  public jobGraph:any=[];
  public search:any='';
  public jobbarChart :any= {
    type: 'Bar',
    data: {
      labels: [],
      series: [[]]
    },
    options: {
      chartPadding: {
        top: 15,
        left: -25
      },
      axisX: {
        showLabel: true,
        showGrid: false
      },
      axisY: {
        showLabel: false,
        showGrid: false
      },
      fullWidth: true
    }
  };
  public temp:any = {
    count:[],
    date:[]
  }
t={
  labels: [  5, 6, 2, 5, 5, 12, 14, 2  ],
  series: [[50, 40, 30, 70, 50, 20, 30]]
}
  tempOrganizationId: any = null;
  tempOrganizationStatus: any;
  public pageNo=1;
  public totalCount=20;
  organizationSearchData = { ///       Organization search data
    'currentPage': 0,
    'name': '',
    'organizationId': '',
    'rowsPerPage': 10,
    'sortBy': '',
    'sortDirection': 'DESC'
  };
  userSearchData = {
    'currentPage': 0,
    'rowsPerPage': 0,
    'sortBy': '',
    'sortDirection': 'DESC',
    'username': ''
  };
  productSearchData = {
    'currentPage': 0,
    'productName': '',
    'rowsPerPage': 10,
    'sortBy': '',
    'sortDirection': 'DESC'
  };
}
