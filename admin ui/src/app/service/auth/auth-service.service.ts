import { Injectable } from "@angular/core";
import { FunctionsService } from "@service/function/functions.service";

@Injectable({
  providedIn: "root"
})
export class AuthServiceService {
  get isLoggedIn() {
    if (this.method.getStorage("loggedStatus")) {
      return this.method.getStorage("loggedStatus");
    }
    return false;
  }
  setLogginStatus(val: boolean) {
    this.method.setStorage("loggedStatus", val);
  }

  constructor(private method: FunctionsService) {}
}
