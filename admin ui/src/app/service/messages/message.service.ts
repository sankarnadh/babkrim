import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
  providedIn: "root"
})
export class MessageService {
  constructor(private translateService: TranslateService) {
    this.translateService.get("messageservice.noLocationAvailable").subscribe(
      (res: string) => {

        this.userNameRequired = this.translateService.instant(
          "messageservice.userNameRequired"
        );
        this.passwordRequired = this.translateService.instant(
          "messageservice.passwordRequired"
        );
        this.success = this.translateService.instant("messageservice.success");
        this.error = this.translateService.instant("messageservice.error");
        this.sessionout = this.translateService.instant(
          "messageservice.sessionout"
        );
        this.plzLogin = this.translateService.instant(
          "messageservice.plzLogin"
        );
        this.networkerror = this.translateService.instant(
          "messageservice.networkerror"
        );
        this.serverdown = this.translateService.instant(
          "messageservice.serverdown"
        );
        this.noLocationAvailable = this.translateService.instant(
          "messageservice.noLocationAvailable"
        );
      },
      er => {
        console.log(er);
      }
    );
  }

  userNameRequired: String;
  passwordRequired: String;
  success: String;
  error: String;
  sessionout: String;
  plzLogin: String;
  networkerror: String;
  serverdown: String;
  noLocationAvailable: String;
}
