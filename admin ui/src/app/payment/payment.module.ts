import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { PaymentRoutingModule } from './payment-routing.module';
import { AllpayementrequestComponent } from "./pages/allpayementrequest/allpayementrequest.component";

@NgModule({
  declarations: [AllpayementrequestComponent],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    SharedModule
  ]
})
export class PaymentModule { }
