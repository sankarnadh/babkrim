import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllpayementrequestComponent } from "./pages/allpayementrequest/allpayementrequest.component";


const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "allPaymentRequest",
        component: AllpayementrequestComponent,
        data: {
          title: "allPaymentRequest",
          urls: [
            { title: "dashboard", url: "/dashboard" },
            { title: "allPaymentRequest" }
          ]
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
