import { Component, OnInit, ViewChild} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { GlobalVariableService } from "@service/globalvariable/global-variable.service";
import { FunctionsService } from "@service/function/functions.service";
import { Router } from "@angular/router";
import { AdminApiService } from "@adminapi/admin-api.service";
import { Setting } from "../allpayementrequestTableSetting";
import { NgbModal, ModalDismissReasons} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-allpayementrequest',
  templateUrl: './allpayementrequest.component.html',
  styleUrls: ['./allpayementrequest.component.css']
})
export class AllpayementrequestComponent implements OnInit {

  @ViewChild("bankDetailsModal") bankDetailsModal: any;
  settings = this.setting.settings; // Smart table settings
  public event: any;
  public page: any;
  modalTitile: string;
  bankDetails: Object;
  processRequest: FormGroup;

  constructor(
    public variable: GlobalVariableService,
    private method: FunctionsService,
    private router: Router,
    public setting: Setting,
    private modalService: NgbModal,
    private api: AdminApiService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.variable.pageNo = 1;
    this.api.getAllPayment();
    this.processRequest = this.formBuilder.group({
      comment : [, Validators.required],
      status : [, Validators.required],
      referenceNumber : [],
      requestId :[]
    });
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.variable.pageNo = 1;
    this.variable.paymentList = [];
    this.variable.search=''
  }
  customEvent(event) {
    if (event.action == "Process") {
      this.modalTitile = 'Process Payment Request';
      const requestDetails =  event.data;
      this.processRequest.patchValue({
        referenceNumber : requestDetails.userIdentifier ,
        requestId :requestDetails.requestId
      })
      this.showModal()
    } else if (event.action  == "Bank Details") {
      this.modalTitile = 'Bank Details';
      console.log(event.data.bankDetails);
      this.bankDetails = event.data.bankDetails;
      this.showModal()
    }
  }

  pageChange() {
    this.variable.pageNo = this.page;
    this.api.getAllPayment();
  }
  search(){
    this.variable.pageNo = 1;
    this.api.getAllPayment();
  }

  closeResul: string;
  showModal() {
  
        this.variable.modalVariable = this.modalService.open(

          this.bankDetailsModal, { size: 'lg' }).result.then((result) => {
            this.processPaymentRequest(result);
          }, (reason) => {
            this.processRequest.reset();
          })
  }

  processPaymentRequest(reason){

    this.api.processPaymentRequest(this.processRequest.value);
    this.processRequest.reset();
  }
}
