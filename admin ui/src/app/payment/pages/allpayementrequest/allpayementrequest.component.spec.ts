import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllpayementrequestComponent } from "./allpayementrequest.component";

describe('AllpayementrequestComponent', () => {
  let component: AllpayementrequestComponent;
  let fixture: ComponentFixture<AllpayementrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllpayementrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllpayementrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
