import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { VerificationRenderComponent } from 'app/user/component/verification-render/verification-render.component';
import { DateRenderComponent } from 'app/shared/page/date-render/date-render.component';
@Injectable({
  providedIn: 'root'
})
export class Setting {
  constructor(private translateService: TranslateService) {
  //  console.info(this.translateService.instant('messageservice.userNameRequired'));

  }
  public settings = {
    columns: {
      fullName: {
        title: this.translateService.instant('fullname'),
        filter: false
      },
      userType: {
        title: this.translateService.instant('usertype'),
        filter: false
      },
      createdTime: {
        title: this.translateService.instant('createdon'),
        type: 'custom',
        renderComponent: DateRenderComponent,
        filter: false
      },
      walletBalance: {
        title: this.translateService.instant('walletbalance'),
        filter: false
      },
      paymentStatus: {
        title: this.translateService.instant('status'),
        filter: false
      }
    },
    mode: 'external',
    actions: {
      custom: [
        {
          name: 'Bank Details',
          // title: '<i class="fas fa-eye" title="View"></i>&nbsp;&nbsp;'
          title: '<button type="button" title="Bank Details">Bank Details</button>&nbsp;&nbsp;'
        },
        {
          name: 'Process',
          // title: '<i class="fas fa-eye" title="View"></i>&nbsp;&nbsp;'
          title: '<i  class="btn btn-info process-button" title="Process">Process</i>'
        }
      ],
      position: 'right',
      add: false,
      edit: false,
      delete: false
    },
    pager: {
      display: true,
      perPage: 20
    }
  };
}
