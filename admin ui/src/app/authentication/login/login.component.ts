import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '@auth/auth-service.service';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from '@service/messages/message.service';
import { AdminApiService } from '@adminapi/admin-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  constructor(
    public auth: AuthServiceService,
    public message: MessageService,
    public router: Router,
    private api: AdminApiService,
    public formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  loginform = true;
  recoverform = false;
  loginForm: FormGroup;
  submitted = false;
  get l() {
    return this.loginForm.controls;
  }
  showRecoverForm() {
    this.loginform = !this.loginform;
    this.recoverform = !this.recoverform;
  }
  login() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    const data = {
      password: this.l.password.value,
      userName: this.l.username.value
    };
    this.api.adminLogin(data);
  }
  ngOnInit() {
    if (this.auth.isLoggedIn)
      this.router.navigate(['/dashboard'])
  }
}
