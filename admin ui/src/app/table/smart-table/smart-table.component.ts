import { Component, OnInit, Output } from '@angular/core';

import { LocalDataSource } from 'ng2-smart-table';
import { Input, EventEmitter } from '@angular/core'
@Component({
  templateUrl: './smart-table.component.html',
  selector: 'app-smart-table'
})
export class SmarttableComponent implements OnInit {
  source: LocalDataSource;
  source2: LocalDataSource;
  @Input() tableData: any;
  @Input() tableSetting: any;
  @Output() customEvent = new EventEmitter();

  constructor() {


  }
  sendData(event: any) {

    try {
      this.customEvent.emit(event)
    
    } catch (error) {
      console.log(error)
    }

  }
  ngOnInit() { }
}
